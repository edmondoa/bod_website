<?php
// set up the error handling, so that PHP errors won't get shown to the user
// this cannot catch parse errors, like an undefined function, including:
// E_ERROR, E_PARSE, E_CORE_ERROR, E_CORE_WARNING, E_COMPILE_ERROR, E_COMPILE_WARNING, and E_STRICT
// But it will catch everything else!
ini_set ('display_errors', 0);
try {
	require_once('office/account/class.account.php');
	require_once('common/class.data_access.php');
	require_once('common/class.web_interface.php');

	session_start();
	
	$_SESSION['data_access'] = new data_access();
	$_SESSION['web_interface'] = new web_interface();
	
	if ($_REQUEST['status_message']) {
		$_SESSION['status']['message'] = $_REQUEST['status_message'];
	}
	if ($_REQUEST['error_message']) {
		$_SESSION['error']['message'] = $_REQUEST['error_message'];
	}

	// if they are attempting to access the affiliate by username
	if (!preg_match("/\./", $_SERVER['REQUEST_URI']) && !$_GET['id'] && !$_GET['ID']) {
		$username = preg_replace("/^\//", "", $_SERVER['REQUEST_URI']);
		$o_account = new account( array('username'=>$username) );
		$_GET['id'] = $o_account->get_accountid();
		$_SERVER['REQUEST_URI'] = '/';
	}
	
	// load the company if necessary
	if (!$_SESSION['o_company'] || $_REQUEST['COMPANY_ID'] || ($_SERVER['COMPANY_ID'] && $_SERVER['COMPANY_ID'] != $_SESSION['o_company']->get_companyid())) {
		require_once('office/account/class.company.php');
		if ($_REQUEST['COMPANY_ID']) {
			$_SESSION['o_company'] = new company( array('companyid'=>$_REQUEST['COMPANY_ID']) );
		}
		else if ($_SERVER['COMPANY_ID']) {
			$_SESSION['o_company'] = new company( array('companyid'=>$_SERVER['COMPANY_ID']) );
		}
		else {
			$_SESSION['o_company'] = new company( array('companyid'=>6) );
		}
	}
	
	// redirect again if incoming ID since that means you just came from an affiliate site or were redirected and you need to set cookie and redirect again
	if (isset($_GET['ID']) || isset($_GET['id'])) {
		$affiliateid = ($_GET['ID']) ? $_GET['ID'] : $_GET['id'];
		$_SESSION['web_interface']->set_cookie( array('cookie_name'=>'AFFILIATEID', 'cookie_array'=>$affiliateid) );
		header('Location: ' . preg_replace("/\?.*/", "", $_SERVER['REQUEST_URI']));
		exit;
	}
	
	// if the $_COOKIE['AFFILIATEID'] cookie is set set the account
	if ($_COOKIE['AFFILIATEID'] && (!isset($_SESSION['affiliate']) || $_SESSION['affiliate']->get_accountid() != $_COOKIE['AFFILIATEID'])) {
		$_SESSION['affiliate'] = new account( array('accountid'=>$_COOKIE['AFFILIATEID']) );
	}
	else if (!$_COOKIE['AFFILIATEID'] && isset($_SESSION['affiliate'])) {
		unset($_SESSION['affiliate']);
	}
	
	if ($_SESSION['o_company']->get_companyid() == 13 && !isset($_SESSION['affiliate'])) {
		//exit;
	}
	
	$script_name = preg_replace("/^\//", "", $_SERVER['SCRIPT_NAME']);
	//if there is nothing after the url then script_name will contain index.html by default so you need to blank it out.
	if (preg_match("/^index\.(html|php)$/", $script_name)) { $script_name = ''; }
	
	// if the script name begins with areas_ind_{area_nameid} or subscribe_{subscribe_state} then rename to {first part}.php and set $_REQUEST[{second part}] = abbreviation
	if (preg_match("/areas_ind_\d+/", $script_name)) {
		$_REQUEST['area_nameid'] = preg_replace("/^.*areas_ind_(\d+).*$/", "$1", $script_name);
		$script_name = preg_replace("/areas_ind_\d+/", "areas_ind", $script_name);
	}
	else if (preg_match("/subscribe_\w\w/", $script_name)) {
		$_REQUEST['subscribe_state'] = preg_replace("/^.*subscribe_(\w\w).*$/", "$1", $script_name);
		$script_name = preg_replace("/subscribe_\w\w/", "subscribe", $script_name);
	}
	else if (preg_match("/areas_ind_trial_\d+/", $script_name)) {
		$_REQUEST['area_nameid'] = preg_replace("/^.*areas_ind_trial_(\d+).*$/", "$1", $script_name);
		$script_name = preg_replace("/areas_ind_trial_\d+/", "areas_ind_trial", $script_name);
	}
	else if (preg_match("/trial_new_\w\w/", $script_name)) {
		$_REQUEST['subscribe_state'] = preg_replace("/^.*trial_new_(\w\w).*$/", "$1", $script_name);
		$script_name = preg_replace("/trial_new_\w\w/", "trial_new", $script_name);
	}
	else if (preg_match("/trial_\w\w/", $script_name)) {
		$_REQUEST['subscribe_state'] = preg_replace("/^.*trial_(\w\w).*$/", "$1", $script_name);
		$script_name = preg_replace("/trial_\w\w/", "trial", $script_name);
	}
	else if (preg_match("/signup_\w\w/", $script_name)) {
		$_REQUEST['subscribe_state'] = preg_replace("/^.*signup_(\w\w).*$/", "$1", $script_name);
		$script_name = preg_replace("/signup_\w\w/", "signup", $script_name);
	}
	else if (preg_match("/auctions\/\d+\.html/", $script_name)) { // this is for pmleadsnow
		$_REQUEST['auctionId'] = preg_replace("/^.*\/(\d+)\.html$/", "$1", $script_name);
		$script_name = 'other/auction.php';
	}
	
	if ($script_name) {
		//if the script name ends with a /, then append index.php
		if (!(preg_match("/\./", $script_name))) {
			$script_name .= '/index.php';
		}
		//else replace .html or .htm, replace with .php
		else if (preg_match("/\.htm[l]$/", $script_name)) {
			$script_name = preg_replace("/\..+$/i", "", $script_name);
			$script_name .= '.php';
		}
		//replace o with other
		if (preg_match("/^o\//", $script_name)) {
			$script_name = preg_replace("/^o\//", "other/", $script_name);
		}
		// if it doesn't have office or other in it by now then put other in it
		if (!preg_match("/^(office|other)\//", $script_name)) {
			$script_name = 'other/' . $script_name;
		}
	}
	else {
		//default to other/index.php
		$script_name = 'other/index.php';
	}

	if ($_SERVER['ENVIRONMENT'] != 'DEV') {
		// if they have ssl in their path then make sure they are on the secure url
		if (preg_match("/\/ssl\//", $script_name) && $_SERVER['SERVER_PORT'] != 443) {
			// redirect to secure location and make sure to pass through ID
			header('Location: https://' . $_SERVER['SECURE_URL'] . $_SERVER['REQUEST_URI'] . '?ID=' . $_COOKIE['AFFILIATEID'] . '&COMPANY_ID=' . $_SESSION['o_company']->get_companyid());
		}
		else if (!preg_match("/\/ssl\//", $script_name) && $_SERVER['SERVER_PORT'] == 443) { // if they are NOT in and ssl dir but they are still on secure then go to non-secure
			// redirect to non-secure location and make sure to pass through ID
			header('Location: http://' . $_SESSION['o_company']->get_company_url() . $_SERVER['REQUEST_URI'] . '?ID=' . $_COOKIE['AFFILIATEID'] . '&COMPANY_ID=' . $_SESSION['o_company']->get_companyid());
		}
	}
	
	if (preg_match("/^office/i", $script_name)) {
		if (isset($_SESSION['office']->account)) {
			if ($_SESSION['office']->account->get_accountid()) {
				include_once($_SESSION['web_interface']->get_server_path($script_name));
			}
			else {
				include_once($_SESSION['web_interface']->get_server_path('other/login.php'));
			}
		}
		else {
			include_once($_SESSION['web_interface']->get_server_path('other/login.php'));
		}
	}
	else if (preg_match("/^other/i", $script_name)) {
		include_once($_SESSION['web_interface']->get_server_path($script_name));
	}
	else {
		include_once($_SESSION['web_interface']->get_server_path('other/index.php'));
	}

	unset($_SESSION['error']);
	unset($_SESSION['status']);
}
catch (Exception $exception) {
	error_log("PHP ERROR: [".$exception->getMessage()."] in line ".$exception->getLine()." of ".$exception->getFile());
	include($_SERVER['DOCUMENT_ROOT'] . '/web/errorpage.php');
	die;
}
?>

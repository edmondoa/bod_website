<?php
#
# @copyright Copyright (c) 2017 Green Seed Technologies, Inc. All rights reserved.
#
# @author David Hoover
# @author David McLaughlin <david@greenseedtech.com>
#

require_once('cardvaultsdk/cardvaultapi.php');
require_once('common/constants.php');

class authorize_net {
	
	// public
	function __construct($args = NULL) {
	}
	
	function one_time_charge($args)
	{
		$result = [];
		if (!empty($args['x_trans_id'])) {
			$result['x_trans_id'] = $args['x_trans_id'];
		}
		try {
			$request = ['gateway' => 'USAePay'];
			if (!empty($args['x_type']) && $args['x_type'] == 'CREDIT') {
				$request['ordertype'] = 'CREDIT';
			}
			foreach (['token' => 'x_token',
				'amount' => 'x_amount',
				'transaction_id' => 'reference_id',
				] as $cardvaultField => $bodField) {
				if (!empty($args[$bodField])) {
					$request[$cardvaultField] = $args[$bodField];
				}
			}
			if (empty($args['customerFacingMessage'])) {
				$customerFacingMessage = "We're sorry."
					.' We are having some temporary delays processing your credit card.'
					.' Please try again later.';
			} else {
				$customerFacingMessage = $args['customerFacingMessage'];
			}
			if (empty($args['customerFacingMessageNumber'])) {
				$customerFacingMessageNumber = BOD_CARDVAULT_CHARGE_FAILURE;
			} else {
				$customerFacingMessageNumber = $args['customerFacingMessageNumber'];
			}
			$cardvaultResult = cardvaultsdk\charge($request,
				$customerFacingMessage, $customerFacingMessageNumber);
			$result['status'] = 1;
			foreach (['message' => 'decline_reason',
				'approval_code' => 'approval_code',
				'transaction_id' => 'reference_id',]
				as $bodField => $cardvaultField) {
				if (!empty($cardvaultResult->$cardvaultField)) {
					$result[$bodField] = $cardvaultResult->$cardvaultField;
				}
			}
			return $result;
		} catch (Exception $exception) {
			$response = json_decode($exception->internal_message['response_body']);
			if (!empty($response->reference_id)) {
				$result['transaction_id'] = $response->reference_id;
			}
			if (!empty($response->decline_reason)) {
				$result['message'] = $response->decline_reason;
			}
			return $result;
		}
	}
}

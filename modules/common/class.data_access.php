<?php
#
# @copyright Copyright (c) 2017 Green Seed Technologies, Inc. All rights reserved.
#
# @author David Hoover
# @author David McLaughlin <david@greenseedtech.com>
#

require_once('common/log.php');
require_once('common/constants.php');

class data_access {
	
	// public
	function __construct($args = NULL) {
		$this->initialize($args);
	}
	
	function initialize($args = NULL) {
		try {
			foreach (['host', 'name', 'username', 'password'] as $field) {
				$lowerField = "db_$field";
				$upperField = strtoupper($lowerField);
				if (empty($args[$lowerField]) && !empty($_SERVER[$upperField])) {
					$args[$lowerField] = $_SERVER[$upperField];
				}
			}
			$this->_db_connect($args);
		}
		catch (Exception $exception) {
			throw $exception;
		}
	}
	
	function db_quote($value = NULL) {
		if ($value) {
			if (get_magic_quotes_gpc()) {
				$value = stripslashes($value);
			}
			if (!is_numeric($value)) {
				$value = mysqli_real_escape_string($this->database_connection, $this->_convertSmartQuotes($value));
			}
		}
		else {
			$value = '';
		}
		return $value;
	}
	
	function query($args = NULL) {
		if (preg_match("/^select/i", $args['query']) || preg_match("/^desc/i", $args['query'])) {
			if (!preg_match("/file_path/", $args['query'])) {
				//error_log($args['query']);
			}
			try {
				$handle = mysqli_query($this->database_connection, $args['query']);
				if (empty($handle)) {
					handleCritical(
						$this->database_connection->error.':'.
						$this->database_connection->errno,
						"We're sorry. We are having some temporary delays.",
						BOD_DB_FAILURE
						);
				}
				$results = array();
				$num_rows = mysqli_num_rows($handle);
				while ($row = mysqli_fetch_assoc($handle)) {
					array_push($results, $row);
				}
				mysqli_free_result($handle);
				return array($results, $num_rows);
			}
			catch (Exception $exception) {
				throw $exception;
			}
		}
		else if (preg_match("/^insert/i", $args['query'])) {
			try {
				//error_log($args['query']);
				mysqli_query($this->database_connection, $args['query']);
				return mysqli_insert_id($this->database_connection);
			}
			catch (Exception $exception) {
				throw $exception;
			}
		}
		else {
			try {
				//error_log($args['query']);
				mysqli_query($this->database_connection, $args['query']);
				return 0;
			}
			catch (my_exception $exception) {
				throw $exception;
			}
		}
	}
		
	
	// get and set

	// private
	private $database_connection;

	function _convertSmartQuotes($string) { 
		$search = array(chr(145), chr(146), chr(147), chr(148), chr(151), "\xe2\x80\x9c", "\xe2\x80\x9d", "\xe2\x80\x98", "\xe2\x80\x99"); 
		$replace = array("'", "'", '"', '"', '-', '"', '"', '\'', '\''); 
		return str_replace($search, $replace, $string); 
	}
	
	private function _db_connect($args = NULL) {
		$this->database_connection = mysqli_connect($args['db_host'], $args['db_username'], $args['db_password']);
		mysqli_set_charset($this->database_connection, 'utf8');
		if (!$this->database_connection) {
			throw new Exception('Could not establish database connection.');
		}
		if (!mysqli_select_db($this->database_connection, $args['db_name'])) {
			throw new Exception('Could not connect to database: ' . $args['db_name'] . ', ' . $args['db_username'] . ', ' . $args['db_password']);
		}
	}
	
}
?>

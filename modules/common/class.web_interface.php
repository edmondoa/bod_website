<?php

class web_interface {

	//public
	function __construct() {
	}
	
	function check_required($args = NULL) {
		unset ($_SESSION['error']);
		
		if ($args['required']) {
			$fields = explode(",", $args['required']);
			foreach ($fields as $field) {
				if (preg_match("/\|/", $field)) {
					$or_fields = explode("|", $field);
					$not_found = 1;
					foreach ($or_fields as $or_field) {
						if (preg_match("/\&/", $or_field)) {
							$or_and_fields = explode("&", $or_field);
							foreach ($or_and_fields as $or_and_field) {
								if (!$_REQUEST[$or_and_field]) {
									$not_found = 1;
									$_SESSION['error']['badfield'][$or_and_field] = true;
								}
								else {
									$not_found = 0;
								}
							}
						}
						else if ($_REQUEST[$or_field]) {
							$not_found = 0;
							break;
						}
					}
					if ($not_found) {
						$_SESSION['error']['badfield'][$or_field] = true;
					}
				}
				else {
					if (!$_REQUEST[$field]) {
						$_SESSION['error']['badfield'][$field] = true;
					}
				}
			}
		}
		if (isset($_SESSION['error']['badfield'])) {
			$_SESSION['error']['message'] = 'One or more required fields is missing. Please correct and try again.';
		}
	}

	function convert_string_for_form($string = NULL) {
		if ($string) {
			$string = preg_replace("/<br\/?>/", "\n", $string);
			$string = $this->_unhtmlentities($string);
		}
		return $string;
	}
	
	function convert_string_for_input($string = NULL) {
		if ($string) {
			$string = $this->convert_string_for_form($string);
			$string = str_replace('"', '&#34;', $string);
		}
		return $string;
	}
	
	function convert_string_for_javascript($string = NULL) {
		if ($string) {
			$string = $this->_unhtmlentities($string);
			$string = addslashes($string);
		}
		return $string;
	}
	
	function convert_string_from_form($string = NULL) {
		if ($string) {
			$string = $this->_microsoft_word_fix($string);
			$string = $this->_htmlentities($string);
			$string = str_replace("&amp;", "&", $string);
			if ($this->check_for_html($string)) {
				$string = preg_replace("/\r/", "", $string);
			}
			else {
				$string = preg_replace("/\r\n/", "<br/>", $string);
			}
		}
		return $string;
	}
	
	function destroySession() {
		$errorLogString = '##### DESTROY SESSION: script=' . $_SERVER['SCRIPT_NAME'] . ', ';
		if (count($_REQUEST)) {
			foreach ($_REQUEST as $key=>$value) {
				$errorLogString .= $key . '=' . $value . ', ';
			}
			$errorLogString = substr($errorLogString, 0, -2);
		}
		$errorLogString .= ' #####';
		//error_log($errorLogString);
		unset($_SESSION['office']);
		header('Location: /');
	}
	
	function getDecrypt($message) {
		$message = base64_decode($message);
		$message = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $_SERVER['MY_ENCRYPTION_KEY'], $message, MCRYPT_MODE_ECB);
		$block = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB);
		$len = strlen($message);
		$pad = ord($message[$len-1]);
		return substr($message, 0, $len-$pad);
	}
	
	function getEncrypt($message) {
		$block = mcrypt_get_block_size('rijndael_128', 'ecb');
		$pad = $block - (strlen($message) % $block);
		$message .= str_repeat(chr($pad), $pad);
		return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $_SERVER['MY_ENCRYPTION_KEY'], $message, MCRYPT_MODE_ECB));
	}

	function getHashString($args = NULL) {
		$hashString = '';
		if ($args['salt'] && $args['string']) {
			$hashString = sha1($args['salt'] . $args['string']);
		}
		return $hashString;
	}
	
	function find_file($args = NULL) {
		$file = preg_replace("/^\//", "", $args['file']);
		$file = preg_replace("/\s/", "", $file);
		if ($args['directory'] && file_exists($_SERVER['DOCUMENT_ROOT'] . '/web/' . $args['directory'] . '/' . $file)) {
			return $_SERVER['DOCUMENT_ROOT'] . '/web/' . $args['directory'] . '/' . $file;
		}
		else if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/web/company_def/' . $file)) {
			return $_SERVER['DOCUMENT_ROOT'] . '/web/company_def/' . $file;
		}
		return '';
	} 
	
	function generateRandomString($stringLength = 8) {
		$characterSeed = str_split('ABCDEFGHIJKLMNOPQRSTUVWXYZ');
		$digitSeed = str_split('0123456789');
		$randomString = '';
		for ($characterPosition = 1; $characterPosition <= $stringLength; $characterPosition++) {
			$rand = rand(0, 25);
			$randomString .= $characterSeed[$rand];
			$rand = rand(0, 9);
			$randomString .= $digitSeed[$rand];
			$characterPosition++;
		}
		$randomString = substr($randomString, 0, $stringLength);
		return $randomString;
	}
	
	function get_cookie($cookie_name = NULL) {
		$cookie_array = array();
		if ($cookie_name) {
			$cookie_string = $_COOKIE[$cookie_name];
			if ($cookie_string) {
				if (preg_match("/\|/", $cookie_string)) {
					$parts = explode("|", $cookie_string);
					foreach ($parts as $part) {
						if ($part) {
							preg_match_all("/^(.+?)=(.*)$/", $part, $array);
							if ($array[1][0] && $array[2][0]) {
								$cookie_array[$array[1][0]] = $array[2][0];
							}
						}
					}
				}
				else if (preg_match("/=/", $cookie_string)) {
					preg_match_all("/^(.+?)=(.*)$/", $cookie_string, $array);
					$cookie_array[$array[1][0]] = $array[2][0];
				}
				else {
					$cookie_array['value'] = $cookie_string;
				}
			}
		}
		return $cookie_array;
	}

	function get_path($file) {
		$file = preg_replace("/^\//", "", $file);
		$file = preg_replace("/\s/", "", $file);
		if ($_SESSION['o_company']->get_directory() && file_exists($_SERVER['DOCUMENT_ROOT'] . '/web/' . $_SESSION['o_company']->get_directory() . '/' . $file)) {
			return '/web/' . $_SESSION['o_company']->get_directory() . '/' . $file;
		}
		else if ($_SESSION['o_company']->get_parent_directory() && file_exists($_SERVER['DOCUMENT_ROOT'] . '/web/' . $_SESSION['o_company']->get_parent_directory() . '/' . $file)) {
			return '/web/' . $_SESSION['o_company']->get_parent_directory() . '/' . $file;
		}
		else if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/web/company_def/' . $file)) {
			return '/web/company_def/' . $file;
		}
		else {
			include_once($_SERVER['DOCUMENT_ROOT'] . '/web/errorpage.php');
		}
	} 

	function get_row_class(&$row_num = 0) {
		if ($row_num) {
			$row_num--;
		}
		else {
			$row_num++;
		}
		return $row_num;
	}
	
	function get_server_path($file) {
		$web_path = $this->get_path($file);
		return $_SERVER['DOCUMENT_ROOT'] . $web_path;
	}

	function missing_required_input($fieldname) {
		if (isset($_SESSION['error']['badfield'][$fieldname])) {
			return 'class="atn"';
		}
		return '';
	}
	
	function missing_required_label($fieldname) {
		if (isset($_SESSION['error']['badfield'][$fieldname])) {
			return 'class="red"';
		}
		return '';
	}
	
	function set_cookie($args = NULL) {
		if ($args['cookie_name']) {
			if (is_array($args['cookie_array'])) {
				foreach ($args['cookie_array'] as $key => $value) {
					$cookie_string .= $key . '=' . $value . '|';
				}
			}
			else {
				$cookie_string = $args['cookie_array'];
			}
			setcookie($args['cookie_name'], $cookie_string, time()+(60*60*24*365), "/");
		}
	}

	public function verifyEmail($email = NULL) {
		if (!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*(\+[a-z0-9-]+)?@[a-z0-9-]+(\.[a-z0-9-]+)*$/i" , $email)) {
			return false;
		}
		return true;
	}
	
	//private
	private function _microsoft_word_fix($string = NULL) {
		$bad = array(chr(133), chr(145), chr(146), chr(147), chr(148), chr(150), chr(151));
		$good = array("...", "'", "'", '&quot;', '&quot;', '&mdash;', '&mdash;');
		return str_replace($bad, $good, $string);
	}
		
	private function _htmlentities($string = NULL) {
		$translation_table = get_html_translation_table(HTML_ENTITIES,ENT_QUOTES);
		$translation_table[chr(34)] = '"';
		$translation_table[chr(38)] = '&';
		$translation_table[chr(39)] = '\'';
		$translation_table[chr(60)] = '<';
		$translation_table[chr(62)] = '>';
		return preg_replace("/&(?![A-Za-z]{0,4}\w{2,3};|#[0-9]{2,3};)/","&amp;" , strtr($string, $translation_table));
	}
		
	private function _unhtmlentities($string = NULL)  {
		$translation_table = get_html_translation_table(HTML_ENTITIES,ENT_QUOTES);
		$translation_table = array_flip($translation_table);
		$string = strtr($string, $translation_table);
		$string = preg_replace('/&#(\d+);/me', "chr('\\1')", $string);
		$string = preg_replace("/&quot;/", "\"", $string);
		return $string;
	}
}
?>

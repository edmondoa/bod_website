<?php
#
# @copyright Copyright (c) 2017 Green Seed Technologies, Inc. All rights reserved.
#
# @author David McLaughlin <david@greenseedtech.com>
#

require_once(__DIR__.'/log.php');

function getEnvironmentOrGlobalVariable($varname)
{
	$value = getenv(strtoupper($varname));
	if ($value !== false) {
		return $value;
	}
	if (file_exists(__DIR__.'/config.php')) {
		require_once(__DIR__.'/config.php');
	}
	if (array_key_exists($varname, $GLOBALS)) {
		return $GLOBALS[$varname];
	}
	return false;
}

function debug()
{
	return getEnvironmentOrGlobalVariable('DEBUG') ? true : false;
}

function verbose()
{
	return getEnvironmentOrGlobalVariable('VERBOSE') ? true : false;
}

function pretend()
{
	return getEnvironmentOrGlobalVariable('PRETEND') ? true : false;
}

function mask($o)
{
	if (is_object($o)) {
		$copy = clone($o);
		foreach (['number', 'UMcard'] as $number) {
			if (!empty($copy->$number) && strlen($copy->$number) > 8) {
				$copy->$number =
					substr($copy->$number, 0, 4)
					.str_repeat('x', strlen($copy->$number)-8)
					.substr($copy->$number, -4, 4)
					;
			}
		}
		foreach (['cardcode', 'UMcvv2'] as $maskItem) {
			if (!empty($copy->$maskItem)) {
				$copy->$maskItem = str_repeat('x', strlen($copy->$maskItem));
			}
		}
		return $copy;
	} elseif (is_array($o)) {
		foreach (['number', 'UMcard'] as $number) {
			if (!empty($o[$number]) && strlen($o[$number]) > 8) {
				$o[$number] =
					substr($o[$number], 0, 4)
					.str_repeat('x', strlen($o[$number])-8)
					.substr($o[$number], -4, 4)
					;
			}
		}
		foreach (['cardcode', 'UMcvv2'] as $maskItem) {
			if (!empty($o[$maskItem])) {
				$o[$maskItem] = str_repeat('x', strlen($o[$maskItem]));
			}
		}
		return $o;
	} else {
		return preg_replace_callback(
			[
			'|(UMcvv2=)([^&]*)()|',
			'|(UMcard=\d{4})(\d*)(\d{4})|',
			'|(<cardnumber>\d{4})(\d*)(\d{4}</cardnumber>)|',
			'|(<cvmvalue>)(.*)(</cvmvalue>)|',
			'|("number":"?\d{4})(\d{3}\d*)(\d{4}"?[^\d])|',
			'|("cardcode":"?)([^",}\]].*)("?[^\d])|',
			'|([a-zA-Z0-9`~!$%^&*()-_+={}[\]]+://[a-zA-Z0-9`~!$%^&*()-_+={}[\]]+:)([a-zA-Z0-9`~!$%^&*()-_+={}[\]]*)(@[a-zA-Z0-9`~!$%^&*()-_+={}[\]]+\.[a-zA-Z0-9`~!$%^&*()-_+={}[\]]+)|',
			"|(CC_NUMBERS.*[`\s]NUMBER[`]?\s*=\s*')([^']*)(')|",
			'|(CC_NUMBERS.*[`\s]NUMBER[`]?\s*=\s*")([^"]*)(")|',
			],
			function ($matches)
			{
				return $matches[1].str_repeat('x', strlen($matches[2])).$matches[3];
			},
			$o);
	}
}

function gs_safe_arr_get($arr, $key)
{
	if (!is_array($arr))
		return '';

	return array_key_exists($key, $arr) ? $arr[$key] : '';
}

function sstack_trace()
{
	$result = '';
	$level = 0;
	foreach (debug_backtrace() as $frame) {
		foreach (['class', 'function', 'file', 'line'] as $field)
			if (empty($frame[$field]))
				$frame[$field] = '<unspecified>';
		$result .= "#$level $frame[class]::$frame[function] $frame[file]:$frame[line]\n";
		++$level;
	}
	return $result;
}

function cli_friendly_header($string)
{
	if (php_sapi_name() === 'cli') {
		print_r($string);
		return true;
	}
	if (func_num_args() == 1)
		return header($string);
	if (func_num_args() == 2)
		return header($string, func_get_arg(1));
	if (func_num_args() == 3)
		return header($string, func_get_arg(1), func_get_arg(2));
}

class ByOwnerDailyException extends \Exception
{
	public $message;
	public $message_number;
	public $status_code;
	public $internal_message;
}

function buildErrorHeader($customerFacingMessage, $httpStatusCode)
{
	cli_friendly_header("HTTP/1.1 $httpStatusCode "
		.str_replace(["\n", "\r"], '',
	print_r($customerFacingMessage, true)), true, $httpStatusCode);
	cli_friendly_header('Content-Type: application/json');
}

function handleCritical($internalMessage,
	$customerFacingMessage, $customerFacingMessageNumber,
	$httpStatusCode=500)
{
	$result = new ByOwnerDailyException();
	$result->message = $customerFacingMessage;
	$result->message_number = $customerFacingMessageNumber;
	$result->status_code = $httpStatusCode;
	if (getEnvironmentOrGlobalVariable('BOD_EXIT_ON_CRITICAL')) {
		logCritical($internalMessage);
		buildErrorHeader($customerFacingMessageNumber, $httpStatusCode);
		print(json_encode($result));
		exit($httpStatusCode);
	}
	$result->internal_message = $internalMessage;
	throw $result;
}

<?php
#
# @copyright Copyright (c) 2017 Green Seed Technologies, Inc. All rights reserved.
#
# @author David McLaughlin <david@greenseedtech.com>
#

global $LOG_LEVEL;
$LOG_LEVEL = LOG_DEBUG;

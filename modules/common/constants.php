<?php
#
# @copyright Copyright (c) 2017 Green Seed Technologies, Inc. All rights reserved.
#
# @author David McLaughlin <david@greenseedtech.com>
#

define('BOD_CARDVAULT_STORE_FAILURE', 40000);
define('BOD_CARDVAULT_DELETE_FAILURE', 40001);
define('BOD_CARDVAULT_CHARGE_FAILURE', 40002);

define('BOD_DB_FAILURE', 50000);

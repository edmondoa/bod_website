<?php
#
# @copyright Copyright (c) 2015--2017 Green Seed Technologies, Inc. All rights reserved.
#
# @author David McLaughlin <david@greenseedtech.com>
#
# a lightweight conviencence wrapper for mysqli
#
# purpose to provide an easy way to query the database for rows or values
# and also do insert/updates.
#

require_once(__DIR__.'/common.php');
require_once(__DIR__.'/log.php');

class db
{
	private $connection = null;

	public function __construct($host, $username, $password,
		$customerFacingMessage, $customerFacingMessageNumber)
	{
		logDebug("db: connect($host, $username, PASSWORD)");
		$this->connection = @new mysqli($host, $username, $password);
		if (empty($this->connection->connect_errno)) {
			return;
		}
		handleCritical($this->connection->connect_error
			.' '.$this->connection->connect_errno,
			$customerFacingMessage, $customerFacingMessageNumber);
	}

	# document says close will return TRUE or FALSE, but may return NULL.
	public function __destruct()
	{
		if ($this->connection instanceof mysqli) {
			if (@$this->connection->close() == false) {
				logWarning(DB_CLOSE_FAILURE);
			}
		}
		$this->connection = null;
	}

	# query is a convenience wrapper for connection->query.
	# Use this method for executing queries where you don't want to get any data.
	public function query($query, $customerFacingMessage, $customerFacingMessageNumber)
	{
		$result = @$this->connection->query($query, MYSQLI_STORE_RESULT);
		if (!empty($this->connection->connect_errno)) {
			handleCritical($this->connection->connect_errno.' '
				.print_r($query, true).': '.$this->connection->connect_error,
				$customerFacingMessage, $customerFacingMessageNumber);
		} elseif (!empty($this->connection->errno)) {
			handleCritical($this->connection->errno.' '
				.print_r($query, true).': '.$this->connection->error,
				$customerFacingMessage, $customerFacingMessageNumber);
		}
		return $result;
	}

	# convenience function to get an array of key value pairs for matching rows of the input query select.
	# getRows either dies or returns an array.
	public function getRows($query, $customerFacingMessage, $customerFacingMessageNumber, $limit=null)
	{
		$result = @$this->connection->query($query);
		if ($result instanceof mysqli_result) {
			$rows = [];
			$count = 0;
			while ((is_null($limit) || $count < $limit) &&
				!is_null($row = $result->fetch_assoc())) {
				# I don't think this condition is possible with fetch_assoc.
				if (count($row) <= 0) {
					handleCritical("Internal error: MySQL returned a row without any values.",
						$customerFacingMessage, $customerFacingMessageNumber);
				}
				$rows[] = $row;
				++$count;
			}
			$result->free();
			return $rows;
		}
		if ($result === true) {
			# Getting here indicates a bug in the code.
			# The caller of getRows should be asking for data,
			# but the type of query passed does not return data.
			handleCritical('Internal error:'
				.' the caller of getRows should'
				.' pass in a MySQL query that returns rows.',
				$customerFacingMessage, $customerFacingMessageNumber);
		}
		# Getting here is likely a bad query.
		if (!empty($this->connection->connect_errno)) {
			handleCritical($this->connection->connect_error
				.' '.$this->connection->connect_errno,
				$customerFacingMessage, $customerFacingMessageNumber);
		}
		handleCritical($this->connection->error.' '.$this->connection->errno,
			$customerFacingMessage, $customerFacingMessageNumber);
	}

	public function getRow($query, $customerFacingMessage,
		$customerFacingMessageNumber)
	{
		$rows = $this->getRows($query, $customerFacingMessage,
			$customerFacingMessageNumber, 1);
		if (count($rows) > 0) {
			return reset($rows);
		}
		handleCritical('getRow failed to match any rows.',
			$customerFacingMessage, $customerFacingMessageNumber, 404);
	}

	public function getValues($query, $customerFacingMessage,
		$customerFacingMessageNumber, $limit=null)
	{
		$rows = $this->getRows($query, $customerFacingMessage,
			$customerFacingMessageNumber, $limit);
		$result = [];
		foreach ($rows as $row) {
			# we don't want to return the COLUMN NAME; just the values.
			$result[] = reset($row);
		}
		return $result;
	}

	public function getValue($query, $customerFacingMessage,
		$customerFacingMessageNumber)
	{
		$rows = $this->getRows($query, $customerFacingMessage,
			$customerFacingMessageNumber, 1);
		if (count($rows) > 0) {
			foreach ($rows as $row) {
				# we don't want the COLUMN NAME.
				return reset($row);
			}
		}
		handleCritical('getRows failed.',
			$customerFacingMessage, $customerFacingMessageNumber, 404);
	}

	# side effect of not quoting now() and curdate() and null
	public function quote($escapestr, $addQuotes=true)
	{
		if (is_array($escapestr)) {
			return array_map(
				function($element)
				{
					return $this->quote($element);
				},
				$escapestr);

		}
		if (is_object($escapestr) || is_array($escapestr) ||
			is_resource($escapestr)) {
			handleCritical("quote expected string, got: ".print_r($escapestr, true));
		}
		if (is_null($escapestr)) {
			return ' null ';
		}
		foreach (['now', 'curdate' ] as $keyword) {
			if (preg_match("/^$keyword\(\s*\)$/i", $escapestr)) {
				return " $escapestr ";
			}
		}
		$result = @$this->connection->escape_string($escapestr);
		if (is_null($result)) {
			handleCritical('escape_string failure: missing valid mysqli connection');
		}
		if ($addQuotes) {
			return "'$result'";
		}
		return $result;
	}

	# convienence function to build an 'in' query portion.
	public function in(array $array, $addQuotes=true)
	{
		return 'in('.implode(',', $this->quote($array, $addQuotes)).')';
	}

	public static function quoteTable($escapestr, $addQuotes=true)
	{
		$result = preg_replace('/`/', '\\`', $escapestr);
		if ($addQuotes)
			return "`$result`";
		return $result;
	}

	public static function quoteDatabase($escapestr, $addQuotes=true)
	{
		return static::quoteTable($escapestr, $addQuotes);
	}

	public function getMatchedRows(
		$customerFacingMessage, $customerFacingMessageNumber)
	{
		if (preg_match('/\d+/', $this->connection->info, $matches)) {
			return $matches[0];
		}
		handleCritical('Cannot find the matched rows in the connection info',
			$customerFacingMessage, $customerFacingMessageNumber);
	}

	public function getAffectedRows()
	{
		return $this->connection->affected_rows;
	}

	public function getInsertId()
	{
		return $this->connection->insert_id;
	}
}

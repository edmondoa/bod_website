<?php
#
# @copyright Copyright (c) 2015--2017 Green Seed Technologies, Inc. All rights reserved.
#
# @author Sasha Pachev <sasha@greenseedtech.com>
# @author David McLaughlin <david@greenseedtech.com>
#

require_once(__DIR__.'/common.php');

if (file_exists(__DIR__.'/config.php')) {
	require_once(__DIR__.'/config.php');
}

# see RFC 5424
if (!defined('LOG_NONE')) {
	define('LOG_NONE', LOG_EMERG-1);
}
if (!defined('LOG_VERBOSE')) {
	define('LOG_VERBOSE', LOG_INFO);
}

global $CARDVAULTSDK_LOG_LEVELS;
$CARDVAULTSDK_LOG_LEVELS = [
	'none' => LOG_NONE,
	'emergency' => LOG_EMERG,
	'alert' => LOG_ALERT,
	'critical' => LOG_CRIT,
	'error' => LOG_ERR,
	'warning' => LOG_WARNING,
	'notice' => LOG_NOTICE,
	'info' => LOG_INFO,
	'verbose' => LOG_INFO, # verbose is a synonym for info
	'debug' => LOG_DEBUG,
	];

global $cardvaultLogLevels;
global $cardvaultcardvaultLogFiles;
$cardvaultLogLevels = [];
$cardvaultLogFiles = [];

if (file_exists(__DIR__.'/config.php')) {
	require_once(__DIR__.'/config.php');
}

# If you want to override config.php on the command line use environment variables:
#
# DEBUG=1 php file.php
# or
# LOG_LEVEL=7 php file.php
# or
# DEBUG=1 LOG=/dev/tty php file.php
#
# Otherwise, pushLogLevel overrides both environment variables and
# config.php's global variables
# and is used to temporarily suspend logging.
#
$logLevel = strtolower(trim(getEnvironmentOrGlobalVariable('LOG_LEVEL')));
if (!is_null($logLevel)) {
	if ($logLevel = (int)$logLevel) {
		pushLogLevel($logLevel);
	} elseif (array_key_exists($logLevel, $CARDVAULTSDK_LOG_LEVELS)) {
		pushLogLevel($CARDVAULTSDK_LOG_LEVELS[$logLevel]);
	}
}
if (debug()) {
	pushLogLevel(LOG_DEBUG);
} elseif (verbose()) {
	pushLogLevel(LOG_INFO);
}

$logFile = getEnvironmentOrGlobalVariable('LOG');
if (!empty($logFile)) {
	pushLogFile($logFile);
}

function exclusiveWrite($fp, $entry)
{
	# If we fail in flock it is better
	# to write something and possibly garble the log
	# than to write nothing at all.
	@flock($fp, LOCK_EX);
	@fwrite($fp, $entry);
	@fflush($fp);
	@flock($fp, LOCK_UN);
}

function getLogLevel()
{
	global $cardvaultLogLevels;
	if (!empty($cardvaultLogLevels)) {
		return end($cardvaultLogLevels);
	}
	# default log level is error or worse.
	return LOG_ERR;
}

function pushLogLevel($logLevel)
{
	global $cardvaultLogLevels;
	if (is_array($cardvaultLogLevels)) {
		$end = end($cardvaultLogLevels);
	}
	$cardvaultLogLevels[] = $logLevel;
	return isset($end) ? ($end === false ? null : $end) : null;
}

function popLogLevel()
{
	global $cardvaultLogLevels;
	return array_pop($cardvaultLogLevels);
}

function getLogFile()
{
	global $cardvaultLogFiles;
	if (!empty($cardvaultLogFiles)) {
		return end($cardvaultLogFiles);
	}
	# the default log file will be syslog.
	return null;
}

function pushLogFile($logFile)
{
	global $cardvaultLogFiles;
	if (is_array($cardvaultLogFiles)) {
		$end = end($cardvaultLogFiles);
	}
	$cardvaultLogFiles[] = $logFile;
	return isset($end) ? ($end === false ? null : $end) : null;
}

function popLogFile()
{
	global $cardvaultLogFiles;
	return array_pop($cardvaultLogFiles);
}

function logEntry($message, $logLevel, $stackTrace=null)
{
	if ($logLevel <= getLogLevel()) {
		if (is_null($stackTrace)) {
			$stackTrace = $logLevel <= LOG_ERR;
		}
		$user = gs_safe_arr_get($_SERVER, 'PHP_AUTH_USER');
		$uri = gs_safe_arr_get($_SERVER, 'REQUEST_URI');
		$entry = array_reduce([$user, $uri, mask(print_r($message, true))],
			function($carry, $item)
			{
				if (empty($item)) {
					return $carry;
				}
				if (empty($carry)) {
					return $item;
				}
				return "$carry $item";
			});
		if (!empty($entry) && substr($entry, -1) != "\n") {
			$entry .= "\n";
		}
		if ($stackTrace) {
			$entry .= sstack_trace();
		}
		$logFile = getLogFile();
		if (!empty($logFile)) {
			if (is_resource($logFile)) {
				exclusiveWrite($logFile, $entry);
			} else {
				if (!($fp = @fopen($logFile, 'a'))) {
					error_log("Failed to open $logFile for append, tried to write: $entry");
					return;
				}
				exclusiveWrite($fp, $entry);
				@fclose($fp);
			}
		} else {
			syslog($logLevel, $entry);
		}
	}
}

function logEmergency($msg, $stackTrace=true)
{
	logEntry($msg, LOG_EMERG, $stackTrace);
}

function logAlert($msg, $stackTrace=true)
{
	logEntry($msg, LOG_ALERT, $stackTrace);
	mail('error@greenseedtech.com', 'logAlert', $msg, 'From: www service <it@greenseedtech.com>');
}

function logCritical($internalMessage, $stackTrace=true)
{
	logEntry($internalMessage, LOG_CRIT, $stackTrace);
}

function logError($msg, $stackTrace=true)
{
	logEntry($msg, LOG_ERR, $stackTrace);
}

function logWarning($msg, $stackTrace=false)
{
	logEntry($msg, LOG_WARNING, $stackTrace);
}

function logNotice($msg, $stackTrace=false)
{
	logEntry($msg, LOG_NOTICE, $stackTrace);
}

function logInfo($msg, $stackTrace=false)
{
	logEntry($msg, LOG_INFO, $stackTrace);
}

function logVerbose($msg, $stackTrace=false)
{
	logEntry($msg, LOG_VERBOSE, $stackTrace);
}

function logDebug($msg, $stackTrace=false)
{
	logEntry($msg, LOG_DEBUG, $stackTrace);
}

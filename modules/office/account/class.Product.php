<?php

class Product {

	// public
	function __construct($args = NULL) {
		if ($args['productId']) {
			$query = "SELECT * FROM Product WHERE productId='" . $_SESSION['data_access']->db_quote($args['productId']) . "'";
		}
		else if ($args['name']) {
			$query = "SELECT * FROM Product WHERE name='" . $_SESSION['data_access']->db_quote($args['name']) . "'";
		}
		if (isset($query)) {
			list($results, $numRows) = $_SESSION['data_access']->query( array('query'=>$query) );
		}
		if ($numRows) {
			$this->initialize($results[0]);
		}
		else {
			unset($args['productId']);
			$this->initialize($args);
		}
	}

	function initialize($args = NULL) {
		if ($args) {
			foreach ($args as $key=>$value) {
				$method_name = 'set' . ucfirst($key);
				if (method_exists($this, $method_name)) {
					$this->{$method_name}($value);
				}
				else if (isset($args[$key])) {
					$this->{$key} = $value;
				}
			}
		}
	}

	function delete() {
		$query = "DELETE FROM Product WHERE productId='" . $_SESSION['data_access']->db_quote($this->getProductId()) . "'";
		$_SESSION['data_access']->query( array('query'=>$query) );
	}

	function save() {
		if ($this->getProductId()) {
			$query = "UPDATE Product SET " .
				"name='" . $_SESSION['data_access']->db_quote($this->getName()) . "', " .
				"title='" . $_SESSION['data_access']->db_quote($this->getTitle()) . "' " .
				"WHERE productId=" . $_SESSION['data_access']->db_quote($this->getProductId())
			;
		}
		else {
			$query = "INSERT INTO Product " .
				"(name,title) VALUES (" .
				"'" . $_SESSION['data_access']->db_quote($this->getName()) . "'," .
				"'" . $_SESSION['data_access']->db_quote($this->getTitle()) . "'" .
				")"
			;
		}
		$this->setProductId($_SESSION['data_access']->query( array('query'=>$query) ));
	}

	// generic get and set
	function getProductId() { return $this->productId; }
	function getName() { return $this->name; }
	function getTitle() { return $this->title; }

	function setProductId($value = NULL) { $this->productId = ($value) ? $value : $this->productId; }

	// private
	private $productId;
	private $name;
	private $title;

}

class ProductUtilities {

	// public
	function __construct($args = NULL) {
		$this->initialize($args);
	}

	function initialize($args = NULL) {
		$this->setOrderBy($args['orderBy']);
	}

	function getList($args = NULL) {
		$args['wherePlus'] = ($args['wherePlus']) ? preg_replace("/ AND /" , " WHERE ", $args['wherePlus']) : '';
		$query = "SELECT * FROM Product " . $args['wherePlus'] . " ORDER BY " . $this->getOrderBy();
		if ($args['limit']) {
			$query .= ' LIMIT ' . $args['limit'];
		}
		list($results, $numRows) = $_SESSION['data_access']->query( array('query'=>$query) );
		$list = array();
		foreach ($results as $row) {
			$Product = new Product();
			$Product->initialize($row);
			$list[] = $Product;
		}
		return $list;
	}

	function getListCompany($args = NULL) {
		$list = array();
		if ($args['companyId']) {
  		$query = "SELECT p.* FROM Product p, CompanyProductEntry cpe WHERE p.productId=cpe.productId AND cpe.companyId=" . $_SESSION['data_access']->db_quote($args['companyId']) . $args['wherePlus'] . " ORDER BY " . $this->getOrderBy();
  		if ($args['limit']) {
  			$query .= ' LIMIT ' . $args['limit'];
  		}
  		list($results, $numRows) = $_SESSION['data_access']->query( array('query'=>$query) );
  		foreach ($results as $row) {
  			$Product = new Product();
  			$Product->initialize($row);
  			$list[] = $Product;
  		}
		}
		return $list;
	}
	
	// generic get and set
	function getOrderBy() { return $this->orderBy; }

	function setOrderBy($value = NULL) { $this->orderBy = ($value) ? $value : 'title'; }

	// private
	private $orderBy;

}

?>

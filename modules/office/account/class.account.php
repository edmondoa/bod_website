<?php
#
# @copyright Copyright (c) 2017 Green Seed Technologies, Inc. All rights reserved.
#
# @author Dave Hoover
# @author David McLaughlin <david@greenseedtech.com>
#

require_once('office/account/class.address.php');
require_once('office/account/class.area_name.php');
require_once('office/account/class.company.php');
require_once('cardvaultsdk/cardvaultapi.php');
require_once('common/log.php');
require_once('common/constants.php');

class account {

	//public
	public $o_business_address;
	public $o_company; 
	public $o_personal_address;
	
	function __construct($args = NULL) {
		if (empty($args['customerFacingMessage'])) {
			$customerFacingMessage = "We're sorry."
				.' We are having some temporary delays creating a customer account.'
				.' Please try again later.';
		} else {
			$customerFacingMessage = $args['customerFacingMessage'];
		}
		if (!empty($args['username']) && !empty($args['password'])) {
			// need to find any accounts that match the username and then see which one matches the password since the password in the db is hashed
			$foundMatch = FALSE;
			$query = "SELECT * FROM account WHERE username='" . $_SESSION['data_access']->db_quote($args['username']) . "'";
			list($results, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
			unset($num_rows);
			if (count($results) > 0) {
				foreach ($results as $row) {
					$hashedPassword = $_SESSION['web_interface']->getHashString( array('salt'=>$row['accountid'], 'string'=>$args['password']) );
					if ($hashedPassword == $row['password']) {
						$foundMatch = TRUE;
						$query = "SELECT * FROM account WHERE accountid='" . $_SESSION['data_access']->db_quote($row['accountid']) . "'";
						break;
					}
				}
			}
			if (!$foundMatch) {
				unset($query);
			}
		}
		else if (!empty($args['main_email_address']) && !empty($args['password'])) {
			// need to find any accounts that match the username and then see which one matches the password since the password in the db is hashed
			$foundMatch = FALSE;
			$query = "SELECT * FROM account WHERE (main_email_address='" . $_SESSION['data_access']->db_quote($args['main_email_address']) . "' OR username='" . $_SESSION['data_access']->db_quote($args['main_email_address']) . "')";
			list($results, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
			unset($num_rows);
			if (count($results) > 0) {
				foreach ($results as $row) {
					$hashedPassword = $_SESSION['web_interface']->getHashString( array('salt'=>$row['accountid'], 'string'=>$args['password']) );
					if ($hashedPassword == $row['password']) {
						$foundMatch = TRUE;
						$query = "SELECT * FROM account WHERE accountid='" . $_SESSION['data_access']->db_quote($row['accountid']) . "'";
						break;
					}
				}
			}
			if (!$foundMatch) {
				unset($query);
			}
		}
		else if (!empty($args['username'])) {
			$query = "SELECT * FROM account WHERE username='" . $_SESSION['data_access']->db_quote($args['username']) . "'";
		}
		else if (!empty($args['main_email_address'])) {
			$query = "SELECT * FROM account WHERE main_email_address='" . $_SESSION['data_access']->db_quote($args['main_email_address']) . "'";
		}
		else if (!empty($args['accountid'])) {
			$query = "SELECT * FROM account WHERE accountid='" . $_SESSION['data_access']->db_quote($args['accountid']) . "'";
		}
		if (isset($query)) {
			try {
				list($results, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
			}
			catch (Exception $exception) {
				throw $exception;
			}
		}
		if (!empty($num_rows)) {
			$this->initialize($results[0], $customerFacingMessage);
		}
		else {
			$this->initialize($args, $customerFacingMessage);
		}
	}
	
	public function initialize($args, $customerFacingMessage=null)
	{
		if (empty($args['customerFacingMessage'])) {
			$customerFacingMessage = "We're sorry."
				.' We are having some temporary delays pulling up a customer account.'
				.' Please try again later.';
		}
		if (!empty($args)) {
			foreach ($args as $key=>$value) {
				$method_name = 'set_' . $key;
				if (method_exists($this, $method_name)) {
					$this->{$method_name}($value);
				}
				else if (isset($args[$key])) {
					$this->{$key} = $value;
				}
			}
		}
		$this->set_area_names();
		$this->setProducts();
		$this->set_robot_names();
		try {
			$this->o_business_address = new address( array('addressid'=>$this->get_business_addressid(), 'type'=>'business') );
			$this->o_business_address->initialize($args);
			
			$this->o_company = new company( array('companyid'=>$this->get_companyid()) );
			
			$this->o_personal_address = new address( array('addressid'=>$this->get_personal_addressid(), 'type'=>'personal') );
			$this->o_personal_address->initialize($args);
			$this->ensureTokenized($customerFacingMessage);
		}
		catch (Exception $exception) {
			throw $exception;
		}
	}
	
	public function activate_account($args=null)
	{
		if (empty($args['customerFacingMessage'])) {
			$customerFacingMessage = "We're sorry."
				.' We are having some temporary delays activating your account.'
				.' Please try again later.';
		} else {
			$customerFacingMessage = $args['customerFacingMessage'];
		}
		if (empty($args['customerFacingMessageNumber'])) {
			$customerFacingMessageNumber = BOD_CARDVAULT_CHARGE_FAILURE;
		} else {
			$customerFacingMessageNumber = $args['customerFacingMessageNumber'];;
		}
		if (preg_match("/dahoover/", $this->get_username())) {
			$r_one_time_charge['status'] = 1;
		}
		else {
			require_once('common/class.authorize_net.php');
			$o_authorize_net = new authorize_net();
			$r_one_time_charge = $o_authorize_net->one_time_charge(
				array(
					'x_type'=>'AUTH_CAPTURE',
					'x_cust_id'=>$this->get_customer_number(),
					'x_token'=>$this->getToken(),
					'x_exp_date'=>$this->get_card_exp_date(),
					'x_description'=>'Reactivating Account',
					'x_amount'=>$this->get_rate(),
					'x_first_name'=>$this->get_first_name(),
					'x_last_name'=>$this->get_last_name(),
					'x_address'=>$this->o_business_address->get_line1() . ' ' . $this->o_business_address->get_line2(),
					'x_city'=>$this->o_business_address->get_city(),
					'x_state'=>$this->o_business_address->get_state(),
					'x_zip'=>$this->o_business_address->get_postal_code(),
					'customerFacingMessage' => $customerFacingMessage,
					'customerFacingMessageNumber' => $customerFacingMessageNumber,
				));
		}
		if ($r_one_time_charge['status'] != 1) {
			throw new Exception('There was an error processing the transaction:<br/>' . $r_one_time_charge['message']);
		}
		require_once('office/account/class.billing.php');
		$o_billing = new billing(
			array(
				'accountid'=>$this->get_accountid(),
				'amount'=>$this->get_rate(),
				'type'=>'AUTH_CAPTURE',
				'approval_code'=>$r_one_time_charge['approval_code'],
				'transaction_id'=>$r_one_time_charge['transaction_id'],
				'notes'=>'Reactivating Account'
			)
		);
		$o_billing->save();
		// calculate next bill date
		$month = date('n');
		$day = date('j');
		$year = date('Y');
		if ($day > 28) {
			$day = 1;
			$month++;
		}
		if ($month > 12) {
			$month = 1;
			$year++;
		}
		if ($this->get_subscription_type() == 'Annual') {
			$next_bill_date = date('Y-m-d', strtotime("+1 YEAR", strtotime($year . '-' . $month . '-' . $day)));
		}
		else if ($this->get_subscription_type() == 'Monthly') {
			$next_bill_date = date('Y-m-d', strtotime("+1 MONTH", strtotime($year . '-' . $month . '-' . $day)));
		}
		$this->set_notes($this->get_notes() . "\n" . date('Y-m-d') . ": Account Activated: Card Charged for $" . $this->get_rate() . " and Changed Status from " . strtoupper($this->get_billing_status()) . " to " . strtoupper('active') . " and set End Date to 2021-12-31 and set Next Billing Date to " . $next_bill_date . "\n");
		$this->set_billing_status('active');
		$this->set_end_date('2027-12-31');
		$this->set_next_bill_date($next_bill_date);
		$this->save();
	}
	
	function cancel_by_account() {
		$new_end_date = date('Y-m-d', strtotime("-1 Day", strtotime($this->get_next_bill_date())));
		// put in a note
		$this->set_notes($this->get_notes() . "\n" . date('Y-m-d') . ": Request to Cancel: Changed Status from " . strtoupper($this->get_billing_status()) . " to " . strtoupper('queued') . " and set End Date to " . $new_end_date . "\n");
		$this->set_billing_status('queued');
		$this->set_end_date($new_end_date);
		$this->save();
		
		require_once('office/account/class.letter.php');
		if ($this->get_companyid() == 13) { // PM Leads
			$o_letter = new letter( array('title'=>'201b Customer Cancellation Email FRBO') );
		}
		else {
			$o_letter = new letter( array('title'=>'201a Customer Cancellation Email FSBO') );
		}
		$email_segments = $o_letter->merge_segments( array('o_account'=>$this) );
		$headers = '';
		if ($email_segments['cc_list']) {
			$headers .= "Cc: " . $email_segments['cc_list'] . "\r\n";
		}
		if ($email_segments['bcc_list']) {
			$headers .= "Bcc: " . $email_segments['bcc_list'] . "\r\n";
		}
		$headers .= "From: " . $email_segments['from_email'];
		mail($this->get_main_email_address(), $email_segments['subject'], $email_segments['body'], $headers);
	}
	
	function continue_subscription() {
		$this->set_notes($this->get_notes() . "\n" . date('Y-m-d') . ": Cancelled Cancellation: Changed Status from " . strtoupper($this->get_billing_status()) . " to " . strtoupper('active') . " and set End Date to 2021-12-31\n");
		$this->set_billing_status('active');
		$this->set_end_date('2021-12-31');
		$this->save();
	}
	
	function create() {
		if (empty($customerFacingMessage)) {
			$customerFacingMessage = "We're sorry."
				.' We are having some temporary delays creating the customer account.'
				.' Please try again later.';
		}
		if (!$this->get_created()) {
			$this->set_created(date('Y-m-d'));
		}
		if (!$this->get_billing_status()) {
			$this->set_billing_status('active');
		}
		$this->ensureTokenized($customerFacingMessage);
		$query = "INSERT INTO account " . 
			"(accountid, affiliateid, account_type, username, first_name, last_name, business_name, main_email_address, billing_email_address, alert_email_address, email_attachment, personal_phone, business_phone, fax_phone, cell_phone, companyid, personal_addressid, business_addressid, pass_question, pass_answer, created, billing_status, billing_day, billing_type, billing_type_other, card_name, card_type, card_number, card_exp_date, start_date, end_date, customer_number, customer_type, customer_type_other, invoice_number, authorize_net_number, subscription_type, subscription_type_other, license_redistribute, license_how_many, rate, monthly_average, referral_source, referral_source_other, url, notes, prospecting_notes, trial_to_customer, cancel_reason) " . 
			"VALUES (" . 
			"'', " . 
			"'" . $_SESSION['data_access']->db_quote($this->get_affiliateid()) . "', " . 
			"'" . $_SESSION['data_access']->db_quote($this->get_account_type()) . "', " . 
			"'" . $_SESSION['data_access']->db_quote($this->get_username()) . "', " . 
			"'" . $_SESSION['data_access']->db_quote($this->get_first_name()) . "', " . 
			"'" . $_SESSION['data_access']->db_quote($this->get_last_name()) . "', " . 
			"'" . $_SESSION['data_access']->db_quote($this->get_business_name()) . "', " . 
			"'" . $_SESSION['data_access']->db_quote($this->get_main_email_address()) . "', " . 
			"'" . $_SESSION['data_access']->db_quote($this->get_billing_email_address()) . "', " . 
			"'" . $_SESSION['data_access']->db_quote($this->get_alert_email_address()) . "', " . 
			"'" . $_SESSION['data_access']->db_quote($this->get_email_attachment()) . "', " . 
			"'" . $_SESSION['data_access']->db_quote($this->get_personal_phone()) . "', " . 
			"'" . $_SESSION['data_access']->db_quote($this->get_business_phone()) . "', " . 
			"'" . $_SESSION['data_access']->db_quote($this->get_fax_phone()) . "', " . 
			"'" . $_SESSION['data_access']->db_quote($this->get_cell_phone()) . "', " .
			"'" . $_SESSION['data_access']->db_quote($this->get_companyid()) . "', " . 
			"'" . $_SESSION['data_access']->db_quote($this->get_personal_addressid()) . "', " . 
			"'" . $_SESSION['data_access']->db_quote($this->get_business_addressid()) . "', " . 
			"'" . $_SESSION['data_access']->db_quote($this->get_pass_question()) . "', " . 
			"'" . $_SESSION['data_access']->db_quote($this->get_pass_answer()) . "', " . 
			"NOW(), " . 
			"'" . $_SESSION['data_access']->db_quote($this->get_billing_status()) . "', " . 
			"'" . $_SESSION['data_access']->db_quote($this->get_billing_day()) . "', " . 
			"'" . $_SESSION['data_access']->db_quote($this->get_billing_type()) . "', " .
			"'" . $_SESSION['data_access']->db_quote($this->get_billing_type_other()) . "', " .  
			"'" . $_SESSION['data_access']->db_quote($this->get_card_name()) . "', " . 
			"'" . $_SESSION['data_access']->db_quote($this->get_card_type()) . "', " . 
			"'" . $_SESSION['data_access']->db_quote($this->get_card_number()) . "', " . 
			"'" . $_SESSION['data_access']->db_quote($this->get_card_exp_date()) . "', " . 
			"'" . $_SESSION['data_access']->db_quote($this->get_start_date()) . "', " . 
			"'" . $_SESSION['data_access']->db_quote($this->get_end_date()) . "', " . 
			"'" . $_SESSION['data_access']->db_quote($this->get_customer_number()) . "', " .
			"'" . $_SESSION['data_access']->db_quote($this->get_customer_type()) . "', " .
			"'" . $_SESSION['data_access']->db_quote($this->get_customer_type_other()) . "', " . 
			"'" . $_SESSION['data_access']->db_quote($this->get_invoice_number()) . "', " . 
			"'" . $_SESSION['data_access']->db_quote($this->get_authorize_net_number()) . "', " . 
			"'" . $_SESSION['data_access']->db_quote($this->get_subscription_type()) . "', " .
			"'" . $_SESSION['data_access']->db_quote($this->get_subscription_type_other()) . "', " .
			"'" . $_SESSION['data_access']->db_quote($this->get_license_redistribute()) . "', " .
			"'" . $_SESSION['data_access']->db_quote($this->get_license_how_many()) . "', " .
			"'" . $_SESSION['data_access']->db_quote($this->get_rate()) . "', " . 
			"'" . $_SESSION['data_access']->db_quote($this->get_monthly_average()) . "', " .
			"'" . $_SESSION['data_access']->db_quote($this->get_referral_source()) . "', " .
			"'" . $_SESSION['data_access']->db_quote($this->get_referral_source_other()) . "', " .
			"'" . $_SESSION['data_access']->db_quote($this->get_url()) . "', " . 
			"'" . $_SESSION['data_access']->db_quote($this->get_notes()) . "', " .
			"'" . $_SESSION['data_access']->db_quote($this->get_prospecting_notes()) . "', " .
			"'" . $_SESSION['data_access']->db_quote($this->get_trial_to_customer()) . "', " . 
			"'" . $_SESSION['data_access']->db_quote($this->get_cancel_reason()) . "'" .
			")";
		try {
			$this->set_accountid($_SESSION['data_access']->query( array('query'=>$query) ));
		}
		catch (Exception $exception) {
			throw $exception;
		}
	}
	
	public function delete($customerFacingMessage='')
	{
		if (empty($customerFacingMessage)) {
			$customerFacingMessage = "We're sorry."
				.' We are having some temporary delays removing your customer account.'
				.' Please try again later.';
		}
		$accountid_tables = array('account','area_name_entry','billing');
		foreach ($accountid_tables as $table_name) {
			$query = "DELETE FROM " . $table_name . " WHERE accountid='" . $_SESSION['data_access']->db_quote($this->get_accountid()) . "'";
			try {
				$_SESSION['data_access']->query( array('query'=>$query) );
			}
			catch (Exception $exception) {
				throw $exception;
			}
		}
		// delete account addresses
		$this->o_business_address->delete();
		$this->o_personal_address->delete();
		$token = $this->getToken();
		if (!empty($token)) {
			cardvaultsdk\delete(['token' => $token], $customerFacingMessage,
				BOD_CARDVAULT_DELETE_FAILURE);
		}
	}
	
	function get_array_listing_fields() {
		$query = "SELECT listing_fields FROM account_listing_fields WHERE accountid=" . $_SESSION['data_access']->db_quote($this->get_accountid());
		list($results, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
		$listing_fields = explode(',', $results[0]['listing_fields']);
		$temp_array = array();
		foreach ($listing_fields as $listing_field) {
			if ($listing_field) {
				$temp_array[$listing_field] = TRUE;
			}
		}
		ksort($temp_array);
		return $temp_array;
	}
	
	function get_array_property_type_fields() {
		$query = "SELECT property_type_fields FROM account_property_type_fields WHERE accountid=" . $_SESSION['data_access']->db_quote($this->get_accountid());
		list($results, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
		$property_type_fields = explode(',', $results[0]['property_type_fields']);
		$temp_array = array();
		foreach ($property_type_fields as $property_type_field) {
			if ($property_type_field) {
				$temp_array[$property_type_field] = TRUE;
			}
		}
		ksort($temp_array);
		return $temp_array;
	}
	
	function get_hash_search_fields() {
		$query = "SELECT search_fields FROM account_search_fields WHERE accountid=" . $_SESSION['data_access']->db_quote($this->get_accountid());
		list($results, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
		$search_fields = explode(',', $results[0]['search_fields']);
		$temp_array = array();
		foreach ($search_fields as $search_field) {
			$keyValue = explode("=", $search_field);
			$key = $keyValue[0];
			$value = $keyValue[1];
			if ($key && $value) {
				$temp_array[$key] = $value;
			}
		}
		return $temp_array;
	}
	
	function get_server_path($file) {
		$file = preg_replace("/^\//", "", $file);
		$file = preg_replace("/\s/", "", $file);
		if ($this->o_company->get_directory() && file_exists($_SERVER['DOCUMENT_ROOT'] . '/web/' . $this->o_company->get_directory() . '/' . $file)) {
			return $_SERVER['DOCUMENT_ROOT'] . '/web/' . $this->o_company->get_directory() . '/' . $file;
		}
		else if ($this->o_company->get_parent_directory() && file_exists($_SERVER['DOCUMENT_ROOT'] . '/web/' . $this->o_company->get_parent_directory() . '/' . $file)) {
			return $_SERVER['DOCUMENT_ROOT'] . '/web/' . $this->o_company->get_parent_directory() . '/' . $file;
		}
		else if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/web/company_def/' . $file)) {
			return $_SERVER['DOCUMENT_ROOT'] . '/web/company_def/' . $file;
		}
		else {
			include_once($_SERVER['DOCUMENT_ROOT'] . '/web/errorpage.php');
		}
	} 
	
	function get_table_fields() {
		$query = "desc account";
		list($results, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
		$temp_array = array();
		foreach ($results as $result) {
			$temp_array[] = $result['Field'];
		}
		sort($temp_array);
		return $temp_array;
	}
	
	function invalid_card_exp_date($card_exp_month = NULL, $card_exp_year = NULL) {
		if ($card_exp_year > date('Y')) {
			return FALSE;
		}
		if ($card_exp_month >= date('m')) {
			return FALSE;
		}
		return TRUE;
	}
	
	function invalid_card_number($args = NULL) {
		if ($args['card_type'] != 'AMEX' && strlen($args['card_number']) != 16) {
			return TRUE;
		}
		if ($args['card_type'] == 'AMEX' && strlen($args['card_number']) != 15) {
			return TRUE;
		}
		if ($args['card_type'] == 'AMEX' && !preg_match("/^3/", $args['card_number'])) {
			return TRUE;
		}
		if ($args['card_type'] == 'VISA' && !preg_match("/^4/", $args['card_number'])) {
			return TRUE;
		}
		if ($args['card_type'] == 'MC' && !preg_match("/^5/", $args['card_number'])) {
			return TRUE;
		}
		if ($args['card_type'] == 'DISC' && !preg_match("/^6/", $args['card_number'])) {
			return TRUE;
		}
		return FALSE;
	}
	
	function invalid_email($email) {
		$array = $_SESSION['web_interface']->check_emails( array(emails=>$email) );
		if ($array['invalid_emails']) {
			return TRUE;
		}
		return FALSE;
	}
	
	function invalid_username($username = NULL) {
		if (preg_match("/[^\w-\.@\+]/", $username) || strlen($username) < 2) {
			return TRUE;
		}
		return FALSE;
	}
	
	function is_admin() {
		if (preg_match("/admin/i", $this->get_account_type())) {
			return TRUE;
		}
		return FALSE;
	}
	
	function is_billing() {
		if (preg_match("/billing/i", $this->get_account_type())) {
			return TRUE;
		}
		return FALSE;
	}
	
	function is_developer() {
		if (preg_match("/developer/i", $this->get_account_type())) {
			return TRUE;
		}
		return FALSE;
	}
	
	function is_customer() {
		if (preg_match("/customer/i", $this->get_account_type())) {
			return TRUE;
		}
		return FALSE;
	}
	
	function is_main_email_address_taken($main_email_address = NULL) {
		$query = "SELECT accountid FROM account WHERE main_email_address='" . $_SESSION['data_access']->db_quote($main_email_address) . "'";
		try {
			list($results, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
			if (($num_rows && !$this->get_accountid()) || ($num_rows && $this->get_accountid() && $results[0]['accountid'] != $this->get_accountid())) {
				return 1;
			}
			else {
				return 0;
			}
		}
		catch (Exception $exception) {
			throw $exception;
		}
	}
	
	function isOwner() {
		if (isset($_SESSION['office']) && isset($_SESSION['office']->account)) {
			// if logged in person is admin they can do whatever they want
			if ($_SESSION['office']->account->get_account_type() == 'ADMIN') {
				return TRUE;
			}
			else {
				if ($this->get_accountid()) {
					// if the person is modifying their own information
					if ($this->get_accountid() == $_SESSION['office']->account->get_accountid()) {
						return TRUE;
					}
					// if it is a private label modifying someone else's info
					else if ($_SESSION['office']->account->get_account_type() == 'PRIVATE_LABEL' && $this->get_companyid() == $_SESSION['office']->account->get_companyid()) {
						return TRUE;
					}
				}
				// it is a blank account so return true
				else {
					return TRUE;
				}
			}
		}
		return FALSE;
	}
			
	function is_private() {
		if (preg_match("/private/i", $this->get_account_type())) {
			return TRUE;
		}
		return FALSE;
	}
	
	function is_support() {
		if (preg_match("/support/i", $this->get_account_type())) {
			return TRUE;
		}
		return FALSE;
	}

	function is_upload() {
		if (preg_match("/upload/i", $this->get_account_type())) {
			return TRUE;
		}
		return FALSE;
	}
	
	function is_username_taken($username = NULL) {
		$query = "SELECT accountid FROM account WHERE username='" . $_SESSION['data_access']->db_quote($username) . "'";
		try {
			list($results, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
			if ($num_rows && $results[0]['accountid'] != $this->get_accountid()) {
				return 1;
			}
			else {
				return 0;
			}
		}
		catch (Exception $exception) {
			throw $exception;
		}
	}
	
	function merge_info($args = NULL) {
		if ($args['string']) {
			$query = "SELECT *, DATE_FORMAT(last_login, '%M %e, %Y') AS last_login, DATE_FORMAT(created, '%M %e, %Y') AS created, DATE_FORMAT(billing_status_date, '%M %e, %Y') AS billing_status_date, DATE_FORMAT(next_bill_date, '%M %e, %Y') AS next_bill_date, DATE_FORMAT(start_date, '%M %e, %Y') AS start_date, DATE_FORMAT(end_date, '%M %e, %Y') AS end_date FROM account WHERE accountid=" . $_SESSION['data_access']->db_quote($this->get_accountid());
			list($results, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
			foreach ($results[0] as $key=>$value) {
				$args['string'] = str_ireplace("{" . $args['key_modifier'] . $key . "}", $value, $args['string']);
			}
		}
		return $args['string'];
	}
	
	public function reprocess_credit_card($args)
	{
		if (empty($args['customerFacingMessage'])) {
			$customerFacingMessage = "We're sorry."
				.' We are having some temporary delays reactivating your account.'
				.' Please try again later.';
		} else {
			$customerFacingMessage = $args['customerFacingMessage'];
		}
		if (empty($args['customerFacingMessageNumber'])) {
			$customerFacingMessageNumber = BOD_CARDVAULT_CHARGE_FAILURE;
		} else {
			$customerFacingMessageNumber = $args['customerFacingMessageNumber'];
		}
		if (preg_match("/dahoover/", $this->get_username())) {
			$r_one_time_charge['status'] = 1;
		}
		else {
			require_once('common/class.authorize_net.php');
			$o_authorize_net = new authorize_net();
			$r_one_time_charge = $o_authorize_net->one_time_charge(
				array(
					'x_type'=>'AUTH_CAPTURE',
					'x_cust_id'=>$this->get_customer_number(),
					'x_token'=>$this->getToken(),
					'x_exp_date'=>$this->get_card_exp_date(),
					'x_description'=>'Re-processing Credit Card',
					'x_amount'=>$this->get_rate(),
					'x_first_name'=>$this->get_first_name(),
					'x_last_name'=>$this->get_last_name(),
					'x_address'=>$this->o_business_address->get_line1() . ' ' . $this->o_business_address->get_line2(),
					'x_city'=>$this->o_business_address->get_city(),
					'x_state'=>$this->o_business_address->get_state(),
					'x_zip'=>$this->o_business_address->get_postal_code(),
					'customerFacingMessage' => $customerFacingMessage,
					'customerFacingMessageNumber' => $customerFacingMessageNumber,
				));
		}
		if ($r_one_time_charge['status'] != 1) {
			throw new Exception('There was an error processing the transaction:<br/>' . $r_one_time_charge['message']);
		}
		require_once('office/account/class.billing.php');
		$o_billing = new billing(
			array(
				'accountid'=>$this->get_accountid(),
				'amount'=>$this->get_rate(),
				'type'=>'AUTH_CAPTURE',
				'approval_code'=>$r_one_time_charge['approval_code'],
				'transaction_id'=>$r_one_time_charge['transaction_id'],
				'notes'=>'Re-processing Credit Card'
			)
		);
		$o_billing->save();
		if ($this->get_subscription_type() == 'Annual') {
			$next_bill_date = date('Y-m-d', strtotime("+1 YEAR", strtotime($this->get_next_bill_date())));
		}
		else if ($this->get_subscription_type() == 'Monthly') {
			$next_bill_date = date('Y-m-d', strtotime("+1 MONTH", strtotime($this->get_next_bill_date())));
		}
		$this->set_notes($this->get_notes() . "\n" . date('Y-m-d') . ": Re-processing Credit Card: Card Charged for $" . $this->get_rate() . " and Next Billing Date set to " . $next_bill_date . "\n");
		$this->set_next_bill_date($next_bill_date);
		$this->save();
	}
	
	function save($args = NULL) {
		if (empty($customerFacingMessage)) {
			$customerFacingMessage = "We're sorry."
				.' We are having some temporary delays saving the customer account.'
				.' Please try again later.';
		}
		// if they have a logo save it
		if (!empty($_FILES['new_logo']['name'])) {
			if ($this->get_logo_path() && file_exists($_SERVER['DOCUMENT_ROOT'] . $this->get_logo_path())) {
				unlink($_SERVER['DOCUMENT_ROOT'] . $this->get_logo_path());
			}
			move_uploaded_file($_FILES['new_logo']['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . '/web/files/' . $this->get_accountid() . '_logo.jpg');
			$this->set_logo_path('/web/files/' . $this->get_accountid() . '_logo.jpg');
		}

		$this->o_business_address->save();
		$this->set_business_addressid($this->o_business_address->get_addressid());
		$this->o_personal_address->save();
		$this->set_personal_addressid($this->o_personal_address->get_addressid());
		$query = "UPDATE account SET " . 
			"affiliateid='" . $_SESSION['data_access']->db_quote($this->get_affiliateid()) . "', " . 
			"account_type='" . $_SESSION['data_access']->db_quote($this->get_account_type()) . "', " .  
			"username='" . $_SESSION['data_access']->db_quote($this->get_username()) . "', " . 
			"password='" . $_SESSION['data_access']->db_quote($this->get_password()) . "', " .
			"first_name='" . $_SESSION['data_access']->db_quote($this->get_first_name()) . "', " .
			"last_name='" . $_SESSION['data_access']->db_quote($this->get_last_name()) . "', " .
			"business_name='" . $_SESSION['data_access']->db_quote($this->get_business_name()) . "', " .
			"main_email_address='" . $_SESSION['data_access']->db_quote($this->get_main_email_address()) . "', " .
			"billing_email_address='" . $_SESSION['data_access']->db_quote($this->get_billing_email_address()) . "', " .
			"alert_email_address='" . $_SESSION['data_access']->db_quote($this->get_alert_email_address()) . "', " . 
			"email_attachment='" . $_SESSION['data_access']->db_quote($this->get_email_attachment()) . "', " . 
			"personal_phone='" . $_SESSION['data_access']->db_quote($this->get_personal_phone()) . "', " .
			"business_phone='" . $_SESSION['data_access']->db_quote($this->get_business_phone()) . "', " .
			"fax_phone='" . $_SESSION['data_access']->db_quote($this->get_fax_phone()) . "', " .
			"cell_phone='" . $_SESSION['data_access']->db_quote($this->get_cell_phone()) . "', " .
			"companyid='" . $_SESSION['data_access']->db_quote($this->get_companyid()) . "', " .
			"personal_addressid='" . $_SESSION['data_access']->db_quote($this->get_personal_addressid()) . "', " . 
			"business_addressid='" . $_SESSION['data_access']->db_quote($this->get_business_addressid()) . "', " .
			"logo_path='" . $_SESSION['data_access']->db_quote($this->get_logo_path()) . "', " .
			"pass_question='" . $_SESSION['data_access']->db_quote($this->get_pass_question()) . "', " .
			"pass_answer='" . $_SESSION['data_access']->db_quote($this->get_pass_answer()) . "', " . 
			"billing_status='" . $_SESSION['data_access']->db_quote($this->get_billing_status()) . "', " . 
			"billing_type='" . $_SESSION['data_access']->db_quote($this->get_billing_type()) . "', " .
			"billing_type_other='" . $_SESSION['data_access']->db_quote($this->get_billing_type_other()) . "', " .
			"next_bill_date='" . $_SESSION['data_access']->db_quote($this->get_next_bill_date()) . "', " .
			"card_name='" . $_SESSION['data_access']->db_quote($this->get_card_name()) . "', " . 
			"card_type='" . $_SESSION['data_access']->db_quote($this->get_card_type()) . "', "
			;
		if (!empty($this->get_card_number())) {
			$this->ensureTokenized($customerFacingMessage);
			$query .=
				"card_number='" . $_SESSION['data_access']->db_quote($this->get_card_number()) . "', ";
		}
		$query .=
			"card_exp_date='" . $_SESSION['data_access']->db_quote($this->get_card_exp_date()) . "', " .
			"start_date='" . $_SESSION['data_access']->db_quote($this->get_start_date()) . "', " . 
			"end_date='" . $_SESSION['data_access']->db_quote($this->get_end_date()) . "', " . 
			"customer_number='" . $_SESSION['data_access']->db_quote($this->get_customer_number()) . "', " .
			"customer_type='" . $_SESSION['data_access']->db_quote($this->get_customer_type()) . "', " .
			"customer_type_other='" . $_SESSION['data_access']->db_quote($this->get_customer_type_other()) . "', " . 
			"invoice_number='" . $_SESSION['data_access']->db_quote($this->get_invoice_number()) . "', " . 
			"authorize_net_number='" . $_SESSION['data_access']->db_quote($this->get_authorize_net_number()) . "', " . 
			"subscription_type='" . $_SESSION['data_access']->db_quote($this->get_subscription_type()) . "', " .
			"subscription_type_other='" . $_SESSION['data_access']->db_quote($this->get_subscription_type_other()) . "', " .
			"license_redistribute='" . $_SESSION['data_access']->db_quote($this->get_license_redistribute()) . "', " .
			"license_how_many='" . $_SESSION['data_access']->db_quote($this->get_license_how_many()) . "', " .
			"rate='" . $_SESSION['data_access']->db_quote($this->get_rate()) . "', " . 
			"monthly_average='" . $_SESSION['data_access']->db_quote($this->get_monthly_average()) . "', " .
			"referral_source='" . $_SESSION['data_access']->db_quote($this->get_referral_source()) . "', " .
			"referral_source_other='" . $_SESSION['data_access']->db_quote($this->get_referral_source_other()) . "', " .
			"url='" . $_SESSION['data_access']->db_quote($this->get_url()) . "', " . 
			"notes='" . $_SESSION['data_access']->db_quote($this->get_notes()) . "', " . 
			"prospecting_notes='" . $_SESSION['data_access']->db_quote($this->get_prospecting_notes()) . "', " . 
			"trial_to_customer='" . $_SESSION['data_access']->db_quote($this->get_trial_to_customer()) . "', " . 
			"cancel_reason='" . $_SESSION['data_access']->db_quote($this->get_cancel_reason()) . "', " . 
			"month_additional_area_pricing='" . $_SESSION['data_access']->db_quote($this->get_month_additional_area_pricing()) . "', " . 
			"month_one_area_pricing='" . $_SESSION['data_access']->db_quote($this->get_month_one_area_pricing()) . "', " . 
			"month_two_area_pricing='" . $_SESSION['data_access']->db_quote($this->get_month_two_area_pricing()) . "', " . 
			"year_additional_area_pricing='" . $_SESSION['data_access']->db_quote($this->get_year_additional_area_pricing()) . "', " . 
			"year_one_area_pricing='" . $_SESSION['data_access']->db_quote($this->get_year_one_area_pricing()) . "', " . 
			"year_two_area_pricing='" . $_SESSION['data_access']->db_quote($this->get_year_two_area_pricing()) . "' " . 
			"WHERE accountid='" . $_SESSION['data_access']->db_quote($this->get_accountid()) . "'"
		;
		try {
			$_SESSION['data_access']->query( array('query'=>$query) );
		}
		catch (Exception $exception) {
			throw $exception;
		}
	}
	
	function save_area_names($area_names = NULL) {
		if ($area_names) {
			try {
				$query = "DELETE FROM area_name_entry WHERE accountid='" . $_SESSION['data_access']->db_quote($this->get_accountid()) . "'";
				$_SESSION['data_access']->query( array('query'=>$query) );
				foreach ($area_names as $area_name) {
					$query = "INSERT INTO area_name_entry VALUES ('" . $_SESSION['data_access']->db_quote($area_name) . "', '" . $_SESSION['data_access']->db_quote($this->get_accountid()) . "')";
					$_SESSION['data_access']->query( array('query'=>$query) );
				}
				$current_count_area_names = count($this->get_list_area_names());
				$this->set_area_names();
			}
			catch (Exception $exception) {
				throw $exception;
			}
		}
	}
	
	function save_listing_fields($listing_fields = NULL) {
		try {
			$query = "DELETE FROM account_listing_fields WHERE accountid='" . $_SESSION['data_access']->db_quote($this->get_accountid()) . "'";
			$_SESSION['data_access']->query( array('query'=>$query) );
			if (!empty($listing_fields)) {
				foreach ($listing_fields as $listing_field) {
					$string_listing_fields .= $listing_field . ',';
				}
				$string_listing_fields = substr($string_listing_fields, 0, -1);
				if ($string_listing_fields) {
					$query = "INSERT INTO account_listing_fields VALUES ('" . $_SESSION['data_access']->db_quote($this->get_accountid()) . "', '" . $_SESSION['data_access']->db_quote($string_listing_fields) . "')";
					$_SESSION['data_access']->query( array('query'=>$query) );
				}
			}
		}
		catch (Exception $exception) {
			throw $exception;
		}
	}
	
	function save_property_type_fields($property_type_fields = NULL) {
		try {
			$query = "DELETE FROM account_property_type_fields WHERE accountid='" . $_SESSION['data_access']->db_quote($this->get_accountid()) . "'";
			$_SESSION['data_access']->query( array('query'=>$query) );
			if (!empty($property_type_fields)) {
				foreach ($property_type_fields as $property_type_field) {
					$string_property_type_fields .= $property_type_field . ',';
				}
				$string_property_type_fields = substr($string_property_type_fields, 0, -1);
				if ($string_property_type_fields) {
					$query = "INSERT INTO account_property_type_fields VALUES ('" . $_SESSION['data_access']->db_quote($this->get_accountid()) . "', '" . $_SESSION['data_access']->db_quote($string_property_type_fields) . "')";
					$_SESSION['data_access']->query( array('query'=>$query) );
				}
			}
		}
		catch (Exception $exception) {
			throw $exception;
		}
	}
	
	function save_search_fields($search_fields = NULL) {
		try {
			$query = "DELETE FROM account_search_fields WHERE accountid='" . $_SESSION['data_access']->db_quote($this->get_accountid()) . "'";
			$_SESSION['data_access']->query( array('query'=>$query) );
			if (!empty($search_fields)) {
				foreach ($search_fields as $search_key=>$search_value) {
					$string_search_fields .= $search_key . '=' . $search_value . ',';
				}
				$string_search_fields = substr($string_search_fields, 0, -1);
				if ($string_search_fields) {
					$query = "INSERT INTO account_search_fields VALUES ('" . $_SESSION['data_access']->db_quote($this->get_accountid()) . "', '" . $_SESSION['data_access']->db_quote($string_search_fields) . "')";
					$_SESSION['data_access']->query( array('query'=>$query) );
				}
			}
		}
		catch (Exception $exception) {
			throw $exception;
		}
	}
	
	function save_notes() {
		$query = "UPDATE account SET notes='" . $_SESSION['data_access']->db_quote($this->get_notes()) . "' WHERE accountid='" . $_SESSION['data_access']->db_quote($this->get_accountid()) . "'";
		try {
			$_SESSION['data_access']->query( array('query'=>$query) );
		}
		catch (Exception $exception) {
			throw $exception;
		}
	}
	
	function save_robot_names($robot_names = NULL) {
		$query = "DELETE FROM robot_name_entry WHERE accountid='" . $_SESSION['data_access']->db_quote($this->get_accountid()) . "'";
		$_SESSION['data_access']->query( array('query'=>$query) );
		if ($robot_names) {
			try {
				foreach ($robot_names as $robot_name) {
					$query = "INSERT INTO robot_name_entry VALUES ('" . $_SESSION['data_access']->db_quote($robot_name) . "', '" . $_SESSION['data_access']->db_quote($this->get_accountid()) . "')";
					$_SESSION['data_access']->query( array('query'=>$query) );
				}
			}
			catch (Exception $exception) {
				throw $exception;
			}
		}
		$this->set_robot_names();
	}
	
	function saveProduct($args = NULL) {
		if ($this->get_accountid() && count($args['product']) > 0) {
			$query = "DELETE FROM AccountProductEntry WHERE accountid=" . $_SESSION['data_access']->db_quote($this->get_accountid());
			$_SESSION['data_access']->query( array('query'=>$query) );
			foreach ($args['product'] as $productId) {
				$query = "INSERT INTO AccountProductEntry (accountId, productId) VALUES ('" . $_SESSION['data_access']->db_quote($this->get_accountid()) . "', " . $_SESSION['data_access']->db_quote($productId) . ")";
				$_SESSION['data_access']->query( array('query'=>$query) );
			}
		}
	}
	
	function setProduct($args = NULL) {
		if ($this->get_accountid() && $args['productName']) {
			require_once('office/account/class.Product.php');
			$Product = new Product( array('name'=>$args['productName']) );
			if ($Product->getProductId() && !$this->arrayProducts[$Product->getProductId()]) {
				$query = "INSERT INTO AccountProductEntry (accountId, productId) VALUES ('" . $_SESSION['data_access']->db_quote($this->get_accountid()) . "', " . $_SESSION['data_access']->db_quote($Product->getProductId()) . ")";
				$_SESSION['data_access']->query( array('query'=>$query) );
			}
		}
	}
			
	function update_status($new_status = NULL) {
		if ($new_status && $this->get_billing_status() && $new_status != $this->get_billing_status()) {
			$query = "UPDATE account SET " . 
				"billing_status_date=NOW(), " . 
				"billing_status='" . $_SESSION['data_access']->db_quote($new_status) . "' " . 
				"WHERE accountid='" . $_SESSION['data_access']->db_quote($this->get_accountid()) . "'"
			;
			try {
				$_SESSION['data_access']->query( array('query'=>$query) );
			}
			catch (Exception $exception) {
				throw $exception;
			}
		}
	}
	
	function validate_data($args = NULL) {
		foreach ($args['fields'] as $field_name => $type) {
			if ($args['data'][$field_name]) {
				switch (strtolower($type)) {

					//handle a CARD EXP MONTH validation
					case 'card_exp_date':
						if ($this->invalid_card_exp_date($args['data']['card_exp_month'], $args['data']['card_exp_year'])) {
							$_SESSION['error']['message'] = "_GLOBAL_BLOCK_BAD_DATA";
							$_SESSION['error']['badfield'][$field_name] = true;
						}
					break;
				
					//handle a CARD NUMBER validation
					case 'card_number':
						if ($this->invalid_card_number( array('card_type'=>$args['data']['card_type'], card_number=>$args['data'][$field_name]) )) {
							$_SESSION['error']['badfield'][$field_name] = true;
						}
					break;

					//handle a EMAIL validation
					case 'email':
						if ($this->invalid_email($args['data'][$field_name])) {
							$_SESSION['error']['message'] = "_GLOBAL_BLOCK_BAD_DATA";
							$_SESSION['error']['badfield'][$field_name] = true;
						}
					break;
									
					//handle a ROUTING NUMBER validation
					case 'routing_number':
						if ($this->invalid_routing_number( array(routing_number=>$args['data'][$field_name]) )) {
							$_SESSION['error']['message'] = "_GLOBAL_BLOCK_BAD_DATA";
							$_SESSION['error']['badfield'][$field_name] = true;
						}
					break;
				
					//handle a USERNAME validation
					case 'username':
						if ($this->invalid_username($args['data'][$field_name])) {
							$_SESSION['error']['message'] = "_GLOBAL_BLOCK_BAD_DATA";
							$_SESSION['error']['badfield'][$field_name] = true;
						}
					break;
			
				}
			}
		}
	}
	
	// get and set
	function get_account_type() { return $this->account_type; }
	function get_accountid() { return $this->accountid; }
	function get_affiliateid() { return $this->affiliateid; }
	function get_alert_email_address() { return $this->alert_email_address; }
	function getArrayProducts() { return $this->arrayProducts; }
	function get_authorize_net_number() { return $this->authorize_net_number; }
	function get_billing_day() { return $this->billing_day; }
	function get_billing_email_address() { return $this->billing_email_address; }
	function get_billing_status() { return $this->billing_status; }
	function get_billing_status_date() { return $this->billing_status_date; }
	function get_billing_type() { return $this->billing_type; }
	function get_billing_type_other() { return $this->billing_type_other; }
	function get_business_addressid() { return $this->business_addressid; }
	function get_business_name() { return $this->business_name; }
	function get_business_name_short() {
		if (strlen($this->get_business_name()) <= 23) {
			return $this->get_business_name();
		}
		else {
			return substr($this->get_business_name(), 0, 20) . '...';
		}
	}
	function get_business_phone() { return $this->business_phone; }
	function get_cancel_reason() { return $this->cancel_reason; }
	function get_card_exp_date() { return $this->card_exp_date; }
	function get_card_name() { return $this->card_name; }
	function get_card_number($args = NULL) {
		if ($this->card_number) {
			$decryptedCardNumber = $_SESSION['web_interface']->getDecrypt($this->card_number);
			if ($args['account_type'] == 'ADMIN') {
				return $decryptedCardNumber;
			}
			else if ($args['account_type']) {
				return 'XXXX' . substr($decryptedCardNumber, -4);
			}
			return $this->card_number;
		}
		return '';
	}

	public function getToken()
	{
		$cardNumber = $this->get_card_number(['account_type' => 'ADMIN']);
		if (!empty($cardNumber) && $cardNumber[0] == 'T') {
			return substr($cardNumber, 1); # throw away the T
		}
		return null;
	}

	private function ensureTokenized($customerFacingMessage)
	{
		$cardNumber = $this->get_card_number(['account_type' => 'ADMIN']);
		if (!empty($cardNumber) && $cardNumber[0] != 'T') {
			$result = cardvaultsdk\store([
				'number' => $cardNumber,
				'expiration_date' => '20'.substr($this->get_card_exp_date(), -2)
					.'-'.substr($this->get_card_exp_date(), 0, 2),
				'name' => $this->get_card_name(),
				'street' => $this->o_business_address->getStreet(),
				'zip' => $this->o_business_address->get_postal_code(),
				], $customerFacingMessage,
				BOD_CARDVAULT_STORE_FAILURE
				);
			$this->card_number = $_SESSION['web_interface']->getEncrypt("T$result->token");
		}
	}

	function get_card_type() { return $this->card_type; }
	function get_cell_phone() { return $this->cell_phone; }
	function get_companyid() { return $this->companyid; }
	function get_created() { return $this->created; }
	function get_customer_number() { return $this->customer_number; }
	function get_customer_type() { return $this->customer_type; }
	function get_customer_type_other() { return $this->customer_type_other; }
	function get_email_attachment() { return $this->email_attachment; }
	function get_end_date() { return $this->end_date; }
	function get_fax_phone() { return $this->fax_phone; }
	function get_first_name() { return $this->first_name; }
	function get_ftp_file() { return $this->ftp_file; }
	function get_full_name() { return $this->get_first_name() . ' ' . $this->get_last_name(); }
	function get_full_name_short() {
		if (strlen($this->get_full_name()) <= 23) {
			return $this->get_full_name();
		}
		else {
			return substr($this->get_full_name(), 0, 20) . '...';
		}
	}
	function get_invoice_number() { return $this->invoice_number; }
	function get_last_login() { return $this->last_login; }
	function get_last_name() { return $this->last_name; }
	function get_license_how_many() { return $this->license_how_many; }
	function get_license_redistribute() { return $this->license_redistribute; }	
	function get_list_area_names() { return $this->list_area_names; }
	function get_list_robot_names() { return $this->list_robot_names; }
	function get_login_attempt() { return $this->login_attempt; }
	function get_logo_path() { return $this->logo_path; }
	function get_login_count() { return $this->login_count; }
	function get_main_email_address() { return $this->main_email_address; }
	function get_month_additional_area_pricing() { return $this->month_additional_area_pricing; }
	function get_month_one_area_pricing() { return $this->month_one_area_pricing; }
	function get_month_two_area_pricing() { return $this->month_two_area_pricing; }
	function get_monthly_average() { return $this->monthly_average; }
	function get_next_bill_date() { return $this->next_bill_date; }
	function get_notes() { return $this->notes; }
	function get_pass_answer() { return $this->pass_answer; }
	function get_pass_question() { return $this->pass_question; }
	function get_password() { return $this->password; }
	function get_personal_addressid() { return $this->personal_addressid; }
	function get_personal_phone() { return $this->personal_phone; }
	function get_prospecting_notes() { return $this->prospecting_notes; }
	function get_rate() { return $this->rate; }
	function get_referral_source() { return $this->referral_source; }
	function get_referral_source_other() { return $this->referral_source_other; }
	function get_trial_to_customer() { return $this->trial_to_customer; }
	function get_url() { return $this->url; }
	function get_start_date() { return $this->start_date; }
	function get_string_area_names() { return $this->string_area_names; }
	function getStringProductIds() { return $this->stringProductIds; }
	function getStringProductNames() { return $this->stringProductNames; }
	function get_string_robot_names() { return $this->string_robot_names; }
	function get_subscription_type() { return $this->subscription_type; }
	function get_subscription_type_other() { return $this->subscription_type_other; }
	function get_username() { return $this->username; }
	function get_year_additional_area_pricing() { return $this->year_additional_area_pricing; }
	function get_year_one_area_pricing() { return $this->year_one_area_pricing; }
	function get_year_two_area_pricing() { return $this->year_two_area_pricing; }
	
	function set_accountid($value) { $this->accountid = ($value) ? $value : $this->accountid; }
	function set_area_names() {
		$this->list_area_names = array();
		$this->string_area_names = ',';
		$query = "SELECT * FROM area_name_entry WHERE accountid='" . $_SESSION['data_access']->db_quote($this->get_accountid()) . "' ORDER BY area_name";
		try {
			list($results, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
			foreach ($results as $row) {
				$this->list_area_names[] = $row['area_name'];
				$this->string_area_names .= $row['area_name'] . ',';
			}
		}
		catch (Exception $exception) {
			throw $exception;
		}
	}
	function set_billing_status($value = NULL) { $this->billing_status = $value; }
	function set_business_addressid($value) { $this->business_addressid = ($value) ? $value : $this->business_addressid; }
	function set_created($value = NULL) { $this->created = $value; }
	function set_customer_number($value = NULL) { $this->customer_number = $value; }
	function set_email_attachment($value = NULL) { $this->email_attachment = $value; }
	function set_end_date($value = NULL) { $this->end_date = ($value == '0000-00-00') ? '' : $value; }
	function set_logo_path($value = NULL) { $this->logo_path = $value; }
	function set_next_bill_date($value = NULL) { $this->next_bill_date = ($value && $value != '0000-00-00') ? $value : ''; }
	function set_notes($value = NULL) { $this->notes = $value; }
	function set_personal_addressid($value) { $this->personal_addressid = ($value) ? $value : $this->personal_addressid; }
	function setProducts() {
		$this->arrayProducts = array();
		$this->stringProductIds = ',';
		$this->stringProductNames = ',';
		$query = "SELECT ape.productId, name FROM AccountProductEntry ape, Product p WHERE ape.productId=p.productId AND ape.accountId='" . $_SESSION['data_access']->db_quote($this->get_accountid()) . "'";
		list($results, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
		foreach ($results as $row) {
			$this->arrayProducts[$row['productId']] = $row['name'];
			$this->stringProductIds .= $row['name'] . ',';
			$this->stringProductNames .= $row['productId'] . ',';
		}
	}
	function set_robot_names() {
		$this->list_robot_names = array();
		$this->string_robot_names = ',';
		$query = "SELECT * FROM robot_name_entry WHERE accountid='" . $_SESSION['data_access']->db_quote($this->get_accountid()) . "' ORDER BY robot_name";
		try {
			list($results, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
			foreach ($results as $row) {
				$this->list_robot_names[] = $row['robot_name'];
				$this->string_robot_names .= $row['robot_name'] . ',';
			}
		}
		catch (Exception $exception) {
			throw $exception;
		}
	}
	function set_start_date($value = NULL) { $this->start_date = ($value == '0000-00-00') ? '' : $value; }
	

	//private
	private $account_type;
	private $accountid;
	private $affiliateid;
	private $alert_email_address;
	private $arrayProducts = array();		
	private $authorize_net_number;
	private $billing_day;
	private $billing_email_address;
	private $billing_status;
	private $billing_status_date;
	private $billing_type;
	private $billing_type_other;
	private $business_addressid;
	private $business_name;
	private $business_phone;
	private $cancel_reason;
	private $card_exp_date;
	private $card_name;
	private $card_number;
	private $card_type;
	private $cell_phone;
	private $companyid;
	private $created;
	private $customer_number;
	private $customer_type;
	private $customer_type_other;
	private $email_attachment;
	private $end_date;
	private $fax_phone;
	private $first_name;
	private $ftp_file;
	private $invoice_number;
	private $last_login;
	private $last_name;
	private $license_how_many;
	private $license_redistribute;
	private $list_area_names = array();
	private $list_robot_names = array();
	private $login_attempt;
	private $login_count;
	private $logo_path;
	private $main_email_address;
	private $month_additional_area_pricing;
	private $month_one_area_pricing;
	private $month_two_area_pricing;
	private $monthly_average;
	private $next_bill_date;
	private $notes;
	private $pass_answer;
	private $pass_question;
	private $password;
	private $personal_addressid;
	private $personal_phone;
	private $prospecting_notes;
	private $rate;
	private $referral_source;
	private $referral_source_other;
	private $start_date;
	private $string_area_names;
	private $stringProductIds;
	private $stringProductNames;
	private $string_robot_names;
	private $subscription_type;
	private $subscription_type_other;
	private $trial_to_customer;
	private $url;
	private $username;
	private $year_additional_area_pricing;
	private $year_one_area_pricing;
	private $year_two_area_pricing;

}

class account_utilities {

	//public
	function __construct($args = NULL) {
		$this->initialize($args);
	}
	
	function initialize($args = NULL) {
	}

	function find_account($args = NULL) {
		if ($args['where_plus']) {
			$query = "SELECT account.accountid FROM account WHERE account_type NOT LIKE '%admin%' " . $args['where_plus'];
			try {
				list($results, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
				$o_account = new account( array(accountid=>$results[0]['accountid']) );
				return $o_account;
			}
			catch (Exception $exception) {
				throw $exception;
			}
		}
		else {
			return 0;
		}
	}
	
	function get_list($args = NULL) {
		$args['order_by'] = ($args['order_by']) ? $args['order_by'] : 'customer_number, last_name, first_name';
		$query = "SELECT * FROM account WHERE account_type NOT LIKE '%admin%' " . $args['where_plus'] . " ORDER BY " . $args['order_by'];
		try {
			list($results, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
			$list = array(); 
			foreach ($results as $row) {
				if ($args['expecting'] == 'hash') {
					$list[$row['accountid']] = $row;
				}
				else {
					$o_account = new account();
					$o_account->initialize($row);
					$list[] = $o_account;
				}
			}
			return $list;
		}
		catch (Exception $exception) {
			throw $exception;
		}
	}
	
	function get_list_for_search($args = NULL) {
		$where_plus = '';
		if ($args['keyword']) {
			$_REQUEST['word'] = $args['keyword'];
			$where_plus .= " AND (customer_number like '%" . $_SESSION['data_access']->db_quote($args['keyword']) . "%' OR first_name like '%" . $_SESSION['data_access']->db_quote($args['keyword']) . "%' OR last_name like '%" . $_SESSION['data_access']->db_quote($args['keyword']) . "%' OR main_email_address LIKE '%" . $_SESSION['data_access']->db_quote($args['keyword']) . "%' OR business_name like '%" . $_SESSION['data_access']->db_quote($args['keyword']) . "%' OR username like '%" . $args['keyword'] . "%' OR accountid like '%" . $args['keyword'] . "%' OR personal_phone like '%" . $args['keyword'] . "%' OR business_phone like '%" . $args['keyword'] . "%')";
		}
		if ($args['companyid']) {
			if ($args['companyid'] > 0) {
				$where_plus .= " AND companyid='" . $args['companyid'] . "' ";
			}
		}
		if ($args['account_type']) {
			$where_plus .= " AND account_type='" . $args['account_type'] . "' ";
		}
		if ($args['billing_status']) {
			$where_plus .= " AND billing_status='" . $args['billing_status'] . "' ";
		}
		if ($args['start_year'] && $args['start_month'] && $args['start_day']) {
			$args['start_date'] = $args['start_year'] . '-' . $args['start_month'] . '-' . $args['start_day'];
		}
		if ($args['end_year'] && $args['end_month'] && $args['end_day']) {
			$args['end_date'] = $args['end_year'] . '-' . $args['end_month'] . '-' . $args['end_day'];
		}
		if ($args['billing_status'] == 'canceled') {
			$where_plus .= " AND end_date >= '" . $args['start_date'] . "' AND end_date <= '" . $args['end_date'] . "' ";
			$order_by = ' customer_number, last_name, first_name ';
		}
		else {
			$where_plus .= " AND created >= '" . $args['start_date'] . "' AND created <= '" . $args['end_date'] . "' ";
			$order_by = ' customer_number, last_name, first_name ';
		}
		if ($_SESSION['office']->account->is_private()) {
			$where_plus .= " AND account_type != 'PRIVATE_LABEL' AND companyid='" . $_SESSION['office']->account->get_companyid() . "' ";
		}
		if (isset($where_plus)) {
			try {
				return $this->get_list( array('order_by'=>$order_by, 'where_plus'=>$where_plus) );
			}
			catch (Exception $exception) {
				throw $exception;
			}
		}
	}

	function get_list_new($args = NULL) {
		$where_plus = " AND account_type='NEW' ";
		try {
			return $this->get_list( array(where_plus=>$where_plus) );
		}
		catch (Exception $exception) {
			throw $exception;
		}
	}
	
	function get_list_overdue($args = NULL) {
		$where_plus = " AND next_bill_date <= NOW() AND billing_status='active' AND billing_type='Credit Card' ";
		try {
			return $this->get_list( array('order_by'=>'next_bill_date', 'where_plus'=>$where_plus) );
		}
		catch (Exception $exception) {
			throw $exception;
		}
	}
	
	function get_num($args = NULL) {
		$query = "SELECT count(*) as num FROM account WHERE billing_status IN ('active','queued') " . $args['where_plus'];
		try {
			list($results, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
			$num = ($results[0]['num']) ? $results[0]['num'] : 0;
			return $num;
		}
		catch (Exception $exception) {
			throw $exception;
		}
	}
	
	//generic get and set
	
	//private
}
?>

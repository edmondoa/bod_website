<?php
#
# @copyright Copyright (c) 2017 Green Seed Technologies, Inc. All rights reserved.
#
# @author Dave Hoover
# @author David McLaughlin <david@greenseedtech.com>
#

class address {

	//constants
	const ERROR_NO_RECORD = '_LOCAL_ERROR_NO_RECORD';

	//public
	
	function __construct($args = NULL) {
		if ($args['type']) {
			$this->set_type($args['type']);
		}
		if ($args['addressid']) {
			$query = "SELECT * FROM address WHERE addressid='" . $_SESSION['data_access']->db_quote($args['addressid']) . "'";
		}
		if (isset($query)) {
			try {
				list($results, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
			}
			catch (Exception $exception) {
				throw $exception;
			}
		}
		if (!empty($num_rows)) {
			$this->initialize($results[0]);
		}
		else {
			$this->initialize($args);
		}
	}
	
	function initialize($args = NULL) {
		if (!empty($args)) {
			foreach ($args as $key=>$value) {
				$key = preg_replace("/" . $this->get_type() . "_/", "", $key);
				$args[$key] = $value;
			}
			foreach ($args as $key=>$value) {
				$method_name = 'set_' . $key;
				if (method_exists($this, $method_name)) {
					$this->{$method_name}($value);
				}
				else if (isset($args[$key])) {
					$this->{$key} = $value;
				}
			}
		}
	}
	
	function delete() {
		$query = "DELETE FROM address WHERE addressid='" . $_SESSION['data_access']->db_quote($this->get_addressid()) . "'";
		try {
			$_SESSION['data_access']->query( array('query'=>$query) );
		}
		catch (Exception $exception) {
			throw $exception;
		}
	}
	
	function invalid_postal_code($postal_code) {
		if (strlen($postal_code) < 4) {
			return TRUE;
		}
		return FALSE;
	}
	
	function save() {
		if ($this->get_addressid()) {
			$query = "UPDATE address SET line1='" . $_SESSION['data_access']->db_quote($this->get_line1()) . "', line2='" . $_SESSION['data_access']->db_quote($this->get_line2()) . "', city='" . $_SESSION['data_access']->db_quote($this->get_city()) . "', state='" . $_SESSION['data_access']->db_quote($this->get_state()) . "', country='" . $_SESSION['data_access']->db_quote($this->get_country()) . "', postal_code='" . $_SESSION['data_access']->db_quote($this->get_postal_code()) . "' WHERE addressid='" . $_SESSION['data_access']->db_quote($this->get_addressid()) . "'";
		}
		else if ($this->get_line1() || $this->get_line2() || $this->get_city() || $this->get_state() || $this->get_postal_code()) {
			$query = "INSERT INTO address (addressid, line1, line2, city, state, country, postal_code) VALUES ('', '" . $_SESSION['data_access']->db_quote($this->get_line1()) . "', '" . $_SESSION['data_access']->db_quote($this->get_line2()) . "', '" . $_SESSION['data_access']->db_quote($this->get_city()) . "', '" . $_SESSION['data_access']->db_quote($this->get_state()) . "', '" . $_SESSION['data_access']->db_quote($this->get_country()) . "', '" . $_SESSION['data_access']->db_quote($this->get_postal_code()) . "')";
		}
		if (isset($query)) {
			try {
				$this->set_addressid($_SESSION['data_access']->query( array('query'=>$query) ));
			}
			catch (Exception $exception) {
				throw $exception;
			}
		}
	}
	
	function validate_data($args = NULL) {
		foreach ($args['fields'] as $field_name => $type) {
			if ($args['data'][$field_name]) {
				switch (strtolower($type)) {
	
					//handle a POSTAL CODE validation
					case 'postal_code':
						if ($this->invalid_postal_code($args['data'][$field_name])) {
							$_SESSION['error']['message'] = "_GLOBAL_BLOCK_BAD_DATA";
							$_SESSION['error']['badfield'][$field_name] = true;
						}
					break;
					
				}
			}
		}
	}
	
	//generic get and set
	function get_addressid() { return $this->addressid; }
	function get_city() { return $this->city; }
	function get_country() { return $this->country; }
	function get_line1() { return $this->line1; }
	function get_line2() { return $this->line2; }
	function getStreet()
	{
		return trim("$this->line1 $this->line2");
	}
	function get_postal_code() { return $this->postal_code; }
	function get_state() { return $this->state; }
	function get_type() { return $this->type; }
	
	function set_addressid($value) { $this->addressid = ($value) ? $value : $this->addressid; }
	function set_type($value) { $this->type = $value; }
	
	//private
	private $addressid;
	private $city;
	private $country;
	private $line1;
	private $line2;
	private $postal_code;
	private $state;
	private $type;
	
}
?>

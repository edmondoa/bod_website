<?php
#
# @copyright Copyright (c) 2017 Green Seed Technologies, Inc. All rights reserved.
#
# @author Dave Hoover
# @author David McLaughlin <david@greenseedtech.com>
#

class area_name {

	//public
	function __construct($args = NULL) {
		if ($args['area_nameid']) {
			$query = "SELECT * FROM area_name WHERE area_nameid='" . $_SESSION['data_access']->db_quote($args['area_nameid']) . "'";
		}
		else if ($args['area_name']) {
			$query = "SELECT * FROM area_name WHERE area_name='" . $_SESSION['data_access']->db_quote($args['area_name']) . "'";
		}
		else if ($args['area_state'] && $args['area_title']) {
			$query = "SELECT * FROM area_name WHERE area_state='" . $_SESSION['data_access']->db_quote($args['area_state']) . "' AND area_title='" . $_SESSION['data_access']->db_quote($args['area_title']) . "'";
		}
		if (isset($query)) {
			try {
				list($results, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
			}
			catch (Exception $exception) {
				throw $exception;
			}
		}
		if ($num_rows) {
			$this->initialize($results[0]);
		}
		else {
			$this->initialize($args);
		}
	}
	
	function initialize($args = NULL) {
		if ($args) {
			foreach ($args as $key=>$value) {
				$method_name = 'set_' . $key;
				if (method_exists($this, $method_name)) {
					$this->{$method_name}($value);
				}
				else if (isset($args[$key])) {
					$this->{$key} = $value;
				}
			}
		}
	}
	
	function delete() {
		try {
			$query = "DELETE FROM area_name WHERE area_nameid='" . $_SESSION['data_access']->db_quote($this->get_area_nameid()) . "'";
			$_SESSION['data_access']->query( array('query'=>$query) );
			$query = "DELETE FROM area_name_entry WHERE area_name='" . $_SESSION['data_access']->db_quote($this->get_area_name()) . "'";
			$_SESSION['data_access']->query( array('query'=>$query) );
		}
		catch (Exception $exception) {
			throw $exception;
		}
	}
	
	function save() {
		$newArea = FALSE;
		if ($this->get_area_nameid()) {
			$query = "UPDATE area_name SET area_name='" . $_SESSION['data_access']->db_quote($this->get_area_name()) . "', area_title='" . $_SESSION['data_access']->db_quote($this->get_area_title()) . "', area_state='" . $_SESSION['data_access']->db_quote($this->get_area_state()) . "', area_country='" . $_SESSION['data_access']->db_quote($this->get_area_country()) . "', description='" . $_SESSION['data_access']->db_quote($this->get_description()) . "', cities='" . $_SESSION['data_access']->db_quote($this->get_cities()) . "' WHERE area_nameid='" . $_SESSION['data_access']->db_quote($this->get_area_nameid()) . "'";
		}
		else {
			$newArea = TRUE;
			$query = "INSERT INTO area_name (area_nameid, area_name, area_title, area_state, area_country, description, cities) VALUES ('', '" . $_SESSION['data_access']->db_quote($this->get_area_name()) . "', '" . $_SESSION['data_access']->db_quote($this->get_area_title()) . "', '" . $_SESSION['data_access']->db_quote($this->get_area_state()) . "', '" . $_SESSION['data_access']->db_quote($this->get_area_country()) . "', '" . $_SESSION['data_access']->db_quote($this->get_description()) . "', '" . $_SESSION['data_access']->db_quote($this->get_cities()) . "')";
		}
		if (isset($query)) {
			try {
				$this->set_area_nameid($_SESSION['data_access']->query( array('query'=>$query) ));
				
				// auto add to redex accounts if new area
				if ($newArea) {
					$query = "INSERT INTO area_name_entry (area_name, accountid) VALUES ('" . $_SESSION['data_access']->db_quote($this->get_area_name()) . "', 1140)";
					$_SESSION['data_access']->query( array('query'=>$query) );
					$query = "INSERT INTO area_name_entry (area_name, accountid) VALUES ('" . $_SESSION['data_access']->db_quote($this->get_area_name()) . "', 6659)";
					$_SESSION['data_access']->query( array('query'=>$query) );
				}
			}
			catch (Exception $exception) {
				throw $exception;
			}
		}
	}
	
	//generic get and set
	function get_area_country() { return $this->area_country; }
	function get_area_name() { return $this->area_name; }
	function get_area_nameid() { return $this->area_nameid; }
	function get_area_state() { return $this->area_state; }
	function get_area_state_abbreviation() { return preg_replace("/^(\w\w)-.*$/", "$1", $this->get_area_name()); }
	function get_area_title() { return $this->area_title; }
	function get_cities() { return $this->cities; }
	function get_description() { return $this->description; }
	
	function set_area_nameid($value) { $this->area_nameid = ($value) ? $value : $this->area_nameid; }
	
	//private
	private $area_country;
	private $area_name;
	private $area_nameid;
	private $area_state;
	private $area_title;
	private $cities;
	private $description;
	
}

class area_name_utilities {

	//public
	function __construct($args = NULL) {
		$this->initialize($args);
	}
	
	function initialize($args = NULL) {
		if ($args) {
			foreach ($args as $key=>$value) {
				$method_name = 'set_' . $key;
				if (method_exists($this, $method_name)) {
					$this->{$method_name}($value);
				}
				else if (isset($args[$key])) {
					$this->{$key} = $value;
				}
			}
		}
	}
	
	function get_area_state($args = NULL) {
		$query = "SELECT DISTINCT(area_state) AS area_state FROM area_name WHERE area_name LIKE '" . $_SESSION['data_access']->db_quote($args['state']) . "-%'";
		try {
			list($results, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
			return $results[0]['area_state'];
		}
		catch (Exception $exception) {
			throw $exception;
		}
	}
	
	function get_list($args = NULL) {
		$query = "SELECT * FROM area_name " . $args['where'] . " ORDER BY area_state, area_title";
		try {
			list($results, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
			$list = array(); 
			foreach ($results as $row) {
				$o_area_name = new area_name();
				$o_area_name->initialize($row);
				$list[] = $o_area_name;
			}
			return $list;
		}
		catch (Exception $exception) {
			throw $exception;
		}
	}
	
	function get_list_accountids_for_each_area_name() {
		$list_accountids_for_each_area_name = array();
		$list_area_name = $this->get_list_area_names();
		foreach ($list_area_name as $area_name) {
			$query = "SELECT accountid FROM area_name_entry WHERE area_name='" . $_SESSION['data_access']->db_quote($area_name) . "' ORDER BY accountid";
			try {
				list($results, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
				foreach ($results as $row) {
					$list_accountids_for_each_area_name[$area_name] .= $row['accountid'] . ',';
				}
			}
			catch (Exception $exception) {
				throw $exception;
			}
		}
		return $list_accountids_for_each_area_name;
	}
	
	function get_list_area_names($args = NULL) {
		$query = "SELECT area_name FROM area_name " . $args['where'] . " ORDER BY area_name";
		try {
			list($results, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
			$list = array(); 
			foreach ($results as $row) {
				$list[] = $row['area_name'];
			}
			return $list;
		}
		catch (Exception $exception) {
			throw $exception;
		}
	}
	
	function get_list_area_states($args = NULL) {
		$args['where_plus'] = preg_replace("/^ AND /", " WHERE ", $args['where_plus']);
		$query = "SELECT area_country, area_state FROM area_name " . $args['where_plus'] . " GROUP BY area_country, area_state ORDER BY area_country desc, area_state";
		try {
			list($results, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
			$list = array(); 
			foreach ($results as $row) {
				$list[] = $row['area_state'];
			}
			return $list;
		}
		catch (Exception $exception) {
			throw $exception;
		}
	}
	
	function get_list_area_titles($args = NULL) {
		$list = array();
		if ($args['area_state']) {
			$query = "SELECT area_title FROM area_name WHERE area_state='" . $_SESSION['data_access']->db_quote($args['area_state']) . "' ORDER BY area_title";
		}
		else if ($args['where']) {
			$query = "SELECT area_title FROM area_name " . $args['where'] . " ORDER BY area_title";
		}
		if (isset($query)) {
			try {
				list($results, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
				foreach ($results as $row) {
					$list[] = $row['area_title'];
				}
			}
			catch (Exception $exception) {
				throw $exception;
			}
		}
		return $list;
	}
	
	function get_list_state_time_zone() {
		$hash = array();
		$query = "SELECT * FROM state_time_zone";
		list($results, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
		if (count($results) > 0) {
			foreach ($results as $row) {
				$hash[$row['state']] = $row['time_zone'];
			}
		}
		return $hash;
	}
		
	function get_string_states_in_time_zone($args = NULL) {
		if ($args['time_zone']) {
			$string = ',';
			$query = "SELECT state FROM state_time_zone WHERE time_zone='" . $_SESSION['data_access']->db_quote($args['time_zone']) . "' ORDER BY state";
			list($results, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
			foreach ($results as $row) {
				if ($args['mysql_formatted']) {
					$string .= "'" . $_SESSION['data_access']->db_quote($row['state']) . "',";
				}
				else {
					$string .= $row['state'] . ',';
				}
			}
			if ($args['mysql_formatted']) {
				$string = preg_replace("/^,(.*),$/" , "$1", $string);
			}
			return $string;
		}
		return '';
	}
	
	function get_time_zones() {
		$array = array();
		$query = "SELECT distinct(time_zone) AS time_zone FROM state_time_zone ORDER BY time_zone";
		list($results, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
		foreach ($results as $row) {
			$array[] = $row['time_zone'];
		}
		return $array;
	}
				
	//generic get and set
	function get_accountid() { return $this->accountid; }
	
	function set_accountid($value) { $this->accountid = ($value) ? $value : $_SESSION['office']->account->get_accountid(); }
	
	//private
	private $accountid;
	
}

?>

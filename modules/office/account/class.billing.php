<?php
#
# @copyright Copyright (c) 2017 Green Seed Technologies, Inc. All rights reserved.
#
# @author Dave Hoover
# @author David McLaughlin <david@greenseedtech.com>
#

class billing {
	// public
	function __construct($args = NULL) {
		if (!empty($args['billingid'])) {
			$query = "SELECT * FROM billing WHERE billingid='" . $_SESSION['data_access']->db_quote($args['billingid']) . "'";
		}
		if (isset($query)) {
			try {
				list($results, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
			}
			catch (Exception $exception) {
				throw $exception;
			}
		}
		if (!empty($num_rows)) {
			$this->initialize($results[0]);
		}
		else {
			unset($args['billingid']);
			$this->initialize($args);
		}
	}

	function initialize($args = NULL) {
		if (isset($args)) {
			foreach ($args as $key=>$value) {
				$key = preg_replace("/" . $this->get_type() . "_/", "", $key);
				$args[$key] = $value;
			}
		}
		if (!empty($args)) {
			foreach ($args as $key=>$value) {
				$method_name = 'set_' . $key;
				if (method_exists($this, $method_name)) {
					$this->{$method_name}($value);
				}
				else if (isset($args[$key])) {
					$this->{$key} = $value;
				}
			}
		}
	}

	function delete() {
		$query = "DELETE FROM billing WHERE billingid='" . $_SESSION['data_access']->db_quote($this->get_billingid()) . "'";
		try {
			$_SESSION['data_access']->query( array('query'=>$query) );
		}
		catch (Exception $exception) {
			throw $exception;
		}
	}

	function save() {
		if ($this->get_billingid()) {
			$query = "UPDATE billing SET amount=" . $_SESSION['data_access']->db_quote($this->amount()) . "', type='" . $_SESSION['data_access']->db_quote($this->get_type()) . "', approval_code='" . $_SESSION['data_access']->db_quote($this->get_approval_code()) . "', transaction_id='" . $_SESSION['data_access']->db_quote($this->get_transaction_id()) . "', notes='" . $_SESSION['data_access']->db_quote($this->get_notes()) . "' WHERE billingid='" . $_SESSION['data_access']->db_quote($this->get_billingid()) . "'";
		}
		else {
			$query = "INSERT INTO billing (accountid, created, amount, type, approval_code, transaction_id, notes) VALUES ('" . $_SESSION['data_access']->db_quote($this->get_accountid()) . "', NOW(), " . $_SESSION['data_access']->db_quote($this->get_amount()) . ", '" . $_SESSION['data_access']->db_quote($this->get_type()) . "', '" . $_SESSION['data_access']->db_quote($this->get_approval_code()) . "', '" . $_SESSION['data_access']->db_quote($this->get_transaction_id()) . "', '" . $_SESSION['data_access']->db_quote($this->get_notes()) . "')";
		}
		if (isset($query)) {
			try {
				$this->set_billingid($_SESSION['data_access']->query( array('query'=>$query) ));
			}
			catch (Exception $exception) {
				throw $exception;
			}
		}
	}

	// generic get and set
	function get_billingid() { return $this->billingid; }
	function get_accountid() { return $this->accountid; }
	function get_amount() { return $this->amount; }
	function get_approval_code() { return $this->approval_code; }
	function get_created() { return $this->created; }
	function get_notes() { return $this->notes; }
	function get_transaction_id() { return $this->transaction_id; }
	function get_type() { return $this->type; }

	function set_billingid($value = NULL) { $this->billingid = ($value) ? $value : $this->get_billingid(); }
	function set_amount($value = NULL) { $this->amount = ($value) ? $value : '0.00'; }

	// private
	private $billingid;
	private $accountid;
	private $amount;
	private $approval_code;
	private $created;
	private $notes;
	private $transaction_id;
	private $type;

}

class billing_utilities {
	// public
	function __construct($args = NULL) {
		$this->initialize($args);
	}

	function initialize($args = NULL) {
		$this->set_order_by($args['order_by']);
	}

	function get_list($args = NULL) {
		$args['where_plus'] = ($args['where_plus']) ? preg_replace("/ AND /" , " WHERE ", $args['where_plus']) : '';
		$query = "SELECT * FROM billing " . $args['where_plus'] . " ORDER BY " . $this->get_order_by();
		list($results, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
		foreach ($results as $row) {
			$o_billing = new billing();
			$o_billing->initialize($row);
			$list[] = $o_billing;
		}
		return $list;
	}
	
	function get_most_recent_charge($args = NULL) {
		$args['where_plus'] = ($args['where_plus']) ? preg_replace("/ AND /" , " WHERE ", $args['where_plus']) : '';
		$query = "SELECT * FROM billing " . $args['where_plus'] . " ORDER BY created DESC LIMIT 1";
		list($results, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
		$most_recent_charge = ($results[0]['amount']) ? $results[0]['amount'] : 0;
		return $most_recent_charge; 
	}

	// generic get and set
	function get_order_by() { return $this->order_by; }

	function set_order_by($value = NULL) { $this->order_by = ($value) ? $value : 'created desc'; }

	// private
	private $order_by;

}

?>

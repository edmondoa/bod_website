<?php
#
# @copyright Copyright (c) 2017 Green Seed Technologies, Inc. All rights reserved.
#
# @author Dave Hoover
# @author David McLaughlin <david@greenseedtech.com>
#

class company {

	//public
	
	function __construct($args = NULL) {
		if ($args['companyid']) {
			$query = "SELECT * FROM company WHERE companyid='" . $_SESSION['data_access']->db_quote($args['companyid']) . "'";
		}
		if (isset($query)) {
			try {
				list($results, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
			}
			catch (Exception $exception) {
				throw $exception;
			}
		}
		if (!empty($num_rows)) {
			$this->initialize($results[0]);
		}
		else {
			$this->initialize($args);
		}
	}
	
	function initialize($args = NULL) {
		if (is_array($args)) {
			foreach ($args as $key=>$value) {
				$method_name = 'set_' . $key;
				if (method_exists($this, $method_name)) {
					$this->{$method_name}($value);
				}
				else if (isset($args[$key])) {
					$this->{$key} = $value;
				}
			}
		}
	}
	
	function getProduct($args = NULL) {
		require_once('office/account/class.Product.php');
		$ProductUtilities = new ProductUtilities();
		return $ProductUtilities->getListCompany( array('companyId'=>$this->get_companyid()) );
	}
	
	function get_table_fields() {
		$query = "desc company";
		list($results, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
		$temp_array = array();
		foreach ($results as $result) {
			$temp_array[] = $result['Field'];
		}
		sort($temp_array);
		return $temp_array;
	}
	
	function merge_info($args = NULL) {
		if ($args['string']) {
			$args['string'] = str_ireplace("{" . $args['key_modifier'], "{", $args['string']);
			$query = "SELECT * FROM company WHERE companyid=" . $_SESSION['data_access']->db_quote($this->get_companyid());
			list($results, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
			foreach ($results[0] as $key=>$value) {
				$args['string'] = str_ireplace("{" . $key . "}", $value, $args['string']);
			}
		}
		return $args['string'];
	}
	
	// get and set
	function get_company_url() { return $this->company_url; }
	function get_companyid() { return $this->companyid; }
	function get_directory() { return $this->directory; }
	function get_parent_directory() { return $this->parent_directory; }
	function get_main_email_address() { return $this->main_email_address; }
	function get_month_additional_area_pricing() { return $this->month_additional_area_pricing; }
	function get_month_one_area_pricing() { return $this->month_one_area_pricing; }
	function get_month_two_area_pricing() { return $this->month_two_area_pricing; }
	function get_title() { return $this->title; }
	function get_url() { return $this->url; }
	function get_year_additional_area_pricing() { return $this->year_additional_area_pricing; }
	function get_year_one_area_pricing() { return $this->year_one_area_pricing; }
	function get_year_two_area_pricing() { return $this->year_two_area_pricing; }
	function get_from_email() { return $this->from_email; }
	function get_reply_to_email() { return $this->reply_to_email; }

	function set_companyid($value) { $this->companyid = ($value) ? $value : $this->companyid; }
	
	//private
	private $company_url;
	private $companyid;
	private $directory;
	private $parent_directory;
	private $main_email_address;
	private $month_additional_area_pricing;
	private $month_one_area_pricing;
	private $month_two_area_pricing;
	private $title;
	private $url;
	private $year_additional_area_pricing;
	private $year_one_area_pricing;
	private $year_two_area_pricing;
	private $from_email;
	private $reply_to_email;

}

class company_utilities {

	//public
	function __construct($args = NULL) {
		$this->initialize($args);
	}
	
	function initialize($args = NULL) {
	}

	function get_list($args = NULL) {
		$args['order_by'] = ($args['order_by']) ? $args['order_by'] : 'title';
		$query = "SELECT * FROM company ORDER BY " . $args['order_by'];
		try {
			list($results, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
			$list = array(); 
			foreach ($results as $row) {
				$o_company = new company();
				$o_company->initialize($row);
				$list[] = $o_company;
			}
			return $list;
		}
		catch (Exception $exception) {
			throw $exception;
		}
	}
	
	//generic get and set
	
	//private
}
?>

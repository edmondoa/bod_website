<?php
#
# @copyright Copyright (c) 2017 Green Seed Technologies, Inc. All rights reserved.
#
# @author Dave Hoover
# @author David McLaughlin <david@greenseedtech.com>
#

class letter {

	// public
	function __construct($args = NULL) {
		if ($args['letterid']) {
			$query = "SELECT * FROM letter WHERE letterid='" . $_SESSION['data_access']->db_quote($args['letterid']) . "'";
		}
		else if ($args['title']) {
			$query = "SELECT * FROM letter WHERE title like '%" . $_SESSION['data_access']->db_quote($args['title']) . "%'";
		}
		if (isset($query)) {
			list($results, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
		}
		if ($num_rows) {
			$this->initialize($results[0]);
		}
		else {
			unset($args['letterid']);
			$this->initialize($args);
		}
	}

	function initialize($args = NULL) {
		if ($args) {
			foreach ($args as $key=>$value) {
				$method_name = 'set_' . $key;
				if (method_exists($this, $method_name)) {
					$this->{$method_name}($value);
				}
				else if (isset($args[$key])) {
					$this->{$key} = $value;
				}
			}
		}
	}

	function delete() {
		$query = "DELETE FROM letter WHERE letterid='" . $_SESSION['data_access']->db_quote($this->get_letterid()) . "'";
		$_SESSION['data_access']->query( array('query'=>$query) );
	}

	function merge_segments($args = NULL) {
		$return_array = array();
		$o_account = $args['o_account'];
		$email_segments = array('bcc_list', 'body', 'cc_list', 'from_email', 'subject');

    foreach ($email_segments as $email_segment) {
    	$function_name = 'get_' . $email_segment;
    	${$email_segment} = $o_account->merge_info( array('string'=>$this->$function_name()) );
    	${$email_segment} = $o_account->o_company->merge_info( array('key_modifier'=>'company_', 'string'=>${$email_segment}) );
    	
    	// handle the special segments
    	// secure_card_number
    	if (preg_match("/\{secure_card_number\}/", ${$email_segment})) {
    		${$email_segment} = preg_replace("/\{secure_card_number\}/", $o_account->get_card_number( array('account_type'=>$o_account->get_account_type()) ), ${$email_segment});
    	}
    	// next_bill whatever
    	if (preg_match("/\{next_bill/", ${$email_segment})) {
				if ($o_account->get_billing_status() == 'queued') {
					${$email_segment} = preg_replace("/\{next_bill_day\},?/", '', ${$email_segment});
					${$email_segment} = preg_replace("/\{next_bill_month\}/", 'None', ${$email_segment});
					${$email_segment} = preg_replace("/\{next_bill_year\}/", '', ${$email_segment});
				}
				else {
					${$email_segment} = preg_replace("/\{next_bill_day\}/", date('jS', strtotime($o_account->get_next_bill_date())), ${$email_segment});
					${$email_segment} = preg_replace("/\{next_bill_month\}/", date('F', strtotime($o_account->get_next_bill_date())), ${$email_segment});
					${$email_segment} = preg_replace("/\{next_bill_year\}/", date('Y', strtotime($o_account->get_next_bill_date())), ${$email_segment});
				}
    	}
    	// welcome_letter_pricing
    	if (preg_match("/\{welcome_letter_pricing\}/", ${$email_segment})) {
    		$welcome_letter_pricing = $o_account->get_subscription_type() . ' pricing: \$' . $o_account->get_rate() . '.00 for ' . count($o_account->get_list_area_names()) . ' area';
    		if (count($o_account->get_list_area_names()) > 1) {
    			$welcome_letter_pricing .= 's';
    		}
    		$welcome_letter_pricing .= "\n" . '(Add an additional area for only \$';
    		if ($o_account->get_subscription_type() == 'Monthly') {
    			if (count($o_account->get_list_area_names()) == 1) {
    				$welcome_letter_pricing .= ($o_account->o_company->get_month_two_area_pricing() - $o_account->o_company->get_month_one_area_pricing());
    			}
    			else {
    				$welcome_letter_pricing .= $o_account->o_company->get_month_additional_area_pricing();
    			}
    			$welcome_letter_pricing .= '/month!)';
    		}
    		else if ($o_account->get_subscription_type() == 'Annual') {
    			if (count($o_account->get_list_area_names()) == 1) {
    				$welcome_letter_pricing .= (($o_account->o_company->get_year_two_area_pricing() - $o_account->o_company->get_year_one_area_pricing())*12);
    			}
    			else {
    				$welcome_letter_pricing .= ($o_account->o_company->get_year_additional_area_pricing()*12);
    			}
    			$welcome_letter_pricing .= '/year!)';
    		}
    		${$email_segment} = preg_replace("/\{welcome_letter_pricing\}/", $welcome_letter_pricing, ${$email_segment});
    	}
			// next_bill_date_string
			if (preg_match("/\{next_bill_date_string\}/", ${$email_segment})) {
				if ($o_account->get_subscription_type() == 'Monthly') {
					${$email_segment} = preg_replace("/\{next_bill_date_string\}/", 'the ' . date('jS', strtotime($o_account->get_next_bill_date())) . ' day of each month', ${$email_segment});
				}
				else if ($o_account->get_subscription_type() == 'Annual') {
					${$email_segment} = preg_replace("/\{next_bill_date_string\}/", date('F', strtotime($o_account->get_next_bill_date())) . ' ' . date('jS', strtotime($o_account->get_next_bill_date())) . ' of each year', ${$email_segment});
				}
			}
    	// subscription_areas
    	if (preg_match("/\{subscription_areas\}/", ${$email_segment})) {
    		$subscription_areas = '';
    		foreach ($o_account->get_list_area_names() as $area_name) {
    			$subscription_areas .= $area_name . "\n";
    		}
    		${$email_segment} = preg_replace("/\{subscription_areas\}/", $subscription_areas, ${$email_segment});
    	}
    	// plural_areas
    	if (preg_match("/\{plural_areas\}/", ${$email_segment})) {
    		if (count($o_account->get_list_area_names()) > 1) {
    			${$email_segment} = preg_replace("/\{plural_areas\}/", "s", ${$email_segment});
    		}
    	}
    	// monthly_pricing_equivalent
    	if (preg_match("/\{monthly_pricing_equivalent\}/", ${$email_segment})) {
    		if (count($o_account->get_list_area_names()) == 1) {
    			${$email_segment} = preg_replace("/\{monthly_pricing_equivalent\}/", $o_account->o_company->get_month_one_area_pricing(), ${$email_segment});
    		}
    		else if (count($o_account->get_list_area_names()) == 2) {
    			${$email_segment} = preg_replace("/\{monthly_pricing_equivalent\}/", $o_account->o_company->get_month_two_area_pricing(), ${$email_segment});
    		}
    		else {
    			$monthly_pricing_equivalent = $o_account->o_company->get_month_two_area_pricing() + ((count($o_account->get_list_area_names()) - 2) * $o_account->o_company->get_month_additional_area_pricing());
    			${$email_segment} = preg_replace("/\{monthly_pricing_equivalent\}/", $monthly_pricing_equivalent, ${$email_segment});
    		}
    	}
    	// most_recent_charge
    	if (preg_match("/\{most_recent_charge\}/", ${$email_segment})) {
    		require_once('office/account/class.billing.php');
    		$o_billing_utilities = new billing_utilities();
    		$most_recent_charge = $o_billing_utilities->get_most_recent_charge( array('where_plus'=>" AND accountid='" . $o_account->get_accountid() . "' ") );
    		${$email_segment} = preg_replace("/\{most_recent_charge\}/", $most_recent_charge, ${$email_segment});
    	}
    	// number_of_areas
    	if (preg_match("/\{number_of_areas\}/", ${$email_segment})) {
    		${$email_segment} = preg_replace("/\{number_of_areas\}/", count($o_account->get_list_area_names()), ${$email_segment});
    	}
    	// today_ whatever
    	if (preg_match("/\{today_/", ${$email_segment})) {
    		${$email_segment} = preg_replace("/\{today_day\}/", date('jS'), ${$email_segment});
    		${$email_segment} = preg_replace("/\{today_month\}/", date('F'), ${$email_segment});
    		${$email_segment} = preg_replace("/\{today_year\}/", date('Y'), ${$email_segment});
    		${$email_segment} = preg_replace("/\{today_date\}/", date('F j, Y'), ${$email_segment});
    	}
    	// csv_attachment
    	if (preg_match("/\{csv_attachment\}/", ${$email_segment})) {
    		if ($o_account->get_email_attachment()) {
    			${$email_segment} = preg_replace("/\{csv_attachment\}/", ' (with comma delimited (.csv) file attached)', ${$email_segment});
    		}
    	}
    	// start_end_day_difference
    	if (preg_match("/\{start_end_day_difference\}/", ${$email_segment})) {
    		$start_end_day_difference = floor((strtotime($o_account->get_end_date() . ' 13:00:00') - strtotime($o_account->get_start_date() . ' 12:00:00'))/(60*60*24));
    		${$email_segment} = preg_replace("/\{start_end_day_difference\}/", $start_end_day_difference, ${$email_segment});
    	}

    	// remove any additional merge fields
    	${$email_segment} = preg_replace("/\{.+?\}/", "", ${$email_segment});
			$return_array[$email_segment] = ${$email_segment};
    }
		return $return_array;
	}
	
	function save() {
		if ($this->get_letterid()) {
			$query = "UPDATE letter SET " .
				"bcc_list='" . $_SESSION['data_access']->db_quote($this->get_bcc_list()) . "', " .
				"body='" . $_SESSION['data_access']->db_quote($this->get_body()) . "', " .
				"cc_list='" . $_SESSION['data_access']->db_quote($this->get_cc_list()) . "', " .
				"created='" . $_SESSION['data_access']->db_quote($this->get_created()) . "', " .
				"from_email='" . $_SESSION['data_access']->db_quote($this->get_from_email()) . "', " .
				"subject='" . $_SESSION['data_access']->db_quote($this->get_subject()) . "', " .
				"title='" . $_SESSION['data_access']->db_quote($this->get_title()) . "' " .
				"WHERE letterid=" . $_SESSION['data_access']->db_quote($this->get_letterid())
			;
		}
		else {
			$query = "INSERT INTO letter (bcc_list,body,cc_list,created,from_email,subject,title) " . 
				"VALUES (" .
					"'" . $_SESSION['data_access']->db_quote($this->get_bcc_list()) . "'," .
					"'" . $_SESSION['data_access']->db_quote($this->get_body()) . "'," .
					"'" . $_SESSION['data_access']->db_quote($this->get_cc_list()) . "'," .
					"NOW()," .
					"'" . $_SESSION['data_access']->db_quote($this->get_from_email()) . "'," .
					"'" . $_SESSION['data_access']->db_quote($this->get_subject()) . "'," .
					"'" . $_SESSION['data_access']->db_quote($this->get_title()) . "'" .
				")"
			;
		}
		$this->set_letterid($_SESSION['data_access']->query( array('query'=>$query) ));
	}

	// generic get and set
	function get_letterid() { return $this->letterid; }
	function get_bcc_list() { return $this->bcc_list; }
	function get_body() { return $this->body; }
	function get_cc_list() { return $this->cc_list; }
	function get_created() { return $this->created; }
	function get_from_email() { return $this->from_email; }
	function get_subject() { return $this->subject; }
	function get_title() { return $this->title; }

	function set_letterid($value = NULL) { $this->letterid= ($value) ? $value : $this->letterid; }

	// private
	private $letterid;
	private $bcc_list;
	private $body;
	private $cc_list;
	private $created;
	private $from_email;
	private $subject;
	private $title;

}

class letter_utilities {

	// public
	function __construct($args = NULL) {
		$this->initialize($args);
	}

	function initialize($args = NULL) {
		$this->set_order_by($args['order_by']);
	}

	function get_list($args = NULL) {
		$args['where_plus'] = preg_replace("/^ AND /", " WHERE ", $args['where_plus']);
		$query = "SELECT * FROM letter " . $args['where_plus'] . " ORDER BY " . $this->get_order_by();
		try {
			list($results, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
			$list = array(); 
			foreach ($results as $row) {
				$o_letter = new letter();
				$o_letter->initialize($row);
				$list[] = $o_letter;
			}
			return $list;
		}
		catch (Exception $exception) {
			throw $exception;
		}
	}

	// generic get and set
	function get_order_by() { return $this->order_by; }

	function set_order_by($value = NULL) { $this->order_by = ($value) ? $value : 'title'; }

	// private
	private $order_by;

}

?>

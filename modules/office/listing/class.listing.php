<?php
#
# @copyright Copyright (c) 2017 Green Seed Technologies, Inc. All rights reserved.
#
# @author Dave Hoover
# @author David McLaughlin <david@greenseedtech.com>
#

require_once('office/account/class.account.php');
require_once('office/account/class.area_name.php');

class listing {

	// const
	const MESSAGE_DELETED = 'The Listing has been deleted.';
	const MESSAGE_SAVED = 'The Listing has been saved.';
		
	// public
	function __construct($args = NULL) {
		if ($args['listingid']) {
			$query = "SELECT * FROM listing WHERE listingid='" . $_SESSION['data_access']->db_quote($args['listingid']) . "'";
		}
		if (isset($query)) {
			try {
				list($results, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
			}
			catch (Exception $exception) {
				throw $exception;
			}
		}
		if ($num_rows) {
			$this->initialize($results[0]);
		}
		else {
			$this->initialize($args);
		}
	}
	
	function initialize($args = NULL) {
		if ($args) {
			foreach ($args as $key=>$value) {
				$method_name = 'set_' . $key;
				if (method_exists($this, $method_name)) {
					$this->{$method_name}($value);
				}
				else if (isset($args[$key])) {
					$this->{$key} = $value;
				}
			}
		}
		$this->set_order_by($args['order_by']);
	}

	function delete() {
		$query = "DELETE FROM listing WHERE listingid='" . $_SESSION['data_access']->db_quote($this->get_listingid()) . "'";
		try {
			$_SESSION['data_access']->query( array('query'=>$query) );
			return listing::MESSAGE_DELETED;
		}
		catch (Exception $exception) {
			throw $exception;
		}
	}
	
	function exists($args = NULL) {
		if ($args['refindkey']) {
			$where = " WHERE refindkey='" . $_SESSION['data_access']->db_quote($args['refindkey']) . "' ";
		}
		if (isset($where)) {
			$query = "SELECT listingid FROM listing " . $where;
			try {
				list($results, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
			}
			catch (Exception $exception) {
				throw $exception;
			}
		}
		if ($num_rows) {
			return TRUE;
		}
		return FALSE;
	}
	
	function get_next_or_previous_listingid($args = NULL) {
		if ($args['query']) {
			if ($args['type'] == 'next') {
				$position = $args['position']+1;
			}
			else {
				$position = $args['position']-1;
			}
			if ($position >= 0) {
				$query = $args['query'] . " LIMIT " . $position . ",1";
				try {
					list($results, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
					return $results[0]['listingid'];
				}
				catch (Exception $exception) {
					throw $exception;
				}
			}
		}
		return 0;
	}
	
	function get_table_fields() {
		$query = "desc listing";
		list($results, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
		$temp_array = array();
		foreach ($results as $result) {
			$temp_array[] = $result['Field'];
		}
		sort($temp_array);
		return $temp_array;
	}
	
	function processed() {
		$query = "UPDATE listing set processed=NOW() WHERE listingid='" . $_SESSION['data_access']->db_quote($this->get_listingid()) . "'";
		try {
			$_SESSION['data_access']->query( array('query'=>$query) );
		}
		catch (Exception $exception) {
			throw $exception;
		}
	}
	
	function save() {
		if ($this->get_listingid()) {
			$query = "UPDATE listing SET " .
				"lead_id='" . $_SESSION['data_access']->db_quote($this->get_lead_id()) . "', " .
				"firstextractiondate='" . $_SESSION['data_access']->db_quote($this->get_firstextractiondate()) . "', " .
				"refindkey='" . $_SESSION['data_access']->db_quote($this->get_refindkey()) . "', " . 
				"refind_value='" . $_SESSION['data_access']->db_quote($this->get_refind_value()) . "', " . 
				"robot_name='" . $_SESSION['data_access']->db_quote($this->get_robot_name()) . "', " . 
				"robot_nametemp='" . $_SESSION['data_access']->db_quote($this->get_robot_nametemp()) . "', " . 
				"date_posted='" . $_SESSION['data_access']->db_quote($this->get_date_posted()) . "', " . 
				"data_source_name='" . $_SESSION['data_access']->db_quote($this->get_data_source_name()) . "', " . 
				"data_source_namepublish='" . $_SESSION['data_access']->db_quote($this->get_data_source_namepublish()) . "', " . 
				"data_source_type='" . $_SESSION['data_access']->db_quote($this->get_data_source_type()) . "', " . 
				"default_area_code='" . $_SESSION['data_access']->db_quote($this->get_default_area_code()) . "', " . 
				"area_county='" . $_SESSION['data_access']->db_quote($this->get_area_county()) . "', " . 
				"logo='" . $_SESSION['data_access']->db_quote($this->get_logo()) . "', " . 
				"logo_url='" . $_SESSION['data_access']->db_quote($this->get_logo_url()) . "', " . 
				"logo_classification='" . $_SESSION['data_access']->db_quote($this->get_logo_classification()) . "', " . 
				"pt_review='" . $_SESSION['data_access']->db_quote($this->get_pt_review()) . "', " . 
				"property_type='" . $_SESSION['data_access']->db_quote($this->get_property_type()) . "', " . 
				"fsbo_classification='" . $_SESSION['data_access']->db_quote($this->get_fsbo_classification()) . "', " . 
				"keyword_review='" . $_SESSION['data_access']->db_quote($this->get_keyword_review()) . "', " . 
				"area_st='" . $_SESSION['data_access']->db_quote($this->get_area_st()) . "', " . 
				"area_name='" . $_SESSION['data_access']->db_quote($this->get_area_name()) . "', " . 
				"area_misc='" . $_SESSION['data_access']->db_quote($this->get_area_misc()) . "', " .
				"scrub_edit='" . $_SESSION['data_access']->db_quote($this->get_scrub_edit()) . "', " . 
				"description_ad='" . $_SESSION['data_access']->db_quote($this->get_description_ad()) . "', " . 
				"ad_url='" . $_SESSION['data_access']->db_quote($this->get_ad_url()) . "', " . 
				"contact_email='" . $_SESSION['data_access']->db_quote($this->get_contact_email()) . "', " . 
				"contact_phone1='" . $_SESSION['data_access']->db_quote($this->get_contact_phone1()) . "', " . 
				"scrub_edit1='" . $_SESSION['data_access']->db_quote($this->get_scrub_edit1()) . "', " . 
				"contact_name1='" . $_SESSION['data_access']->db_quote($this->get_contact_name1()) . "', " . 
				"contact_street1='" . $_SESSION['data_access']->db_quote($this->get_contact_street1()) . "', " . 
				"contact_city1='" . $_SESSION['data_access']->db_quote($this->get_contact_city1()) . "', " . 
				"contact_st1='" . $_SESSION['data_access']->db_quote($this->get_contact_st1()) . "', " . 
				"contact_zip1='" . $_SESSION['data_access']->db_quote($this->get_contact_zip1()) . "', " . 
				"contact_biz='" . $_SESSION['data_access']->db_quote($this->get_contact_biz()) . "', " . 
				"contact_bizname='" . $_SESSION['data_access']->db_quote($this->get_contact_bizname()) . "', " . 
				"scrub_edit2='" . $_SESSION['data_access']->db_quote($this->get_scrub_edit2()) . "', " . 
				"contact_phone2='" . $_SESSION['data_access']->db_quote($this->get_contact_phone2()) . "', " . 
				"phone1_lookupsource='" . $_SESSION['data_access']->db_quote($this->get_phone1_lookupsource()) . "', " .
				"phone2_lookupsource='" . $_SESSION['data_access']->db_quote($this->get_phone2_lookupsource()) . "', " . 
				"contact_name2='" . $_SESSION['data_access']->db_quote($this->get_contact_name2()) . "', " . 
				"contact_street2='" . $_SESSION['data_access']->db_quote($this->get_contact_street2()) . "', " . 
				"contact_city2='" . $_SESSION['data_access']->db_quote($this->get_contact_city2()) . "', " . 
				"contact_st2='" . $_SESSION['data_access']->db_quote($this->get_contact_st2()) . "', " . 
				"contact_zip2='" . $_SESSION['data_access']->db_quote($this->get_contact_zip2()) . "', " . 
				"property_street='" . $_SESSION['data_access']->db_quote($this->get_property_street()) . "', " . 
				"property_street2='" . $_SESSION['data_access']->db_quote($this->get_property_street2()) . "', " . 
				"property_city='" . $_SESSION['data_access']->db_quote($this->get_property_city()) . "', " . 
				"property_st='" . $_SESSION['data_access']->db_quote($this->get_property_st()) . "', " . 
				"property_zip='" . $_SESSION['data_access']->db_quote($this->get_property_zip()) . "', " . 
				"square_feet='" . $_SESSION['data_access']->db_quote($this->get_square_feet()) . "', " . 
				"bedrooms='" . $_SESSION['data_access']->db_quote($this->get_bedrooms()) . "', " . 
				"bathrooms='" . $_SESSION['data_access']->db_quote($this->get_bathrooms()) . "', " . 
				"lot_size='" . $_SESSION['data_access']->db_quote($this->get_lot_size()) . "', " . 
				"year_built='" . $_SESSION['data_access']->db_quote($this->get_year_built()) . "', " . 
				"garage='" . $_SESSION['data_access']->db_quote($this->get_garage()) . "', " . 
				"price='" . $_SESSION['data_access']->db_quote($this->get_price()) . "', " . 
				"phone1_dnc='" . $_SESSION['data_access']->db_quote($this->get_phone1_dnc()) . "', " . 
				"phone1_prefixfinder='" . $_SESSION['data_access']->db_quote($this->get_phone1_prefixfinder()) . "', " . 
				"phone1_pf_areacode='" . $_SESSION['data_access']->db_quote($this->get_phone1_pf_areacode()) . "', " . 
				"phone1_pf_prefix='" . $_SESSION['data_access']->db_quote($this->get_phone1_pf_prefix()) . "', " . 
				"phone1_pf_city='" . $_SESSION['data_access']->db_quote($this->get_phone1_pf_city()) . "', " . 
				"phone1_pf_st='" . $_SESSION['data_access']->db_quote($this->get_phone1_pf_st()) . "', " . 
				"phone1_pf_county='" . $_SESSION['data_access']->db_quote($this->get_phone1_pf_county()) . "', " . 
				"phone1_pf_county2='" . $_SESSION['data_access']->db_quote($this->get_phone1_pf_county2()) . "', " . 
				"phone1_pf_county3='" . $_SESSION['data_access']->db_quote($this->get_phone1_pf_county3()) . "', " . 
				"phone1_pf_county4='" . $_SESSION['data_access']->db_quote($this->get_phone1_pf_county4()) . "', " . 
				"phone1_pf_msa='" . $_SESSION['data_access']->db_quote($this->get_phone1_pf_msa()) . "', " . 
				"phone2_dnc='" . $_SESSION['data_access']->db_quote($this->get_phone2_dnc()) . "', " . 
				"phone2_prefixfinder='" . $_SESSION['data_access']->db_quote($this->get_phone2_prefixfinder()) . "', " . 
				"phone2_pf_areacode='" . $_SESSION['data_access']->db_quote($this->get_phone2_pf_areacode()) . "', " . 
				"phone2_pf_prefix='" . $_SESSION['data_access']->db_quote($this->get_phone2_pf_prefix()) . "', " . 
				"phone2_pf_city='" . $_SESSION['data_access']->db_quote($this->get_phone2_pf_city()) . "', " . 
				"phone2_pf_st='" . $_SESSION['data_access']->db_quote($this->get_phone2_pf_st()) . "', " . 
				"phone2_pf_county='" . $_SESSION['data_access']->db_quote($this->get_phone2_pf_county()) . "', " . 
				"phone2_pf_county2='" . $_SESSION['data_access']->db_quote($this->get_phone2_pf_county2()) . "', " . 
				"phone2_pf_county3='" . $_SESSION['data_access']->db_quote($this->get_phone2_pf_county3()) . "', " . 
				"phone2_pf_county4='" . $_SESSION['data_access']->db_quote($this->get_phone2_pf_county4()) . "', " . 
				"phone2_pf_msa='" . $_SESSION['data_access']->db_quote($this->get_phone2_pf_msa()) . "', " . 
				"dup_phoneinfo='" . $_SESSION['data_access']->db_quote($this->get_dup_phoneinfo()) . "', " . 
				"misc1='" . $_SESSION['data_access']->db_quote($this->get_misc1()) . "', " .
				"zip_approximate='" . $_SESSION['data_access']->db_quote($this->get_zip_approximate()) . "', " .
				"zip_confidence='" . $_SESSION['data_access']->db_quote($this->get_zip_confidence()) . "', " .
				"latitude='" . $_SESSION['data_access']->db_quote($this->get_latitude()) . "', " .
				"longitude='" . $_SESSION['data_access']->db_quote($this->get_longitude()) . "', " .
				"map_url='" . $_SESSION['data_access']->db_quote($this->get_map_url()) . "', " .
				"language='" . $_SESSION['data_access']->db_quote($this->get_language()) . "', " .
				"translate='" . $_SESSION['data_access']->db_quote($this->get_translate()) . "', " .
				"date_ad='" . $_SESSION['data_access']->db_quote($this->get_date_ad()) . "', " .
				"product='" . $_SESSION['data_access']->db_quote($this->get_product()) . "', " .  
				"status='" . $_SESSION['data_access']->db_quote($this->get_status()) . "' " .  
				"WHERE listingid=" . $_SESSION['data_access']->db_quote($this->get_listingid())
			;
		}
		else {
			$query = "INSERT INTO listing (listingid, lead_id, firstextractiondate, refindkey, refind_value, robot_name, robot_nametemp, date_posted, data_source_name, data_source_namepublish, data_source_type, default_area_code, area_county, logo, logo_url, logo_classification, pt_review, property_type, fsbo_classification, keyword_review, area_st, area_name, area_misc, scrub_edit, description_ad, ad_url, contact_email, contact_phone1, scrub_edit1, contact_name1, contact_street1, contact_city1, contact_st1, contact_zip1, contact_biz, contact_bizname, scrub_edit2, contact_phone2, phone1_lookupsource, phone2_lookupsource, contact_name2, contact_street2, contact_city2, contact_st2, contact_zip2, property_street, property_street2, property_city, property_st, property_zip, square_feet, bedrooms, bathrooms, lot_size, year_built, garage, price, phone1_dnc, phone1_prefixfinder, phone1_pf_areacode, phone1_pf_prefix, phone1_pf_city, phone1_pf_st, phone1_pf_county, phone1_pf_county2, phone1_pf_county3, phone1_pf_county4, phone1_pf_msa, phone2_dnc, phone2_prefixfinder, phone2_pf_areacode, phone2_pf_prefix, phone2_pf_city, phone2_pf_st, phone2_pf_county, phone2_pf_county2, phone2_pf_county3, phone2_pf_county4, phone2_pf_msa, dup_phoneinfo, misc1, created, zip_approximate, zip_confidence, latitude, longitude, map_url, language, translate, date_ad, product, status) VALUES ('', '" . $_SESSION['data_access']->db_quote($this->get_lead_id()) . "', '" . $_SESSION['data_access']->db_quote($this->get_firstextractiondate()) . "', '" . $_SESSION['data_access']->db_quote($this->get_refindkey()) . "', '" . $_SESSION['data_access']->db_quote($this->get_refind_value()) . "', '" . $_SESSION['data_access']->db_quote($this->get_robot_name()) . "', '" . $_SESSION['data_access']->db_quote($this->get_robot_nametemp()) . "', '" . $_SESSION['data_access']->db_quote($this->get_date_posted()) . "', '" . $_SESSION['data_access']->db_quote($this->get_data_source_name()) . "', '" . $_SESSION['data_access']->db_quote($this->get_data_source_namepublish()) . "', '" . $_SESSION['data_access']->db_quote($this->get_data_source_type()) . "', '" . $_SESSION['data_access']->db_quote($this->get_default_area_code()) . "', '" . $_SESSION['data_access']->db_quote($this->get_area_county()) . "', '" . $_SESSION['data_access']->db_quote($this->get_logo()) . "', '" . $_SESSION['data_access']->db_quote($this->get_logo_url()) . "', '" . $_SESSION['data_access']->db_quote($this->get_logo_classification()) . "', '" . $_SESSION['data_access']->db_quote($this->get_pt_review()) . "', '" . $_SESSION['data_access']->db_quote($this->get_property_type()) . "', '" . $_SESSION['data_access']->db_quote($this->get_fsbo_classification()) . "', '" . $_SESSION['data_access']->db_quote($this->get_keyword_review()) . "', '" . $_SESSION['data_access']->db_quote($this->get_area_st()) . "', '" . $_SESSION['data_access']->db_quote($this->get_area_name()) . "', '" . $_SESSION['data_access']->db_quote($this->get_area_misc()) . "', '" . $_SESSION['data_access']->db_quote($this->get_scrub_edit()) . "', '" . $_SESSION['data_access']->db_quote($this->get_description_ad()) . "', '" . $_SESSION['data_access']->db_quote($this->get_ad_url()) . "', '" . $_SESSION['data_access']->db_quote($this->get_contact_email()) . "', '" . $_SESSION['data_access']->db_quote($this->get_contact_phone1()) . "', '" . $_SESSION['data_access']->db_quote($this->get_scrub_edit1()) . "', '" . $_SESSION['data_access']->db_quote($this->get_contact_name1()) . "', '" . $_SESSION['data_access']->db_quote($this->get_contact_street1()) . "', '" . $_SESSION['data_access']->db_quote($this->get_contact_city1()) . "', '" . $_SESSION['data_access']->db_quote($this->get_contact_st1()) . "', '" . $_SESSION['data_access']->db_quote($this->get_contact_zip1()) . "', '" . $_SESSION['data_access']->db_quote($this->get_contact_biz()) . "', '" . $_SESSION['data_access']->db_quote($this->get_contact_bizname()) . "', '" . $_SESSION['data_access']->db_quote($this->get_scrub_edit2()) . "', '" . $_SESSION['data_access']->db_quote($this->get_contact_phone2()) . "', '" . $_SESSION['data_access']->db_quote($this->get_phone1_lookupsource()) . "', '" . $_SESSION['data_access']->db_quote($this->get_phone2_lookupsource()) . "', '" . $_SESSION['data_access']->db_quote($this->get_contact_name2()) . "', '" . $_SESSION['data_access']->db_quote($this->get_contact_street2()) . "', '" . $_SESSION['data_access']->db_quote($this->get_contact_city2()) . "', '" . $_SESSION['data_access']->db_quote($this->get_contact_st2()) . "', '" . $_SESSION['data_access']->db_quote($this->get_contact_zip2()) . "', '" . $_SESSION['data_access']->db_quote($this->get_property_street()) . "', '" . $_SESSION['data_access']->db_quote($this->get_property_street2()) . "', '" . $_SESSION['data_access']->db_quote($this->get_property_city()) . "', '" . $_SESSION['data_access']->db_quote($this->get_property_st()) . "', '" . $_SESSION['data_access']->db_quote($this->get_property_zip()) . "', '" . $_SESSION['data_access']->db_quote($this->get_square_feet()) . "', '" . $_SESSION['data_access']->db_quote($this->get_bedrooms()) . "', '" . $_SESSION['data_access']->db_quote($this->get_bathrooms()) . "', '" . $_SESSION['data_access']->db_quote($this->get_lot_size()) . "', '" . $_SESSION['data_access']->db_quote($this->get_year_built()) . "', '" . $_SESSION['data_access']->db_quote($this->get_garage()) . "', '" . $_SESSION['data_access']->db_quote($this->get_price()) . "', '" . $_SESSION['data_access']->db_quote($this->get_phone1_dnc()) . "', '" . $_SESSION['data_access']->db_quote($this->get_phone1_prefixfinder()) . "', '" . $_SESSION['data_access']->db_quote($this->get_phone1_pf_areacode()) . "', '" . $_SESSION['data_access']->db_quote($this->get_phone1_pf_prefix()) . "', '" . $_SESSION['data_access']->db_quote($this->get_phone1_pf_city()) . "', '" . $_SESSION['data_access']->db_quote($this->get_phone1_pf_st()) . "', '" . $_SESSION['data_access']->db_quote($this->get_phone1_pf_county()) . "', '" . $_SESSION['data_access']->db_quote($this->get_phone1_pf_county2()) . "', '" . $_SESSION['data_access']->db_quote($this->get_phone1_pf_county3()) . "', '" . $_SESSION['data_access']->db_quote($this->get_phone1_pf_county4()) . "', '" . $_SESSION['data_access']->db_quote($this->get_phone1_pf_msa()) . "', '" . $_SESSION['data_access']->db_quote($this->get_phone2_dnc()) . "', '" . $_SESSION['data_access']->db_quote($this->get_phone2_prefixfinder()) . "', '" . $_SESSION['data_access']->db_quote($this->get_phone2_pf_areacode()) . "', '" . $_SESSION['data_access']->db_quote($this->get_phone2_pf_prefix()) . "', '" . $_SESSION['data_access']->db_quote($this->get_phone2_pf_city()) . "', '" . $_SESSION['data_access']->db_quote($this->get_phone2_pf_st()) . "', '" . $_SESSION['data_access']->db_quote($this->get_phone2_pf_county()) . "', '" . $_SESSION['data_access']->db_quote($this->get_phone2_pf_county2()) . "', '" . $_SESSION['data_access']->db_quote($this->get_phone2_pf_county3()) . "', '" . $_SESSION['data_access']->db_quote($this->get_phone2_pf_county4()) . "', '" . $_SESSION['data_access']->db_quote($this->get_phone2_pf_msa()) . "', '" . $_SESSION['data_access']->db_quote($this->get_dup_phoneinfo()) . "', '" . $_SESSION['data_access']->db_quote($this->get_misc1()) . "', NOW(), '" . $_SESSION['data_access']->db_quote($this->get_zip_approximate()) . "', '" . $_SESSION['data_access']->db_quote($this->get_zip_confidence()) . "', '" . $_SESSION['data_access']->db_quote($this->get_latitude()) . "', '" . $_SESSION['data_access']->db_quote($this->get_longitude()) . "', '" . $_SESSION['data_access']->db_quote($this->get_map_url()) . "', '" . $_SESSION['data_access']->db_quote($this->get_language()) . "', '" . $_SESSION['data_access']->db_quote($this->get_translate()) . "', '" . $_SESSION['data_access']->db_quote($this->get_date_ad()) . "', '" . $_SESSION['data_access']->db_quote($this->get_product()) . "', '" . $_SESSION['data_access']->db_quote($this->get_status()) . "')"; 
		}
		try {
			$this->set_listingid($_SESSION['data_access']->query( array('query'=>$query) ));
			return listing::MESSAGE_SAVED;
		}
		catch (Exception $exception) {
			throw $exception;
		}
	}
	
	// get and set
	function get_listingid() { return $this->listingid; }
	function get_lead_id() { return $this->lead_id; }
	function get_firstextractiondate() { return $this->firstextractiondate; }
	function get_refindkey() { return $this->refindkey; }
	function get_refind_value() { return $this->refind_value; }
	function get_robot_name() { return $this->robot_name; }
	function get_robot_nametemp() { return $this->robot_nametemp; }
	function get_date_posted() { return $this->date_posted; }
	function get_data_source_name() { return $this->data_source_name; }
	function get_data_source_namepublish() { return $this->data_source_namepublish; }
	function get_data_source_type() { return $this->data_source_type; }
	function get_default_area_code() { return $this->default_area_code; }
	function get_area_county() { return $this->area_county; }
	function get_logo() { return $this->logo; }
	function get_logo_url() { return $this->logo_url; }
	function get_logo_classification() { return $this->logo_classification; }
	function get_pt_review() { return $this->pt_review; }
	function get_property_type() { return $this->property_type; }
	function get_fsbo_classification() { return $this->fsbo_classification; }
	function get_keyword_review() { return $this->keyword_review; }
	function get_area_st() { return $this->area_st; }
	function get_area_name() { return $this->area_name; }
	function get_area_misc() { return $this->area_misc; }
	function get_scrub_edit() { return $this->scrub_edit; }
	function get_description_ad() { return $this->description_ad; }
	function get_ad_url() { return $this->ad_url; }
	function get_contact_email() { return $this->contact_email; }
	function get_contact_phone1() { return $this->contact_phone1; }
	function get_scrub_edit1() { return $this->scrub_edit1; }
	function get_contact_name1() { return $this->contact_name1; }
	function get_contact_street1() { return $this->contact_street1; }
	function get_contact_city1() { return $this->contact_city1; }
	function get_contact_st1() { return $this->contact_st1; }
	function get_contact_zip1() { return $this->contact_zip1; }
	function get_contact_biz() { return $this->contact_biz; }
	function get_contact_bizname() { return $this->contact_bizname; }
	function get_scrub_edit2() { return $this->scrub_edit2; }
	function get_contact_phone2() { return $this->contact_phone2; }
	function get_phone1_lookupsource() { return $this->phone1_lookupsource; }
	function get_phone2_lookupsource() { return $this->phone2_lookupsource; }
	function get_contact_name2() { return $this->contact_name2; }
	function get_contact_street2() { return $this->contact_street2; }
	function get_contact_city2() { return $this->contact_city2; }
	function get_contact_st2() { return $this->contact_st2; }
	function get_contact_zip2() { return $this->contact_zip2; }
	function get_property_street() { return $this->property_street; }
	function get_property_street2() { return $this->property_street2; }
	function get_property_city() { return $this->property_city; }
	function get_property_st() { return $this->property_st; }
	function get_property_zip() { return $this->property_zip; }
	function get_square_feet() { return $this->square_feet; }
	function get_bedrooms() { return $this->bedrooms; }
	function get_bathrooms() { return $this->bathrooms; }
	function get_lot_size() { return $this->lot_size; }
	function get_year_built() { return $this->year_built; }
	function get_garage() { return $this->garage; }
	function get_price() { return $this->price; }
	function get_phone1_dnc() { return $this->phone1_dnc; }
	function get_phone1_prefixfinder() { return $this->phone1_prefixfinder; }
	function get_phone1_pf_areacode() { return $this->phone1_pf_areacode; }
	function get_phone1_pf_prefix() { return $this->phone1_pf_prefix; }
	function get_phone1_pf_city() { return $this->phone1_pf_city; }
	function get_phone1_pf_st() { return $this->phone1_pf_st; }
	function get_phone1_pf_county() { return $this->phone1_pf_county; }
	function get_phone1_pf_county2() { return $this->phone1_pf_county2; }
	function get_phone1_pf_county3() { return $this->phone1_pf_county3; }
	function get_phone1_pf_county4() { return $this->phone1_pf_county4; }
	function get_phone1_pf_msa() { return $this->phone1_pf_msa; }
	function get_phone2_dnc() { return $this->phone2_dnc; }
	function get_phone2_prefixfinder() { return $this->phone2_prefixfinder; }
	function get_phone2_pf_areacode() { return $this->phone2_pf_areacode; }
	function get_phone2_pf_prefix() { return $this->phone2_pf_prefix; }
	function get_phone2_pf_city() { return $this->phone2_pf_city; }
	function get_phone2_pf_st() { return $this->phone2_pf_st; }
	function get_phone2_pf_county() { return $this->phone2_pf_county; }
	function get_phone2_pf_county2() { return $this->phone2_pf_county2; }
	function get_phone2_pf_county3() { return $this->phone2_pf_county3; }
	function get_phone2_pf_county4() { return $this->phone2_pf_county4; }
	function get_phone2_pf_msa() { return $this->phone2_pf_msa; }
	function get_dup_phoneinfo() { return $this->dup_phoneinfo; }
	function get_misc1() { return $this->misc1; }
	function get_order_by() { return $this->order_by; }
	function get_created() { return $this->created; }
	function get_processed() { return $this->processed; }
	function get_zip_approximate() { return $this->zip_approximate; }
	function get_zip_confidence() { return $this->zip_confidence; }
	function get_latitude() { return $this->latitude; }
	function get_longitude() { return $this->longitude; }
	function get_map_url() { return $this->map_url; }
	function get_language() { return $this->language; }
	function get_translate() { return $this->translate; }
	function get_date_ad() { return $this->date_ad; }
	function get_product() { return $this->product; }
	function get_status() { return $this->status; }
	
	function get_state() { return substr($this->get_area_name(), 0, 2); }
	
	function set_listingid($value = NULL) { $this->listingid = ($value) ? $value : $this->get_listingid(); }
	function set_order_by($value = NULL) { $this->order_by = ($value) ? $value : 'created desc, state, area_name, contact_name'; }

	//private
	private $listingid;
	private $lead_id;
	private $firstextractiondate;
	private $refindkey;
	private $refind_value;
	private $robot_name;
	private $robot_nametemp;
	private $date_posted;
	private $data_source_name;
	private $data_source_namepublish;
	private $data_source_type;
	private $default_area_code;
	private $area_county;
	private $logo;
	private $logo_url;
	private $logo_classification;
	private $pt_review;
	private $property_type;
	private $fsbo_classification;
	private $keyword_review;
	private $area_st;
	private $area_name;
	private $area_misc;
	private $scrub_edit;
	private $description_ad;
	private $ad_url;
	private $contact_email;
	private $contact_phone1;
	private $scrub_edit1;
	private $contact_name1;
	private $contact_street1;
	private $contact_city1;
	private $contact_st1;
	private $contact_zip1;
	private $contact_biz;
	private $contact_bizname;
	private $scrub_edit2;
	private $contact_phone2;
	private $phone1_lookupsource;
	private $phone2_lookupsource;
	private $contact_name2;
	private $contact_street2;
	private $contact_city2;
	private $contact_st2;
	private $contact_zip2;
	private $property_street;
	private $property_street2;
	private $property_city;
	private $property_st;
	private $property_zip;
	private $square_feet;
	private $bedrooms;
	private $bathrooms;
	private $lot_size;
	private $year_built;
	private $garage;
	private $price;
	private $phone1_dnc;
	private $phone1_prefixfinder;
	private $phone1_pf_areacode;
	private $phone1_pf_prefix;
	private $phone1_pf_city;
	private $phone1_pf_st;
	private $phone1_pf_county;
	private $phone1_pf_county2;
	private $phone1_pf_county3;
	private $phone1_pf_county4;
	private $phone1_pf_msa;
	private $phone2_dnc;
	private $phone2_prefixfinder;
	private $phone2_pf_areacode;
	private $phone2_pf_prefix;
	private $phone2_pf_city;
	private $phone2_pf_st;
	private $phone2_pf_county;
	private $phone2_pf_county2;
	private $phone2_pf_county3;
	private $phone2_pf_county4;
	private $phone2_pf_msa;
	private $dup_phoneinfo;
	private $misc1;
	private $order_by;
	private $created;
	private $processed;
	private $zip_approximate;
	private $zip_confidence;
	private $latitude;
	private $longitude;
	private $map_url;
	private $language;
	private $translate;
	private $date_ad;
	private $product;
	private $status;

}

class listing_utilities {
	
	// public
	function __construct($args = NULL) {
		$this->initialize($args);
	}
	
	function initialize($args = NULL) {
		$this->set_order_by($args['order_by']);
	}
	
	function get_list($args = NULL) {
		$where = $args['where_plus'];
		if ($args['area_name']) {
			$where .= " AND area_name LIKE '%" . $_SESSION['data_access']->db_quote($args['area_name']) . "%' ";
		}
		if ($_SESSION['office']->account->is_admin() && count($args['area_name_checkbox']) >= 1) { // admin and they want a specific list based on area name
			$where .= " AND area_name IN (";
			foreach ($args['area_name_checkbox'] as $index=>$area_name) {
				$where .= "'" . $_SESSION['data_access']->db_quote($area_name) . "',";
			}
			$where = preg_replace("/,$/", ") ", $where);
		}
		else if (!$_SESSION['office']->account->is_admin() && !$_SESSION['office']->account->is_developer() && !$_SESSION['office']->account->is_support() && !$_SESSION['office']->account->is_upload()) {  // customers etc
			$where .= " AND area_name IN (";
			if (count($args['area_name_checkbox']) >= 1) {
				foreach ($args['area_name_checkbox'] as $index=>$area_name) {
					$where .= "'" . $area_name . "',";
				}
			}
			else {
				foreach ($_SESSION['office']->account->get_list_area_names() as $area_name) {
					$where .= "'" . $_SESSION['data_access']->db_quote($area_name) . "',";
				}
			}
			$where = preg_replace("/,$/", ") ", $where);
			
			// only search for products they are signed up for
			$where .= " AND product IN ('',";
			foreach ($_SESSION['office']->account->getArrayProducts() as $productId=>$name) {
				$where .= "'" . $name . "',";
			}
			$where = preg_replace("/,$/", ") ", $where);
		}
		if ($args['productName']) {
			$where .= " AND product='" . $args['productName'] . "' ";
		}
		if ($args['propertyTypes']) {
			$where .= " AND property_type IN (" . $_SESSION['data_access']->db_quote($args['propertyTypes']) . ") ";
		}
		if ($args['minPrice']) {
			$where .= " AND price >= " . $_SESSION['data_access']->db_quote(preg_replace("/[^\d]/", "", $args['minPrice'])) . " ";
		}
		if ($args['maxPrice']) {
			$where .= " AND price <= " . $_SESSION['data_access']->db_quote(preg_replace("/[^\d]/", "", $args['maxPrice'])) . " ";
		}
		if ($args['bedrooms']) {
			$where .= " AND bedrooms >= " . $args['bedrooms'] . " ";
		}
		if ($args['bathrooms']) {
			$where .= " AND bathrooms >= " . $args['bathrooms'] . " ";
		}
		if ($args['garage']) {
			$where .= " AND garage >= " . $args['garage'] . " ";
		}
		if ($args['square_feet']) {
			$where .= " AND square_feet >= " . $args['square_feet'] . " ";
		}
		if ($args['year_built']) {
			$where .= " AND year_built >= " . $args['year_built'] . " ";
		}
		if ($args['lot_size']) {
			$where .= " AND lot_size >= " . $args['lot_size'] . " ";
		}
		if ($args['lead_id']) {
			$where .= " AND lead_id = " . $args['lead_id'] . " ";
		}
		if ($args['contact_phone']) {
			$where .= " AND (contact_phone1 LIKE '%" . $args['contact_phone'] . "%' OR contact_phone2 LIKE '%" . $args['contact_phone'] . "%') ";
		}
		if ($args['description_ad']) {
			$where .= " AND description_ad LIKE '%" . $_SESSION['data_access']->db_quote($args['description_ad']) . "%' ";
		}
		if ($args['scrub_edit']) {
			$where .= " AND scrub_edit2 LIKE '%" . $_SESSION['data_access']->db_quote($args['scrub_edit']) . "%' ";
		}
		if ($args['no_dnc']) {
			$where .= " AND phone1_dnc='' ";
		}
		if ($args['with_address_only']) {
			$where .= " AND property_street REGEXP '^[0-9]' AND ((property_city != '' AND property_st != '') OR (property_zip!='')) ";
		}
		if ($args['with_phone_only']) {
			$where .= " AND (contact_phone1 != '') ";
		}
		if ($args['with_email_only']) {
			$where .= " AND (contact_email != '') ";
		}
		if ($args['start_month'] && $args['start_day'] && $args['start_year'] && $args['end_month'] && $args['end_day'] && $args['end_year']) {
			// have to set the real start date to yesterday at 10:00pm
			$start_date = date('Y-m-d', strtotime($args['start_year'] . '-' . $args['start_month'] . '-' . $args['start_day']));
			$where .= " AND date_posted >= '" . $start_date . "' AND date_posted <= '" . $args['end_year'] . "-" . $args['end_month'] . "-" . $args['end_day'] . "' ";
		}
		
		// do the following if they are a CUSTOMER
		if ($_SESSION['office']->account->get_account_type() == 'CUSTOMER') {
			// add in where clause for the robot names
			$robot_names = explode(",", $_SESSION['office']->account->get_string_robot_names());
			$string_robot_names = '';
			foreach ($robot_names as $robot_name) {
				if ($robot_name) {
					$string_robot_names .= "'" . $_SESSION['data_access']->db_quote($robot_name) . "',";
				}
			}
			$string_robot_names = preg_replace("/,$/", "", $string_robot_names);
			if ($string_robot_names) {
				$where .= " AND robot_name NOT IN (" . $string_robot_names . ") ";
			}
			// they can only view listings newer than 3 months
			$where .= " AND date_posted >= DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 3 MONTH), '%Y-%m-%d') ";
		}
		
		$where = preg_replace("/^ AND /" , " WHERE ", $where);
		if ($args['cmd'] == 'count') {
			$query = "SELECT count(*) as num, area_name FROM listing " . $where . " GROUP BY area_name ORDER BY area_name";
		}
		else {
			$query = "SELECT listingid, area_name, SUBSTR(description_ad, 1, 50) AS description_ad, created, date_posted, product FROM listing " . $where . " ORDER BY " . $this->get_order_by();
		}
		$_SESSION['listing_search_query'] = $query;
		try {
			list($results, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
			$list = array(); 
			foreach ($results as $row) {
				if ($args['cmd'] == 'count') {
					$list[] = $row;
					$list['total_count'] += $row['num'];
				}
				else {
					$o_listing = new listing();
					$o_listing->initialize($row);
					$list[] = $o_listing;
				}
			}
			return $list;
		}
		catch (Exception $exception) {
			throw $exception;
		}
	}
	
	function get_list_robot_names() {
		$query = "SELECT DISTINCT(robot_name) AS robot_name FROM listing ORDER BY robot_name";
		try {
			list($results, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
			$list = array(); 
			foreach ($results as $row) {
				$list[] = $row['robot_name'];
			}
			return $list;
		}
		catch (Exception $exception) {
			throw $exception;
		}
	}
	
	function get_most_recent_listing() {
		$query = "SELECT MAX(created) AS created FROM listing";
		try {
			list($results, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
			return $results[0]['created'];
		}
		catch (Exception $exception) {
			throw $exception;
		}
	}
	
	function send_listings($args = NULL) {
		require_once($_SESSION['web_interface']->get_server_path('other/misc/listing_fields.php'));

		// get the states that should be going out based on time_zone
		$o_area_name_utilities = new area_name_utilities();
		$string_allowed_states = $o_area_name_utilities->get_string_states_in_time_zone( array('time_zone'=>$args['time_zone']) );
		
		$string_listingids = '';
		
		if (preg_match("/\d/", $args['start_date']) && preg_match("/\d/", $args['end_date'])) {
			$where_plus = " AND date_posted >= '" . $args['start_date'] . "' AND date_posted <= '" . $args['end_date'] . "' ";
			if ($args['start_date'] == $args['end_date']) {
				$date_range = date('F j, Y', strtotime($args['start_date']));
			}
			else {
				$date_range = date('F j, Y', strtotime($args['start_date'])) . ' - ' . date('F j, Y', strtotime($args['end_date']));
			}
		}
		else {
			$where_plus = " AND (processed='' OR processed IS NULL OR processed='0000-00-00') ";
			$date_range = date('F j, Y');
		}
		if ($args['accountid']) {
			$query = "SELECT accountid FROM account WHERE accountid='" . $args['accountid'] . "'";
		}
		else if (!$args['all_accounts']) { // only send emails to those who have listings to send (default)
			$query = "SELECT distinct(substring(area_name, 1, 2)) AS distinct_state FROM listing WHERE (processed='' OR processed IS NULL OR processed='0000-00-00')";
			list($r_listings, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
			$string_states = '';
			foreach ($r_listings as $r_listing) {
				// only add the state if it is in string_allowed_states
				if (preg_match("/," . $r_listing['distinct_state'] . ",/i", $string_allowed_states)) {
					$string_states .= "'" . $r_listing['distinct_state'] . "',";
				}
			}
			$string_states = preg_replace("/,$/", "", $string_states);
			$query = "SELECT DISTINCT(account.accountid) AS accountid FROM account, area_name_entry WHERE account.accountid=area_name_entry.accountid AND SUBSTRING(area_name_entry.area_name, 1, 2) IN (" . $_SESSION['data_access']->db_quote($string_states) . ") AND account_type NOT LIKE '%admin%' AND billing_status IN ('active','queued') AND (start_date <= NOW() AND end_date >= NOW()) ORDER BY account.accountid";
		}
		else {
			$query = "SELECT accountid FROM account WHERE account_type NOT LIKE '%admin%' AND billing_status IN ('active','queued') AND (start_date <= NOW() AND end_date >= NOW()) ORDER BY accountid";
		}
		try {
			list($r_account, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
			foreach ($r_account as $row) {
				$area_name_count = array();
				$email_body = '';
				$email_attachment = '';
				$ftp_attachment = '';
				$header_line = '';
				
				$o_account = new account( array('accountid'=>$row['accountid']) );
				if ($o_account->get_accountid() && $o_account->get_main_email_address()) {
					// set the email attachment columns
					$array_account_listings = $o_account->get_array_listing_fields();
					if (count($array_account_listings) > 0) {
						foreach ($allListingFields as $databaseField=>$displayField) {
							if ($array_account_listings[$databaseField]) {
								$header_line .= $displayField . ',';
								$arrayListingFields[$databaseField] = $displayField;
							}
						}
					}
					// handle the sending of the file to real property management
					else if ($o_account->get_accountid() == 5374) {
						$header_line = 'Franchise ID,Lead First Name,Lead Last Name,Lead Phone,Lead Email,Lead Source,Comments,Address,City,State,Zip,Lead Business Name,Owner Type,Number of Properties,Occupancy Type,Priority Status,Corporate Campaign,Decision Time Frame,Hot Button,Management Type,Management Fee %,Management Flat Fee,Lease Fee %,Setup Fee,Presentation Reviewed,Referral First Name,Referral Last Name,Referral Phone,Referral Email,Referral Address,Referral City,Referral State,Referral Zip,Referral Business Name,Referral Type,Realtor License #,Rental Property 1 Address,Rental Property 1 City,Rental Property 1 State,Rental Property 1 Zip,Rental Property 1 Occupancy Type,Rental Property 1 Date Available,Rental Property 1 Desired Rent,Rental Property 1 Market Rent (CMA),Rental Property 1 RPM Suggested Rent,Rental Property 1 Property Comment,Rental Property 1 Bedrooms,Rental Property 1 Bathrooms,Rental Property 1 Square Footage,Rental Property 1 Pets,Rental Property 1 Year Built,phone1_dnc,';
					}
					else {
						foreach ($defaultListingFields as $databaseField=>$displayField) {
							$header_line .= $displayField . ',';
						}
					}
					$header_line = substr($header_line, 0, -1);
				
					$area_names = $o_account->get_list_area_names();
					$string_area_names = '';
					foreach ($area_names as $area_name) {
						$area_state = substr($area_name, 0, 2);
						if ($area_name) {
							// if they have put in a time_zone then make sure this area can be included
							if (!$args['time_zone'] || ($args['time_zone'] && preg_match("/," . $area_state . ",/i", $string_allowed_states))) {
								$string_area_names .= "'" . $_SESSION['data_access']->db_quote($area_name) . "',";
								$area_name_count[$area_name] = 0;
							}
						}
					}
					$string_area_names = preg_replace("/,$/", "", $string_area_names);
					
					$robot_names = explode(",", $o_account->get_string_robot_names());
					$string_robot_names = '';
					foreach ($robot_names as $robot_name) {
						if ($robot_name) {
							$string_robot_names .= "'" . $_SESSION['data_access']->db_quote($robot_name) . "',";
						}
					}
					$string_robot_names = preg_replace("/,$/", "", $string_robot_names);
					$robot_where_plus = '';
					if ($string_robot_names) {
						$robot_where_plus = " AND robot_name NOT IN (" . $string_robot_names . ") ";
					}
					
					// only search for products they are signed up for
					$where_plus .= " AND product IN ('',";
					foreach ($o_account->getArrayProducts() as $productId=>$name) {
						$where_plus .= "'" . $name . "',";
					}
					$where_plus = preg_replace("/,$/", ") ", $where_plus);
					
					// only search for property_types they want
					$array_account_property_type_fields = $o_account->get_array_property_type_fields();
					if (count($array_account_property_type_fields) > 0) {
						$where_plus .= " AND property_type IN (";
						foreach ($array_account_property_type_fields as $propertyType=>$boolean) {
							$where_plus .= "'" . $propertyType . "',";
						}
						$where_plus = preg_replace("/,$/", ") ", $where_plus);
					}

					// set up the email body stuff
					$email_bottom = file_get_contents( $o_account->get_server_path('office/listing/email_bottom.txt') );
					$email_generic_body = file_get_contents( $o_account->get_server_path('office/listing/email_generic_body.txt') );
					$temp_row = file_get_contents( $o_account->get_server_path('office/listing/email_row.txt') );
					$email_top = file_get_contents( $o_account->get_server_path('office/listing/email_top.txt') );
					$email_top_no_listings = file_get_contents( $o_account->get_server_path('office/listing/email_top_no_listings.txt') );

					if ($string_area_names) {
						$query = "SELECT distinct(area_name) as area_name FROM listing WHERE area_name IN (" . $string_area_names . ") " . $robot_where_plus . $where_plus . " ORDER BY area_name";
						list($r_area_name, $num_rows) = $_SESSION['data_access']->query( array('query'=>$query) );
						foreach ($r_area_name as $row) {
							$listing_count = 1;

							// get the list of listings
							$listArgs['where_plus'] = " AND area_name='" . $_SESSION['data_access']->db_quote($row['area_name']) . "' " . $robot_where_plus . $where_plus;
							$account_search_fields = $o_account->get_hash_search_fields();
							foreach ($account_search_fields as $key=>$value) {
								$listArgs[$key] = $value;
							}
							$this->set_order_by('property_type');
							$list_listing = $this->get_list($listArgs);
							foreach ($list_listing as $item_listing) {
								$o_listing = new listing( array('listingid'=>$item_listing->get_listingid()) );
								if ($o_listing->get_listingid()) {
									
									// add to string_listingids to set processed date at end of process
									if (!$args['accountid'] && !preg_match("/" . $o_listing->get_listingid() . ",/", $string_listingids)) {
										$string_listingids .= $o_listing->get_listingid() . ',';
									}
									
									$area_name_count[$o_listing->get_area_name()]++; // add 1 to the total number of listings in this area_name
									// add attachment row
									$phone1_dnc = ($o_listing->get_phone1_dnc() == 'T') ? '(Do Not Call Registry)' : '';
									$phone2_dnc = ($o_listing->get_phone2_dnc() == 'T') ? '(Do Not Call Registry)' : '';
									
									// add the email attachment row
									if (count($array_account_listings) > 0) {
										$row = '';
										foreach ($arrayListingFields as $databaseField=>$displayField) {
											if ($databaseField == 'phone1_dnc') {
												$row .= '"' . $phone1_dnc . '",';
											}
											else if ($databaseField == 'phone2_dnc') {
												$row .= '"' . $phone2_dnc . '",';
											}
											else {
	          						$function_name = 'get_' . $databaseField;
          							$row .= '"' . $o_listing->$function_name() . '",';
											}
          					}
										$row = substr($row, 0, -1);
										$email_attachment .= $row . "\n";
										$ftp_attachment .= $row . "\n";	
									}
									// handle the sending of the file to real property management
									else if ($o_account->get_accountid() == 5374) {
										list($firstName, $lastName) = explode(" ", $o_listing->get_contact_name1(), 2);
										$email_attachment .= '"CallCenter","' . $firstName . '","' . $lastName . '","' . $o_listing->get_contact_phone1() . '","' . $o_listing->get_contact_email() . '","DD Leads","' . $o_listing->get_description_ad() . '","' . $o_listing->get_contact_street1() . '","' . $o_listing->get_contact_city1() . '","' . $o_listing->get_contact_st1() . '","' . $o_listing->get_contact_zip1() . '","","","","","","","","","","","","","","","' . $o_listing->get_lead_id() . '","","","","","","","","","","","' . $o_listing->get_property_street() . '","' . $o_listing->get_property_city() . '","' . $o_listing->get_property_st() . '","' . $o_listing->get_property_zip() . '","","","' . $o_listing->get_price() . '","","","","' . $o_listing->get_bedrooms() . '","' . $o_listing->get_bathrooms() . '","' . $o_listing->get_square_feet() . '","","' . $o_listing->get_year_built() . '","' . $o_listing->get_phone1_dnc() . '"' . "\n";
									}
									else {
										$row = '';
										foreach ($defaultListingFields as $databaseField=>$displayField) {
											if ($databaseField == 'phone1_dnc') {
												$row .= '"' . $phone1_dnc . '",';
											}
											else if ($databaseField == 'phone2_dnc') {
												$row .= '"' . $phone2_dnc . '",';
											}
											else {
	          						$function_name = 'get_' . $databaseField;
          							$row .= '"' . $o_listing->$function_name() . '",';
											}
										}
										$row = substr($row, 0, -1);
										$email_attachment .= $row . "\n";
										
										if ($o_account->get_accountid() == 1140 || $o_account->get_accountid() == 6659) {
											$ftp_attachment .= '"' . $o_listing->get_lead_id() . '","' . $o_listing->get_date_posted() . '","' . $o_listing->get_data_source_namepublish() . '","' . $o_listing->get_area_st() . '","' . $o_listing->get_area_name() . '","' . $o_listing->get_description_ad() . '","' . $o_listing->get_contact_email() . '","' . $o_listing->get_contact_phone1() . '","' . $o_listing->get_contact_name1() . '","' . $o_listing->get_contact_street1() . '","' . $o_listing->get_contact_city1() . '","' . $o_listing->get_contact_st1() . '","' . $o_listing->get_contact_zip1() . '","' . $o_listing->get_contact_phone2() . '","' . $o_listing->get_contact_name2() . '","' . $o_listing->get_contact_street2() . '","' . $o_listing->get_contact_city2() . '","' . $o_listing->get_contact_st2() . '","' . $o_listing->get_contact_zip2() . '","' . $o_listing->get_square_feet() . '","' . $o_listing->get_bedrooms() . '","' . $o_listing->get_bathrooms() . '","' . $o_listing->get_lot_size() . '","' . $o_listing->get_year_built() . '","' . $o_listing->get_garage() . '","' . $o_listing->get_price() . '","' . $phone1_dnc . '","' . $o_listing->get_phone1_pf_areacode() . '","' . $o_listing->get_phone1_pf_prefix() . '","' . $o_listing->get_phone1_pf_city() . '","' . $o_listing->get_phone1_pf_st() . '","' . $phone2_dnc . '","' . $o_listing->get_phone2_pf_areacode() . '","' . $o_listing->get_phone2_pf_prefix() . '","' . $o_listing->get_phone2_pf_city() . '","' . $o_listing->get_phone2_pf_st() . '","' . $o_listing->get_property_street() . '","' . $o_listing->get_property_street2() . '","' . $o_listing->get_property_city() . '","' . $o_listing->get_property_st() . '","' . $o_listing->get_property_zip() . '","' . $o_listing->get_zip_confidence() . '","' . $o_listing->get_zip_approximate() . '","' . $o_listing->get_latitude() . '","' . $o_listing->get_longitude() . '","' . $o_listing->get_map_url() . '","' . $o_listing->get_logo_url() . '","' . $o_listing->get_product() . '"' . "\n";
										}
										else {
											$row = '';
											foreach ($allListingFields as $databaseField=>$displayField) {
												$function_name = 'get_' . $databaseField;
												$row .= '"' . $o_listing->$function_name() . '",';
											}
											$row = substr($row, 0, -1);
											$ftp_attachment .= $row . "\n";
										}

  									// add email row
  									$row = $temp_row;
  									$row = preg_replace("/<<count>>/", $listing_count, $row);
  									$row = preg_replace("/<<data_source_namepublish>>/", $o_listing->get_data_source_namepublish(), $row);
  									$row = preg_replace("/<<date_posted>>/", date('F j, Y', strtotime($o_listing->get_date_posted())), $row);
  									$row = preg_replace("/<<lead_id>>/", $o_listing->get_lead_id(), $row);
										$row = preg_replace("/<<property_type>>/", $o_listing->get_property_type(), $row);
  									$row = preg_replace("/<<area_name>>/", $o_listing->get_area_name(), $row);
  									$row = preg_replace("/<<area_misc>>/", $o_listing->get_area_misc(), $row);
  									// making hard returns and have to escape the $ sign for some reason
  									$description_ad = preg_replace("/(.{65}.+? )/", "$1\n", $o_listing->get_description_ad());
  									$description_ad = str_replace("$", "\\$", $description_ad);
  									$row = preg_replace("/<<description_ad>>/", $description_ad, $row);
  
  									// figure out property_info merge field
  									$string = '';
  									if ($o_listing->get_property_street() || $o_listing->get_property_city() || $o_listing->get_property_zip()) {
  										$string .= 'STREET: ' . $o_listing->get_property_street() . "\n";
  										if ($o_listing->get_property_street2()) {
  											$string .= '        ' . $o_listing->get_property_street2() . "\n";
  										}
  										$string .= 'CITY: ' . $o_listing->get_property_city() . "\n";
  										$string .= 'STATE: ' . $o_listing->get_property_st() . "\n";
  										$string .= 'ZIP: ' . $o_listing->get_property_zip();
  									}
  									$row = preg_replace("/<<property_info>>/", $string, $row);
  									
  									// figure out contact_info1 merge field
  									$string = 'EMAIL: ' . $o_listing->get_contact_email() . "\n";
  									if ($o_listing->get_contact_phone1()) {
  										$string .= "PHONE 1 REVERSE LOOKUP: " . $o_listing->get_contact_phone1();
  										if ($o_listing->get_phone1_dnc() == 'T') {
  											$string .= '* (Do Not Call Registry)';
  										}
  									}
  									if ($o_listing->get_contact_name1()) {
  										$string .= "\n" . $o_listing->get_contact_name1();
  									}
  									if ($o_listing->get_contact_street1()) {
  										$string .= "\n" . $o_listing->get_contact_street1();
  									}
  									if ($o_listing->get_phone1_prefixfinder() == 'Y') {
  										$string .= "\nArea Code/Prefix Lookup: (" . $o_listing->get_phone1_pf_areacode() . ') ' . $o_listing->get_phone1_pf_prefix() . '-xxxx (' . $o_listing->get_phone1_pf_city() . ', ' . $o_listing->get_phone1_pf_st() . ')';
  									}
  									if ($o_listing->get_contact_city1() || $o_listing->get_contact_st1() || $o_listing->get_contact_zip1()) {
  										$string .= "\n";
  										if ($o_listing->get_contact_city1()) {
  											$string .= $o_listing->get_contact_city1() . ', ';
  										}
  										if ($o_listing->get_contact_st1()) {
  											$string .= $o_listing->get_contact_st1() . ' ';
  										}
  										if ($o_listing->get_contact_zip1()) {
  											$string .= $o_listing->get_contact_zip1();
  										}
  									}
										$row = preg_replace("/<<contact_info1>>/", $string, $row);
  
  									$string = '';
  									if ($o_listing->get_contact_phone2()) {
  										$string .= "\n\nPHONE 2 REVERSE LOOKUP: " . $o_listing->get_contact_phone2();
  										if ($o_listing->get_phone2_dnc() == 'T') {
  											$string .= '* (Do Not Call Registry)';
  										}
  									}
  									if ($o_listing->get_contact_name2()) {
  										$string .= "\n" . $o_listing->get_contact_name2();
  									}
  									if ($o_listing->get_contact_street2()) {
  										$string .= "\n" . $o_listing->get_contact_street2();
  									}
  									if ($o_listing->get_phone2_prefixfinder() == 'Y') {
  										$string .= "\nArea Code/Prefix Lookup: (" . $o_listing->get_phone2_pf_areacode() . ') ' . $o_listing->get_phone2_pf_prefix() . '-xxxx (' . $o_listing->get_phone2_pf_city() . ', ' . $o_listing->get_phone2_pf_st() . ')';
  									}
  									if ($o_listing->get_contact_city2() || $o_listing->get_contact_st2() || $o_listing->get_contact_zip2()) {
  										$string .= "\n";
  										if ($o_listing->get_contact_city2()) {
  											$string .= $o_listing->get_contact_city2() . ', ';
  										}
  										if ($o_listing->get_contact_st2()) {
  											$string .= $o_listing->get_contact_st2() . ' ';
  										}
  										if ($o_listing->get_contact_zip2()) {
  											$string .= $o_listing->get_contact_zip2();
  										}
  									}
  									$row = preg_replace("/<<contact_info2>>/", $string, $row);
  
  									$email_body .= $row;
									}
									$listing_count++;
								}
							}
						}
					}
				}
				// if the account has listing fields get the generic body
				if (count($array_account_listings) > 0) {
					$body = $email_generic_body;
				}
				else if ($email_body) {
					$temp_email_top = $email_top;
					if ($o_account->get_account_type() != 'TRIAL') {
						$temp_email_top = preg_replace("/<<company_address>>/", "105 South State St.  #204\nOrem, UT  84058", $temp_email_top);
					}
					$area_name_count_rows = '';
					foreach ($area_name_count as $area_name=>$count) {
						$area_name_count_rows .= $area_name . ': ' . $count . "\n";
					}
					$temp_email_top = str_replace("<<area_name_count_rows>>", $area_name_count_rows, $temp_email_top);
					$body = $temp_email_top . $email_body;
				}
				else {
					$body = $email_top_no_listings;
				}
				
				$body .= $email_bottom;
				
				// merge in company info
				$body = preg_replace("/<<company_url>>/", $o_account->o_company->get_url(), $body);
				$body = preg_replace("/<<company_title>>/", $o_account->o_company->get_title(), $body);
				$body = preg_replace("/<<date_range>>/", $date_range, $body);
				$body = preg_replace("/<<today>>/", date('F j, Y'), $body);
				$body = preg_replace("/<<today_weekday>>/", date('l'), $body);
				
				$boundary = '_____' . date('Ymd') . '_____';
				$from_email = ($o_account->o_company->get_from_email()) ? $o_account->o_company->get_from_email() : 'fsbo@' . $o_account->o_company->get_url();
				$reply_to_email = ($o_account->o_company->get_reply_to_email()) ? $o_account->o_company->get_reply_to_email() : 'info@' . $o_account->o_company->get_url();
				$headers = "From: " . $o_account->o_company->get_title() . " <" . $from_email . ">\r\n" . 
					"Reply-To: " . $reply_to_email . "\r\n" . 
					"MIME-Version: 1.0\r\n" . 
					"Content-Type: multipart/mixed; boundary=\"" . $boundary . "\"";
				$body = "--" . $boundary . "\r\n" . 
					"Content-Type: text/plain; charset=iso-8859-1\r\n" . 
					"Content-Transfer-Encoding: 8bit\r\n" . 
					"Content-Disposition: inline\r\n\r\n" . 
					$body . "\r\n"
				;
				
				if ($o_account->get_email_attachment() && $email_attachment) {
					$body .= "--" . $boundary . "\r\n" . 
						"Content-Type: application/vnd.ms-excel; name=\"" . date('Y_m_d') . "_Leads.csv\"\r\n" . 
						"Content-Transfer-Encoding: base64\r\n" . 
						"Content-Disposition: inline; filename=\"" . date('Y_m_d') . "_Leads.csv\"\r\n\r\n" . 
						chunk_split(base64_encode($header_line . "\n" . $email_attachment)) . "\r\n"
					;
				}
				
				// handle the sending of the file to theredx
				if ($o_account->get_accountid() == 1140) {
					$file_name = $_SERVER['DOCUMENT_ROOT'] . '/files/' . date('Y_m_d_H_i') . '_FSBO_Leads_' . $o_account->get_accountid() . '.csv';
					file_put_contents($file_name, "Lead_ID,Date,Data_Source,Area_ST,Area_Name,Description_Ad,Contact_Email,Contact_Phone1,Contact_Name1,Contact_Street1,Contact_City1,Contact_ST1,Contact_ZIP1,Contact_Phone2,Contact_Name2,Contact_Street2,Contact_City2,Contact_ST2,Contact_ZIP2,Square_Feet,Bedrooms,Bathrooms,Lot_Size,Year_Built,Garage,Price,Phone1_DNC,Phone1_PF_AreaCode,Phone1_PF_Prefix,Phone1_PF_City,Phone1_PF_ST,Phone2_DNC,Phone2_PF_AreaCode,Phone2_PF_Prefix,Phone2_PF_City,Phone2_PF_ST,Property_Street,Property_Street_2,Property_City,Property_ST,Property_ZIP,zip_confidence,zip_approximate,latitude,longitude,map_url,logo_url,Product\n" . $ftp_attachment);
					if (file_exists($file_name)) {
						system('/usr/bin/sha1sum ' . $file_name . ' > ' . $file_name . '.sha1');
						if (file_exists($file_name . '.sha1')) {
							system('/usr/bin/scp ' . $file_name . ' fsbo@fsbo1.theredx.com:');
							system('/usr/bin/scp ' . $file_name . '.sha1' . ' fsbo@fsbo1.theredx.com:');
							system('/usr/bin/scp ' . $file_name . ' fsbo@fsbo2.theredx.com:');
							system('/usr/bin/scp ' . $file_name . '.sha1' . ' fsbo@fsbo2.theredx.com:');
						}
					}
				}
				// handle the sending of the file to anyone with ftp_file set
				else if ($o_account->get_ftp_file()) {
					$file_name = $_SERVER['DOCUMENT_ROOT'] . '/files/' . date('Y_m_d') . '_Leads_' . $o_account->get_accountid() . '.csv';
					file_put_contents($file_name, $ftp_attachment, FILE_APPEND);
				}
				
				$body .= "--" . $boundary . "--\r\n";
				$body = preg_replace("/<<[^>].+?>>/", "", $body);
				mail($o_account->get_main_email_address(), $date_range . " Leads for " . $o_account->get_first_name() . ' ' . $o_account->get_last_name(), $body, $headers);
			}
		}
		catch (Exception $exception) {
			throw $exception;
		}
		$listingids = explode(",", $string_listingids);
		foreach ($listingids as $listingid) {
			if ($listingid) {
				$o_listing = new listing( array('listingid'=>$listingid) );
				$o_listing->processed();
			}
		}
	}
	
	/*
		function: upload
		notes: the data fields map to the corresponding database fields:
		0: firstextractiondate
		1: refindkey
		2: refind_value
		3: robot_name
		4: robot_nametemp
		5: date_posted
		6: data_source_name
		7: data_source_namepublish
		8: data_source_type
		9: default_area_code
		10: area_county
		11: logo
		12: logo_url
		13: logo_classification
		14: pt_review
		15: property_type
		16: fsbo_classification
		17: keyword_review
		18: area_st
		19: area_name
		20: area_misc
		21: scrub_edit
		22: description_ad
		23: ad_url
		24: contact_email
		25: contact_phone1
		26: phone1_lookupsource
		27: scrub_edit1
		28: contact_name1
		29: contact_street1
		30: contact_city1
		31: contact_st1
		32: contact_zip1
		33: contact_biz
		34: contact_bizname
		35: scrub_edit2
		36: contact_phone2
		37: phone2_lookupsource
		38: contact_name2
		39: contact_street2
		40: contact_city2
		41: contact_st2
		42: contact_zip2
		43: property_street
		44: property_street2
		45: property_city
		46: property_st
		47: property_zip
		48: zip_confidence
		49: zip_approximate
		50: latitude
		51: longitude
		52: map_url
		53: square_feet
		54: bedrooms
		55: bathrooms
		56: lot_size
		57: year_built
		58: garage
		59: price
		60: phone1_dnc
		61: phone1_prefixfinder
		62: phone1_pf_areacode
		63: phone1_pf_prefix
		64: phone1_pf_city
		65: phone1_pf_st
		66: phone1_pf_county
		67: phone1_pf_county2
		68: phone1_pf_county3
		69: phone1_pf_county4
		70: phone1_pf_msa
		71: phone2_dnc
		72: phone2_prefixfinder
		73: phone2_pf_areacode
		74: phone2_pf_prefix
		75: phone2_pf_city
		76: phone2_pf_st
		77: phone2_pf_county
		78: phone2_pf_county2
		79: phone2_pf_county3
		80: phone2_pf_county4
		81: phone2_pf_msa
		82: dup_phoneinfo
		83: misc1
		84: processed
		85: lead_id
		86: language
		87: translate
		88: date_ad
		89: product
		90: status
	*/
	function upload($args = NULL) {
		require_once($_SESSION['web_interface']->get_server_path('other/misc/property_type_fields.php'));
		
		$o_listing = new listing();
		$num_records = 0;
		$num_duplicate_records = 0;
		
		if ($args['file_location']) {
			$handle = fopen($args['file_location'], 'r');
			while (($data = fgetcsv($handle)) !== FALSE) {
				if (!preg_match("/^\d/", $data[0])) {
					continue;
				}
				if (!preg_match("/\w/", $data[19])) {
					continue;
				}
				$num_records++;
				if ($o_listing->exists( array('refindkey'=>$data[1]) )) {
					$num_duplicate_records++;
				}
				else {
					$num_fields = count($data);
					for ($count=0; $count < $num_fields; $count++) {
						// firstextractiondate conversion
						if ($count == 0) {
							$data[$count] = date('Y-m-d H:i:s', strtotime($data[$count]));
						}
						// date_posted conversion
						if ($count == 5) {
							$data[$count] = date('Y-m-d', strtotime($data[$count]));
						}
						// property_type conversion
						if ($count == 15) {
							// see if the property_type is one we support
							if (!in_array($data[$count], $propertyTypeFields)) {
								$data[$count] = 'General';
							}
						}
						// area_name conversion
						if ($count == 19) {
							$data[$count] = $data[18] . '-' . $data[$count];
						}
						// description_ad conversion
						if ($count == 22) {
							$data[$count] = preg_replace("/\"/", "", $data[$count]);
						}
						// garage conversion
						if ($count == 58) {
							$data[$count] = preg_replace("/[^\d.]/", "", $data[$count]);
						}
						// price conversion
						if ($count == 59) {
							$data[$count] = preg_replace("/\D/", "", $data[$count]);
						}
						// processes conversion
						if ($count == 84) {
							if ($data[$count]) {
								$data[$count] = date('Y-m-d');
							}
							else {
								$data[$count] = '';
							}
						}
					}
					$query = "INSERT INTO listing (firstextractiondate, refindkey, refind_value, robot_name, robot_nametemp, date_posted, data_source_name, data_source_namepublish, data_source_type, default_area_code, area_county, logo, logo_url, logo_classification, pt_review, property_type, fsbo_classification, keyword_review, area_st, area_name, area_misc, scrub_edit, description_ad, ad_url, contact_email, contact_phone1, phone1_lookupsource, scrub_edit1, contact_name1, contact_street1, contact_city1, contact_st1, contact_zip1, contact_biz, contact_bizname, scrub_edit2, contact_phone2, phone2_lookupsource, contact_name2, contact_street2, contact_city2, contact_st2, contact_zip2, property_street, property_street2, property_city, property_st, property_zip, zip_confidence, zip_approximate, latitude, longitude, map_url, square_feet, bedrooms, bathrooms, lot_size, year_built, garage, price, phone1_dnc, phone1_prefixfinder, phone1_pf_areacode, phone1_pf_prefix, phone1_pf_city, phone1_pf_st, phone1_pf_county, phone1_pf_county2, phone1_pf_county3, phone1_pf_county4, phone1_pf_msa, phone2_dnc, phone2_prefixfinder, phone2_pf_areacode, phone2_pf_prefix, phone2_pf_city, phone2_pf_st, phone2_pf_county, phone2_pf_county2, phone2_pf_county3, phone2_pf_county4, phone2_pf_msa, dup_phoneinfo, misc1, processed, lead_id, language, translate, date_ad, product, status, created) VALUES (";
					for ($count = 0; $count <= 90; $count++) {
						$query .= "'" . $_SESSION['data_access']->db_quote($data[$count]) . "', ";
					}
					$query .= "NOW())";
					try {
						$_SESSION['data_access']->query( array('query'=>$query) );
					}
					catch (Exception $exception) {
						throw $exception;
					}
				}
			}
			fclose($handle);
		}
		return array($num_records, $num_duplicate_records);
	}
	
	/*
		function: upload_cl_file
		notes: the data fields map to the corresponding database fields:
		0: lead_id
		1: language
		2: translate
		3: date_ad
		4: contact_name1
		5: contact_phone1
		6: contact_email
		7: ignore
		8: ignore
		9: product
	*/
	function upload_cl_file($args = NULL) {
		$o_listing = new listing();
		$num_records = 0;
		
		if ($args['file_location']) {
			$handle = fopen($args['file_location'], 'r');
			while (($data = fgetcsv($handle)) !== FALSE) {
				if (strtolower($data[0]) == 'lead_id') {
					continue;
				}
				$num_records++;
				$num_fields = count($data);
				$query = "INSERT INTO listing (lead_id, language, translate, date_ad, contact_name1, contact_phone1, contact_email, product, created) VALUES (";
				for ($count = 0; $count < $num_fields; $count++) {
					if ($count == 7 || $count == 8) {
						continue;
					}
					$query .= "'" . $_SESSION['data_access']->db_quote($data[$count]) . "', ";
				}
				$query .= "NOW())";
				try {
					$_SESSION['data_access']->query( array('query'=>$query) );
				}
				catch (Exception $exception) {
					throw $exception;
				}
			}
			fclose($handle);
		}
		return $num_records;
	}
	
	/*
		function: upload_copy_paste_file
		notes: the data fields map to the corresponding database fields:
		1: ad_url
		1: logo_url
		2: contact_name1
		3: contact_phone1
		4: property_street
		5: property_city
		6: property_st
		7: property_zip
		8: price
		9: description_ad
		10: property_type
		11: bedrooms
		12: bathrooms
		13: year_built
		14: square_feet
		15: lot_size
		16: garage
		17: misc1
		18: product
	*/
	function upload_copy_paste_file($args = NULL) {
		require_once($_SESSION['web_interface']->get_server_path('other/misc/property_type_fields.php'));
		
		$o_listing = new listing();
		$num_records = 0;
		
		if ($args['file_location']) {
			$handle = fopen($args['file_location'], 'r');
			while (($data = fgetcsv($handle)) !== FALSE) {
				if (strtolower($data[0]) == 'ad_url') {
					continue;
				}
				$num_records++;
				$num_fields = count($data);
				for ($count=0; $count < $num_fields; $count++) {
					// description_ad conversion
					if ($count == 9) {
						$data[$count] = preg_replace("/\"/", "", $data[$count]);
					}
					// garage conversion
					if ($count == 16) {
						$data[$count] = preg_replace("/[^\d.]/", "", $data[$count]);
					}
					// price conversion
					if ($count == 8) {
						$data[$count] = preg_replace("/\D/", "", $data[$count]);
					}
				}
				$query = "INSERT INTO listing (ad_url, logo_url, contact_name1, contact_phone1, property_street, property_city, property_st, property_zip, price, description_ad, property_type, bedrooms, bathrooms, year_built, square_feet, lot_size, garage, misc1, product, created) VALUES (";
				for ($count = 0; $count < $num_fields; $count++) {
					$query .= "'" . $_SESSION['data_access']->db_quote($data[$count]) . "', ";
				}
				$query .= "NOW())";
				try {
					$_SESSION['data_access']->query( array('query'=>$query) );
				}
				catch (Exception $exception) {
					throw $exception;
				}
			}
			fclose($handle);
		}
		return $num_records;
	}
	
	// get and set
	function get_order_by() { return $this->order_by; }
	function get_order_by_class($value) {
		if  (preg_match("/^" . $value . "/", $this->get_order_by())) {
			return 'class="order_by"';
		}
		return '';
	}
	
	function set_order_by($value = NULL) { $this->order_by = ($value) ? $value : 'area_name, date_posted desc, contact_name1'; }
	
	// private
	private $order_by;
	
}

?>

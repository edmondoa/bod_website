<?php
#
# @copyright Copyright (c) 2017 Green Seed Technologies, Inc. All rights reserved.
#
# @author David Hoover
# @author David McLaughlin <david@greenseedtech.com>
#

set_include_path(__DIR__.'/../modules');

// if it is coming from web
$_DOCUMENT_ROOT = $_SERVER['argv'][1];

$_BACKUP_FILE_NAME = $_DOCUMENT_ROOT . '/scripts/backup/BILLING_CRON_' . date('Y-m-d') . '.txt';

print $_BACKUP_FILE_NAME . "\n";
// start the process
file_put_contents($_BACKUP_FILE_NAME, '##### STARTING: ' . date('H:i:s') . " #####\n", FILE_APPEND);
		
if (preg_match("/hoover/", $_SERVER['PWD'])) { // if on dev server (crude i know)
	$db = mysqli_connect('dldbm', 'dahoover', '');
}
else {
	$db = mysqli_connect('dldbm', 'bod.Us3r', 'bod.P@$$');
}
if (mysqli_select_db($db, 'byownerdaily')) {
	// set up email body
	$email_body = 'The following accounts were billed today (' . date('Y-m-d') . "):\n\n";
	// get todays billings
	$query = "SELECT * FROM account WHERE next_bill_date='" . date('Y-m-d') . "' AND next_bill_date!='0000-00-00' AND billing_status='active' AND billing_type='Credit Card'";
	list($r_accounts, $num_rows) = query( array('query'=>$query) );
	foreach ($r_accounts as $r_account) {
		$token = getDecrypt($r_account['card_number']);
		if (!empty($token) && $token[0] == 'T') {
			$token = substr($token, 1); # throw away the T
		}
		file_put_contents($_BACKUP_FILE_NAME, "(" . date('H:i:s') . ") account: " . $r_account['accountid'] . "\n", FILE_APPEND);
		$account_errors = '';
		$email_body .= 'Name: ' . $r_account['first_name'] . ' ' . $r_account['last_name'] . "\n";
		$email_body .= 'Account ID: ' . $r_account['accountid'] . "\n";
		if ($token) {
			$email_body .= 'CC #: XXXXXXXX' . substr($token, -4) . " (this was the original card number and could have changed)\n";
		}
		else {
			$account_errors .= "CC #: ERROR (no credit card)\n";
		}
		if ($r_account['card_exp_date']) {
			$email_body .= 'CC Exp. Date: ' . $r_account['card_exp_date'] . "\n";
		}
		else {
			$account_errors .= "CC Exp. Date: ERROR (no expiration date)\n";
		}
		if ($r_account['rate']) {
			$email_body .= 'Amount: ' . $r_account['rate'] . "\n";
		}
		else {
			$account_errors .= "Amount: ERROR (no rate)\n";
		}
		if ($r_account['subscription_type']) {
			if ($r_account['subscription_type'] == 'Monthly' || $r_account['subscription_type'] == 'Annual') {
				$email_body .= 'Subscription Type: ' . $r_account['subscription_type'] . "\n";
				
				// get address
				$query = "SELECT * FROM address WHERE addressid='" . $r_account['business_addressid'] . "'";
				list($r_addresses, $num_rows) = query( array('query'=>$query) );
				$r_address = $r_addresses[0];
				
				// now try the actual billing
				require_once($_DOCUMENT_ROOT . '/modules/common/class.authorize_net.php');
				$o_authorize_net = new authorize_net();
				$r_one_time_charge = $o_authorize_net->one_time_charge(
					array(
						'x_type'=>'AUTH_CAPTURE',
						'x_cust_id'=>$r_account['customer_number'],
						'x_token'=>$token,
						'x_exp_date'=>$r_account['card_exp_date'],
						'x_description'=>$_REQUEST['notes'],
						'x_amount'=>$r_account['rate'],
						'x_first_name'=>$r_account['first_name'],
						'x_last_name'=>$r_account['last_name'],
						'x_address'=>$r_address['line1'] . ' ' . $r_address['line2'],
						'x_city'=>$r_address['city'],
						'x_state'=>$r_address['state'],
						'x_zip'=>$r_address['postal_code']
					)
				);
				file_put_contents($_BACKUP_FILE_NAME, print_r($r_one_time_charge) . "\n", FILE_APPEND);
				if ($r_one_time_charge['status'] != 1) { // ERROR
					$account_errors .= "Bill Account: ERROR (" . $r_one_time_charge['message'] . ")\n";
					
					$query = "INSERT INTO billing (accountid, created, amount, type, approval_code, transaction_id, notes) VALUES ('" . $r_account['accountid'] . "', NOW(), " . $r_account['rate'] . ", 'AUTO', '" . $r_one_time_charge['approval_code'] . "', '" . $r_one_time_charge['transaction_id'] . "', 'AUTO BILLING FAILED ON " . date('Y-m-d') . " FOR $" . $r_account['rate'] . "')";
					query( array('query'=>$query) );
				}
				else {
					$query = "INSERT INTO billing (accountid, created, amount, type, approval_code, transaction_id, notes) VALUES ('" . $r_account['accountid'] . "', NOW(), " . $r_account['rate'] . ", 'AUTO', '" . $r_one_time_charge['approval_code'] . "', '" . $r_one_time_charge['transaction_id'] . "', 'AUTO BILLED ON " . date('Y-m-d') . " FOR $" . $r_account['rate'] . "')";
					query( array('query'=>$query) );
					
					// if succeded then set next bill date and update it
					if ($r_account['subscription_type'] == 'Monthly') {
						$new_next_bill_date = getNextMonthSameDay( array('year'=>date('Y', strtotime($r_account['next_bill_date'])), 'month'=>date('m', strtotime($r_account['next_bill_date'])), 'day'=>date('d', strtotime($r_account['next_bill_date']))) );
					}
					else if ($r_account['subscription_type'] == 'Annual') {
						$new_next_bill_date = date('Y-m-d', strtotime("+1 YEAR", strtotime($r_account['next_bill_date'])));
					}
					
					// update the next bill date
					$query = "UPDATE account SET next_bill_date='" . $new_next_bill_date . "' WHERE accountid='" . $r_account['accountid'] . "'";
					query( array('query'=>$query) );
					$email_body .= 'Next Bill Date: ' . $new_next_bill_date . "\n";
				}
			}
			else {
				$account_errors .= 'Subscription Type: ' . $r_account['subscription_type'] . " ERROR (invalid subscription type)\n";
			}
		}
		else {
			$account_errors .= "Subscription Type: ERROR (no subscription type)\n";
		}
		if (empty($account_errors)) {
			$email_body .= "Billing Results: SUCCESS\n";
		}
		else {
			$email_body .= "Billing Results: ERROR\n" . $account_errors;
		}
		$email_body .= "\n\n";
	}
	file_put_contents($_BACKUP_FILE_NAME, '##### STARTING SEND: ' . date('H:i:s') . " #####\n", FILE_APPEND);
	file_put_contents($_BACKUP_FILE_NAME, $email_body . "\n", FILE_APPEND);
	mail('admin@byownerdaily.com', 'Billing for: ' . date('Y-m-d'), $email_body, '', '-f billing@byownerdaily.com');
	file_put_contents($_BACKUP_FILE_NAME, '##### ENDING SEND: ' . date('H:i:s') . " #####\n", FILE_APPEND);
	// end the process
	file_put_contents($_BACKUP_FILE_NAME, '##### ENDING: ' . date('H:i:s') . " #####\n\n", FILE_APPEND);
}
else {
	print "ERROR CONNECTING TO DATABASE\n";
}
exit;

#####
// other functions
function db_quote($value = NULL) {
	global $db;
	if ($value) {
		if (get_magic_quotes_gpc()) {
			$value = stripslashes($value);
		}
		if (!is_numeric($value)) {
			$value = mysqli_real_escape_string($db, $value);
		}
	}
	else {
		$value = '';
	}
	return $value;
}

function getDecrypt($message) {
	$message = base64_decode($message);
	$message = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, 'H6@/61)V/Ld0;?-pW[wFGvh0E`sLj3jc', $message, MCRYPT_MODE_ECB);
	$block = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB);
	$len = strlen($message);
	$pad = ord($message[$len-1]);
	return substr($message, 0, $len-$pad);
}

function getNextMonthSameDay($args = NULL) {
	$args['year'] = ($args['year']) ? $args['year'] : date('Y');
	$args['month'] = ($args['month']) ? $args['month'] : date('m');
	$args['day'] = ($args['day']) ? $args['day'] : date('d');
	$numberOfDaysNextMonth = date('t', strtotime($args['year'] . '-' . $args['month'] . '-01' . " +1 MONTH"));
	if ($numberOfDaysNextMonth < $args['day']) {
		return date('Y-m-t', strtotime($args['year'] . '-' . $args['month'] . '-01' . " +1 MONTH"));
	}
	return date('Y-m-' . $args['day'], strtotime($args['year'] . '-' . $args['month'] . '-01' . " +1 MONTH"));
}

function query($args = NULL) {
	global $db;
	print $args['query'] . "\n";
	if (preg_match("/^select/i", $args['query'])) {
		if (!preg_match("/file_path/", $args['query'])) {
			//error_log($args['query']);
		}
		try {
			error_log($args['query']);
			$handle = mysqli_query($db, $args['query']);
			$results = array();
			$num_rows = mysqli_num_rows($handle);
			while ($row = mysqli_fetch_assoc($handle)) {
				array_push($results, $row);
			}
			mysqli_free_result($handle);
			return array($results, $num_rows);
		}
		catch (Exception $exception) {
			error_log($args['query']);
			throw $exception;
		}
	}
	else if (preg_match("/^insert/i", $args['query'])) {
		try {
			//error_log($args['query']);
			mysqli_query($db, $args['query']);
			return mysqli_insert_id($db);
		}
		catch (Exception $exception) {
			error_log($args['query']);
			throw $exception;
		}
	}
	else {
		try {
			//error_log($args['query']);
			mysqli_query($db, $args['query']);
			return 0;
		}
		catch (my_exception $exception) {
			error_log($args['query']);
			throw $exception;
		}
	}
}

?>

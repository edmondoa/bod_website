<?php
#
# @copyright Copyright (c) 2017 Green Seed Technologies, Inc. All rights reserved.
#
# @author David Hoover
# @author David McLaughlin <david@greenseedtech.com>
#

# archive listings to a backup database listing_backup
// if it is coming from web
$_DOCUMENT_ROOT = $_SERVER['argv'][1];

if (preg_match("/hoover/", $_SERVER['PWD'])) { // if on dev server (crude i know)
	$db = mysqli_connect('dldbm', 'dahoover', '');
}
else {
	$db = mysqli_connect('dldbm', 'bod.Us3r', 'bod.P@$$');
}
if (mysqli_select_db($db, 'byownerdaily')) {
	$globalContinue = 1;
	while ($globalContinue) {
		print 'START CLEAR DATABASE: ' . date('Y-m-d H:i:s') . "\n";
		$startTime = time();
		$continue = 1;
  	$numRecords = 0;
  	while ($continue) {
  		$query = "SELECT listingid FROM listing WHERE created < '" . date('Y-m-d', strtotime("14 MONTHS AGO")) . "' LIMIT 1";
  		list($listing, $num_rows) = query( array('query'=>$query) );
  		if ($num_rows == 1) {
  			$query = "INSERT INTO listing_backup SELECT * FROM listing WHERE listingid=" . $listing[0]['listingid'];
  			query( array('query'=>$query) );
  			$query = "DELETE FROM listing WHERE listingid=" . $listing[0]['listingid'];
  			query( array('query'=>$query) );
  			$numRecords++;
  		}
  		else {
  			$continue = 0;
				$globalContinue = 0;
  		}
  		if ($numRecords == 1000) {
  			$continue = 0;
  		}
  	}
  	$endTime = time();
  	$diffTime = $endTime - $startTime;
  	print 'END CLEAR DATABASE: ' . date('Y-m-d H:i:s') . ' - ' . $diffTime . "\n\n";
  	sleep(10);
	}
}
exit;

# other subroutines
function query($args = NULL) {
	global $db;
	print $args['query'] . "\n";
	if (preg_match("/^select/i", $args['query'])) {
		if (!preg_match("/file_path/", $args['query'])) {
			//error_log($args['query']);
		}
		try {
			error_log($args['query']);
			$handle = mysqli_query($db, $args['query']);
			$results = array();
			$num_rows = mysqli_num_rows($handle);
			while ($row = mysqli_fetch_assoc($handle)) {
				array_push($results, $row);
			}
			mysqli_free_result($handle);
			return array($results, $num_rows);
		}
		catch (Exception $exception) {
			error_log($args['query']);
			throw $exception;
		}
	}
	else if (preg_match("/^insert/i", $args['query'])) {
		try {
			//error_log($args['query']);
			mysqli_query($db, $args['query']);
			return mysqli_insert_id($db);
		}
		catch (Exception $exception) {
			error_log($args['query']);
			throw $exception;
		}
	}
	else {
		try {
			//error_log($args['query']);
			mysqli_query($db, $args['query']);
			return 0;
		}
		catch (my_exception $exception) {
			error_log($args['query']);
			throw $exception;
		}
	}
}

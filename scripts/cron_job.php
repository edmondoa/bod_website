<?php
#
# @copyright Copyright (c) 2017 Green Seed Technologies, Inc. All rights reserved.
#
# @author David Hoover
# @author David McLaughlin <david@greenseedtech.com>
#
# a script to end trials and cancel accounts
#

set_include_path(__DIR__.'/../modules');

// if it is coming from web
$_DOCUMENT_ROOT = $_SERVER['argv'][1];
if (preg_match("/\/dev\//", $_SERVER['PWD']) || preg_match("/\/prod\//", $_SERVER['PWD'])) { // if on amazon
	$dbHost = 'localhost';
} else {
	$db = mysqli_connect('dldbm', 'bod.Us3r', 'bod.P@$$');
	global $_SERVER;
	$_SERVER['DB_HOST'] = 'dldbm';
	$_SERVER['DB_NAME'] = 'byownerdaily';
	$_SERVER['DB_USERNAME'] = 'bod.Us3r';
	$_SERVER['DB_PASSWORD'] = 'bod.P@$$';
	$dbHost = 'dldbm';
}
$db = mysqli_connect($dbHost, 'bod.Us3r', 'bod.P@$$');
if (mysqli_select_db($db, 'byownerdaily')) {
	trial_ended();
	trial_ending();
	cancel_accounts();
}
exit;

function cancel_accounts() {
	$query = "UPDATE account SET billing_status='canceled', billing_status_date=NOW() WHERE end_date IS NOT NULL AND end_date!='0000-00-00' AND end_date <= NOW() AND account_type IN ('CUSTOMER','TRIAL')";
	query( array('query'=>$query) );
}

function trial_ended() {
	global $_DOCUMENT_ROOT;
	$query = "SELECT * FROM account WHERE account_type='TRIAL' AND end_date=NOW() AND card_number!=''";
	list($r_account, $num_rows) = query( array('query'=>$query) );
	foreach ($r_account as $account) {
		$query = "select * from area_name_entry where accountid='" . $account['accountid'] . "' order by area_name";
		list($r_area_name_entry, $num_rows) = query( array('query'=>$query) );
		$area_names = '';
		foreach ($r_area_name_entry as $area_name_entry) {
			$area_names .= $area_name_entry['area_name'] . "\n";
		}
		$email_body = file_get_contents($_DOCUMENT_ROOT . '/scripts/trial_end_admin.email');
		$email_body = preg_replace("/<<accountid>>/", $account['accountid'], $email_body);
		$email_body = preg_replace("/<<account_type>>/", $account['account_type'], $email_body);
		$email_body = preg_replace("/<<username>>/", $account['username'], $email_body);
		$email_body = preg_replace("/<<first_name>>/", $account['first_name'], $email_body);
		$email_body = preg_replace("/<<last_name>>/", $account['last_name'], $email_body);
		$email_body = preg_replace("/<<business_name>>/", $account['business_name'], $email_body);
		$email_body = preg_replace("/<<main_email_address>>/", $account['main_email_address'], $email_body);
		$email_body = preg_replace("/<<personal_phone>>/", $account['personal_phone'], $email_body);
		$email_body = preg_replace("/<<business_phone>>/", $account['business_phone'], $email_body);
		$email_body = preg_replace("/<<fax_phone>>/", $account['fax_phone'], $email_body);
		$email_body = preg_replace("/<<cell_phone>>/", $account['cell_phone'], $email_body);
		$email_body = preg_replace("/<<created>>/", $account['created'], $email_body);
		$email_body = preg_replace("/<<start_date>>/", $account['start_date'], $email_body);
		$email_body = preg_replace("/<<end_date>>/", $account['end_date'], $email_body);
		$email_body = preg_replace("/<<customer_number>>/", $account['customer_number'], $email_body);
		$email_body = preg_replace("/<<area_names>>/", $area_names, $email_body);
		$email_body = preg_replace("/<<notes>>/", $account['notes'], $email_body);
		mail('admin@byownerdaily.com', 'A ByOwnerDaily FSBO sample period has converted!', $email_body, "From: ByOwnerDaily <sales@byownerdaily.com>\r\n");	
		// update account
		$start_date = date('Y-m-d', strtotime("TOMORROW"));
		$end_date = '2019-12-31';
		$month = date('n', strtotime("TOMORROW"));
		$day = date('j', strtotime("TOMORROW"));
		$year = date('Y', strtotime("TOMORROW"));
		if ($day > 28) {
			$day = 1;
			$month++;
		}
		if ($month > 12) {
			$month = 1;
			$year++;
		}
		$next_bill_date = date('Y-m-d', strtotime($year . '-' . $month . '-' . $day));
		$query = "UPDATE account SET trial_to_customer=1, start_date='" . $start_date . "', end_date='" . $end_date . "', account_type='CUSTOMER', billing_status='active', billing_day='" . $day . "', next_bill_date='" . $next_bill_date . "' WHERE accountid=" . $account['accountid'];
		query( array('query'=>$query) );
	}
}

function trial_ending() {
	global $dbHost;
	session_start();
	require_once('common/class.data_access.php');
	require_once('office/account/class.letter.php');
	require_once('office/account/class.account.php');
	
	$_SESSION['data_access'] = new data_access( array('db_host'=>$dbHost, 'db_name'=>'byownerdaily', 'db_username'=>'bod.Us3r', 'db_password'=>'bod.P@$$') );
	
	$query = "SELECT * FROM account WHERE account_type='TRIAL' AND end_date='" . date('Y-m-d', strtotime("TOMORROW")) . "' AND (card_number='' OR card_number IS NULL)";
	list($r_account, $num_rows) = query( array('query'=>$query) );
	foreach ($r_account as $account) {
		$o_account = new account( array('accountid'=>$account['accountid']) );
		$o_letter = new letter( array('title'=>'Trial Customer Converted with NO BILLING INFO IN FIELDS') );
		$email_segments = $o_letter->merge_segments( array('o_account'=>$o_account) );
		$headers = '';
		if ($email_segments['cc_list']) {
			$headers .= "Cc: " . $email_segments['cc_list'] . "\r\n";
		}
		if ($email_segments['bcc_list']) {
			$headers .= "Bcc: " . $email_segments['bcc_list'] . "\r\n";
		}
		$headers .= "From: " . $email_segments['from_email'];
		mail($o_account->get_username(), $email_segments['subject'], $email_segments['body'], $headers);
	}
}

/*
sub trial_ending_client {
	my $query = "select * from account where account_type='TRIAL' and end_date=NOW()";
	my $h_account = &while_query($query);
	while (my $account = $h_account->fetchrow_hashref) {
		if ($r_account['main_email_address}) {
			$query = "SELECT * FROM company WHERE companyid='" . $r_account['companyid} . "'";
			my $company = &query($query);
			my $email_body = `cat $root/trial_end_client.email`;
			$email_body =~ s/<<title>>/$company->{title}/eg;
			$email_body =~ s/<<url>>/$company->{url}/eg;
			my $args = {
				'from'=>$company->{title} . ' <sales@' . $company->{url} . '>',
				'to'=>$r_account['main_email_address},
				'cc'=>$company->{main_email_address},
				'subject'=>'Your ' . $company->{title} . ' FSBO sample period has ended!',
				'data'=>$email_body
			};
			&send_email($args);
		}
	}
}
*/

# other subroutines
function query($args = NULL) {
	global $db;
	if (preg_match("/^select/i", $args['query'])) {
		if (!preg_match("/file_path/", $args['query'])) {
			//error_log($args['query']);
		}
		try {
			error_log($args['query']);
			$handle = mysqli_query($db, $args['query']);
			$results = array();
			$num_rows = mysqli_num_rows($handle);
			while ($row = mysqli_fetch_assoc($handle)) {
				array_push($results, $row);
			}
			mysqli_free_result($handle);
			return array($results, $num_rows);
		}
		catch (Exception $exception) {
			error_log($args['query']);
			throw $exception;
		}
	}
	else if (preg_match("/^insert/i", $args['query'])) {
		try {
			//error_log($args['query']);
			mysqli_query($db, $args['query']);
			return mysqli_insert_id($db);
		}
		catch (Exception $exception) {
			error_log($args['query']);
			throw $exception;
		}
	}
	else {
		try {
			//error_log($args['query']);
			mysqli_query($db, $args['query']);
			return 0;
		}
		catch (my_exception $exception) {
			error_log($args['query']);
			throw $exception;
		}
	}
}

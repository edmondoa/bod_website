#!/usr/bin/perl
use DBI;
my $db;
my $db_host;
my $root = '';
if ($ARGV[0]) {
	$root = $ARGV[0];
	($db_host) = $root =~ /.*\/(byowner.+?)\/.*/i;
	$db = DBI->connect("DBI:mysql:$db_host;host=localhost",'root','mysql');
	
	&trial_ending_admin;
	#&trial_ending_client;
	&cancel_accounts;

	$db->disconnect;
}
exit;

sub cancel_accounts {
	my $query = "UPDATE account SET billing_status='canceled', billing_status_date=NOW() WHERE end_date IS NOT NULL AND end_date!='0000-00-00' AND end_date <= NOW() AND account_type IN ('CUSTOMER','TRIAL')";
	&query($query);
}

sub trial_ending_admin {
	my $query = "select * from account where account_type='TRIAL' and end_date=NOW()";
	my $h_account = &while_query($query);
	while (my $account = $h_account->fetchrow_hashref) {
		$query = "select * from area_name_entry where accountid='" . $account->{accountid} . "' order by area_name";
		my $h_area_name_entry = &while_query($query);
		my $area_names = '';
		while (my $r_area_name_entry = $h_area_name_entry->fetchrow_hashref) {
			$area_names .= $r_area_name_entry->{area_name} . "\n";
		}
		my $email_body = `cat $root/trial_end_admin.email`;
		$email_body =~ s/<<accountid>>/$account->{accountid}/g;
		$email_body =~ s/<<account_type>>/$account->{account_type}/g;
		$email_body =~ s/<<username>>/$account->{username}/g;
		$email_body =~ s/<<first_name>>/$account->{first_name}/g;
		$email_body =~ s/<<last_name>>/$account->{last_name}/g;
		$email_body =~ s/<<business_name>>/$account->{business_name}/g;
		$email_body =~ s/<<main_email_address>>/$account->{main_email_address}/g;
		$email_body =~ s/<<personal_phone>>/$account->{personal_phone}/g;
		$email_body =~ s/<<business_phone>>/$account->{business_phone}/g;
		$email_body =~ s/<<fax_phone>>/$account->{fax_phone}/g;
		$email_body =~ s/<<cell_phone>>/$account->{cell_phone}/g;
		$email_body =~ s/<<created>>/$account->{created}/g;
		$email_body =~ s/<<start_date>>/$account->{start_date}/g;
		$email_body =~ s/<<end_date>>/$account->{end_date}/g;
		$email_body =~ s/<<customer_number>>/$account->{customer_number}/g;
		$email_body =~ s/<<area_names>>/$area_names/g;
		$email_body =~ s/<<notes>>/$account->{notes}/g;
		my $args = {
			'from'=>'ByOwnerDaily <sales@byownerdaily.com>',
			'to'=>'dahoover@hashmedia.com,info@byownerdaily.com,bod.fsbo@gmail.com',
			'subject'=>'A ByOwnerDaily FSBO sample period has ended!',
			'data'=>$email_body
		};
		&send_email($args);	
	}
}
sub trial_ending_client {
	my $query = "select * from account where account_type='TRIAL' and end_date=NOW()";
	my $h_account = &while_query($query);
	while (my $account = $h_account->fetchrow_hashref) {
		if ($account->{main_email_address}) {
			$query = "SELECT * FROM company WHERE companyid='" . $account->{companyid} . "'";
			my $company = &query($query);
			my $email_body = `cat $root/trial_end_client.email`;
			$email_body =~ s/<<title>>/$company->{title}/eg;
			$email_body =~ s/<<url>>/$company->{url}/eg;
			my $args = {
				'from'=>$company->{title} . ' <sales@' . $company->{url} . '>',
				'to'=>$account->{main_email_address},
				'cc'=>$company->{main_email_address},
				'subject'=>'Your ' . $company->{title} . ' FSBO sample period has ended!',
				'data'=>$email_body
			};
			&send_email($args);
		}
	}
}

# other subroutines
sub debug {
	my ($line) = @_;
	open(FILE, ">>/home/www/docroot/hoover/debug.txt");
	print FILE "$line\n";
	close(FILE);
}

sub query {
	my ($query) = @_;
	if ($query !~ /^select/i) {
		$db->do($query) || &dberr($query);
	}
	else {
		my $moron = $db->prepare($query);
		$moron->execute();
		my $results = $moron->fetchrow_hashref();
		return $results;
	}
}
 
sub send_email {
	my ($args) = shift;
	use MIME::Lite;
	my $sender = MIME::Lite->new(
		From=>$args->{from},,
		To=>$args->{to},
		Cc=>$args->{cc},
		Subject=>$args->{subject},
		Data=>$args->{data}
	);
	$sender->send("sendmail", "/usr/sbin/sendmail -t -oi -oem -f fsbo\@byownerdaily.com");
}
		
sub while_query {
	my ($query) = @_;
	my $moron = $db->prepare($query);
	$moron->execute();
	return $moron;
}

<?php

$_DEFAULT_COMPANY = 'company_def';
$_ROOT_DIR = preg_replace("/scripts$/", "web", $_SERVER['PWD']);
$_DEFAULT_START_DIR = "$_ROOT_DIR/$_DEFAULT_COMPANY";

// put each company info here
$company_hash = array(
	'default'=>array(
		'COMPANY_DIR'=>'company_def',
		'COMPANY_ID'=>'1',
		'DB_HOST'=>'208.187.239.42',
		'DB_NAME'=>'byownerdaily',
		'DB_PASSWORD'=>'',
		'DB_USERNAME'=>'dahoover'
	)
);

foreach ($company_hash as $company_name=>$company) {
	// get the server variables we need
	$a5_conf_file_contents = file_get_contents($a5_conf_file . '_set_env.inc');
	preg_match_all("/SetEnv (.+?) (.*)/", $a5_conf_file_contents, $a5_conf_file_array);
	$count = 0;
	foreach ($a5_conf_file_array[0] as $row) {
		$_SERVER[$a5_conf_file_array[1][$count]] = $a5_conf_file_array[2][$count];
		$count++;
	}
	if ($argv[1] && $argv[1] != $_SERVER['DB_NAME']) {
		continue;
	}
	print $_SERVER['DB_NAME'] . "\n";
	$db = oci_connect($_SERVER['DB_NAME'], $_SERVER['DB_PASSWORD'], $_SERVER['DB_SID']);

	$query = "delete from file_path";
	query( array('query'=>$query, 'db'=>$db) );

	$company_directories = array($_DEFAULT_COMPANY);
	if ($_SERVER['DEFAULT_DIRECTORY'] != 'company_def') {
		$company_directories[] = $_SERVER['DEFAULT_DIRECTORY'];
	}
	$package_directories = array($_DEFAULT_PACKAGE);
	$language_directories = array($_DEFAULT_LANGUAGE);
	
	$directories = array();
	$file_path_hash = array();
				
	$query = "select * from package order by packageid";
	list($r_package, $num_rows) = query( array('query'=>$query, 'db'=>$db) );
	foreach ($r_package as $package) {
		if ($package['name'] != $_DEFAULT_PACKAGE) {
			$package_directories[] = $package['name'];
		} 
	}
	
	$query = "select * from language order by languageid";
	list($r_language, $num_rows) = query( array('query'=>$query, 'db'=>$db) );
	foreach ($r_language as $language) {
		if ($language['name'] != $_DEFAULT_LANGUAGE) {
			$language_directories[] = $language['name'];
		} 
	}

	foreach ($language_directories as $language_directory) {
		foreach ($package_directories as $package_directory) {
			foreach ($company_directories as $company_directory) {
				if (preg_grep('/' . $company_directory . '\/' . $package_directory . '\/' . $language_directory . '/', $directories)) {
					continue;
				}
				$directories[] = $company_directory . '/' . $package_directory . '/' . $language_directory;
			}
		}
	}

	$files = array();
	foreach ($directories as $directory) {
		get_files($_ROOT_DIR . '/' . $directory, $files);
	}

	foreach ($directories as $directory) {
		if (preg_match("/^" . $_SERVER['DEFAULT_DIRECTORY'] . "/", $directory)) {
			foreach ($files as $file) {
				$search_directories = get_search_directories($directory);
				$search_directory = array_pop($search_directories);
				while ($search_directory) {
					if (file_exists($_ROOT_DIR . '/' . $search_directory . '/' . $file)) {
						if (!$file_path_hash[$directory][$file]) {
							$file_path_hash[$directory][$file] = '/web/' . $search_directory . '/' . $file;
						}
						$search_directory = '';
					}
					else {
						$search_directory = array_pop($search_directories);
					}
				}
			}
		}
	}
	
	$file_pathid = 1;
	foreach ($file_path_hash as $directory=>$file_array) {
		$directory_array = explode('/', $directory);
		$company = $directory_array[0];
		$package = $directory_array[1];
		$language = $directory_array[2];
		foreach ($file_array as $file_name=>$file_path) {
			$query = "INSERT INTO file_path (file_pathid, company_directory, package_directory, language_directory, file_name, file_path) values (" . $file_pathid . ", '" . $company . "', '" . $package . "', '" . $language . "', '" . $file_name . "', '" . $file_path . "')";
			query( array('query'=>$query, 'db'=>$db) );
			$file_pathid++;
		}
	}
}
print "Done\n";
exit;

#####
function get_files($dir, &$files) {
	global $_ROOT_DIR;
	
	if (is_dir($dir)) {
		$dh = opendir($dir);
		while (!is_bool($file = readdir($dh))) {
			if ($file == '.' || $file == '..' || $file == 'CVS' || preg_grep('|' . $dir . '/' . $file . '|', $files)) {
				continue;
			}
			if (is_dir($dir . '/' . $file)) {
				get_files($dir . '/' . $file, $files);
			}
			else {
				$sub_dir = preg_replace('|' . $_ROOT_DIR . '/company_\w+/package_\w+/lang_\w+(.*)|', "$1", $dir);
				$sub_dir = preg_replace("|^/|", "", $sub_dir);
				if (!preg_grep('|' . $sub_dir . '/' . $file . '|', $files)) {
					$location = $sub_dir . '/' . $file;
					$location = preg_replace("|^/|", "", $location);
					$files[] = $location;
				}
			} 
		}
	}
}

function get_search_directories($directory) {
	global $_DEFAULT_COMPANY;
	global $_DEFAULT_PACKAGE;
	global $_DEFAULT_LANGUAGE;
	
	$search_directories = array();
	$directory_array = explode('/', $directory);
	$company = $directory_array[0];
	$package = $directory_array[1];
	$language = $directory_array[2];
	
	$company_directories = array($_DEFAULT_COMPANY);
	if ($company != $_DEFAULT_COMPANY) {
		$company_directories[] = $company;
	}

	$package_directories = array($_DEFAULT_PACKAGE);
	if ($package != $_DEFAULT_PACKAGE) {
		$package_directories[] = $package;
	}

	$language_directories = array($_DEFAULT_LANGUAGE);
	if ($language != $_DEFAULT_LANGUAGE) {
		$language_directories[] = $language;
	}
		
	foreach ($language_directories as $language_directory) {
		foreach ($package_directories as $package_directory) {
			foreach ($company_directories as $company_directory) {
				$search_directories[] = $company_directory . '/' . $package_directory . '/' . $language_directory;
			}
		}
	}
	return $search_directories;
}

// other functions
function db_quote($string) {
	$string = preg_replace("/'/", "''", $string);
	return "'" . $string . "'";
}

function query($args = NULL) {
	if (preg_match("/^select/i", $args['query'])) {
		if (preg_match("/ LIMIT /i", $args['query'])) {
			$limit = preg_replace("/.* LIMIT (.*)$/i", "$1", $args['query']);
			$args['query'] = preg_replace("/ LIMIT (.*)$/i", "", $args['query']);
			if (preg_match("/,/", $limit)) {
				$limits = explode(",", $limit);
				$start_limit = $limits[0];
				$end_limit = $start_limit + $limits[1];
				$args['query'] = "SELECT * FROM ( SELECT q.*, rownum rnum FROM ( " . $args['query'] . " ) q WHERE rownum <= " . $end_limit . " ) WHERE rnum > $start_limit";
			}
			else {
				$args['query'] = "SELECT * FROM ( SELECT q.*, rownum rnum FROM ( " . $args['query'] . " ) q WHERE rownum <= " . $limit . " ) WHERE rnum >= 0";
			}
		}
		//print $args['query'] . "\n";
		$results = array();
		$statement = oci_parse($args['db'], $args['query']);
		oci_execute($statement);
		$num_rows = 0;
		while ($row = oci_fetch_array($statement, OCI_ASSOC + OCI_RETURN_LOBS)) {
			//force all the field names to be lower case (Oracle returns uc field names)
			$rowLC = array();
			foreach ($row as $k=>$v) {
				$rowLC[strtolower($k)] = $v;
			}
			array_push($results, $rowLC);
			$num_rows++;
		}
		return array($results, $num_rows);
	}
	else if (preg_match("/^insert/i", $args['query'])) {
		if (preg_match("/NEXT_VAL/", $args['query'])) {
			$table_name = preg_replace("/^insert into (.+?) .*$/is", "$1", $args['query']);
			$q_next_val = "select " . $table_name . "_seq.nextval from dual";
			list($results, $num_rows) = query( array(query=>$q_next_val, db=>$args['db']) );
			$next_val = $results[0]['nextval'];
			$args['query'] = preg_replace("/NEXT_VAL/", $next_val, $args['query']);
		}
		//print $args['query'] . "\n";
		$statement = oci_parse($args['db'], $args['query']);
		if (!empty($args['binds'])) {
			foreach ($args['binds'] as $key=>$value) {
				oci_bind_by_name($statement, ':' . $key, $value);
			}
		}
		oci_execute($statement);
		return $next_val;
	}
	else {
		//print $args['query'] . "\n";
		$statement = oci_parse($args['db'], $args['query']);
		oci_execute($statement);
		return 0;
	}
}

?>
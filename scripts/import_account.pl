#!/usr/bin/perl
$field_map = {
	'0'=>'customer_number',
	'1'=>'subscription_type',
	'2'=>'authorize_net_number',
	'3'=>'billing_day',
	'4'=>'billing_type',
	'5'=>'rate',
	'6'=>'monthly_average',
	'7'=>'card_number',
	'8'=>'card_type',
	'9'=>'card_exp_date',
	'10'=>'card_name',
	'11'=>'business_phone',
	'12'=>'personal_phone',
	'13'=>'cell_phone',
	'14'=>'fax_phone',
	'15'=>'customer_type',
	'16'=>'referral_source',
	'17'=>'referral_source_other',
	'18'=>'business_name',
	'19'=>'line1',
	'20'=>'city',
	'21'=>'state',
	'22'=>'postal_code'
};

use DBI;
my $db = DBI->connect("DBI:mysql:byownerdaily;host=localhost",'root','mysql');
open(FILE, 'bod_import.txt');
while (<FILE>) {
	$line = $_;
	chomp($line);
	my @fields = split("\t", $line);
	$query = "SELECT * FROM account WHERE customer_number='" . $fields[0] . "'";
	$account = &query($query);
	if (!$account->{accountid}) {
		print 'Account Not Found: ' . $fields[0] . "\n";
	}
	else {
		for ($count = 1; $count <= 18; $count++) {
			if (!$account->{$field_map->{$count}} || $count == 3) {
				if ($count == 5 || $count == 6) {
					$fields[$count] =~ s/\s//g;
				}
				$account->{$field_map->{$count}} = $fields[$count];
			}
		}
		$query = "UPDATE account SET subscription_type='" . &db_quote($account->{subscription_type}) . "', authorize_net_number='" . &db_quote($account->{authorize_net_number}) . "', billing_day='" . &db_quote($account->{billing_day}) . "', billing_type='" . &db_quote($account->{billing_type}) . "', rate='" . &db_quote($account->{rate}) . "', monthly_average='" . &db_quote($account->{monthly_average}) . "', card_number='" . &db_quote($account->{card_number}) . "', card_type='" . &db_quote($account->{card_type}) . "', card_exp_date='" . &db_quote($account->{card_exp_date}) . "', card_name='" . &db_quote($account->{card_name}) . "', business_phone='" . &db_quote($account->{business_phone}) . "', personal_phone='" . &db_quote($account->{personal_phone}) . "', cell_phone='" . &db_quote($account->{cell_phone}) . "', fax_phone='" . &db_quote($account->{fax_phone}) . "', customer_type='" . &db_quote($account->{customer_type}) . "', referral_source='" . &db_quote($account->{referral_source}) . "', referral_source_other='" . &db_quote($account->{referral_source_other}) . "', business_name='" . &db_quote($account->{business_name}) . "' WHERE accountid='" . $account->{accountid} . "'";
		print "$query\n";
		&query($query);
		
		if ($account->{personal_addressid}) {
			$query = "SELECT * FROM address WHERE addressid='" . $account->{personal_addressid} . "'";
			$address = &query($query);
			for ($count = 19; $count <= 22; $count++) {
				if (!$address->{$field_map->{$count}}) {
					$address->{$field_map->{$count}} = $fields[$count];
				}
			}
			$query = "UPDATE address SET line1='" . &db_quote($address->{line1}) . "', city='" . &db_quote($address->{city}) . "', state='" . &db_quote($address->{state}) . "', country='US', postal_code='" . &db_quote($address->{postal_code}) . "' WHERE addressid='" . $address->{addressid} . "'";
			print "$query\n";
			&query($query);
		}
		else {
			$query = "INSERT INTO address (addressid, line1, city, state, country, postal_code) VALUES ('', '" . &db_quote($fields[19]) . "', '" . &db_quote($fields[20]) . "', '" . &db_quote($fields[21]) . "', 'US', '" . &db_quote($fields[22]) . "')";
			$addressid = &id_query($query);
			print "$query\n";
			$query = "UPDATE account SET personal_addressid='" . $addressid . "' WHERE accountid='" . $account->{accountid} . "'";
			print "$query\n";
			&query($query);
		}
		
		if ($account->{business_addressid}) {
			$query = "SELECT * FROM address WHERE addressid='" . $account->{business_addressid} . "'";
			$address = &query($query);
			for ($count = 19; $count <= 22; $count++) {
				if (!$address->{$field_map->{$count}}) {
					$address->{$field_map->{$count}} = $fields[$count];
				}
			}
			$query = "UPDATE address SET line1='" . &db_quote($address->{line1}) . "', city='" . &db_quote($address->{city}) . "', state='" . &db_quote($address->{state}) . "', country='US', postal_code='" . &db_quote($address->{postal_code}) . "' WHERE addressid='" . $address->{addressid} . "'";
			print "$query\n";
			&query($query);
		}
		else {
			$query = "INSERT INTO address (addressid, line1, city, state, country, postal_code) VALUES ('', '" . &db_quote($fields[19]) . "', '" . &db_quote($fields[20]) . "', '" . &db_quote($fields[21]) . "', 'US', '" . &db_quote($fields[22]) . "')";
			$addressid = &id_query($query);
			print "$query\n";
			$query = "UPDATE account SET business_addressid='" . $addressid . "' WHERE accountid='" . $account->{accountid} . "'";
			print "$query\n";
			&query($query);
		}
	}
}
exit;

# other subroutines
sub db_quote {
	my ($string) = @_;
	$string =~ s/(['"])/\\$1/g;
	return $string;
}

sub id_query {
	my ($query) = @_;
	my $handle = &while_query($query);
	my $id = $handle->{mysql_insertid};
	$handle->finish;
	return $id;
}

sub query {
	my ($query) = @_;
	if ($query !~ /^select/i) {
		$db->do($query) || &dberr($query);
	}
	else {
		my $moron = $db->prepare($query);
		$moron->execute();
		my $results = $moron->fetchrow_hashref();
		return $results;
	}
}
 
sub while_query {
	my ($query) = @_;
	my $moron = $db->prepare($query);
	$moron->execute();
	return $moron;
}

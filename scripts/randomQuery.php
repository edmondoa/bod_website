#!/usr/local/bin/php
<?php
$db = mysqli_connect('dldbm', 'bod.Us3r', 'bod.P@$$');
mysqli_select_db($db, 'byownerdaily');

/*/
// hash the passwords
$query = "SELECT * FROM account";
$handle = mysqli_query($db, $query);
$maxLength = 0;
while ($result = mysqli_fetch_assoc($handle)) {
	if ($result['password'] && strlen($result['password']) < 40) {
		$hashed = sha1($result['accountid'] . $result['password']);
		print $result['password'] . ' - ' . $hashed . "\n";
		$query = "UPDATE account SET password='" . db_quote($hashed) . "' WHERE accountid=" . $result['accountid'];
		print $query . "\n";
		mysqli_query($db, $query);
	}
} 
//*/

/*/
// encrypt the card numbers
$query = "SELECT * FROM account WHERE card_number != ''";
$handle = mysqli_query($db, $query);
$maxLength = 0;
while ($result = mysqli_fetch_assoc($handle)) {
	if (strlen($result['card_number']) < 20) {
		$encrypted = getEncrypt($result['card_number']);
		$decrypted = getDecrypt($encrypted);
		print $result['card_number'] . ' - ' . $encrypted . ' - ' . $decrypted . "\n";
		$query = "UPDATE account SET card_number='" . db_quote($encrypted) . "' WHERE accountid=" . $result['accountid'];
		print $query . "\n";
		//mysqli_query($db, $query);
	}
}
//*/
exit;

function db_quote($value = NULL) {
	global $db;
	if ($value) {
		if (get_magic_quotes_gpc()) {
			$value = stripslashes($value);
		}
		if (!is_numeric($value)) {
			$value = mysqli_real_escape_string($db, $value);
		}
	}
	else {
		$value = '';
	}
	return $value;
}

function getDecrypt($message) {
	$message = base64_decode($message);
	$message = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, '', $message, MCRYPT_MODE_ECB);
	$block = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB);
	$len = strlen($message);
	$pad = ord($message[$len-1]);
	return substr($message, 0, $len-$pad);
}

function getEncrypt($message) {
	$block = mcrypt_get_block_size('rijndael_128', 'ecb');
	$pad = $block - (strlen($message) % $block);
	$message .= str_repeat(chr($pad), $pad);
	return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, '', $message, MCRYPT_MODE_ECB));
}
?>
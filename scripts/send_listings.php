<?php
$_TEST_MODE = FALSE;

// if it is coming from web
if ($_REQUEST['cmd'] == 'fork' && $_REQUEST['time_zone']) {
	print 'success';
	error_log('/usr/local/bin/php ' . $_SERVER['DOCUMENT_ROOT'] . '/scripts/send_listings.php ' . $_REQUEST['cmd'] . ' ' . $_REQUEST['time_zone'] . ' ' . $_SERVER['DOCUMENT_ROOT'] . ' ' . $_REQUEST['product'] . ' ' . $_REQUEST['noEmptyEmails'] . ' > /dev/null &');
	exec('/usr/local/bin/php ' . $_SERVER['DOCUMENT_ROOT'] . '/scripts/send_listings.php ' . $_REQUEST['cmd'] . ' ' . $_REQUEST['time_zone'] . ' ' . $_SERVER['DOCUMENT_ROOT'] . ' ' . $_REQUEST['product'] . ' ' . $_REQUEST['noEmptyEmails'] . ' > /dev/null &');
	exit;
}
else {
	// get the arguments
	$cmd = $_SERVER['argv'][1];
	$time_zone = $_SERVER['argv'][2];
	$_DOCUMENT_ROOT = $_SERVER['argv'][3];
	$product = $_SERVER['argv'][4];
	$noEmptyEmails = $_SERVER['argv'][5];

	if ($_TEST_MODE) {
		file_put_contents($_DOCUMENT_ROOT . '/scripts/debug.txt', 'cmd: ' . $cmd . "\ntime_zone: " . $time_zone . "\ndocument_root: " . $_DOCUMENT_ROOT . "\nproduct: " . $product . "\nnoEmptyEmails: " . $noEmptyEmails . "\n");
		//exit;
	}
	
	// only continue if the two values are set
	if ($cmd == 'fork' && $time_zone) {
		require_once($_DOCUMENT_ROOT . '/web/company_def/other/misc/listing_fields.php');
		$_BACKUP_FILE_NAME = $_DOCUMENT_ROOT . '/scripts/backup/SEND_LISTINGS_' . date('Y-m-d') . '.' . $time_zone;
		
		// start the process
		file_put_contents($_BACKUP_FILE_NAME, '##### STARTING: ' . date('H:i:s') . " #####\n", FILE_APPEND);
		
		if (preg_match("/hoover/", $_SERVER['PWD'])) { // if on dev server (crude i know)
			$db = mysqli_connect('dldbm', 'dahoover', '');
		}
		else {
			$db = mysqli_connect('dldbm', 'bod.Us3r', 'bod.P@$$');
		}
		if (mysqli_select_db($db, 'byownerdaily')) {
			$string_listingids = '';
			
			// get the states that should be going out based on time_zone
			$query = "SELECT state FROM state_time_zone WHERE time_zone='" . $time_zone . "' ORDER BY state";
			list($results, $num_rows) = query( array('query'=>$query) );
			$string_allowed_states = ',';
			foreach ($results as $row) {
				$string_allowed_states .= $row['state'] . ',';
			}
			file_put_contents($_BACKUP_FILE_NAME, "(" . date('H:i:s') . ") string_allowed_states: " . $string_allowed_states . "\n", FILE_APPEND);
			
			$where_plus = "(processed='' OR processed IS NULL OR processed='0000-00-00')";
			if ($product) {
				$where_plus .= " AND product='" . $product . "'";
			}
			$date_range = date('F j, Y');
			
			$query = "SELECT distinct(substring(area_name, 1, 2)) AS distinct_state FROM listing WHERE " . $where_plus;
			list($r_listings, $num_rows) = query( array('query'=>$query) );
			$string_states = '';
			foreach ($r_listings as $r_listing) {
				// only add the state if it is in string_allowed_states
				if (preg_match("/," . $r_listing['distinct_state'] . ",/i", $string_allowed_states)) {
					$string_states .= "'" . $r_listing['distinct_state'] . "',";
				}
			}
			$string_states = preg_replace("/,$/", "", $string_states);
			
			// put the time_zone listings into their own table
			$query = "DELETE FROM listing_" . $time_zone;
			query( array('query'=>$query) );
			$query = "INSERT INTO listing_" . $time_zone . " SELECT * FROM listing WHERE SUBSTRING(area_name, 1, 2) IN (" . $string_states . ") AND " . $where_plus;
			file_put_contents($_BACKUP_FILE_NAME, "(" . date('H:i:s') . ") listing query: " . $query . "\n", FILE_APPEND);
			query( array('query'=>$query) );
			
			$query = "SELECT DISTINCT(account.accountid) AS accountid FROM account, area_name_entry WHERE account.accountid=area_name_entry.accountid AND SUBSTRING(area_name_entry.area_name, 1, 2) IN (" . $string_states . ") AND account_type NOT LIKE '%admin%' AND billing_status IN ('active','queued') AND (start_date <= NOW() AND end_date >= NOW()) ORDER BY account.customer_number";
			file_put_contents($_BACKUP_FILE_NAME, "(" . date('H:i:s') . ") main query: " . $query . "\n", FILE_APPEND);

			list($r_accounts, $num_rows) = query( array('query'=>$query) );
			foreach ($r_accounts as $r_account) {
				$area_name_count = array();
				$array_account_listings = array();
				$arrayListingFields = array();
				$ftp_attachment = '';
				$email_body = '';
				$email_attachment = '';
				$emptyEmail = TRUE;
				$header_line = '';
				$search_where_plus = '';
			
				// set up the account info
				$query = "SELECT * FROM account WHERE accountid='" . $r_account['accountid'] . "'";
				list($i_account, $num_rows) = query( array('query'=>$query) );
				$account = $i_account[0];
				if ($account['accountid'] && $account['main_email_address']) {
					file_put_contents($_BACKUP_FILE_NAME, "(" . date('H:i:s') . ") account: " . $account['accountid'] . "\n", FILE_APPEND);
					$account['company'] = array();
					$account['list_area_names'] = array();
					$account['string_area_names'] = ',';
					$account['list_robot_names'] = array();
					$account['string_robot_names'] = ',';
					
					// get the account company info
					$query = "SELECT * FROM company WHERE companyid='" . $account['companyid'] . "'";
					list($i_company, $num_rows) = query( array('query'=>$query) );
					$account['company'] = $i_company[0];
					
					// get the account's areas
					$query = "SELECT * FROM area_name_entry WHERE accountid='" . $account['accountid'] . "' ORDER BY area_name";
					list($results, $num_rows) = query( array('query'=>$query) );
					foreach ($results as $row) {
						$account['list_area_names'][] = $row['area_name'];
						$account['string_area_names'] .= $row['area_name'] . ',';
					}
					
					// get the account's robots
					$query = "SELECT * FROM robot_name_entry WHERE accountid='" . $account['accountid'] . "' ORDER BY robot_name";
					list($results, $num_rows) = query( array('query'=>$query) );
					foreach ($results as $row) {
						$account['list_robot_names'][] = $row['robot_name'];
						$account['string_robot_names'] .= $row['robot_name'] . ',';
					}
					
					// get the account's listing fields if any
					$query = "SELECT listing_fields FROM account_listing_fields WHERE accountid=" . $account['accountid'];
					list($results, $num_rows) = query( array('query'=>$query) );
					$listing_fields = explode(',', $results[0]['listing_fields']);
					foreach ($listing_fields as $listing_field) {
						if ($listing_field) {
							$array_account_listings[$listing_field] = TRUE;
						}
					}
					ksort($array_account_listings);
					if (count($array_account_listings) > 0) {
						foreach ($allListingFields as $databaseField=>$displayField) {
							if ($array_account_listings[$databaseField]) {
								$header_line .= $displayField . ',';
								$arrayListingFields[$databaseField] = $displayField;
							}
						}
					}
					// handle the sending of the file to real property management
					else if ($account['accountid'] == 5374) {
						$header_line = 'Franchise ID,Lead First Name,Lead Last Name,Lead Phone,Lead Email,Lead Source,Comments,Address,City,State,Zip,Lead Business Name,Owner Type,Number of Properties,Occupancy Type,Priority Status,Corporate Campaign,Decision Time Frame,Hot Button,Management Type,Management Fee %,Management Flat Fee,Lease Fee %,Setup Fee,Presentation Reviewed,Referral First Name,Referral Last Name,Referral Phone,Referral Email,Referral Address,Referral City,Referral State,Referral Zip,Referral Business Name,Referral Type,Realtor License #,Rental Property 1 Address,Rental Property 1 City,Rental Property 1 State,Rental Property 1 Zip,Rental Property 1 Occupancy Type,Rental Property 1 Date Available,Rental Property 1 Desired Rent,Rental Property 1 Market Rent (CMA),Rental Property 1 RPM Suggested Rent,Rental Property 1 Property Comment,Rental Property 1 Bedrooms,Rental Property 1 Bathrooms,Rental Property 1 Square Footage,Rental Property 1 Pets,Rental Property 1 Year Built,phone1_dnc,';
					}
					else {
						foreach ($defaultListingFields as $databaseField=>$displayField) {
							$header_line .= $displayField . ',';
						}
					}
					$header_line = substr($header_line, 0, -1);
				
					// get the area names for the account
					$string_area_names = '';
					foreach ($account['list_area_names'] as $area_name) {
						$area_state = substr($area_name, 0, 2);
						if ($area_name) {
							// if they have put in a time_zone then make sure this area can be included
							if (preg_match("/," . $area_state . ",/i", $string_allowed_states)) {
								$string_area_names .= "'" . db_quote($area_name) . "',";
								$area_name_count[$area_name] = 0;
							}
						}
					}
					$string_area_names = preg_replace("/,$/", "", $string_area_names);
					
					// get the robot names for the account
					$robot_names = explode(",", $account['string_robot_names']);
					$string_robot_names = '';
					foreach ($robot_names as $robot_name) {
						if ($robot_name) {
							$string_robot_names .= "'" . db_quote($robot_name) . "',";
						}
					}
					$string_robot_names = preg_replace("/,$/", "", $string_robot_names);
					$robot_where_plus = '';
					if ($string_robot_names) {
						$robot_where_plus = " AND robot_name NOT IN (" . $string_robot_names . ") ";
					}
					
					// get the account_search_fields for the account
					$query = "SELECT search_fields FROM account_search_fields WHERE accountid=" . $account['accountid'];
					list($results, $num_rows) = query( array('query'=>$query) );
					if ($num_rows > 0) {
						$search_fields = explode(',', $results[0]['search_fields']);
						foreach ($search_fields as $search_field) {
							if ($search_field) {
								$search_field_array = explode("=", $search_field);
								$search_field_name = $search_field_array[0];
								$search_field_value = $search_field_array[1];
								if ($search_field_value) {
									if ($search_field_name == 'minPrice') {
										$search_where_plus .= " AND price >= " . mysqli_real_escape_string($db, preg_replace("/[^\d]/", "", $search_field_value)) . " ";
									}
									if ($search_field_name == 'maxPrice') {
										$search_where_plus .= " AND price <= " . mysqli_real_escape_string($db, preg_replace("/[^\d]/", "", $search_field_value)) . " ";
									}
									if ($search_field_name == 'bedrooms') {
										$search_where_plus .= " AND bedrooms >= " . $search_field_value . " ";
									}
									if ($search_field_name == 'bathrooms') {
										$search_where_plus .= " AND bathrooms >= " . $search_field_value . " ";
									}
									if ($search_field_name == 'garage') {
										$search_where_plus .= " AND garage >= " . $search_field_value . " ";
									}
									if ($search_field_name == 'square_feet') {
										$search_where_plus .= " AND square_feet >= " . $search_field_value . " ";
									}
									if ($search_field_name == 'year_built') {
										$search_where_plus .= " AND year_built >= " . $search_field_value . " ";
									}
									if ($search_field_name == 'lot_size') {
										$search_where_plus .= " AND lot_size >= " . $search_field_value . " ";
									}
									if ($search_field_name == 'lead_id') {
										$search_where_plus .= " AND lead_id = " . $search_field_value . " ";
									}
									if ($search_field_name == 'contact_phone') {
										$search_where_plus .= " AND (contact_phone1 LIKE '%" . $search_field_value . "%' OR contact_phone2 LIKE '%" . $search_field_value . "%') ";
									}
									if ($search_field_name == 'description_ad') {
										$search_where_plus .= " AND description_ad LIKE '%" . mysqli_real_escape_string($db, $search_field_value) . "%' ";
									}
									if ($search_field_name == 'scrub_edit') {
										$search_where_plus .= " AND scrub_edit2 LIKE '%" . mysqli_real_escape_string($db, $search_field_value) . "%' ";
									}
									if ($search_field_name == 'no_dnc' && $search_field_value == 1) {
										$search_where_plus .= " AND phone1_dnc='' ";
									}
									if ($search_field_name == 'with_address_only' && $search_field_value == 1) {
										$search_where_plus .= " AND property_street REGEXP '^[0-9]' AND ((property_city != '' AND property_st != '') OR (property_zip!='')) ";
									}
									if ($search_field_name == 'with_phone_only' && $search_field_value == 1) {
										$search_where_plus .= " AND (contact_phone1 != '') ";
									}
									if ($search_field_name == 'with_email_only' && $search_field_value == 1) {
										$search_where_plus .= " AND (contact_email != '') ";
									}
								}
							}
						}
					}
					
					// only search for products they are signed up for
					$search_where_plus .= " AND product IN ('',";
					$query = "SELECT name FROM AccountProductEntry ape, Product p WHERE ape.productId=p.productId AND ape.accountid=" . $account['accountid'];
					list($results, $num_rows) = query( array('query'=>$query) );
					foreach ($results as $result) {
						$search_where_plus .= "'" . $result['name'] . "',";
					}
					$search_where_plus = preg_replace("/,$/", ") ", $search_where_plus);
					
					// only search for property_types they want
					$query = "SELECT property_type_fields FROM account_property_type_fields WHERE accountid=" . $account['accountid'];
					list($results, $num_rows) = query( array('query'=>$query) );
					$property_type_fields = explode(',', $results[0]['property_type_fields']);
					$array_account_property_type_fields = array();
					foreach ($property_type_fields as $property_type_field) {
						if ($property_type_field) {
							$array_account_property_type_fields[$property_type_field] = TRUE;
						}
					}
					ksort($array_account_property_type_fields);
					if (count($array_account_property_type_fields) > 0) {
						$search_where_plus .= " AND property_type IN (";
						foreach ($array_account_property_type_fields as $propertyType=>$boolean) {
							$search_where_plus .= "'" . $propertyType . "',";
						}
						$search_where_plus = preg_replace("/,$/", ") ", $search_where_plus);
					}
					
					file_put_contents($_BACKUP_FILE_NAME, "\t(" . date('H:i:s') . ") account data done\n", FILE_APPEND);
					
					// set up the email body stuff
					$email_bottom = file_get_contents( get_server_path( array('companyDirectory'=>$account['company']['directory'], 'companyParentDirectory'=>$account['company']['parent_directory'], 'file'=>'office/listing/email_bottom.txt') ) );
					$email_generic_body = file_get_contents( get_server_path( array('companyDirectory'=>$account['company']['directory'], 'companyParentDirectory'=>$account['company']['parent_directory'], 'file'=>'office/listing/email_generic_body.txt') ) );
					$temp_row = file_get_contents( get_server_path( array('companyDirectory'=>$account['company']['directory'], 'companyParentDirectory'=>$account['company']['parent_directory'], 'file'=>'office/listing/email_row.txt') ) );
					$email_top = file_get_contents( get_server_path( array('companyDirectory'=>$account['company']['directory'], 'companyParentDirectory'=>$account['company']['parent_directory'], 'file'=>'office/listing/email_top.txt') ) );
					$email_top_no_listings = file_get_contents( get_server_path( array('companyDirectory'=>$account['company']['directory'], 'companyParentDirectory'=>$account['company']['parent_directory'], 'file'=>'office/listing/email_top_no_listings.txt') ) );
			
					if ($string_area_names) {
						$query = "SELECT distinct(area_name) as area_name FROM listing_" . $time_zone . " WHERE area_name IN (" . $string_area_names . ") " . $robot_where_plus . $search_where_plus . " ORDER BY area_name";
						list($r_area_names, $num_rows) = query( array('query'=>$query) );
						
						file_put_contents($_BACKUP_FILE_NAME, "\t(" . date('H:i:s') . ") area name done\n", FILE_APPEND);
						
						foreach ($r_area_names as $r_area_name) {
							$listing_count = 1;

							// get the actual listings for this area to send to the account
							$query = "SELECT * FROM listing_" . $time_zone . "  WHERE area_name='" . db_quote($r_area_name['area_name']) . "' " . $robot_where_plus . $search_where_plus . " ORDER BY property_type";
							list($r_listings, $num_rows) = query( array('query'=>$query) );
							
							file_put_contents($_BACKUP_FILE_NAME, "\t(" . date('H:i:s') . ") - $query - listing done\n", FILE_APPEND);
							
							if (count($r_listings) > 0) {
								$emptyEmail = FALSE; // used with $noEmptyEmails
							}
							
							foreach ($r_listings as $listing) {
								if ($listing['listingid']) {
									// add to string_listingids to set processed date at end of process
									if (!preg_match("/" . $listing['listingid'] . ",/", $string_listingids)) {
										$string_listingids .= $listing['listingid'] . ',';
									}
									
									$area_name_count[$listing['area_name']]++; // add 1 to the total number of listings in this area_name
									// add attachment row
									$phone1_dnc = ($listing['phone1_dnc'] == 'T') ? '(Do Not Call Registry)' : '';
									$phone2_dnc = ($listing['phone2_dnc'] == 'T') ? '(Do Not Call Registry)' : '';
									
									if (count($array_account_listings) > 0) {
										$row = '';
										foreach ($arrayListingFields as $databaseField=>$displayField) {
											if ($databaseField == 'phone1_dnc') {
												$row .= '"' . $phone1_dnc . '",';
											}
											else if ($databaseField == 'phone2_dnc') {
												$row .= '"' . $phone2_dnc . '",';
											}
											else {
	          						$row .= '"' . $listing[$databaseField] . '",';
											}
          					}
										$row = substr($row, 0, -1);
										$email_attachment .= $row . "\n";	
									}
									// handle the sending of the file to real property management
									else if ($account['accountid'] == 5374) {
										list($firstName, $lastName) = explode(" ", $listing['contact_name1'], 2);
										$email_attachment .= '"CallCenter","' . $firstName . '","' . $lastName . '","' . $listing['contact_phone1'] . '","' . $listing['contact_email'] . '","DD Leads","' . $listing['description_ad'] . '","' . $listing['contact_street1'] . '","' . $listing['contact_city1'] . '","' . $listing['contact_st1'] . '","' . $listing['contact_zip1'] . '","","","","","","","","","","","","","","","' . $listing['lead_id'] . '","","","","","","","","","","","' . $listing['property_street'] . '","' . $listing['property_city'] . '","' . $listing['property_st'] . '","' . $listing['property_zip'] . '","","","' . $listing['price'] . '","","","","' . $listing['bedrooms'] . '","' . $listing['bathrooms'] . '","' . $listing['square_feet'] . '","","' . $listing['year_built'] . '","' . $listing['phone1_dnc'] . '"' . "\n";
									}
									else {
										$row = '';
										foreach ($defaultListingFields as $databaseField=>$displayField) {
											if ($databaseField == 'phone1_dnc') {
												$row .= '"' . $phone1_dnc . '",';
											}
											else if ($databaseField == 'phone2_dnc') {
												$row .= '"' . $phone2_dnc . '",';
											}
											else {
	          						$row .= '"' . $listing[$databaseField] . '",';
											}
										}
										$row = substr($row, 0, -1);
										$email_attachment .= $row . "\n";
										
										$ftp_attachment .= '"' . $listing['lead_id'] . '","' . $listing['date_posted'] . '","' . $listing['data_source_namepublish'] . '","' . $listing['area_st'] . '","' . $listing['area_name'] . '","' . $listing['description_ad'] . '","' . $listing['contact_email'] . '","' . $listing['contact_phone1'] . '","' . $listing['contact_name1'] . '","' . $listing['contact_street1'] . '","' . $listing['contact_city1'] . '","' . $listing['contact_st1'] . '","' . $listing['contact_zip1'] . '","' . $listing['contact_phone2'] . '","' . $listing['contact_name2'] . '","' . $listing['contact_street2'] . '","' . $listing['contact_city2'] . '","' . $listing['contact_st2'] . '","' . $listing['contact_zip2'] . '","' . $listing['square_feet'] . '","' . $listing['bedrooms'] . '","' . $listing['bathrooms'] . '","' . $listing['lot_size'] . '","' . $listing['year_built'] . '","' . $listing['garage'] . '","' . $listing['price'] . '","' . $phone1_dnc . '","' . $listing['phone1_pf_areacode'] . '","' . $listing['phone1_pf_prefix'] . '","' . $listing['phone1_pf_city'] . '","' . $listing['phone1_pf_st'] . '","' . $phone2_dnc . '","' . $listing['phone2_pf_areacode'] . '","' . $listing['phone2_pf_prefix'] . '","' . $listing['phone2_pf_city'] . '","' . $listing['phone2_pf_st'] . '","' . $listing['property_street'] . '","' . $listing['property_street2'] . '","' . $listing['property_city'] . '","' . $listing['property_st'] . '","' . $listing['property_zip'] . '","' . $listing['zip_confidence'] . '","' . $listing['zip_approximate'] . '","' . $listing['latitude'] . '","' . $listing['longitude'] . '","' . $listing['map_url'] . '","' . $listing['logo_url'] . '"';
										// handle the addition of Product for theredx
										if ($account['accountid'] == 1140 || $account['accountid'] == 6659) {
											$ftp_attachment .= ',"' . $listing['product'] . '"';
										}
										$ftp_attachment .= "\n";
										
  									// add email row
  									$row = $temp_row;
  									$row = preg_replace("/<<count>>/", $listing_count, $row);
  									$row = preg_replace("/<<data_source_namepublish>>/", $listing['data_source_namepublish'], $row);
  									$row = preg_replace("/<<date_posted>>/", date('F j, Y', strtotime($listing['date_posted'])), $row);
  									$row = preg_replace("/<<lead_id>>/", $listing['lead_id'], $row);
										$row = preg_replace("/<<property_type>>/", $listing['property_type'], $row);
  									$row = preg_replace("/<<area_name>>/", $listing['area_name'], $row);
  									$row = preg_replace("/<<area_misc>>/", $listing['area_misc'], $row);
  									// making hard returns and have to escape the $ sign for some reason
  									$description_ad = preg_replace("/(.{65}.+? )/", "$1\n", $listing['description_ad']);
  									$description_ad = str_replace("$", "\\$", $description_ad);
  									$row = preg_replace("/<<description_ad>>/", $description_ad, $row);
  									
  									// figure out property_info merge field
  									$string = '';
  									if ($listing['property_street'] || $listing['property_city'] || $listing['property_zip']) {
  										$string .= 'STREET: ' . $listing['property_street'] . "\n";
  										if ($listing['property_street2']) {
  											$string .= '        ' . $listing['property_street2'] . "\n";
  										}
  										$string .= 'CITY: ' . $listing['property_city'] . "\n";
  										$string .= 'STATE: ' . $listing['property_st'] . "\n";
  										$string .= 'ZIP: ' . $listing['property_zip'];
  									}
  									$row = preg_replace("/<<property_info>>/", $string, $row);
  									
  									// figure out contact_info1 merge field
  									$string = 'EMAIL: ' . $listing['contact_email'] . "\n";
  									if ($listing['contact_phone1']) {
  										$string .= "PHONE 1 REVERSE LOOKUP: " . $listing['contact_phone1'];
  										if ($listing['phone1_dnc'] == 'T') {
  											$string .= '* (Do Not Call Registry)';
  										}
  									}
  									if ($listing['contact_name1']) {
  										$string .= "\n" . $listing['contact_name1'];
  									}
  									if ($listing['contact_street1']) {
  										$string .= "\n" . $listing['contact_street1'];
  									}
  									if ($listing['phone1_prefixfinder'] == 'Y') {
  										$string .= "\nArea Code/Prefix Lookup: (" . $listing['phone1_pf_areacode'] . ') ' . $listing['phone1_pf_prefix'] . '-xxxx (' . $listing['phone1_pf_city'] . ', ' . $listing['phone1_pf_st'] . ')';
  									}
  									if ($listing['contact_city1'] || $listing['contact_st1'] || $listing['contact_zip1']) {
  										$string .= "\n";
  										if ($listing['contact_city1']) {
  											$string .= $listing['contact_city1'] . ', ';
  										}
  										if ($listing['contact_st1']) {
  											$string .= $listing['contact_st1'] . ' ';
  										}
  										if ($listing['contact_zip1']) {
  											$string .= $listing['contact_zip1'];
  										}
  									}
										$row = preg_replace("/<<contact_info1>>/", $string, $row);
  									
  									$string = '';
  									if ($listing['contact_phone2']) {
  										$string .= "\n\nPHONE 2 REVERSE LOOKUP: " . $listing['contact_phone2'];
  										if ($listing['phone2_dnc'] == 'T') {
  											$string .= '* (Do Not Call Registry)';
  										}
  									}
  									if ($listing['contact_name2']) {
  										$string .= "\n" . $listing['contact_name2'];
  									}
  									if ($listing['contact_street2']) {
  										$string .= "\n" . $listing['contact_street2'];
  									}
  									if ($listing['phone2_prefixfinder'] == 'Y') {
  										$string .= "\nArea Code/Prefix Lookup: (" . $listing['phone2_pf_areacode'] . ') ' . $listing['phone2_pf_prefix'] . '-xxxx (' . $listing['phone2_pf_city'] . ', ' . $listing['phone2_pf_st'] . ')';
  									}
  									if ($listing['contact_city2'] || $listing['contact_st2'] || $listing['contact_zip2']) {
  										$string .= "\n";
  										if ($listing['contact_city2']) {
  											$string .= $listing['contact_city2'] . ', ';
  										}
  										if ($listing['contact_st2']) {
  											$string .= $listing['contact_st2'] . ' ';
  										}
  										if ($listing['contact_zip2']) {
  											$string .= $listing['contact_zip2'];
  										}
  									}
  									$row = preg_replace("/<<contact_info2>>/", $string, $row);
  
  									$email_body .= $row;
									}
									$listing_count++;
								}
							}
						}
					}
				}
				// if the account has listing fields get the generic body
				if (count($array_account_listings) > 0) {
					$body = $email_generic_body;
				}
				else if ($email_body) {
					$temp_email_top = $email_top;
					if ($account['account_type'] != 'TRIAL') {
						$temp_email_top = preg_replace("/<<company_address>>/", "105 South State St.  #204\nOrem, UT  84058", $temp_email_top);
					}
					$area_name_count_rows = '';
					foreach ($area_name_count as $area_name=>$count) {
						$area_name_count_rows .= $area_name . ': ' . $count . "\n";
					}
					$temp_email_top = str_replace("<<area_name_count_rows>>", $area_name_count_rows, $temp_email_top);
					$body = $temp_email_top . $email_body;
				}
				else {
					$body = $email_top_no_listings;
				}
				
				$body .= $email_bottom;
				
				// merge in company info
				$body = preg_replace("/<<company_url>>/", $account['company']['url'], $body);
				$body = preg_replace("/<<company_title>>/", $account['company']['title'], $body);
				$body = preg_replace("/<<date_range>>/", $date_range, $body);
				$body = preg_replace("/<<today>>/", date('F j, Y'), $body);
				$body = preg_replace("/<<today_weekday>>/", date('l'), $body);
				
				$boundary = '_____' . date('Ymd') . '_____';
				$from_email = ($account['company']['from_email']) ? $account['company']['from_email'] : 'fsbo@' . $account['company']['url'];
				$reply_to_email = ($account['company']['reply_to_email']) ? $account['company']['reply_to_email'] : 'info@' . $account['company']['url'];
				$headers = "From: " . $account['company']['title'] . " <" . $from_email . ">\r\n" . 
					"Reply-To: " . $reply_to_email . "\r\n" . 
					"MIME-Version: 1.0\r\n" . 
					"Content-Type: multipart/mixed; boundary=\"" . $boundary . "\""
				;
				
				$body = "--" . $boundary . "\r\n" . 
					"Content-Type: text/plain; charset=iso-8859-1\r\n" . 
					"Content-Transfer-Encoding: 8bit\r\n" . 
					"Content-Disposition: inline\r\n\r\n" . 
					$body . "\r\n"
				;
				
				if ($account['email_attachment'] && $email_attachment) {
					$body .= "--" . $boundary . "\r\n" . 
						"Content-Type: application/vnd.ms-excel; name=\"" . date('Y_m_d') . "_Leads.csv\"\r\n" . 
						"Content-Transfer-Encoding: base64\r\n" . 
						"Content-Disposition: inline; filename=\"" . date('Y_m_d') . "_Leads.csv\"\r\n\r\n" . 
						chunk_split(base64_encode($header_line . "\n" . $email_attachment)) . "\r\n"
					;
				}
				
				if (!$_TEST_MODE) {
					// handle the sending of the file to theredx
					if ($account['accountid'] == 1140 || $account['accountid'] == 6659) {
						if ($account['accountid'] == 1140) {
							$file_name = $_DOCUMENT_ROOT . '/files/' . date('Y_m_d_H_i') . '_FSBO_Leads_' . $account['accountid'] . '_' . $time_zone . '.csv';
						}
						else if ($account['accountid'] == 6659) {
							$file_name = $_DOCUMENT_ROOT . '/files/' . date('Y_m_d_H_i') . '_FRBO_Leads_' . $account['accountid'] . '_' . $time_zone . '.csv';
						}
						file_put_contents($file_name, "Lead_ID,Date,Data_Source,Area_ST,Area_Name,Description_Ad,Contact_Email,Contact_Phone1,Contact_Name1,Contact_Street1,Contact_City1,Contact_ST1,Contact_ZIP1,Contact_Phone2,Contact_Name2,Contact_Street2,Contact_City2,Contact_ST2,Contact_ZIP2,Square_Feet,Bedrooms,Bathrooms,Lot_Size,Year_Built,Garage,Price,Phone1_DNC,Phone1_PF_AreaCode,Phone1_PF_Prefix,Phone1_PF_City,Phone1_PF_ST,Phone2_DNC,Phone2_PF_AreaCode,Phone2_PF_Prefix,Phone2_PF_City,Phone2_PF_ST,Property_Street,Property_Street_2,Property_City,Property_ST,Property_ZIP,zip_confidence,zip_approximate,latitude,longitude,map_url,logo_url,Product\n" . $ftp_attachment);
						if (file_exists($file_name)) {
							system('/usr/bin/sha1sum ' . $file_name . ' > ' . $file_name . '.sha1');
							if (file_exists($file_name . '.sha1')) {
								system('/usr/bin/scp ' . $file_name . ' fsbo@fsbo1.theredx.com:');
								system('/usr/bin/scp ' . $file_name . '.sha1' . ' fsbo@fsbo1.theredx.com:');
								system('/usr/bin/scp ' . $file_name . ' fsbo@fsbo2.theredx.com:');
								system('/usr/bin/scp ' . $file_name . '.sha1' . ' fsbo@fsbo2.theredx.com:');
							}
						}
					}
					// handle the sending of the file to anyone with ftp_file set
					else if ($account['ftp_file']) {
						$file_name = $_DOCUMENT_ROOT . '/files/' . date('Y_m_d') . '_Leads_' . $account['accountid'] . '.csv';
						file_put_contents($file_name, $ftp_attachment, FILE_APPEND);
					}
				}
				
				$body .= "--" . $boundary . "--\r\n";
				$body = preg_replace("/<<[^>].+?>>/", "", $body);
				
				if ($noEmptyEmails && $emptyEmail) { // don't want to send an empty email so do nothing
				}
				else {
					file_put_contents($_BACKUP_FILE_NAME, "\t(" . date('H:i:s') . ") email ready\n", FILE_APPEND);
					if (!$_TEST_MODE) {
						file_put_contents($_BACKUP_FILE_NAME, "\t\tsending to: " . $account['main_email_address'] . "\n", FILE_APPEND);
						mail($account['main_email_address'], $date_range . " Leads for " . $account['first_name'] . ' ' . $account['last_name'], $body, $headers);
					}
					file_put_contents($_BACKUP_FILE_NAME, "\t(" . date('H:i:s') . ") email done\n", FILE_APPEND);
				}
			}

			$listingids = explode(",", $string_listingids);
			foreach ($listingids as $listingid) {
				if ($listingid) {
					$query = "UPDATE listing set processed=NOW() WHERE listingid='" . $listingid . "'";
					if (!$_TEST_MODE) {
						query( array('query'=>$query) );
					}
				}
			}

		}
		else {
			file_put_contents($_BACKUP_FILE_NAME, "error: could not connect to database\n", FILE_APPEND);
		}
		
		// end the process
		file_put_contents($_BACKUP_FILE_NAME, '##### ENDING: ' . date('H:i:s') . " #####\n\n", FILE_APPEND);
	}
	else {
		error_log('send_listings: failure');
	}
}
exit;

#####
// other functions
function db_quote($value = NULL) {
	global $db;
	if ($value) {
		if (get_magic_quotes_gpc()) {
			$value = stripslashes($value);
		}
		if (!is_numeric($value)) {
			$value = mysqli_real_escape_string($db, $value);
		}
	}
	else {
		$value = '';
	}
	return $value;
}

function get_server_path($args) {
	global $_DOCUMENT_ROOT;
	
	$file = preg_replace("/^\//", "", $args['file']);
	$file = preg_replace("/\s/", "", $file);
	if ($args['companyDirectory'] && file_exists($_DOCUMENT_ROOT . '/web/' . $args['companyDirectory'] . '/' . $file)) {
		return $_DOCUMENT_ROOT . '/web/' . $args['companyDirectory'] . '/' . $file;
	}
	else if ($args['companyParentDirectory'] && file_exists($_DOCUMENT_ROOT . '/web/' . $args['companyParentDirectory'] . '/' . $file)) {
		return $_DOCUMENT_ROOT . '/web/' . $args['companyParentDirectory'] . '/' . $file;
	}
	else if (file_exists($_DOCUMENT_ROOT . '/web/company_def/' . $file)) {
		return $_DOCUMENT_ROOT . '/web/company_def/' . $file;
	}
	else {
		include_once($_SERVER['DOCUMENT_ROOT'] . '/web/errorpage.php');
	}
} 
	
function query($args = NULL) {
	global $db;
	print $args['query'] . "\n";
	if (preg_match("/^select/i", $args['query'])) {
		if (!preg_match("/file_path/", $args['query'])) {
			//error_log($args['query']);
		}
		try {
			error_log($args['query']);
			$handle = mysqli_query($db, $args['query']);
			$results = array();
			$num_rows = mysqli_num_rows($handle);
			while ($row = mysqli_fetch_assoc($handle)) {
				array_push($results, $row);
			}
			mysqli_free_result($handle);
			return array($results, $num_rows);
		}
		catch (Exception $exception) {
			error_log($args['query']);
			throw $exception;
		}
	}
	else if (preg_match("/^insert/i", $args['query'])) {
		try {
			//error_log($args['query']);
			mysqli_query($db, $args['query']);
			return mysqli_insert_id($db);
		}
		catch (Exception $exception) {
			error_log($args['query']);
			throw $exception;
		}
	}
	else {
		try {
			//error_log($args['query']);
			mysqli_query($db, $args['query']);
			return 0;
		}
		catch (my_exception $exception) {
			error_log($args['query']);
			throw $exception;
		}
	}
}

?>

<?php
// if it is coming from web
if ($_GET['cmd'] == 'fork' && $_GET['time_zone']) {
	print 'success';
	exec('/usr/local/bin/php ' . $_SERVER['DOCUMENT_ROOT'] . '/scripts/send_listings.php ' . $_GET['cmd'] . ' ' . $_GET['time_zone'] . ' ' . $_SERVER['DOCUMENT_ROOT'] . ' > /dev/null &');
	exit;
}
else {
	// get the arguments
	$cmd = $_SERVER['argv'][1];
	$time_zone = $_SERVER['argv'][2];
	$_DOCUMENT_ROOT = $_SERVER['argv'][3];

	// only continue if the two values are set
	if ($cmd == 'fork' && $time_zone) {
		$_BACKUP_FILE_NAME = $_DOCUMENT_ROOT . '/scripts/backup/SEND_LISTINGS_' . date('Y-m-d') . '.' . $time_zone;
		
		// start the process
		file_put_contents($_BACKUP_FILE_NAME, '##### STARTING: ' . date('H:i:s') . " #####\n", FILE_APPEND);
		
		if (preg_match("/hoover/", $_SERVER['PWD'])) { // if on dev server (crude i know)
			mysql_connect('localhost', 'dahoover', '');
		}
		else {
			mysql_connect('localhost', 'root', 'mysql');
		}
		if (mysql_select_db('byownerdaily')) {
			$string_listingids = '';
			
			// get the states that should be going out based on time_zone
			$query = "SELECT state FROM state_time_zone WHERE time_zone='" . $time_zone . "' ORDER BY state";
			list($results, $num_rows) = query( array('query'=>$query) );
			$string_allowed_states = ',';
			foreach ($results as $row) {
				$string_allowed_states .= $row['state'] . ',';
			}
			file_put_contents($_BACKUP_FILE_NAME, "(" . date('H:i:s') . ") string_allowed_states: " . $string_allowed_states . "\n", FILE_APPEND);
			
			$where_plus = " AND (processed='' OR processed IS NULL OR processed='0000-00-00') ";
			$date_range = date('F j, Y');
			
			$query = "SELECT distinct(substring(area_name, 1, 2)) AS distinct_state FROM listing WHERE (processed='' OR processed IS NULL OR processed='0000-00-00')";
			list($r_listings, $num_rows) = query( array('query'=>$query) );
			$string_states = '';
			foreach ($r_listings as $r_listing) {
				// only add the state if it is in string_allowed_states
				if (preg_match("/," . $r_listing['distinct_state'] . ",/i", $string_allowed_states)) {
					$string_states .= "'" . $r_listing['distinct_state'] . "',";
				}
			}
			$string_states = preg_replace("/,$/", "", $string_states);
			$query = "SELECT DISTINCT(account.accountid) AS accountid FROM account, area_name_entry WHERE account.accountid=area_name_entry.accountid AND SUBSTRING(area_name_entry.area_name, 1, 2) IN (" . $string_states . ") AND account_type NOT LIKE '%admin%' AND billing_status IN ('active','queued') AND (start_date <= NOW() AND end_date >= NOW()) ORDER BY account.customer_number";
			file_put_contents($_BACKUP_FILE_NAME, "(" . date('H:i:s') . ") main query: " . $query . "\n", FILE_APPEND);

			$email_bottom = file_get_contents($_DOCUMENT_ROOT . '/web/company_def/office/listing/email_bottom.txt');
			$email_top_no_listings = file_get_contents($_DOCUMENT_ROOT . '/web/company_def/office/listing/email_top_no_listings.txt');
			$email_top_no_listings = preg_replace("/<<today>>/", date('F j, Y'), $email_top_no_listings);
			$email_top_no_listings = preg_replace("/<<today_weekday>>/", date('l'), $email_top_no_listings);
			$temp_email_top = file_get_contents($_DOCUMENT_ROOT . '/web/company_def/office/listing/email_top.txt');
			$temp_email_top = preg_replace("/<<date_range>>/", $date_range, $temp_email_top);
			$temp_email_top = preg_replace("/<<today>>/", date('F j, Y'), $temp_email_top);
			$temp_email_top = preg_replace("/<<today_weekday>>/", date('l'), $temp_email_top);
			$temp_row = file_get_contents($_DOCUMENT_ROOT . '/web/company_def/office/listing/email_row.txt');
			
			list($r_accounts, $num_rows) = query( array('query'=>$query) );
			foreach ($r_accounts as $r_account) {
				$area_name_count = array();
				$ftp_attachment = '';
				$email_body = '';
				$email_attachment = '';
			
				// set up the account info
				$query = "SELECT * FROM account WHERE accountid='" . $r_account['accountid'] . "'";
				list($i_account, $num_rows) = query( array('query'=>$query) );
				$account = $i_account[0];
				if ($account['accountid'] && $account['main_email_address']) {
					file_put_contents($_BACKUP_FILE_NAME, "(" . date('H:i:s') . ") account: " . $account['accountid'] . "\n", FILE_APPEND);
					$account['company'] = array();
					$account['list_area_names'] = array();
					$account['string_area_names'] = ',';
					$account['list_robot_names'] = array();
					$account['string_robot_names'] = ',';
					
					// get the account company info
					$query = "SELECT * FROM company WHERE companyid='" . $account['companyid'] . "'";
					list($i_company, $num_rows) = query( array('query'=>$query) );
					$account['company'] = $i_company[0];
					
					// get the account's areas
					$query = "SELECT * FROM area_name_entry WHERE accountid='" . $account['accountid'] . "' ORDER BY area_name";
					list($results, $num_rows) = query( array('query'=>$query) );
					foreach ($results as $row) {
						$account['list_area_names'][] = $row['area_name'];
						$account['string_area_names'] .= $row['area_name'] . ',';
					}
					
					// get the account's robots
					$query = "SELECT * FROM robot_name_entry WHERE accountid='" . $account['accountid'] . "' ORDER BY robot_name";
					list($results, $num_rows) = query( array('query'=>$query) );
					foreach ($results as $row) {
						$account['list_robot_names'][] = $row['robot_name'];
						$account['string_robot_names'] .= $row['robot_name'] . ',';
					}
				
					$string_area_names = '';
					foreach ($account['list_area_names'] as $area_name) {
						$area_state = substr($area_name, 0, 2);
						if ($area_name) {
							// if they have put in a time_zone then make sure this area can be included
							if (preg_match("/," . $area_state . ",/i", $string_allowed_states)) {
								$string_area_names .= "'" . db_quote($area_name) . "',";
								$area_name_count[$area_name] = 0;
							}
						}
					}
					$string_area_names = preg_replace("/,$/", "", $string_area_names);
					
					$robot_names = explode(",", $account['string_robot_names']);
					$string_robot_names = '';
					foreach ($robot_names as $robot_name) {
						if ($robot_name) {
							$string_robot_names .= "'" . $_SESSION['data_access']->db_quote($robot_name) . "',";
						}
					}
					$string_robot_names = preg_replace("/,$/", "", $string_robot_names);
					$robot_where_plus = '';
					if ($string_robot_names) {
						$robot_where_plus = " AND robot_name NOT IN (" . $string_robot_names . ") ";
					}
					
					file_put_contents($_BACKUP_FILE_NAME, "\t(" . date('H:i:s') . ") account data done\n", FILE_APPEND);
					
					if ($string_area_names) {
						$query = "SELECT distinct(area_name) as area_name FROM listing WHERE area_name IN (" . $string_area_names . ") " . $robot_where_plus . $where_plus . " ORDER BY area_name";
						list($r_area_names, $num_rows) = query( array('query'=>$query) );
						
						file_put_contents($_BACKUP_FILE_NAME, "\t(" . date('H:i:s') . ") area name done\n", FILE_APPEND);
						
						foreach ($r_area_names as $r_area_name) {
							$listing_count = 1;

							$query = "SELECT * FROM listing WHERE area_name='" . db_quote($r_area_name['area_name']) . "' " . $robot_where_plus . $where_plus . " ORDER BY date_posted desc";
							list($r_listings, $num_rows) = query( array('query'=>$query) );
							
							file_put_contents($_BACKUP_FILE_NAME, "\t(" . date('H:i:s') . ") - $query - listing done\n", FILE_APPEND);
							
							foreach ($r_listings as $listing) {
								if ($listing['listingid']) {
									// add to string_listingids to set processed date at end of process
									if (!preg_match("/" . $listing['listingid'] . ",/", $string_listingids)) {
										$string_listingids .= $listing['listingid'] . ',';
									}
									
									$area_name_count[$listing['area_name']]++; // add 1 to the total number of listings in this area_name
									// add attachment row
									$phone1_dnc = ($listing['phone1_dnc'] == 'T') ? '(Do Not Call Registry)' : '';
									$phone2_dnc = ($listing['phone2_dnc'] == 'T') ? '(Do Not Call Registry)' : '';
									
									$email_attachment .= '"' . $listing['lead_id'] . '","' . $listing['date_posted'] . '","' . $listing['data_source_namepublish'] . '","' . $listing['area_st'] . '","' . $listing['area_name'] . '","' . $listing['description_ad'] . '","' . $listing['contact_email'] . '","' . $listing['contact_phone1'] . '","' . $listing['contact_name1'] . '","' . $listing['contact_street1'] . '","' . $listing['contact_city1'] . '","' . $listing['contact_st1'] . '","' . $listing['contact_zip1'] . '","' . $listing['contact_phone2'] . '","' . $listing['contact_name2'] . '","' . $listing['contact_street2'] . '","' . $listing['contact_city2'] . '","' . $listing['contact_st2'] . '","' . $listing['contact_zip2'] . '","' . $listing['square_feet'] . '","' . $listing['bedrooms'] . '","' . $listing['bathrooms'] . '","' . $listing['lot_size'] . '","' . $listing['year_built'] . '","' . $listing['garage'] . '","' . $listing['price'] . '","' . $phone1_dnc . '","' . $listing['phone1_pf_areacode'] . '","' . $listing['phone1_pf_prefix'] . '","' . $listing['phone1_pf_city'] . '","' . $listing['phone1_pf_st'] . '","' . $phone2_dnc . '","' . $listing['phone2_pf_areacode'] . '","' . $listing['phone2_pf_prefix'] . '","' . $listing['phone2_pf_city'] . '","' . $listing['phone2_pf_st'] . '","' . $listing['property_street'] . '","' . $listing['property_street2'] . '","' . $listing['property_city'] . '","' . $listing['property_st'] . '","' . $listing['property_zip'] . "\"\n";
									$ftp_attachment .= '"' . $listing['lead_id'] . '","' . $listing['date_posted'] . '","' . $listing['data_source_namepublish'] . '","' . $listing['area_st'] . '","' . $listing['area_name'] . '","' . $listing['description_ad'] . '","' . $listing['contact_email'] . '","' . $listing['contact_phone1'] . '","' . $listing['contact_name1'] . '","' . $listing['contact_street1'] . '","' . $listing['contact_city1'] . '","' . $listing['contact_st1'] . '","' . $listing['contact_zip1'] . '","' . $listing['contact_phone2'] . '","' . $listing['contact_name2'] . '","' . $listing['contact_street2'] . '","' . $listing['contact_city2'] . '","' . $listing['contact_st2'] . '","' . $listing['contact_zip2'] . '","' . $listing['square_feet'] . '","' . $listing['bedrooms'] . '","' . $listing['bathrooms'] . '","' . $listing['lot_size'] . '","' . $listing['year_built'] . '","' . $listing['garage'] . '","' . $listing['price'] . '","' . $phone1_dnc . '","' . $listing['phone1_pf_areacode'] . '","' . $listing['phone1_pf_prefix'] . '","' . $listing['phone1_pf_city'] . '","' . $listing['phone1_pf_st'] . '","' . $phone2_dnc . '","' . $listing['phone2_pf_areacode'] . '","' . $listing['phone2_pf_prefix'] . '","' . $listing['phone2_pf_city'] . '","' . $listing['phone2_pf_st'] . '","' . $listing['property_street'] . '","' . $listing['property_street2'] . '","' . $listing['property_city'] . '","' . $listing['property_st'] . '","' . $listing['property_zip'] . '","' . $listing['zip_confidence'] . '","' . $listing['zip_approximate'] . '","' . $listing['latitude'] . '","' . $listing['longitude'] . '","' . $listing['map_url'] . '","' . $listing['logo_url'] . "\"\n";
									// add email row
									$row = $temp_row;
									$row = preg_replace("/<<count>>/", $listing_count, $row);
									$row = preg_replace("/<<data_source_namepublish>>/", $listing['data_source_namepublish'], $row);
									$row = preg_replace("/<<date_posted>>/", date('F j, Y', strtotime($listing['date_posted'])), $row);
									$row = preg_replace("/<<lead_id>>/", $listing['lead_id'], $row);
									$row = preg_replace("/<<area_name>>/", $listing['area_name'], $row);
									$row = preg_replace("/<<area_misc>>/", $listing['area_misc'], $row);
									// making hard returns and have to escape the $ sign for some reason
									$description_ad = preg_replace("/(.{65}.+? )/", "$1\n", $listing['description_ad']);
									$description_ad = str_replace("$", "\\$", $description_ad);
									$row = preg_replace("/<<description_ad>>/", $description_ad, $row);
									
									// figure out property_info merge field
									$string = '';
									if ($listing['property_street'] || $listing['property_city'] || $listing['property_zip']) {
										$string .= "Property Information\n";
										$string .= 'STREET: ' . $listing['property_street'] . "\n";
										if ($listing['property_street2']) {
											$string .= '        ' . $listing['property_street2'] . "\n";
										}
										$string .= 'CITY: ' . $listing['property_city'] . "\n";
										$string .= 'STATE: ' . $listing['property_st'] . "\n";
										$string .= 'ZIP: ' . $listing['property_zip'] . "\n\n";
									}
									$row = preg_replace("/<<property_info>>/", $string, $row);
									
									// figure out contact_info1 merge field
									$string = '';
									if ($listing['contact_phone1']) {
										$string .= "PHONE 1 REVERSE LOOKUP: " . $listing['contact_phone1'];
										if ($listing['phone1_dnc'] == 'T') {
											$string .= '* (Do Not Call Registry)';
										}
									}
									if ($listing['contact_name1']) {
										$string .= "\n" . $listing['contact_name1'];
									}
									if ($listing['contact_street1']) {
										$string .= "\n" . $listing['contact_street1'];
									}
									if ($listing['phone1_prefixfinder'] == 'Y') {
										$string .= "\nArea Code/Prefix Lookup: (" . $listing['phone1_pf_areacode'] . ') ' . $listing['phone1_pf_prefix'] . '-xxxx (' . $listing['phone1_pf_city'] . ', ' . $listing['phone1_pf_st'] . ')';
									}
									if ($listing['contact_city1'] || $listing['contact_st1'] || $listing['contact_zip1']) {
										$string .= "\n";
										if ($listing['contact_city1']) {
											$string .= $listing['contact_city1'] . ', ';
										}
										if ($listing['contact_st1']) {
											$string .= $listing['contact_st1'] . ' ';
										}
										if ($listing['contact_zip1']) {
											$string .= $listing['contact_zip1'];
										}
									}
									$row = preg_replace("/<<contact_info1>>/", $string, $row);
									
									$string = '';
									if ($listing['contact_phone2']) {
										$string .= "\n\nPHONE 2 REVERSE LOOKUP: " . $listing['contact_phone2'];
										if ($listing['phone2_dnc'] == 'T') {
											$string .= '* (Do Not Call Registry)';
										}
									}
									if ($listing['contact_name2']) {
										$string .= "\n" . $listing['contact_name2'];
									}
									if ($listing['contact_street2']) {
										$string .= "\n" . $listing['contact_street2'];
									}
									if ($listing['phone2_prefixfinder'] == 'Y') {
										$string .= "\nArea Code/Prefix Lookup: (" . $listing['phone2_pf_areacode'] . ') ' . $listing['phone2_pf_prefix'] . '-xxxx (' . $listing['phone2_pf_city'] . ', ' . $listing['phone2_pf_st'] . ')';
									}
									if ($listing['contact_city2'] || $listing['contact_st2'] || $listing['contact_zip2']) {
										$string .= "\n";
										if ($listing['contact_city2']) {
											$string .= $listing['contact_city2'] . ', ';
										}
										if ($listing['contact_st2']) {
											$string .= $listing['contact_st2'] . ' ';
										}
										if ($listing['contact_zip2']) {
											$string .= $listing['contact_zip2'];
										}
									}
									$row = preg_replace("/<<contact_info2>>/", $string, $row);

									$email_body .= $row;
									$listing_count++;
								}
							}
						}
					}
				}
				if ($email_body) {
					// need to find the right email_top
					if ($account['company']['directory'] && file_exists($_DOCUMENT_ROOT . '/web/' . $account['company']['directory'] . '/office/listing/email_top.txt')) {
						$email_top_path = $_DOCUMENT_ROOT . '/web/' . $account['company']['directory'] . '/office/listing/email_top.txt';
						$temp_email_top = file_get_contents($email_top_path);
						$temp_email_top = preg_replace("/<<date_range>>/", $date_range, $temp_email_top);
						$temp_email_top = preg_replace("/<<today>>/", date('F j, Y'), $temp_email_top);
						$temp_email_top = preg_replace("/<<today_weekday>>/", date('l'), $temp_email_top);
					}
					$email_top = $temp_email_top;
					$area_name_count_rows = '';
					foreach ($area_name_count as $area_name=>$count) {
						$area_name_count_rows .= $area_name . ': ' . $count . "\n";
					}
					$email_top = str_replace("<<area_name_count_rows>>", $area_name_count_rows, $email_top);
					$body = $email_top . $email_body;
				}
				else {
					if ($account['company']['directory'] && file_exists($_DOCUMENT_ROOT . '/web/' . $account['company']['directory'] . '/office/listing/email_top_no_listings.txt')) {
						$temp_email_top_no_listings = file_get_contents($_DOCUMENT_ROOT . '/web/' . $account['company']['directory'] . '/office/listing/email_top_no_listings.txt');
						$temp_email_top_no_listings = preg_replace("/<<today>>/", date('F j, Y'), $temp_email_top_no_listings);
						$temp_email_top_no_listings = preg_replace("/<<today_weekday>>/", date('l'), $temp_email_top_no_listings);
						$body = $temp_email_top_no_listings;
					}
					else {
						$body = $email_top_no_listings;
					}
				}
				
				// get the right email_bottom
				if ($account['company']['directory'] && file_exists($_DOCUMENT_ROOT . '/web/' . $account['company']['directory'] . '/office/listing/email_bottom.txt')) {
					$temp_email_bottom = file_get_contents($_DOCUMENT_ROOT . '/web/' . $account['company']['directory'] . '/office/listing/email_bottom.txt');
				}
				else {
					$temp_email_bottom = $email_bottom;
				}

				$body .= $temp_email_bottom;
				
				// merge in company info
				$body = preg_replace("/<<company_url>>/", $account['company']['url'], $body);
				
				$boundary = '_____' . date('Ymd') . '_____';
				$headers = "From: " . $account['company']['title'] . " <fsbo@" . $account['company']['url'] . ">\r\n" . 
					"Reply-To: info@" . $account['company']['url'] . "\r\n" . 
					"MIME-Version: 1.0\r\n" . 
					"Content-Type: multipart/mixed; boundary=\"" . $boundary . "\"\r\n" . 
					//"Content-Transfer-Encoding: 8bit\r\n\r\n" . 
					"--" . $boundary . "\r\n" . 
					"Content-Type: text/plain; charset=iso-8859-1\r\n" . 
					"Content-Transfer-Encoding: 8bit\r\n" . 
					"Content-Disposition: inline\r\n\r\n" . 
					$body . "\r\n"
				;
				
				if ($account['email_attachment'] && $email_attachment) {
					$headers .= "--" . $boundary . "\r\n" . 
						"Content-Type: application/vnd.ms-excel; name=\"" . date('Y_m_d') . "_FSBO_Leads.csv\"\r\n" . 
						"Content-Transfer-Encoding: base64\r\n" . 
						"Content-Disposition: inline; filename=\"" . date('Y_m_d') . "_FSBO_Leads.csv\"\r\n\r\n" . 
						chunk_split(base64_encode("Lead_ID,Date,Data_Source,Area_ST,Area_Name,Description_Ad,Contact_Email,Contact_Phone1,Contact_Name1,Contact_Street1,Contact_City1,Contact_ST1,Contact_ZIP1,Contact_Phone2,Contact_Name2,Contact_Street2,Contact_City2,Contact_ST2,Contact_ZIP2,Square_Feet,Bedrooms,Bathrooms,Lot_Size,Year_Built,Garage,Price,Phone1_DNC,Phone1_PF_AreaCode,Phone1_PF_Prefix,Phone1_PF_City,Phone1_PF_ST,Phone2_DNC,Phone2_PF_AreaCode,Phone2_PF_Prefix,Phone2_PF_City,Phone2_PF_ST,Property_Street,Property_Street_2,Property_City,Property_ST,Property_ZIP\n" . $email_attachment)) . "\r\n"
					;
				}
				
				// handle the sending of the file to theredx
				if ($account['accountid'] == 1140) {
					$file_name = $_DOCUMENT_ROOT . '/files/' . date('Y_m_d_H_i') . '_FSBO_Leads_' . $account['accountid'] . '_' . $time_zone . '.csv';
					file_put_contents($file_name, "Lead_ID,Date,Data_Source,Area_ST,Area_Name,Description_Ad,Contact_Email,Contact_Phone1,Contact_Name1,Contact_Street1,Contact_City1,Contact_ST1,Contact_ZIP1,Contact_Phone2,Contact_Name2,Contact_Street2,Contact_City2,Contact_ST2,Contact_ZIP2,Square_Feet,Bedrooms,Bathrooms,Lot_Size,Year_Built,Garage,Price,Phone1_DNC,Phone1_PF_AreaCode,Phone1_PF_Prefix,Phone1_PF_City,Phone1_PF_ST,Phone2_DNC,Phone2_PF_AreaCode,Phone2_PF_Prefix,Phone2_PF_City,Phone2_PF_ST,Property_Street,Property_Street_2,Property_City,Property_ST,Property_ZIP,zip_confidence,zip_approximate,latitude,longitude,map_url,logo_url\n" . $ftp_attachment);
					if (file_exists($file_name)) {
						system('/usr/bin/sha1sum ' . $file_name . ' > ' . $file_name . '.sha1');
						if (file_exists($file_name . '.sha1')) {
							system('/usr/bin/scp ' . $file_name . ' fsbo@fsbo1.theredx.com:');
							system('/usr/bin/scp ' . $file_name . '.sha1' . ' fsbo@fsbo1.theredx.com:');
							system('/usr/bin/scp ' . $file_name . ' fsbo@fsbo2.theredx.com:');
							system('/usr/bin/scp ' . $file_name . '.sha1' . ' fsbo@fsbo2.theredx.com:');
						}
					}
				}
				// handle the sending of the file to anyone with ftp_file set
				else if ($account['ftp_file']) {
					$file_name = $_DOCUMENT_ROOT . '/files/' . date('Y_m_d') . '_FSBO_Leads_' . $account['accountid'] . '.csv';
					file_put_contents($file_name, $ftp_attachment, FILE_APPEND);
				}
				
				$headers .= "--" . $boundary . "--\r\n";
				
				file_put_contents($_BACKUP_FILE_NAME, "\t(" . date('H:i:s') . ") email ready\n", FILE_APPEND);
				
				mail($account['main_email_address'], $date_range . " FSBO Leads for " . $account['first_name'] . ' ' . $account['last_name'], '', $headers, '-f fsbo@' . $account['company']['url']);

				file_put_contents($_BACKUP_FILE_NAME, "\t(" . date('H:i:s') . ") email done\n", FILE_APPEND);
			}

			$listingids = explode(",", $string_listingids);
			foreach ($listingids as $listingid) {
				if ($listingid) {
					$query = "UPDATE listing set processed=NOW() WHERE listingid='" . $listingid . "'";
					query( array('query'=>$query) );
				}
			}

		}
		else {
			file_put_contents($_BACKUP_FILE_NAME, "error: could not connect to database\n", FILE_APPEND);
		}
		
		// end the process
		file_put_contents($_BACKUP_FILE_NAME, '##### ENDING: ' . date('H:i:s') . " #####\n\n", FILE_APPEND);
	}
	else {
		error_log('send_listings: failure');
	}
}
exit;

#####
// other functions
function db_quote($value = NULL) {
	if ($value) {
		if (get_magic_quotes_gpc()) {
			$value = stripslashes($value);
		}
		if (!is_numeric($value)) {
			$value = mysql_real_escape_string($value);
		}
	}
	else {
		$value = '';
	}
	return $value;
}

function query($args = NULL) {
	print $args['query'] . "\n";
	if (preg_match("/^select/i", $args['query'])) {
		if (!preg_match("/file_path/", $args['query'])) {
			//error_log($args['query']);
		}
		try {
			$handle = mysql_query($args['query']);
			$results = array();
			$num_rows = mysql_num_rows($handle);
			while ($row = mysql_fetch_assoc($handle)) {
				array_push($results, $row);
			}
			mysql_free_result($handle);
			return array($results, $num_rows);
		}
		catch (Exception $exception) {
			throw $exception;
		}
	}
	else if (preg_match("/^insert/i", $args['query'])) {
		try {
			//error_log($args['query']);
			mysql_query($args['query']);
			return mysql_insert_id();
		}
		catch (Exception $exception) {
			throw $exception;
		}
	}
	else {
		try {
			//error_log($args['query']);
			mysql_query($args['query']);
			return 0;
		}
		catch (my_exception $exception) {
			throw $exception;
		}
	}
}

?>

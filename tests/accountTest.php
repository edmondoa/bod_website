<?php
#
# @copyright Copyright (c) 2017 Green Seed Technologies, Inc. All rights reserved.
#
# @author David McLaughlin <david@greenseedtech.com>
#

session_start();
require_once(__DIR__.'/shared.php');
require_once('office/account/class.account.php');
require_once('common/class.data_access.php');
$_SESSION['data_access'] = new data_access();
require_once('common/class.web_interface.php');
$_SESSION['web_interface'] = new web_interface();

/**
 * @backupGlobals disabled
 */
class accountTest extends \PHPUnit_Framework_TestCase
{
	public static function testDumpToken()
	{
		static::assertTrue(true);
		$accountId = getenv('ACCOUNTID');
		if (!empty($accountId)) {
			if ($accountId == 'ALL') {
				$db = getDbConnection();
				foreach ($db->getValues(
					'select accountid from byownerdaily.account', '', 1)
						as $accountId) {
					try {
						$account = new account(['accountid' => $accountId]);
						print "$accountId ".$account->getToken()."\n";
					} catch(cardvaultsdk\VaultException $exception) {
						print "exception dealing with $accountId\n";
					}
				}
			} else {
				$account = new account(['accountid' => $accountId]);
				var_dump($account->getToken());
			}
		}
	}

	public static function testDuplicate()
	{
		$account = new account(['main_email_address'=>'bogushartman.pratte@gmail.com']);
		static::assertEquals(0, $account->get_accountid());
		$account = new temporaryaccount();
		$account->initialize(['main_email_address'=>'bogushartman.pratte@gmail.com']);
		$account->create();
		$account = new account(['main_email_address'=>'bogushartman.pratte@gmail.com']);
		static::assertNotEquals(0, $account->get_accountid());
	}

	public static function testConstruct()
	{
		$account = new temporaryaccount();
		static::assertTrue($account instanceof account);
	}

	public static function testCreate()
	{
		#$_SESSION['data_access'] = new data_access();
		#$_SESSION['web_interface'] = $webInterface = new web_interface();
		$account = new temporaryaccount([
				'card_number' => $_SESSION['web_interface']->getEncrypt(CREDITCARD_TEST_NUMBER),
				'card_exp_date' => 1249,
				'rate' => 0.41,
				'subscription_type' => 'Monthly',
			]);
		$account->create();
	}

	public static function testActivateAccount()
	{
		# this is for reactivating an account
		#$_SESSION['data_access'] = new data_access();
		#$_SESSION['web_interface'] = $webInterface = new web_interface();
		$account = new temporaryaccount([
				'card_number' => $_SESSION['web_interface']->getEncrypt(CREDITCARD_TEST_NUMBER),
				'card_exp_date' => 1249,
				'rate' => 0.42,
				'subscription_type' => 'Monthly',
			]);
		$account->create();
		$db = getDbConnection();
		$account->activate_account([], '', 0);
		try {
			$amount = $db->getValue('select amount from byownerdaily.billing'
				.' where accountid = '.(int)$account->get_accountid(), '', 0);
			static::assertEquals(0.42, $amount);
		} catch (Exception $exception) {
			var_dump($exception);
		}
	}

	public static function testDecline()
	{
		# this is for reactivating an account
		#$_SESSION['data_access'] = new data_access();
		#$_SESSION['web_interface'] = $webInterface = new web_interface();
		$account = new temporaryaccount([
				'card_number' => $_SESSION['web_interface']->getEncrypt(CREDITCARD_TEST_DECLINED_NUMBER),
				'card_exp_date' => 1249,
				'rate' => 0.42,
				'subscription_type' => 'Monthly',
			]);
		$account->create();
		$db = getDbConnection();
		try {
			$account->activate_account([], '', 0);
			static::assertTrue(false);
		} catch (Exception $exception) {
			static::assertFalse(false);
		}
	}

	public static function testCancelByAccount()
	{
	}

	public static function testContinueSubscription()
	{
	}

	public static function testReprocessRcreditCard()
	{
		#this is a big one
	}
}

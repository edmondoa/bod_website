<?php
#
# @copyright Copyright (c) 2017 Green Seed Technologies, Inc. All rights reserved.
#
# @author David McLaughlin <david@greenseedtech.com>
#

require_once(__DIR__.'/shared.php');
require_once('common/class.authorize_net.php');
require_once('cardvaultsdk/cardvaultapi.php');

/**
 * @backupGlobals disabled
 */
class authorizeNetTest extends \PHPUnit_Framework_TestCase
{
	public static function testOneTimeCharge()
	{
		$storeResult = cardvaultsdk\store([
			'number' => CREDITCARD_TEST_NUMBER,
			'expiration_date' => CREDITCARD_TEST_EXPIRATION_DATE,
			'name' => CREDITCARD_TEST_NAME,
			'street' => CREDITCARD_TEST_STREET,
			'zip' => CREDITCARD_TEST_ZIP,
			], '', 0);
		$authorize_net = new \authorize_net();
		$result = $authorize_net->one_time_charge([
			'x_token' => $storeResult->token,
			#'x_card_num' => 4007000000027,
			#'x_card_num' => 4012888818888,
			#'x_card_num' => 5424000000000015,
			# 'x_card_num' => 4222222222222,
			'x_exp_date' => 1249,
			'x_amount' => rand(1, 1000)
			]);
		cardvaultsdk\delete(['token' => $storeResult->token], '', 0);
		static::assertTrue(!empty($result['approval_code']));
	}
}

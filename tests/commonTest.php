<?php
#
# @copyright Copyright (c) 2015--2017 Green Seed Technologies, Inc. All rights reserved.
#
# @author David McLaughlin <david@greenseedtech.com>
#

require_once(__DIR__.'/shared.php');
require_once('common/common.php');

/**
 * @backupGlobals disabled
 */
class commonTest extends PHPUnit_Framework_TestCase
{
	public function testCCMask()
	{
		foreach ([
			"select NUMBER_ID from REDX.CC_NUMBERS where NUMBER='J07HJAkDI2xbV4iVC+/vWBGvMxSxqHFriXLQfSE812Q='",
			"update REDX.CC_NUMBERS set NUMBER='J07HJAkDI2xbV4iVC+/vWBGvMxSxqHFriXLQfSE812Q=' where NUMBER_ID=42",
			# "insert into REDX.CC_NUMBERS (NUMBER) values ('J07HJAkDI2xbV4iVC+/vWBGvMxSxqHFriXLQfSE812Q=')",
			] as $query) {
			$masked = mask($query);
			static::assertEquals(1, preg_match('|J07HJAkDI2xbV4iVC\+/vWBGvMxSxqHFriXLQfSE812Q=|', $query));
			static::assertNotEquals(1, preg_match('|J07HJAkDI2xbV4iVC\+/vWBGvMxSxqHFriXLQfSE812Q=|', $masked));
		}
	}

	public function testMask()
	{
		foreach ([
			'sftp://username:password@company.com:/resource/',
			'',
			'http://username:password@company.com',
			'http://username:password@company.com/',
			'http://username@company.com/',
			'http://company.com/',
			] as $url) {
			$masked = mask($url);
			static::assertNotEquals(1, preg_match('/password/', $masked));
			static::assertEquals(str_replace('password', 'xxxxxxxx', $url), $masked);
		}

		foreach ([CREDIT_CARD_NUMBER_APPROVED,
			CREDIT_CARD_NUMBER_APPROVED.'',
			] as $number) {
			$o = new stdClass();
			$o->number = $number;
			$o = json_encode($o);
			static::assertEquals(1, preg_match('/'.CREDIT_CARD_NUMBER_APPROVED.'/', print_r($o, true)));
			$o = mask($o);
			static::assertNotEquals(1, preg_match('/'.CREDIT_CARD_NUMBER_APPROVED.'/', print_r($o, true)), $o);
		}
		foreach ([CREDIT_CARD_CARDCODE_TEST,
			CREDIT_CARD_CARDCODE_TEST.'',
			] as $number) {
			$o = new stdClass();
			$o->cardcode = $number;
			$o = json_encode($o);
			static::assertEquals(1, preg_match('/'.CREDIT_CARD_CARDCODE_TEST.'/', print_r($o, true)));
			$o = mask($o);
			static::assertNotEquals(1, preg_match('/'.CREDIT_CARD_CARDCODE_TEST.'/', print_r($o, true)));
		}
	}

	public static function getTestKeys()
	{
		return [
			openssl_pkey_get_private('file://'.__DIR__.'/testkey.key'),
			openssl_pkey_get_public('file://'.__DIR__.'/testkey.pub'),
			];
	}

/*
	public static function testGetSession()
	{
		$barf =& getSession('barf');
		$barf = 42;
		global $_SESSION;
		static::assertEquals(42, $_SESSION['barf']);
	}
	*/

	public static function testIsNull()
	{
		static::assertTrue(is_null(null));
		static::assertFalse(is_null(0));
		static::assertFalse(is_null(''));
	}

/*
	public function test_get_safe()
	{
		$o = ['phone' => '8017225863'];
		self::assertTrue(get_safe($o, 'phone') == 8017225863);
		$o = new \stdClass;
		$o->phone = '8017225863';
		self::assertTrue(get_safe($o, 'phone') == 8017225863);
		self::assertNull(get_safe(null, 'phone'));
	}
	*/
}

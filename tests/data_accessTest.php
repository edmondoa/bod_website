<?php
#
# @copyright Copyright (c) 2017 Green Seed Technologies, Inc. All rights reserved.
#
# @author David McLaughlin <david@greenseedtech.com>
#

require_once(__DIR__.'/shared.php');
require_once('common/class.data_access.php');

/**
 * @backupGlobals disabled
 */
class data_accessTest extends \PHPUnit_Framework_TestCase
{
	public static function testQuote()
	{
		$db = static::getDbConnector();
		static::assertEquals('hello world', $db->db_quote('hello world'));
	}

	private static function getDbConnector()
	{
		return new data_access(['db_name' => 'regression_tests']);
	}

	public static function testConstruct()
	{
		$db = static::getDbConnector();
		static::assertTrue($db instanceof data_access);
	}

	public static function testCreateTable()
	{
		$db = static::getDbConnector();
		static::assertTrue($db instanceof data_access);
		$db->query(['query' => 'create temporary table t ( a int )']);
	}

	public static function testSelect()
	{
		$db = static::getDbConnector();
		static::assertTrue($db instanceof data_access);
		$result = $db->query(['query' => 'select 42 "Answer to the Ultimate Question of Life, the Universe, and Everything"']);
		static::assertEquals(42, $result[0][0]['Answer to the Ultimate Question of Life, the Universe, and Everything']);
	}
}

<?php
#
# @copyright Copyright (c) 2015--2017 Green Seed Technologies, Inc. All rights reserved.
#
# @author David McLaughlin <david@greenseedtech.com>
#

require_once(__DIR__.'/shared.php');
require_once('common/db.php');
require_once('common/common.php');

/**
 * @backupGlobals disabled
 */
class dbTest extends \PHPUnit_Framework_TestCase
{
	public static function testNullSafe()
	{
		$db = static::getDbConnection();
		$value = $db->getValue('select "a" <=> null', '', 0);
		static::assertEquals(0, $value);
		$value = $db->getValue('select not "a" <=> null', '', 0);
		static::assertEquals(1, $value);
		$value = $db->getValue('select !("a" <=> null)', '', 0);
		static::assertEquals(1, $value);
	}

	public static function testBoolean()
	{
		$db = static::getDbConnection();
		$false = $db->getValue('select curdate() > curdate() + interval 1 day', '', 0);
		static::assertEquals(0, $false);
		$true = $db->getValue('select curdate() < curdate() + interval 1 day', '', 0);
		static::assertEquals(1, $true);
	}

	public static function testMax()
	{
		# max and min work for strings.
		$db = static::getDbConnection();
		$db->query('create temporary table regression_tests.t(CUSTOMER_ID int, FIRST_NAME varchar(255))', '', 0);
		$db->query('insert into regression_tests.t values(41, "jon")', '', 0);
		$db->query('insert into regression_tests.t values(41, "jane")', '', 0);
		$db->query('insert into regression_tests.t values(42, "jane")', '', 0);
		$db->query('insert into regression_tests.t values(42, "alfred")', '', 0);
		$max = $db->getValue('select max(FIRST_NAME) from regression_tests.t', '', 0);
		$min = $db->getValue('select min(FIRST_NAME) from regression_tests.t', '', 0);
		static::assertEquals('alfred', $min);
		static::assertEquals('jon', $max);
	}

	public static function testGroupBy()
	{
		$db = static::getDbConnection();
		$db->query('create temporary table regression_tests.t(CUSTOMER_ID int, FIRST_NAME varchar(255))', '', 0);
		$db->query('insert into regression_tests.t values(41, "jon")', '', 0);
		$db->query('insert into regression_tests.t values(41, "jane")', '', 0);
		$db->query('insert into regression_tests.t values(42, "jane")', '', 0);
		$db->query('insert into regression_tests.t values(42, "alfred")', '', 0);
		# You cannot use order by to "pick the right rows" in an aggregate group functions,
		# because MySQL does the order by after it has done the group by
		#$rows = $db->getRows('select * from regression_tests.t group by CUSTOMER_ID order by FIRST_NAME');
		# this is not true. we cannot guarantee order when using group by.
		#foreach ($rows as $row) {
		#	$name = $row['FIRST_NAME'];
		#	switch($row['CUSTOMER_ID']) {
		#		case 41:
		#			static::assertEquals('jane', $name);
		#			break;
		#		case 42:
		#			static::assertEquals('alfred', $name);
		#			break;
		#	}
		#}
		# You can, however, use an aggregate function, like min, to "pick the right rows" with group by,
		# because MySQL does the min for each row it is selecting
		$rows = $db->getRows('select CUSTOMER_ID, min(FIRST_NAME) FIRST_NAME from regression_tests.t group by CUSTOMER_ID', '', 0);
		# this is not true. we cannot guarantee order when using group by.
		foreach ($rows as $row) {
			$name = $row['FIRST_NAME'];
			switch($row['CUSTOMER_ID']) {
				case 41:
					static::assertEquals('jane', $name);
					break;
				case 42:
					static::assertEquals('alfred', $name);
					break;
			}
		}
	}

	public static function testLimit0()
	{
		$db = static::getDbConnection();
		$db->query('create temporary table regression_tests.t(CUSTOMER_ID int, first_name varchar(255), USERNAME varchar(255), unique index(CUSTOMER_ID))', '', 0);
		$db->query('insert into regression_tests.t values(42, "immutable", "someusername")', '', 0);
		$rows = $db->getRows('select * from regression_tests.t limit 0', '', 0);
		static::assertTrue(is_array($rows));
		static::assertEquals(0, count($rows));
	}

	public static function testOrderBy()
	{
		$db = static::getDbConnection();
		$id = $db->getValue('SELECT id from ('
									.' select "42" id'
									.' union select "3" id'
									.' union select "75" id'
									.' union select "2" id'
									.' union select "7" id'
									.' union select "3" id'
									.')t where id in(15, 32, 2, 75, 42, 11, 13)'
									.' order by field(id, 15, 32, 2, 75, 42, 11, 13)', '', 0);
		static::assertEquals(2, $id);

		$id = $db->getValue('SELECT id from ('
									.' select "42" id'
									.' union select "3" id'
									.' union select "75" id'
									.' union select "2" id'
									.' union select "7" id'
									.' union select "3" id'
									.')t'
									.' order by field(id, 15, 32, 2, 75, 42, 11, 13, id)', '', 0);
		static::assertEquals(2, $id);
	}

	public static function testNumber()
	{
		$db = static::getDbConnection();
		$value = $db->getValue('select 234.3e10', '', 0);
		static::assertEquals(2343000000000, $value);
		$value = $db->getValue('select +234.3e10', '', 0);
		static::assertEquals(2343000000000, $value);
		$value = $db->getValue('select -234.3e10', '', 0);
		static::assertEquals(-2343000000000, $value);
		$value = $db->getValue('select -234.3e+10', '', 0);
		static::assertEquals(-2343000000000, $value);
		$value = $db->getValue('select -234.3e-10', '', 0);
		static::assertEquals(-0.00000002343, $value);
	}

/*
	public static function testIsTestingEmail()
	{
		$db = static::getDbConnection();
		static::assertEquals(1, $db->getValue('select count(*) from (select "test1481063353701330@regression_tests.com" email)t where email '.EMAIL_ADDRESS_TESTING_QUERY, '', 0));
		static::assertEquals(0, $db->getValue('select count(*) from (select "test1481063353701330@regression_tests.com" email)t where email not '.EMAIL_ADDRESS_TESTING_QUERY, '', 0));
	}
	*/

	public function testMatchedRows()
	{
		$db = static::getDbConnection();
		$db->query('create temporary table regression_tests.t(CUSTOMER_ID int, first_name varchar(255), USERNAME varchar(255), unique index(CUSTOMER_ID))', '', 0);
		$db->query('insert into regression_tests.t values(42, "immutable", "someusername")', '', 0);
		$db->query('update regression_tests.t set first_name = "Jon" where CUSTOMER_ID = 42', '', 0);
		$matchedRows = $db->getMatchedRows('', 0);
		$affectedRows = $db->getAffectedRows();
		$db->query('update regression_tests.t set first_name = "Jon" where CUSTOMER_ID = 42', '', 0);
		$matchedRows2 = $db->getMatchedRows('', 0);
		$affectedRows2 = $db->getAffectedRows();
		self::assertEquals(1, $affectedRows);
		self::assertEquals(0, $affectedRows2);
		self::assertEquals(1, $matchedRows);
		self::assertEquals(1, $matchedRows2);
	}

	public function testAffectedRows()
	{
		$db = static::getDbConnection();
		$db->query('create temporary table regression_tests.t(CUSTOMER_ID int, first_name varchar(255), USERNAME varchar(255), unique index(CUSTOMER_ID))', '', 0);
		$db->query('insert into regression_tests.t values(42, "immutable", "someusername")', '', 0);
		$db->query('update regression_tests.t set first_name = "Jon" where CUSTOMER_ID = 42', '', 0);
		$affectedRows = $db->getAffectedRows();
		$db->query('update regression_tests.t set first_name = "Jon" where CUSTOMER_ID = 42', '', 0);
		$affectedRows2 = $db->getAffectedRows();
		self::assertEquals(1, $affectedRows);
		self::assertEquals(0, $affectedRows2);
	}

	public static function testUpdate()
	{
		$db = static::getDbConnection();
		$createResult = $db->query('create temporary table regression_tests.dbTest(a int not null AUTO_INCREMENT primary key)', '', 0);
		$insertResult = $db->query('insert into regression_tests.dbTest values(32)', '', 0);
		$updateResult = $db->query('update regression_tests.dbTest set a = 42', '', 0);
		$dropResult = $db->query('drop table if exists regression_tests.dbTest', '', 0);
		static::assertTrue($createResult);
		static::assertTrue($insertResult);
		static::assertTrue($updateResult);
		static::assertTrue($dropResult);
		$createResult = $db->query('create temporary table regression_tests.t(a int, unique index(a))', '', 0);
		static::assertTrue($createResult);
		$version = $db->getValue('select @@version', '', 0);
		if ($version > '5.5') {
			$insertResult = $db->query('insert ignore into regression_tests.t values(1),(2),(32),(32),(4)', '', 0);
			$count = $db->getValue('select count(*) from regression_tests.t', '', 0);
			static::assertTrue($insertResult);
			static::assertEquals(4, $count);
		} else {
			$insertResult = $db->query('insert into regression_tests.t values(1),(2),(32),(32),(4)', '', 0);
			$count = $db->getValue('select count(*) from regression_tests.t', '', 0);
			static::assertFalse($insertResult);
			static::assertEquals(3, $count);
		}
	}

	public static function testLastInsertId()
	{
		$db = static::getDbConnection();
		$createResult = $db->query('create temporary table regression_tests.dbTest(a int not null AUTO_INCREMENT primary key)', '', 0);
		$result = $db->query('insert into regression_tests.dbTest values(null)', '', 0);
		$insertId = $db->getInsertId();
		$dropResult = $db->query('drop table if exists regression_tests.dbTest', '', 0);
		static::assertTrue($createResult);
		static::assertTrue($dropResult);
		static::assertTrue($result);
		static::assertTrue($insertId != 0);
	}

	public static function testCurDate()
	{
		$db = static::getDbConnection();
		$result = $db->quote('CurDate()');
		static::assertEquals(' CurDate() ', $result);
		$result = $db->quote('CurDate( )');
		static::assertEquals(' CurDate( ) ', $result);
	}

	public static function testNow()
	{
		$db = static::getDbConnection();
		$result = $db->quote('NOW()');
		static::assertEquals(' NOW() ', $result);
		$result = $db->quote('NOW( )');
		static::assertEquals(' NOW( ) ', $result);
	}

	public static function testGetRow()
	{
		$db = static::getDbConnection();
		$result = $db->getRow('select 42 ID', '', 0);
		static::assertTrue(is_array($result));
		static::assertEquals(42, $result['ID']);
	}

	public static function testQuoteDatabase()
	{
		static::assertEquals('`hello`', db::quoteDatabase('hello'));
		static::assertEquals('hello', db::quoteDatabase('hello', false));
		static::assertEquals('`he\`llo`', db::quoteDatabase('he`llo'));
	}

	public static function testQuoteTable()
	{
		static::assertEquals('`hello`', db::quoteTable('hello'));
		static::assertEquals('hello', db::quoteTable('hello', false));
		static::assertEquals('`he\`llo`', db::quoteTable('he`llo'));
	}

	public static function testQuote()
	{
		$db = static::getDbConnection();
		$result = $db->quote("hello world");
		static::assertEquals("'hello world'", $result);
		$result = $db->quote(null);
		static::assertEquals(' null ', $result);
		$result = $db->quote("\n", false);
		static::assertEquals('\n', $result);
		$result = $db->quote("\r", false);
		static::assertEquals('\r', $result);
		foreach (['+', '>', '*', '&', '(', '{', ] as $symbol) {
			$result = $db->quote($symbol, false);
			static::assertEquals($symbol, $result);
		}
		foreach (['\0', "'", '"', '\z', '\\', ] as $symbol) {
			$result = $db->quote($symbol, false);
			static::assertEquals('\\'.$symbol, $result);
		}
		/*
		# this was just to see what was being returned from mysql's escape.
		for ($i = 0; $i < 128; ++$i) {
			$symbol = chr($i);
			$result = $db->quote($symbol, false);
			if ($symbol != $result) {
				print "$i\n";
				var_dump($symbol);
			}
		}
		*/
	}

	public function test_emptyrows()
	{
		$db = static::getDbConnection();
		$rows = $db->getRows('select 42 limit 0', '', 0);
		self::assertTrue(is_array($rows));
		self::assertEquals(0, count($rows));
	}

	public function test_get_row_partb()
	{
		$db = static::getDbConnection();
		$db->getValue('select 42', '', 0);
	}

	# this regression_tests looks a bit strange. I am selecting a string, but if you leave off quotes it is a problem.
	public function test_forgot_quotes_syntax_error()
	{
		$db = static::getDbConnection();
		try {
			$db->getValues('select some_string_without_quotes', '', 0);
			static::assertTrue(false);
		} catch (\Exception $exception) {
			static::assertEquals(1, preg_match('/'.ER_BAD_FIELD_ERROR.'/',
				$exception->internal_message), $exception->internal_message);
		}
	}

	public function test_getValues()
	{
		$db = static::getDbConnection();
		$result = $db->getValues('select 42', '', 0);
		self::assertTrue(is_array($result));
		foreach ($result as $cust_id) {
			self::assertTrue(is_numeric($cust_id));
		}
	}

	public function test_connect()
	{
		$db = static::getDbConnection();
		$result = $db->getValue('select 42', '', 0);
		self::assertEquals($result, 42);
	}

	public function test_unknown_host()
	{
		try {
			new db('bogosnamethisnotdefined', null, null, '', 0);
			static::assertTrue(false);
		} catch (\Exception $exception) {
			static::assertTrue(
				preg_match('/'.CR_UNKNOWN_HOST.'/',
					$exception->internal_message) ||
				preg_match('/'.CR_CONNECTION_ERROR.'/',
					$exception->internal_message)
					);
		}
	}

	public function test_syntax_error()
	{
		$db = static::getDbConnection();
		try {
			$db->getValue('select 42 *&(*&)', '', 0);
		} catch (\Exception $exception) {
			static::assertEquals(1,
				preg_match('/'.ER_PARSE_ERROR.'/',
					$exception->internal_message),
					$exception->internal_message
					);
		}
	}

	public function test_getValue()
	{
		$db = static::getDbConnection();
		$result = $db->getValue('select 42', '', 0);
		self::assertEquals(42, $result);
	}

	public function test_no_match_getValue()
	{
		$db = static::getDbConnection();
		$db->query('create temporary table regression_tests.t(a int)', '', 0);
		try {
			$db->getValue('select 42 from regression_tests.t where a = -1', '', 0);
			static::assertTrue(false);
		} catch (\Exception $exception) {
			static::assertEquals(1,
				preg_match('/getRows failed/', $exception->internal_message),
				$exception->internal_message);
		}
	}

	public function test_getValue_when_none_exists()
	{
		$db = static::getDbConnection();
		$db->query('create temporary table regression_tests.t(a int)', '', 0);
		try {
			$db->getValue('select 42 from regression_tests.t where a = -1 limit 1', '', 0);
			static::assertTrue(false);
		} catch (\Exception $exception) {
			static::assertEquals(1,
				preg_match('/getRows failed/', $exception->internal_message),
				$exception->internal_message);
		}
	}

	public function test_getValue_null()
	{
		$db = static::getDbConnection();
		$result = $db->getValue('select null', '', 0);
		self::assertNull($result);
		self::assertNotEquals($result, 42);
	}

	public function test_getValue_false()
	{
		$db = static::getDbConnection();
		$result = $db->getValue('select false', '', 0);
		self::assertNotNull($result);
		self::assertNotEquals($result, 42);
		self::assertEquals($result, 0);
	}

	public function test_quote()
	{
		$customer_id = '@&#*&$*&#1``2123';
		$db = static::getDbConnection();
		$db->query('create temporary table regression_tests.t(a int)', '', 0);
		$result = $db->getValues('select a from regression_tests.t where a = '.$db->quote($customer_id), '', 0);
		self::assertEquals(count($result), 0);
	}

	public function test_getRows()
	{
		$db = static::getDbConnection();
		$db->query('create temporary table regression_tests.t(CUSTOMER_ID int, first_name varchar(255), USERNAME varchar(255))', '', 0);
		$db->query('insert into regression_tests.t values(42, "immutable", "someusername")', '', 0);
		$db->query('insert into regression_tests.t values(52, "immutable", "someusername")', '', 0);
		$result = $db->getRows('select CUSTOMER_ID, USERNAME from regression_tests.t where first_name like "immutable%"', '', 0);
		self::assertTrue(is_array($result));
		self::assertTrue(count($result) > 1);
		self::assertTrue(array_key_exists('CUSTOMER_ID', $result[0]));
		self::assertTrue($result[0]['CUSTOMER_ID'] > 1);
	}

	public function test_get_row()
	{
		$db = static::getDbConnection();
		$result = $db->getRow('select 42 bob', '', 0);
		self::assertTrue(is_array($result));
		self::assertTrue(count($result) == 1);
		foreach ($result as $key => $value) {
			self::assertEquals($key, 'bob');
			self::assertEquals($value, 42);
		}
	}

	public function test_get_row_failure()
	{
		$db = static::getDbConnection();
		$db->query('create temporary table'
			.' regression_tests.t(CUSTOMER_ID int,'
			.' first_name varchar(255),'
			.' USERNAME varchar(255))', '', 0);
		try {
			$db->getRow('select 42 bob from regression_tests.t where customer_id = -1', '', 0);
			static::assertTrue(false);
		} catch (\Exception $exception) {
			static::assertEquals(1,
				preg_match('/getRow failed to match any rows/',
					$exception->internal_message),
				$exception->internal_message);
		}
	}

	public function test_duplicate_key()
	{
		$db = static::getDbConnection();
		$db->query('create temporary table regression_tests.t(CUSTOMER_ID int, first_name varchar(255), USERNAME varchar(255), unique index(CUSTOMER_ID))', '', 0);
		$db->query('insert into regression_tests.t values(42, "immutable", "someusername")', '', 0);
		$result = $db->query('insert ignore into regression_tests.t (CUSTOMER_ID) values(42)', '', 0);
		self::assertEquals(0, $db->getAffectedRows());
	}

	private static function getDbConnection()
	{
		require_once(__DIR__.'/config.php');
		global $DB_HOST;
		global $DB_USER;
		global $DB_PASS;
		return new db($DB_HOST, $DB_USER, $DB_PASS, 'An error occured during testing.', 0);
	}
}

<?php
#
# @copyright Copyright (c) 2017 Green Seed Technologies, Inc. All rights reserved.
#
# @author David McLaughlin <david@greenseedtech.com>
#

require_once(__DIR__.'/shared.php');
require_once('common/log.php');

/**
 * @backupGlobals disabled
 */
class logTest extends \PHPUnit_Framework_TestCase
{
	public function testTrapLogs()
	{
		$resource = fopen('php://memory', 'rw');
		pushLogLevel(LOG_INFO);
		pushLogFile($resource);
		logInfo('hello world');
		fseek($resource, 0, SEEK_SET);
		$result = fread($resource, 4096);
		static::assertEquals(1, preg_match('/hello world/', $result));
		popLogFile();
		popLogLevel();
	}

	public function testHideLogs()
	{
		$resource = fopen('php://memory', 'rw');
		pushLogLevel(LOG_NONE);
		pushLogFile($resource);
		logInfo('hello world');
		fseek($resource, 0, SEEK_SET);
		$result = fread($resource, 4096);
		static::assertEquals(0, preg_match('/hello world/', $result));
		popLogFile();
		popLogLevel();
	}
}

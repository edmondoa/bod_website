<?php
#
# @copyright Copyright (c) 2017 Green Seed Technologies, Inc. All rights reserved.
#
# @author David McLaughlin <david@greenseedtech.com>
#

set_include_path(__DIR__.'/../modules');

require_once('office/account/class.account.php');
require_once('common/db.php');

global $LOG_LEVEL;
$LOG_LEVEL = LOG_INFO;
$LOG_LEVEL = LOG_DEBUG;

global $CARDVAULTSDK_THROW_EXCEPTIONS;
$CARDVAULTSDK_THROW_EXCEPTIONS=1;

define('ER_BAD_FIELD_ERROR', 1054);
define('ER_PARSE_ERROR', 1064);
define('CR_UNKNOWN_HOST', 2005);
define('CR_CONNECTION_ERROR', 2002);

$_SERVER['DB_HOST'] = 'dldbm';
$_SERVER['DB_USERNAME'] = 'bod.Us3r';
$_SERVER['DB_PASSWORD'] = 'bod.P@$$';
$_SERVER['DB_NAME'] = 'byownerdaily';

$_SERVER['MY_ENCRYPTION_KEY'] = 'H6@/61)V/Ld0;?-pW[wFGvh0E`sLj3jc';

global $USE_SANDBOX;
$USE_SANDBOX = true;

class temporaryaccount extends account
{
	public function __construct()
	{
		if (func_num_args()) {
			parent::__construct(func_get_arg(0), '', 0);
		} else {
			parent::__construct([], '', 0);
		}
	}

	public function __destruct()
	{
		$this->delete('');
	}
}

define('CREDIT_CARD_NUMBER_APPROVED', 4111111111111111);

define('CREDITCARD_TEST_NUMBER', 4111111111111111);
define('CREDITCARD_TEST_DECLINED_NUMBER', 4005550000000019);
define('CREDITCARD_TEST_EXPIRATION_DATE', '2077-12');
define('CREDITCARD_TEST_NAME', 'Jon Doe');
define('CREDITCARD_TEST_STREET', '123 Elm St');
define('CREDITCARD_TEST_ZIP', 84058);
define('CREDIT_CARD_CARDCODE_TEST', 123);

function getDbConnection()
{
	require_once(__DIR__.'/config.php');
	global $DB_HOST;
	global $DB_USER;
	global $DB_PASS;
	return new db($DB_HOST, $DB_USER, $DB_PASS, 'An error occured during testing.', 0);
}

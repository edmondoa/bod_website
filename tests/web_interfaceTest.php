<?php
#
# @copyright Copyright (c) 2017 Green Seed Technologies, Inc. All rights reserved.
#
# @author David McLaughlin <david@greenseedtech.com>
#

require_once(__DIR__.'/shared.php');
require_once('common/class.web_interface.php');

/**
 * @backupGlobals disabled
 */
class web_interfaceTest extends \PHPUnit_Framework_TestCase
{
	public static function testConstruct()
	{
		$webInterface = new web_interface();
		static::assertTrue($webInterface instanceof web_interface);
	}
}

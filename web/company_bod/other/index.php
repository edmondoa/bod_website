<?php
$_META_DESCRIPTION = '';
$_META_KEYWORDS = '';
$_PAGE_TITLE = '';

include_once($_SESSION['web_interface']->get_server_path('other/global/top.php'));
?>
<TABLE cellSpacing="0" cellPadding="0" width="699" border="0">									
	<TBODY>
	<TR>
	<TD vAlign=top width=7><IMG src="/web/company_def/img/spacer.gif" height=1 width=7 alt=""></TD>
	<TD vAlign=top width=400>
	<!-- Entire left portion of Main Body table: -->
	<TABLE cellSpacing=0 cellPadding=0 width=400 border=0>
		<TBODY>
		<TR>
		<TD vAlign=top height=125>
		<!-- Outer top left portion of Main Body table: -->
		<TABLE cellSpacing=1 cellPadding=0 bgColor="#dddddd" border=0>
			<TBODY>
			<TR>
			<TD vAlign=top>
			<!-- Inner top left portion of Main Body table: -->
			<TABLE cellSpacing=0 cellPadding=0 width=450 bgColor="#ffffff" border=0>
				<TBODY>
				<TR>
				<TD class=big vAlign=top>
				<DIV style="BACKGROUND-IMAGE: url(<?= $_SESSION['web_interface']->get_path('img/blue_px.jpg') ?>); HEIGHT: 3px">
				<IMG src="/web/company_def/img/spacer.gif" alt="">
				</DIV>
				
				<IMG style="MARGIN-LEFT: 5px; MARGIN-RIGHT: 1px" height=150 width=175 src="<?= $_SESSION['web_interface']->get_path('img/fsbo-leads.jpg') ?>" align=left alt="For Sale by Owner Leads">
				
				<BR style="LINE-HEIGHT: 6px">
				
				<CENTER>
				<h1 class="big17">
				<font size="5" color="black">Daily FSBO Leads</font>
				<br><br><font size="3">"For Sale by Owner" Leads
				<br><br>Emailed to you daily.</font>
				</h1>
				
				<h2 class="big15">
				Highest quality leads + lowest cost =
				<b><font color="#0000ff">Best Value!</font></b>
				</h2>
				
				<h2 class="big15" style="color: #FF0000; padding-left: 150px;">
				NO SETUP FEES!
				</h2>
				
				<FONT style="FONT-SIZE: 15px">										
				The same day thousands of FSBO classified ads are published
				in newspapers, magazines, and other public sources is
				the same <br>day we collect, scrub and email you FSBO leads.
				</FONT>
				</CENTER>
				
				<br>		
				
				<font class="copy14">
				&nbsp;<b>Real Estate</b>, FSBO Sellers need you -- <a href="realtor-leads.html">Click here</a>
				<br>
				&nbsp;<b>Lenders</b>, More loan closes begin with FSBO Leads -- <a href="mortgage-leads.html">Click here</a>
				<br>
				&nbsp;<b>Title Professionals</b>, More business from your network -- <a href="info_title-sale-owner.html">Click here</a>
				
				</TD>
				</TR>
				</TBODY>
			</TABLE>
			</TD>
			</TR>
			</TBODY>
		</TABLE>
		</TD>
		</TR>
		
		<!-- Blank row after newspaper graphic -->
		<TR><TD><IMG height=2 src="/web/company_def/img/spacer.gif" width="1" alt=""></TD></TR>
		
		<TR>
		<TD vAlign=top height=212>
		<!-- Outer bottom portion of Main Body table: -->
		<TABLE cellSpacing=0 cellPadding=0 width=450 border=0>
			<TBODY>
			<TR>
			<TD vAlign=top width=168><!-- Inner bottom left portion of Main Body table: -->
			<TABLE cellSpacing=0 cellPadding=1 bgColor="#dddddd" border=0>
				<TBODY>
				<TR>
				<TD vAlign=top><!-- Inner bottom left portion of Main Body table: -->
				<TABLE cellSpacing=0 cellPadding=0 width=166 bgColor="#ffffff" border=0>
					<TBODY>
					<TR>
					<TD vAlign=top bgColor="#d9e2e6" height=3><IMG src="/web/company_def/img/spacer.gif" alt=""></TD>
					</TR>
					
					<TR>
					<TD style="PADDING-RIGHT: 15px; PADDING-LEFT: 15px; PADDING-BOTTOM: 15px; PADDING-TOP: 15px" 
					vAlign=top height=188>
					<!-- <IMG style="MARGIN-RIGHT: 5px" src="<?= $_SESSION['web_interface']->get_path('img/plan1.jpg') ?>" align=Middle> -->
					<STRONG class="blue big">More Info:</STRONG>
					
					<BR><BR style="LINE-HEIGHT: 6px">
					<DIV style="HEIGHT: 1px; BACKGROUND-COLOR: #a0a0a0">
					<IMG src="/web/company_def/img/spacer.gif" alt=""></DIV>
					
					<BR style="LINE-HEIGHT: 7px">
					<IMG style="MARGIN-LEFT: 5px; MARGIN-RIGHT: 7px" 
					src="<?= $_SESSION['web_interface']->get_path('img/sym1.jpg') ?>" alt="" align=Middle>
					<A class=gray href="/areas_main_trial.html" style="font-size: 14px;"><strong>Free Trial</strong></A>
					
					<br/>
					<BR style="LINE-HEIGHT: 14px">
					<IMG style="MARGIN-LEFT: 5px; MARGIN-RIGHT: 7px" 
					src="<?= $_SESSION['web_interface']->get_path('img/sym1.jpg') ?>" alt="" align=Middle>
					<A class=gray href="sample.html">View Sample</A>

					<BR>
					<BR style="LINE-HEIGHT: 14px">
					<IMG style="MARGIN-LEFT: 5px; MARGIN-RIGHT: 7px" 
					src="<?= $_SESSION['web_interface']->get_path('img/sym1.jpg') ?>" align=Middle alt="">
					<A class=gray href="./areas_main.html">Subscribe</A>																
					
					<BR>
					<BR style="LINE-HEIGHT: 14px"><IMG 
					style="MARGIN-LEFT: 5px; MARGIN-RIGHT: 7px" 
					src="<?= $_SESSION['web_interface']->get_path('img/sym1.jpg') ?>" align=Middle alt="">
					<A class=gray href="services_main.html">Services</A>
					
					<BR>
					<BR style="LINE-HEIGHT: 14px">
					<IMG style="MARGIN-LEFT: 5px; MARGIN-RIGHT: 7px" src="<?= $_SESSION['web_interface']->get_path('img/sym1.jpg') ?>" align=Middle alt="">
					<A class=gray href="./faqs_main.html">FAQ</A>
					
					<BR><BR style="LINE-HEIGHT: 14px">
					<IMG style="MARGIN-LEFT: 5px; MARGIN-RIGHT: 7px" src="<?= $_SESSION['web_interface']->get_path('img/sym1.jpg') ?>" align=Middle alt="">
					<A class=gray href="./company.html">Company Contact</A>
					<!--																<A class=gray href="mailto:info@byownerdaily.com">
					<IMG src="<?= $_SESSION['web_interface']->get_path('img/letter.jpg') ?>" align=Middle border=0></A>
					-->
					<!-- <BR><BR style="LINE-HEIGHT: 12px">
					<IMG style="MARGIN-LEFT: 5px; MARGIN-RIGHT: 7px" src="<?= $_SESSION['web_interface']->get_path('img/sym1.jpg') ?>" align=Middle>
					<A class=gray href="#">Webmail</A>
					
					<BR><BR style="LINE-HEIGHT: 6px">
					<IMG style="MARGIN-LEFT: 5px; MARGIN-RIGHT: 7px" src="<?= $_SESSION['web_interface']->get_path('img/sym1.jpg') ?>" align=Middle>
					<A class=gray href="#">POP3/SMTP</A>
					<BR><BR style="LINE-HEIGHT: 6px">
					<IMG style="MARGIN-LEFT: 5px; MARGIN-RIGHT: 7px" src="<?= $_SESSION['web_interface']->get_path('img/sym1.jpg') ?>" align=Middle>
					
					<br><br><A class=big href="#">More options...</A>
					-->
					
					</TD></TR></TBODY>
				</TABLE>
				</TD></TR></TBODY>
			</TABLE>
			</TD>
			
			<TD vAlign=top width=272>
			
			<!-- Outer bottom left, right portion of Main Body table: -->
			<TABLE cellSpacing=0 cellPadding=1 bgColor="#dddddd" border=0>
				<TBODY>
				<TR>
				<TD vAlign=top>
				<!-- Inner bottom left, right portion of Main Body table: -->
				<TABLE cellSpacing=0 cellPadding=0 width=270 bgColor="#ffffff" border=0>
					<TBODY>
					<TR>
					<TD vAlign=top bgColor="#d9e2e6" height=3><IMG src="/web/company_def/img/spacer.gif" alt=""></TD>
					</TR>
					
					<TR>
					<TD style="PADDING-RIGHT: 15px; PADDING-LEFT: 15px; PADDING-BOTTOM: 15px; PADDING-TOP: 15px" 
					vAlign=top height=188>
					<STRONG class="blue big">Each Lead:</STRONG>
					
					<BR><BR style="LINE-HEIGHT: 6px">
					<DIV style="HEIGHT: 1px; BACKGROUND-COLOR: #a0a0a0">
					<IMG src="/web/company_def/img/spacer.gif" alt=""></DIV>
					
					<BR style="LINE-HEIGHT: 7px">
					<IMG style="MARGIN-LEFT: 5px; MARGIN-RIGHT: 7px" src="<?= $_SESSION['web_interface']->get_path('img/sym1.jpg') ?>" align=Middle alt="">
					Checked with U.S. Do Not Call Registry																
					
					<BR>
					<BR style="LINE-HEIGHT: 14px">
					<IMG style="MARGIN-LEFT: 5px; MARGIN-RIGHT: 7px" src="<?= $_SESSION['web_interface']->get_path('img/sym1.jpg') ?>" align=Middle alt="">
					Reverse Phone Number lookup
					
					<BR>
					<BR style="LINE-HEIGHT: 14px">
					<IMG style="MARGIN-LEFT: 5px; MARGIN-RIGHT: 7px" src="<?= $_SESSION['web_interface']->get_path('img/sym1.jpg') ?>" align=Middle alt="">
					Newspaper sources identified
					
					<BR>
					<BR style="LINE-HEIGHT: 14px">
					<IMG style="MARGIN-LEFT: 5px; MARGIN-RIGHT: 7px" src="<?= $_SESSION['web_interface']->get_path('img/sym1.jpg') ?>" align=Middle alt="">
					Same day delivery
					
					<BR>
					<BR style="LINE-HEIGHT: 14px">
					<IMG style="MARGIN-LEFT: 5px; MARGIN-RIGHT: 7px" src="<?= $_SESSION['web_interface']->get_path('img/sym1.jpg') ?>" align=Middle alt="">
					No Repeat Ads
					
					<BR>
					<BR style="LINE-HEIGHT: 14px">
					<IMG style="MARGIN-LEFT: 5px; MARGIN-RIGHT: 7px" src="<?= $_SESSION['web_interface']->get_path('img/sym1.jpg') ?>" align=Middle alt="">
					No Setup Fees
					
					</TD></TR></TBODY>
				</TABLE>
				</TD></TR></TBODY>
			</TABLE>
			</TD></TR></TBODY>
		</TABLE>
		<br>
		<CENTER><font class="copy14">
		The Inman News referred to the founders as;
		<br><b>“The Lennon and McCartney of online lead generation”.</b>
		<br>click to read: 
		<a href="/web/<?= $_SESSION['o_company']->get_directory() ?>/InmanNews_ByOwnerDaily_printversion.pdf"><b>Give us this day our daily leads</b></a>
		</font></CENTER>
		<br>
		
		
		</TD></TR></TBODY>
	</TABLE>
	</TD>
	<TD vAlign=top width=5><IMG height=1 src="/web/company_def/img/spacer.gif" width="5" alt=""></TD>
	<TD vAlign=top width=235>
	<!-- Outer right portion of Main Body table: -->
	<TABLE cellSpacing=0 cellPadding=0 width=235 border=0>
		<TBODY>
		<TR>
		<TD vAlign=top height=230><!-- Outer lower right portion of Main Body table: -->
		<TABLE cellSpacing=0 cellPadding=1 bgColor="#dddddd" border=0>
		<TBODY>
		<TR>
		<TD vAlign=top><!-- Inner lower right portion of Main Body table: -->
		<TABLE cellSpacing=0 cellPadding=0 width=233 bgColor="#ffffff" border=0>
			<TBODY>
			<TR>
			<TD vAlign=top width=3 bgColor="#d9e2e6">
			<IMG height=1 src="/web/company_def/img/spacer.gif" width="3" alt=""> </TD>
			<TD vAlign=top width=230>
			
			<BR style="LINE-HEIGHT: 8px">
			<a href="realtor-leads.html">																
			<IMG style="MARGIN-LEFT: 10px; MARGIN-RIGHT: 15px" 
			src="<?= $_SESSION['web_interface']->get_path('img/for-sale-by-owner.gif') ?>" align="left" vspace="3" border="0" alt="Real Estate leads Realtor Agent Broker">
			</a>
			
			<a href="realtor-leads.html">
			<STRONG class="big blue">Real Estate Leads</STRONG></a>
			<!--																<STRONG class="big blue">Realtor<small>®</small> Leads</STRONG></a> -->																
			<br><b>FSBO leads</b> for Real Estate Agents and Brokers.
			
			<BR><BR style="LINE-HEIGHT: 8px">
			<DIV style="HEIGHT: 1px; BACKGROUND-COLOR: #dbdbdb">
			<IMG src="/web/company_def/img/spacer.gif" alt=""></DIV>
			
			<BR style="LINE-HEIGHT: 8px">
			<a href="mortgage-leads.html">																
			<IMG style="MARGIN-LEFT: 10px; MARGIN-RIGHT: 15px" 
			src="<?= $_SESSION['web_interface']->get_path('img/Mortgage_1.jpg') ?>" align="left" vspace="3" border="0" alt="Mortgage leads Loan Officer Mortgage Lender leads">
			</a>
			
			<a href="mortgage-leads.html">																
			<STRONG class="big blue">Mortgage Leads</STRONG></a>
			<BR><b>Faster FSBO lists</b> for <br>Lenders and Brokers.
			
			<BR><BR style="LINE-HEIGHT: 8px">
			<DIV style="HEIGHT: 1px; BACKGROUND-COLOR: #dbdbdb">
			<IMG src="/web/company_def/img/spacer.gif" alt=""></DIV>
			
			<BR style="LINE-HEIGHT: 8px">
			<a href="info_title-sale-owner.html">																
			<IMG style="MARGIN-LEFT: 10px; MARGIN-RIGHT: 15px" 
			src="<?= $_SESSION['web_interface']->get_path('img/sale-owner.jpg') ?>" align="left" vspace="3" border="0" alt="Title Company fsbo list">
			</a>
			
			<a href="info_title-sale-owner.html">
			<STRONG class="big blue">Title Leads</STRONG></a>
			<BR><b>Comprehensive FSBO <br>leads</b> for Title Professionals.
			
			<BR><BR style="LINE-HEIGHT: 8px">
			<DIV style="HEIGHT: 1px; BACKGROUND-COLOR: #dbdbdb">
			<IMG src="/web/company_def/img/spacer.gif" alt=""></DIV>
			<BR style="LINE-HEIGHT: 8px">
			<IMG style="MARGIN-LEFT: 10px; MARGIN-RIGHT: 15px" 
			src="<?= $_SESSION['web_interface']->get_path('img/Technology_4.jpg') ?>" align="left" vspace="3" border="0" alt="Daily FSBO classified ads leads">
			<STRONG class="big blue">Classified Ads</STRONG>
			<BR>Millions of classified ads into
			<br>For Sale by Owner leads.<BR>
			<BR style="LINE-HEIGHT: 8px">
			</TD></TR></TBODY>
		</TABLE>
		</TD></TR></TBODY>
	</TABLE>
	</TD></TR>
	
	<TR>
	<TD><IMG height=5 alt="" src="/web/company_def/img/spacer.gif" width=1></TD>
	</TR>
	
	<TR>
	<TD vAlign=top>
	
	<center>
	<FONT style="FONT-SIZE: 16px" color="#000000">
	
	<br>
	<b>News</b>
	</FONT>
		
	<BR><BR>
	Inman News headline article
	<a href="/web/<?= $_SESSION['o_company']->get_directory() ?>/InmanNews_ByOwnerDaily_printversion.pdf">
	<br><b>"Give us this day our daily leads"</b></a>
	
	<BR><BR>		
	
	<I>ByOwner</I><B>DAILY</B> featured on 
	<br>
	<a target="_blank" href="http://www.fsbotalk.com/index.mv?screen=viewshow&amp;xrecno=20">
	FSBOTalk.com</a>		
	
	<BR><BR>
	
	<a href="http://www.inman.com/InmanNews.aspx?ID=46956">Inman Innovator Award Finalist</a>
	<br>(Inman subscription required)
	<br><br><IMG src="<?= $_SESSION['web_interface']->get_path('img/Inman_award.jpg') ?>" hspace=0 border=0 alt="Technology Innovator award finalist">
	
	</center>
	
	</TD></TR></TBODY>
						</TABLE>

					<TD vAlign=top width=8><IMG src="/web/company_def/img/spacer.gif" height=1 width="8" alt=""> 
					</TD>
				</TR>
				</TBODY>
			</TABLE>
		</TD>
	</TR>

	<!-- Last row in main area -->

	<TR>
	<TD><IMG height=0 alt="" src="/web/company_def/img/spacer.gif" width=699></TD>
	</TR>

	</TBODY>
</TABLE>

<TABLE cellSpacing=1 cellPadding=1 width="100%" border=0>
	<TR>
	<TD class="gray16" COLSPAN="9"><center><b>- USA Subscription Quick Links -</b></center></TD>
	</TR>															
	
	<TR>
	<TD class="blue11"><a href="/ssl/subscribe_AL.html">Alabama</a></TD>
	<TD class="blue11"><a href="/ssl/subscribe_AK.html">Alaska</a></TD>
	<TD class="blue11"><a href="/ssl/subscribe_AR.html">Arkansas</a></TD>
	<TD class="blue11"><a href="/ssl/subscribe_AZ.html">Arizona</a></TD>
	<TD class="blue11"><a href="/ssl/subscribe_CA.html">California</a></TD>
	<TD class="blue11"><a href="/ssl/subscribe_CO.html">Colorado</a></TD>
	<TD class="blue11"><a href="/ssl/subscribe_CT.html">Connecticut</a></TD>
	<TD class="blue11"><a href="/ssl/subscribe_DC.html">D.C.</a></TD>
	<TD class="blue11"><a href="/ssl/subscribe_DE.html">Delaware</a></TD>
	</TR>
	<TR>
	<TD class="blue11"><a href="/ssl/subscribe_FL.html">Florida</a></TD>
	<TD class="blue11"><a href="/ssl/subscribe_GA.html">Georgia</a></TD>
	<TD class="blue11"><a href="/ssl/subscribe_HI.html">Hawaii</a></TD>
	<TD class="blue11"><a href="/ssl/subscribe_IA.html">Iowa</a></TD>
	<TD class="blue11"><a href="/ssl/subscribe_ID.html">Idaho</a></TD>
	<TD class="blue11"><a href="/ssl/subscribe_IL.html">Illinois</a></TD>
	<TD class="blue11"><a href="/ssl/subscribe_IN.html">Indiana</a></TD>
	<TD class="blue11"><a href="/ssl/subscribe_KS.html">Kansas</a></TD>
	<TD class="blue11"><a href="/ssl/subscribe_KY.html">Kentucky</a></TD>
	</TR>
	<TR>
	<TD class="blue11"><a href="/ssl/subscribe_LA.html">Louisiana</a></TD>
	<TD class="blue11"><a href="/ssl/subscribe_MA.html">Massachusetts</a></TD>
	<TD class="blue11"><a href="/ssl/subscribe_MD.html">Maryland</a></TD>
	<TD class="blue11"><a href="/ssl/subscribe_ME.html">Maine</a></TD>
	<TD class="blue11"><a href="/ssl/subscribe_MI.html">Michigan</a></TD>
	<TD class="blue11"><a href="/ssl/subscribe_MN.html">Minnesota</a></TD>
	<TD class="blue11"><a href="/ssl/subscribe_MS.html">Mississippi</a></TD>
	<TD class="blue11"><a href="/ssl/subscribe_MO.html">Missouri</a></TD>
	<TD class="blue11"><a href="/ssl/subscribe_MT.html">Montana</a></TD>
	</TR>
	<TR>
	<TD class="blue11"><a href="/ssl/subscribe_NE.html">Nebraska</a></TD>
	<TD class="blue11"><a href="/ssl/subscribe_NV.html">Nevada</a></TD>
	<TD class="blue11"><a href="/ssl/subscribe_NH.html">New Hampshire</a></TD>
	<TD class="blue11"><a href="/ssl/subscribe_NJ.html">New Jersey</a></TD>
	<TD class="blue11"><a href="/ssl/subscribe_NM.html">New Mexico</a></TD>
	<TD class="blue11"><a href="/ssl/subscribe_NY.html">New York</a></TD>
	<TD class="blue11"><a href="/ssl/subscribe_NC.html">North Carolina</a></TD>
	<TD class="blue11"><a href="/ssl/subscribe_ND.html">North Dakota</a></TD>
	<TD class="blue11"><a href="/ssl/subscribe_OH.html">Ohio</a></TD>
	</TR>
	<TR>
	<TD class="blue11"><a href="/ssl/subscribe_OK.html">Oklahoma</a></TD>
	<TD class="blue11"><a href="/ssl/subscribe_OR.html">Oregon</a></TD>
	<TD class="blue11"><a href="/ssl/subscribe_PA.html">Pennsylvania</a></TD>
	<TD class="blue11"><a href="/ssl/subscribe_RI.html">Rhode Island</a></TD>
	<TD class="blue11"><a href="/ssl/subscribe_SC.html">South Carolina</a></TD>
	<TD class="blue11"><a href="/ssl/subscribe_SD.html">South Dakota</a></TD>
	<TD class="blue11"><a href="/ssl/subscribe_TN.html">Tennessee</a></TD>															
	<TD class="blue11"><a href="/ssl/subscribe_TX.html">Texas</a></TD>
	<TD class="blue11"><a href="/ssl/subscribe_UT.html">Utah</a></TD>
	</TR>
	<TR>
	<TD class="blue11"><a href="/ssl/subscribe_VT.html">Vermont</a></TD>
	<TD class="blue11"><a href="/ssl/subscribe_VA.html">Virginia</a></TD>
	<TD class="blue11"><a href="/ssl/subscribe_WA.html">Washington</a></TD>
	<TD class="blue11"><a href="/ssl/subscribe_WI.html">Wisconsin</a></TD>
	<TD class="blue11"><a href="/ssl/subscribe_WV.html">West Virginia</a></TD>															
	</TR>

	<TR>
		<TD>&nbsp;</TD>
	</TR>

</TABLE>

<TABLE cellSpacing=1 cellPadding=1 width="100%" border=0>
	<TR><TD class="gray16" COLSPAN="6"><center><b>- Canada Areas Subscription Quick Links -</b></center></TD></TR>												
	<TR>
		<TD align="center" class="blue11"><a href="/ssl/subscribe_AB.html">Alberta</a></TD>
		<TD align="center" class="blue11"><a href="/ssl/subscribe_BC.html">British Columbia</a></TD>
		<TD align="center" class="blue11"><a href="/ssl/subscribe_MB.html">Manitoba</a></TD>
		<TD align="center" class="blue11"><a href="/ssl/subscribe_NB.html">New Brunswick</a></TD>
		<TD align="center" class="blue11"><a href="/ssl/subscribe_NS.html">Nova Scotia</a></TD>
		<TD align="center" class="blue11"><a href="/ssl/subscribe_ON.html">Ontario</a></TD>
		<TD align="center" class="blue11"><a href="/ssl/subscribe_QC.html">Quebec</a></TD>
		<TD align="center" class="blue11"><a href="/ssl/subscribe_SK.html">Saskatchewan</a></TD>
	</TR>
	<TR><TD>&nbsp;</TD></TR>
</TABLE>

<TABLE cellSpacing=1 cellPadding=1 width="100%" border=0>
	<TR>
		<TD><H2 class=big13><a href="realtor-leads.html">Real Estate Agent, Real Estate Broker</a></h2></TD>
		<TD><H2 class=big13><a href="mortgage-leads.html">Mortgage Lender, Mortgage Broker</a></h2></TD>
		<TD><H2 class=big13><a href="info_title-sale-owner.html">Title Company</a></h2>
	</TR>
</TABLE>
<?php
include_once($_SESSION['web_interface']->get_server_path('other/global/bottom.php'));
?>

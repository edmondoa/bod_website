// -- PURPOSE: to check/uncheck all checkboxes in a form
// -- Usage: checkAll(checkbox that checks/unchecks all, string limiting checkboxes to those whose name begins with it)

function checkAll(checkallbox, beginsWith) {
	var myForm = checkallbox.form;
	for (var i=0;i<myForm.elements.length;i++) {
		var myElement = myForm.elements[i];
		if (myElement != checkallbox && myElement.type == 'checkbox') {
			if ((beginsWith && myElement.id.indexOf(beginsWith)==0) || (!beginsWith)) {
				myElement.checked = checkallbox.checked;
			}
		}
	}
}

function checkEmail(myField) {
	if (myField.value.search(/[\w\-\.\_]+\@[\w\-\.\_]+\.[\w\-\.\_]+/) == -1 && myField.value != "") {
		alert('Please enter a valid email address.');
		myField.focus();
		myField.select();
		return false;
	}
	return true;
}

// -- PURPOSE: to empty a <select>
// -- Usage: clearLlist(list to empty, text to display afterwards)

function clearList(myList, defaultText) {
	for (var i=myList.options.length-1; i>=0; i--){
		myList.options[i] = null;
	}
	myList.options[0] = new Option(defaultText, "", false, false);
}


// -- PURPOSE: to collapse all elements
// -- Usage: collapseAll(list of items to collapse)
function collapseAll() {
	for (var i=0; i<collapseAll.arguments.length; i++) {
		var element = document.getElementById(collapseAll.arguments[i]);
		if (element) {
			element.style.display = "none";
		}
	}
}

function deleteConfirm(delete_message) {
	return window.confirm(delete_message);
}


// -- PURPOSE: to expand all elements
// -- Usage: expandAll(list of items to expand)
function expandAll() {
	for (var i=0; i<expandAll.arguments.length; i++) {
		var element = document.getElementById(expandAll.arguments[i]);
		if (element) {
			element.style.display = "block";
		}
	}
}

// -- PURPOSE: to expand collapsed elements or collapse expanded elements
// -- Usage: expandCollapse(list of items to expand/collapse)
function expandCollapse() {
	for (var i=0; i<expandCollapse.arguments.length; i++) {
		var element = document.getElementById(expandCollapse.arguments[i]);
		if (element) {
			element.style.display = (element.style.display == "none") ? "block" : "none";
			if ((element.id == 'pop1') && (element.style.display == "block")) {
				centerDialog(element.id);
			}
		}
		else {
			alert('could get element for '+expandCollapse.arguments[i]);
		}
	}
}

// -- PURPOSE: to expand an item and collapse many others
// -- Usage: expandFirst(item to expand, list of items to collapse)
function expandFirst() {
	expandAll(expandFirst.arguments[0]);
	for (var i=1; i<expandFirst.arguments.length; i++) {
		collapseAll(expandFirst.arguments[i]);
	}
}

// -- PURPOSE: to open a new window or focus on it if it is opened
// -- Usage: openWindow(url to open, name of window object, width of window - number of pixels or "max" for maximum available, height of window - number of pixels or "max" for maximum available)

var myWindows = new Array();
function openWindow(windowURL, windowName, width, height, startX, startY) {
	var change = false;
	if (myWindows[windowName]){
		if (myWindows[windowName].closed) {
			change = true;
		}
		else if (myWindows[windowName].document.location.href.indexOf(windowURL) == -1) {
			change = true;
		}
	}
	else {
		change = true;
	}

	if (change) {
		if (width == "max") {
			width = screen.availWidth;
		}
		if (height == "max") {
			height = screen.availHeight - 50;
		}
		if (!startX) {
			startX = 0;
		}
		if (!startY) {
			startY = 0;
		}
		var args = "scrollbars=yes,toolbar=no,directories=no,menubar=no,resizable=yes,status=yes,width=" + width + ",height=" + height + ",top=" + startY + ",left=" + startX;
		myWindows[windowName] = window.open(windowURL, windowName, args);
	}
	myWindows[windowName].focus();
}

//Hide layer after
var timerID;
function HideTimedLayer(id) {
	clearTimeout(timerID);
	var e = document.getElementById(id);
	e.style.display = "none";
}

function timedLayer(id) {
	setTimeout("HideTimedLayer(\"" + id + "\")", 50000);
}
<?php
#
# @copyright Copyright (c) 2017 Green Seed Technologies, Inc. All rights reserved.
#
# @author David Hoover
# @author David McLaughlin <david@greenseedtech.com>
#

if ($_SESSION['office']->account->get_account_type() != 'ADMIN' && $_SESSION['office']->account->get_account_type() != 'BILLING' && $_SESSION['office']->account->get_account_type() != 'PRIVATE_LABEL') {
	$_SESSION['web_interface']->destroySession();
	exit;
}
$_PAGE_TITLE = 'Account';

require_once('office/account/class.letter.php');
$o_letter_utilities = new letter_utilities();
$list_letter = $o_letter_utilities->get_list();

require_once('office/account/class.account.php');
$o_account = new account( array('accountid'=>$_REQUEST['accountid']) );
// unset card_number if XXXX
if (preg_match("/X/", $_REQUEST['card_number'])) {
	unset($_REQUEST['card_number']);
}
$o_account->initialize($_REQUEST);
if (!$o_account->isOwner() || ($_SESSION['office']->account->get_account_type() != 'ADMIN' && $_SESSION['office']->account->get_accountid() == $o_account->get_accountid())) {
	$_SESSION['web_interface']->destroySession();
	exit;
}
if ($o_account->get_created()) {
	$created_year = date('Y', strtotime($o_account->get_created()));
	$created_month = date('m', strtotime($o_account->get_created()));
	$created_day = date('d', strtotime($o_account->get_created()));
}
else {
	$created_year = date('Y');
	$created_month = date('m');
	$created_day = date('d');
}
if ($o_account->get_start_date()) {
	$start_year = date('Y', strtotime($o_account->get_start_date()));
	$start_month = date('m', strtotime($o_account->get_start_date()));
	$start_day = date('d', strtotime($o_account->get_start_date()));
}
if ($o_account->get_end_date()) {
	$end_year = date('Y', strtotime($o_account->get_end_date()));
	$end_month = date('m', strtotime($o_account->get_end_date()));
	$end_day = date('d', strtotime($o_account->get_end_date()));
}
if ($o_account->get_next_bill_date() && $o_account->get_next_bill_date() != '0000-00-00') {
	$next_bill_month = date('m', strtotime($o_account->get_next_bill_date()));
	$next_bill_day = date('d', strtotime($o_account->get_next_bill_date()));
	$next_bill_year = date('Y', strtotime($o_account->get_next_bill_date()));
}

$list_billing = array();
if ($o_account->get_accountid()) {
	require_once('office/account/class.billing.php');
	$o_billing_utilities = new billing_utilities();
	$list_billing = $o_billing_utilities->get_list( array('where_plus'=>" AND accountid='" . $_SESSION['data_access']->db_quote($o_account->get_accountid()) . "' ") );
}

require_once('office/account/class.company.php');
$o_company_utilities = new company_utilities();
$listCompany = $o_company_utilities->get_list();

require_once($_SESSION['web_interface']->get_server_path('other/misc/listing_fields.php'));
require_once($_SESSION['web_interface']->get_server_path('other/misc/select_options.php'));
include_once($_SESSION['web_interface']->get_server_path('office/global/top.php'));
?>
<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td class="bodyNav" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('office/account/left_nav_' . $_SESSION['office']->account->get_account_type() . '.php')); ?>
		</td>
		<td class="bodyMain" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('other/misc/status_error.php')); ?>
			<h1>Edit Account</h1>
<?php
if ($o_account->get_accountid() && (time() >= strtotime($o_account->get_start_date()) && time() <= strtotime($o_account->get_end_date()))) {
?>
			You can send listings to this account <a href="/office/listing/send_listings.php?accountid=<?= $o_account->get_accountid() ?>">by date</a> or <a href="/office/listing/search.php?accountid=<?= $o_account->get_accountid() ?>">by searching</a>.
<?php
}
?>
			<form id="accountForm" name="accountForm" method="post" action="/office/account/handle.php" enctype="multipart/form-data">
			<input type="hidden" id="cmd" name="cmd" value="save_account" />
			<input type="hidden" name="accountid" value="<?= $o_account->get_accountid() ?>" />
			<div class="displayInfo">
<?php
if ($o_account->get_accountid()) {
?>
				<fieldset>
					<legend>Areas</legend>
					This account is subscribed to the following areas:<br/>
<?php
	foreach ($o_account->get_list_area_names() as $area_name) {
		print '<strong>' . $area_name . '</strong><br/>';
	}
?>
					<br/><a href="/office/account/area_names.php?accountid=<?= $o_account->get_accountid() ?>">Modify Areas</a>
				</fieldset>
				<fieldset>
					<legend>Listing Fields</legend>
					This account can see the following listing fields:<br/>
<?php
	$listing_fields = $o_account->get_array_listing_fields();
	if (count($listing_fields) > 0) {
		foreach ($listing_fields as $listing_field=>$boolean) {
			print '<strong>' . $allListingFields[$listing_field] . '</strong><br/>';
		}
	}
	else {
		print 'This account will receive ALL listing fields.<br/>';
	}
?>
					<br/><a href="/office/account/listing_fields.php?accountid=<?= $o_account->get_accountid() ?>">Modify Listing Fields</a>
				</fieldset>
				<fieldset>
					<legend>Property Type Fields</legend>
					This account can see the following property type fields:<br/>
<?php
	$property_type_fields = $o_account->get_array_property_type_fields();
	if (count($property_type_fields) > 0) {
		foreach ($property_type_fields as $property_type_field=>$boolean) {
			print '<strong>' . $property_type_field . '</strong><br/>';
		}
	}
	else {
		print 'This account will receive ALL property type fields.<br/>';
	}
?>
					<br/><a href="/office/account/property_type_fields.php?accountid=<?= $o_account->get_accountid() ?>">Modify Property Type Fields</a>
				</fieldset>
				<fieldset>
					<legend>Search Fields</legend>
					This account uses the following search fields:<br/>
<?php
	$search_fields = $o_account->get_hash_search_fields();
	if (count($search_fields) > 0) {
		foreach ($search_fields as $key=>$value) {
			print '<strong>' . $key . ': ' . $value . '</strong><br/>';
		}
	}
	else {
		print 'This account has no search fields.<br/>';
	}
?>
					<br/><a href="/office/account/search_fields.php?accountid=<?= $o_account->get_accountid() ?>">Modify Search Fields</a>
				</fieldset>
<?php
	if (!$_SESSION['office']->account->is_private()) {
?>
				<fieldset>
					<legend>Robots</legend>
					This account is not receiving listings from the following robots:<br/>
<?php
		foreach ($o_account->get_list_robot_names() as $robot_name) {
			print '<strong>' . $robot_name . '</strong><br/>';
		}
?>
					<br/><a href="/office/account/robot_names.php?accountid=<?= $o_account->get_accountid() ?>">Modify Robots</a>
				</fieldset>
<?php
	}
}
?>
				<fieldset>
					<legend>Login Info</legend>
					<table border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td class="left"><span <?= $_SESSION['web_interface']->missing_required_label('username') ?>>Username:</span></td>
							<td class="right"><input autocomplete="new-username" type="text" name="username" size="40" value="<?= $o_account->get_username() ?>" <?= $_SESSION['web_interface']->missing_required_input('username') ?> /></td>
						</tr>
						<tr>
							<td class="left"><span <?= $_SESSION['web_interface']->missing_required_label('password') ?>>New Password:</span></td>
							<td class="right"><input autocomplete="new-password" type="password" name="password" <?= $_SESSION['web_interface']->missing_required_input('password') ?> /></td>
						</tr>
					</table>
				</fieldset>
				<table border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td valign="top">
							<fieldset>
								<legend>Personal Info</legend>
								<table border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td class="left"><span <?= $_SESSION['web_interface']->missing_required_label('first_name') ?>>First Name:</span></td>
										<td class="right"><input type="text" name="first_name" value="<?= $o_account->get_first_name() ?>" <?= $_SESSION['web_interface']->missing_required_input('first_name') ?> /></td>
									</tr>
									<tr>
										<td class="left"><span <?= $_SESSION['web_interface']->missing_required_label('last_name') ?>>Last Name:</span></td>
										<td class="right"><input type="text" name="last_name" value="<?= $o_account->get_last_name() ?>" <?= $_SESSION['web_interface']->missing_required_input('last_name') ?> /></td>
									</tr>
<?php
$address = $o_account->o_personal_address;
include($_SESSION['web_interface']->get_server_path('other/misc/address_form.php'));
?>
									<tr>
										<td class="left"><span <?= $_SESSION['web_interface']->missing_required_label('personal_phone') ?>>Phone:</span></td>
										<td class="right"><input type="text" name="personal_phone" value="<?= $o_account->get_personal_phone() ?>" <?= $_SESSION['web_interface']->missing_required_input('personal_phone') ?> /></td>
									</tr>
									<tr>
										<td class="left">Cell:</td>
										<td class="right"><input type="text" name="cell_phone" value="<?= $o_account->get_cell_phone() ?>" /></td>
									</tr>
								</table>
							</fieldset>
						</td>
						<td>&nbsp;</td>
						<td valign="top">
							<fieldset>
								<legend>Business Info</legend>
								<table border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td class="left"><span <?= $_SESSION['web_interface']->missing_required_label('business_name') ?>>Company Name:</span></td>
										<td class="right"><input type="text" name="business_name" value="<?= $o_account->get_business_name() ?>" <?= $_SESSION['web_interface']->missing_required_input('business_name') ?> /></td>
									</tr>
<?php
$address = $o_account->o_business_address;
include($_SESSION['web_interface']->get_server_path('other/misc/address_form.php'));
?>
									<tr>
										<td class="left"><span <?= $_SESSION['web_interface']->missing_required_label('business_phone') ?>>Phone:</span></td>
										<td class="right"><input type="text" name="business_phone" value="<?= $o_account->get_business_phone() ?>" <?= $_SESSION['web_interface']->missing_required_input('business_phone') ?> /></td>
									</tr>
								</table>
							</fieldset>
						</td>
					</tr>
				</table>
				<fieldset>
					<legend>Contact Info</legend>
					<table border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td class="left"><span <?= $_SESSION['web_interface']->missing_required_label('main_email_address') ?>>Email:</span></td>
							<td class="right"><input type="text" name="main_email_address" value="<?= $o_account->get_main_email_address() ?>" size="35" <?= $_SESSION['web_interface']->missing_required_input('main_email_address') ?> /></td>
						</tr>
						<tr>
							<td class="left" valign="top">Billing Email:</td>
							<td class="right"><input type="text" name="billing_email_address" value="<?= $o_account->get_billing_email_address() ?>" size="35" /></td>
						</tr>
						<tr>
							<td class="left">Fax:</td>
							<td class="right"><input type="text" name="fax_phone" value="<?= $o_account->get_fax_phone() ?>" /></td>
						</tr>
						<tr>
							<td class="left">URL:</td>
							<td class="right"><input type="text" name="url" size="50" value="<?= $o_account->get_url() ?>" /></td>
						</tr>
					</table>
				</fieldset>
				<fieldset>
<script type="text/javascript">
	var listAccountProduct = {<?php
$string = '';
foreach ($o_account->getArrayProducts() as $productId=>$productName) {
	$string .= '"' . $productId . '":"TRUE",';
}
print substr($string, 0, -1) . '};';
foreach ($listCompany as $Company) {
?>
	var companyProduct<?= $Company->get_companyid() ?> = {<?php
	$string = '';
	foreach ($Company->getProduct() as $Product) {
		$string .= '"' . $Product->getProductId() . '":"' . $Product->getTitle() . '",';
	}
	print substr($string, 0, -1) . '};';
}
?>

	function changeProduct(companyId) {
		eval("var temp_companyProduct = companyProduct" + companyId + ";");
		var stringProductCheckboxes = "";
		for (var productId in temp_companyProduct) {
			if (listAccountProduct[productId] == "TRUE") {
				stringProductCheckboxes = stringProductCheckboxes + "<input type=\"checkbox\" name=\"product[]\" value=\"" + productId + "\" checked=\"checked\" /> " + temp_companyProduct[productId] + "<br/>";
			}
			else {
				stringProductCheckboxes = stringProductCheckboxes + "<input type=\"checkbox\" name=\"product[]\" value=\"" + productId + "\" /> " + temp_companyProduct[productId] + "<br/>";
			}
		}
		document.getElementById('productCheckboxes').innerHTML = stringProductCheckboxes;
	}
</script>
					<legend>Admin Info</legend>
					<table border="0" cellpadding="0" cellspacing="0">
<?php
if ($_SESSION['office']->account->is_private()) {
?>
						<tr>
							<td class="left">Company:</td>
							<td class="right">
								<?= $_SESSION['office']->account->o_company->get_title() ?>
								<input type="hidden" name="companyid" value="<?= $_SESSION['office']->account->get_companyid() ?>" />
							</td>
						</tr>
						<tr>
							<td class="left">Product:</td>
							<td class="right">
								<div id="productCheckboxes"></div>
								<script type="text/javascript">
									changeProduct(<?= $_SESSION['office']->account->get_companyid() ?>);
								</script>
							</td>
						</tr>
<?php
}
else {
?>
						<tr>
							<td class="left">Company:</td>
							<td class="right">
								<select id="companyid" name="companyid" onChange="changeProduct(this.value);">
<?php
	foreach ($o_company_utilities->get_list() as $item_company) {
?>
									<option value="<?= $item_company->get_companyid() ?>" <?php if ($o_account->get_companyid() == $item_company->get_companyid()) { print 'selected="selected"'; } ?>><?= $item_company->get_title() ?></option>
<?php
	}
?>
								</select>
							</td>
						</tr>
						<tr>
							<td class="left">Product:</td>
							<td class="right">
								<div id="productCheckboxes"></div>
								<script type="text/javascript">
									changeProduct(document.getElementById('companyid').value);
								</script>
							</td>
						</tr>
<?php
}
?>
						<tr>
							<td class="left">CSV Attachment:</td>
							<td class="right"><input type="checkbox" name="email_attachment" value="1" <?php if ($o_account->get_email_attachment()) { print 'checked="checked"'; } ?> /></td>
						</tr>
<?php
if (!$_SESSION['office']->account->is_private()) {
?>
						<tr>
							<td class="left">Affiliate ID:</td>
							<td class="right">
<?php
	if ($o_account->get_account_type() == 'AFFILIATE') {
		print $o_account->get_accountid();
	}
	else {
?>
								<input type="text" name="affiliateid" value="<?= $o_account->get_affiliateid() ?>" />
<?php
	}
?>
							</td>
						</tr>
<?php
}
?>
						<tr>
							<td class="left">Customer #:</td>
							<td class="right"><input type="text" name="customer_number" value="<?= $o_account->get_customer_number() ?>" /></td>
						</tr>
						<tr>
							<td colspan="2">
								<table border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td class="left">Customer Type:</td>
										<td class="right">
											<select name="customer_type">
												<option value="">&nbsp;</option>
<?php
if ($_SESSION['office']->account->is_private()) {
?>
												<?= select_options( array('type'=>'private_customer_type', 'selected'=>$o_account->get_customer_type()) ) ?>
<?php
}
else {
?>
												<?= select_options( array('type'=>'customer_type', 'selected'=>$o_account->get_customer_type()) ) ?>
<?php
}
?>
											</select>
										</td>
										<td class="left" style="padding-left: 10px;">Other:</td>
										<td class="right"><input type="text" name="customer_type_other" value="<?= $o_account->get_customer_type_other() ?>" /></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td class="left">Signup Date:</td>
							<td class="right">
<?php
if ($_SESSION['office']->account->is_private()) {
	$timestamp = ($o_account->get_accountid()) ? strtotime($o_account->get_created()) : time();
?>
								<?= date('F j, Y', $timestamp) ?>
								<input type="hidden" name="created_month" value="<?= date('m', $timestamp) ?>" />
								<input type="hidden" name="created_day" value="<?= date('d', $timestamp) ?>" />
								<input type="hidden" name="created_year" value="<?= date('Y', $timestamp) ?>" />
<?php
}
else {
?>
								<select name="created_month">
									<?= select_options( array('type'=>'month', 'selected'=>$created_month) ) ?>
								</select>
								<select name="created_day">
									<?= select_options( array('type'=>'day', 'selected'=>$created_day) ) ?>
								</select>
								<select name="created_year">
									<?= select_options( array('type'=>'past_year', 'selected'=>$created_year) ) ?>
								</select>
<?php
}
?>
							</td>
						</tr>
						<tr>
							<td class="left">Start Date:</td>
							<td class="right">
								<select name="start_month">
									<option value=""></option>
									<?= select_options( array('type'=>'month', 'selected'=>$start_month) ) ?>
								</select>
								<select name="start_day">
									<option value=""></option>
									<?= select_options( array('type'=>'day', 'selected'=>$start_day) ) ?>
								</select>
								<select name="start_year">
									<option value=""></option>
									<?= select_options( array('type'=>'number', 'min'=>'2000', 'max'=>date('Y') + 10, 'selected'=>$start_year) ) ?>
								</select>
							</td>
						</tr>
						<tr>
							<td class="left">End Date:</td>
							<td class="right">
								<select name="end_month">
									<option value=""></option>
									<?= select_options( array('type'=>'month', 'selected'=>$end_month) ) ?>
								</select>
								<select name="end_day">
									<option value=""></option>
									<?= select_options( array('type'=>'day', 'selected'=>$end_day) ) ?>
								</select>
								<select name="end_year">
									<option value=""></option>
									<?= select_options( array('type'=>'number', 'min'=>'2000', 'max'=>date('Y') + 10, 'selected'=>$end_year) ) ?>
								</select>
							</td>
						</tr>
<?php
if (!$_SESSION['office']->account->is_private()) {
?>
						<tr>
							<td colspan="2">
								<table border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td class="left">License to Redistribute:</td>
										<td class="right">
											<input type="radio" name="license_redistribute" value="1" <?php if ($o_account->get_license_redistribute()) { print 'checked="checked"'; } ?> /> Yes
											<input type="radio" name="license_redistribute" value="0" <?php if (!$o_account->get_license_redistribute()) { print 'checked="checked"'; } ?> /> No
										</td>
										<td class="left" style="padding-left: 10px;">How Many:</td>
										<td class="right"><input type="text" name="license_how_many" value="<?= $o_account->get_license_how_many() ?>" /></td>
									</tr>
								</table>
							</td>
						</tr>
<?php
}
?>
						<tr>
							<td class="left">Account Type:</td>
							<td class="right">
								<select name="account_type" onChange="showAccountType(this.options[this.selectedIndex].value);">
									<option value="">Select Account Type</option>
<?php
if ($_SESSION['office']->account->is_private()) {
	select_options( array('type'=>'private_account_type', 'selected'=>$o_account->get_account_type()) );
}
else {
	select_options( array('type'=>'account_type', 'selected'=>$o_account->get_account_type()) );
}
?>
								</select>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<div id="AFFILIATEfields" name="AFFILIATEfields" style="display: none;">
									<table border="0" cellpadding="0" cellspacing="0">
<?php
if ($o_account->get_logo_path() && file_exists($_SERVER['DOCUMENT_ROOT'] . $o_account->get_logo_path())) {
?>
										<tr>
											<td class="left">Current Logo:</td>
											<td class="right"><img src="<?= $o_account->get_logo_path() ?>" border="0" width="250" /></td>
										</tr>
<?php
}
?>
										<tr>
											<td class="left">New Logo:</td>
											<td class="right"><input type="file" name="new_logo" id="new_logo" /></td>
										</tr>
										<tr>
											<td class="left">Monthly - 1 Area Pricing:</td>
											<td class="right"><input type="text" name="month_one_area_pricing" value="<?= $o_account->get_month_one_area_pricing() ?>" /></td>
										</tr>
										<tr>
											<td class="left">Monthly - 2 Area Pricing:</td>
											<td class="right"><input type="text" name="month_two_area_pricing" value="<?= $o_account->get_month_two_area_pricing() ?>" /></td>
										</tr>
										<tr>
											<td class="left">Monthly - 3+ Area Pricing:</td>
											<td class="right"><input type="text" name="month_additional_area_pricing" value="<?= $o_account->get_month_additional_area_pricing() ?>" /></td>
										</tr>
										<tr>
											<td class="left">Annual - 1 Area Pricing:</td>
											<td class="right"><input type="text" name="year_one_area_pricing" value="<?= $o_account->get_year_one_area_pricing() ?>" /></td>
										</tr>
										<tr>
											<td class="left">Annual - 2 Area Pricing:</td>
											<td class="right"><input type="text" name="year_two_area_pricing" value="<?= $o_account->get_year_two_area_pricing() ?>" /></td>
										</tr>
										<tr>
											<td class="left">Annual - 3+ Area Pricing:</td>
											<td class="right"><input type="text" name="year_additional_area_pricing" value="<?= $o_account->get_year_additional_area_pricing() ?>" /></td>
										</tr>
									</table>
								</div>
							</td>
						</tr>
						<tr>
							<td class="left">Account Status:</td>
							<td class="right">
								<select name="billing_status">
									<?= select_options( array('type'=>'billing_status', 'selected'=>$o_account->get_billing_status()) ) ?>
								</select>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<table border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td class="left">Referral Source:</td>
										<td>
											<select name="referral_source">
												<option value="">Click to Select</option>
<?php
$source_hash = array('Email','Facebook','Google','Yahoo','MSN','Other Search Engine','Flyer','Magazine','Referral','Sales Call','Convention','Word of Mouth','Other');
foreach ($source_hash as $source) {
?>
												<option value="<?= $source ?>" <?php if ($o_account->get_referral_source() == $source) { print 'selected="selected"'; } ?>><?= $source ?></option>
<?php
}
?>
											</select>
										</td>
										<td class="left" style="padding-left: 10px;">Other:</td>
										<td class="right"><input type="text" name="referral_source_other" value="<?= $o_account->get_referral_source_other() ?>" /></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td class="left">Converted Trial:</td>
							<td class="right"><input type="checkbox" name="trial_to_customer" value="1" <?php if ($o_account->get_trial_to_customer()) { print 'checked="checked"'; } ?> /></td>
						</tr>
						<tr>
							<td class="left">Cancel Reason:</td>
							<td class="right"><input type="text" maxlength="50" name="cancel_reason" size="50" value="<?= $o_account->get_cancel_reason() ?>" /></td>
						</tr>
					</TABLE>
				</fieldset>
<?php
if (!$_SESSION['office']->account->is_private()) {
?>
				<fieldset>
					<legend>Billing Information</legend>
<?php
	if ($_SESSION['office']->account->is_admin()) {
?>
					<a href="/office/account/charge_card.php?accountid=<?= $o_account->get_accountid() ?>">Charge Card</a>&nbsp;&nbsp;
<?php
					/* <a href="/office/account/charge_card.php?accountid=<?= $o_account->get_accountid() ?>">Recurring Billing</a> */
	}
?>
					<table border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td class="left" <?= $_SESSION['web_interface']->missing_required_label('card_name') ?>>Name on Card:</td>
							<td class="right"><input type="text" name="card_name" size="20" value="<?= $o_account->get_card_name() ?>" <?= $_SESSION['web_interface']->missing_required_input('card_name') ?>></td>
						</tr>
						<tr>
							<TD class="left" <?= $_SESSION['web_interface']->missing_required_label('card_type') ?>>Credit Card Type:</td>
							<TD class="right">
								<select name="card_type" <?= $_SESSION['web_interface']->missing_required_input('card_type') ?>>
									<option value="">Click to Select</option>
									<option value="VISA" <?php if ($o_account->get_card_type() == 'VISA') { print 'selected="selected"'; } ?>>Visa</option>
									<option value="MC" <?php if ($o_account->get_card_type() == 'MC') { print 'selected="selected"'; } ?>>MasterCard</option>
									<option value="DISC" <?php if ($o_account->get_card_type() == 'DISC') { print 'selected="selected"'; } ?>>Discover</option>
									<option value="AMEX" <?php if ($o_account->get_card_type() == 'AMEX') { print 'selected="selected"'; } ?>>AmEx</option>
									<option value="Invoice" <?php if ($o_account->get_card_type() == 'Invoice') { print 'selected="selected"'; } ?>>Invoice</option>
								</select>
							</td>
						</tr>
						<TR>
							<TD class="left" <?= $_SESSION['web_interface']->missing_required_label('card_number') ?>>Credit Card #:</td>
							<TD class="right"><input type="text" name="card_number" size="20" value=""></td>
						</tr>
						<tr>
							<TD class="left" <?= $_SESSION['web_interface']->missing_required_label('card_exp_date') ?>>Expiration mmyy:</td>
							<TD class="right"><input type="text" name="card_exp_date" size="5" value="<?= $o_account->get_card_exp_date() ?>" <?= $_SESSION['web_interface']->missing_required_input('card_exp_date') ?>></td>
						</tr>
						<tr>
							<td class="left">Rate:</td>
							<td class="right"><input type="text" name="rate" value="<?= $o_account->get_rate() ?>" /></td>
						</tr>
						<tr>
							<td class="left">Monthly Average:</td>
							<td class="right"><input type="text" name="monthly_average" value="<?= $o_account->get_monthly_average() ?>" /></td>
						</tr>
						<tr>
							<td class="left">Invoice #:</td>
							<td class="right"><input type="text" name="invoice_number" value="<?= $o_account->get_invoice_number() ?>" /></td>
						</tr>
						<tr>
							<td class="left">Merchant Services #:</td>
							<td class="right"><input type="text" name="authorize_net_number" value="<?= $o_account->get_authorize_net_number() ?>" /></td>
						</tr>
						<tr>
							<td class="left">Subscription Type:</td>
							<td class="right">
								<select name="subscription_type">
									<option value=""></option>
									<?= select_options( array('type'=>'subscription_type', 'selected'=>$o_account->get_subscription_type()) ) ?>
								</select>
							</td>
							<td class="left">Other:</td>
							<td class="right"><input type="text" name="subscription_type_other" value="<?= $o_account->get_subscription_type_other() ?>" /></td>
						</tr>
						<tr>
							<td class="left">Billing Type:</td>
							<td class="right">
								<select name="billing_type">
									<option value=""></option>
									<?= select_options( array('type'=>'billing_type', 'selected'=>$o_account->get_billing_type()) ) ?>
								</select>
							</td>
							<td class="left">Other:</td>
							<td class="right"><input type="text" name="billing_type_other" value="<?= $o_account->get_billing_type_other() ?>" /></td>
						</tr>
						<tr>
							<td class="left">Next Bill Date:</td>
							<td class="right">
								<select name="next_bill_month">
									<option value="">Month</option>
									<?= select_options( array('type'=>'month', 'selected'=>$next_bill_month) ) ?>
								</select>
								<select name="next_bill_day">
									<option value="">Day</option>
									<?= select_options( array('type'=>'number', 'min'=>1, 'max'=>28, 'selected'=>$next_bill_day) ) ?>
								</select>
								<select name="next_bill_year">
									<option value="">Year</option>
									<?= select_options( array('type'=>'number', 'max'=>(date('Y') + 2), 'min'=>2000, 'reverse'=>TRUE, 'selected'=>$next_bill_year) ) ?>
								</select>
							</td>
						</tr>
					</table>
					<h3>Billing History</h2>
					<div class="boxHeader">
						<table cellpadding="0" cellspacing="0" border="0">
							<colgroup>
								<col width="100"/>
								<col width="65"/>
								<col width="105"/>
								<col width="125"/>
								<col width="150"/>
							</colgroup>
							<tbody>
							<tr class="head">
								<th>Date</th>
								<th>Amount</th>
								<th>Type</th>
								<th>Approval Code</th>
								<th>Transaction ID</th>
							</tr>
							</tbody>
						</table>
					</div>
					<div class="boxScroll" style="height: 150px;">
						<table cellpadding="3" cellspacing="0" border="0">
							<colgroup>
								<col width="100"/>
								<col width="65"/>
								<col width="105"/>
								<col width="125"/>
								<col width="150"/>
							</colgroup>
							<tbody>
<?php
	if (empty($list_billing)) {
?>
							<tr><td colspan="10">No Billing History at this time.</td></tr>
<?php
	}
	else {
		foreach ($list_billing as $item_billing) {
?>
							<tr class="row<?= $_SESSION['web_interface']->get_row_class($row_num) ?>">
								<td><?= date('Y-m-d', strtotime($item_billing->get_created())) ?></td>
								<td><?= $item_billing->get_amount() ?></td>
								<td>
<?php
			if ($item_billing->get_type() == 'CREDIT' || !$item_billing->get_approval_code()) {
				print $item_billing->get_type();
			}
			else {
				print '<a href="javascript: document.location.href=\'/office/account/charge_card.html?accountid=' . $o_account->get_accountid() . '&billingid=' . $item_billing->get_billingid() . '\';">';
				if ($item_billing->get_type() == 'AUTH_CAPTURE') {
					print 'CHARGE';
				}
				else {
					print $item_billing->get_type();
				}
				print '</a>';
			}
?>
								</td>
								<td><?= $item_billing->get_approval_code() ?></td>
								<td><?= $item_billing->get_transaction_id() ?></td>
							</tr>
							<tr class="row<?= $row_num ?>">
								<td colspan="10"><?= $item_billing->get_notes() ?></td>
							</tr>
<?php
		}
	}
?>
							</tbody>
						</table>
					</div>
				</fieldset>
<?php
}
if ($_SESSION['office']->account->is_admin() || $_SESSION['office']->account->is_billing()) {
?>
				<fieldset>
					<legend>Email</legend>
					Create an Email based on one of the below templates to send to this account.
					<select id="letterid" name="letterid" style="margin: 10px 0px; width: 500px;">
<?php
	if (!empty($list_letter)) {
		foreach ($list_letter as $item_letter) {
?>
						<option value="<?= $item_letter->get_letterid() ?>"><?= $item_letter->get_title() ?></option>
<?php
		}
	}
?>
					</select>&nbsp;
					<button name="send_email" onClick="document.getElementById('cmd').value = 'create_email'; document.accountForm.submit();">Create Email</button>
				</fieldset>
<?php
}
?>
				<fieldset>
					<legend>Notes</legend>
					<textarea name="notes" cols="50" rows="15"><?= $o_account->get_notes() ?></textarea>
				</fieldset>
				<fieldset>
					<legend>Prospecting Notes</legend>
					<textarea name="prospecting_notes" cols="50" rows="15"><?= $o_account->get_prospecting_notes() ?></textarea>
				</fieldset>
<?php
if ($o_account->get_accountid()) {
?>
				<button onClick="document.getElementById('cmd').value = 'save_account'; document.accountForm.submit();" type="button">Update</button>&nbsp;
<?php
	if (!$_SESSION['office']->account->is_private()) {
?>
				<button onClick="if (confirm('ARE YOU SURE YOU WANT TO DELETE THIS ACCOUNT?')) { document.location.href='/office/account/handle.php?cmd=delete&accountid=<?= $o_account->get_accountid() ?>'; }" type="button">Delete This Account</button>
<?php
	}
}
else {
?>
				<input type="submit" value="Save" />
<?php
}
?>
			</div>
			</form>
		</td>
	</tr>
</table>
<script type="text/javascript">
	function credit_card(billingid) {
		if (confirm('ARE YOU SURE YOU WANT TO CREDIT THIS AMOUNT?')) {
			document.location.href='/office/account/handle.html?cmd=credit_card&billingid=' + billingid;
		}
	}
	
	function showAccountType(accountType) {
		if (accountType == 'AFFILIATE') {
			expandFirst('AFFILIATEfields');
		}
		else {
			collapseAll('AFFILIATEfields');
		}
	}
	showAccountType('<?= $o_account->get_account_type() ?>');
</script>
<?php
include_once($_SESSION['web_interface']->get_server_path('office/global/bottom.php'));
?>

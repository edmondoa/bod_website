<?php
$_PAGE_TITLE = 'Account';

include_once($_SESSION['web_interface']->get_server_path('office/global/top.php'));
?>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td class="bodyNav" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('office/account/left_nav_' . $_SESSION['office']->account->get_account_type() . '.php')); ?>
		</td>
		<td class="bodyMain" valign="top">
			<h1>Request to Update Account</h1>
			<p>Your account update was successful!  You will receive an email regarding these changes within 1 - 2 business days.</p>
			<p>If you have questions, email support at <a href="mailto:support@<?= $_SESSION['office']->account->o_company->get_url() ?>">support@<?= $_SESSION['office']->account->o_company->get_url() ?></a> or call us at (801) 494-0470.</p>
		</td>
	</tr>
</table>
<?php
include_once($_SESSION['web_interface']->get_server_path('office/global/bottom.php'));
?>
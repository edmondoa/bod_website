<?php
$_PAGE_TITLE = 'Account';

require_once('office/account/class.account.php');
try {
	$o_account = new account( array('accountid'=>$_REQUEST['accountid']) );
	if (!$o_account->isOwner()) {
		$_SESSION['web_interface']->destroySession();
		exit;
	}
}
catch (Exception $exception) {
	throw $exception;
}

require_once('office/account/class.area_name.php');
try {
	$o_area_name_utilities = new area_name_utilities();
}
catch (Exception $exception) {
	throw $exception;
}

include_once($_SESSION['web_interface']->get_server_path('office/global/top.php'));
?>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td class="bodyNav" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('office/account/left_nav_' . $_SESSION['office']->account->get_account_type() . '.php')); ?>
		</td>
		<td class="bodyMain" valign="top">
			<h1>Manage Areas</h1>
			Select the areas that you would like.
			<form method="post" action="/office/account/handle.php">
			<input type="hidden" name="cmd" value="save_area_names" />
			<input type="hidden" name="accountid" value="<?= $o_account->get_accountid() ?>" />
<?php
foreach ($o_area_name_utilities->get_list_area_states() as $area_state) {
	$checkbox_id = ($area_state == 'Washington D.C.') ? 'DC' : $area_state;
?>
			<fieldset>
				<legend><?= $area_state ?></legend>
				<table border="0" cellspacing="0" cellpadding="0">
					<tr><td><input type="checkbox" onclick="checkAll(this, '<?= $checkbox_id ?>');"/>Check All</td></tr>
					<tr>
<?php
	$count = 0;
	foreach ($o_area_name_utilities->get_list_area_titles( array('area_state'=>$area_state) ) as $area_title) {
		$o_area_name = new area_name( array('area_state'=>$area_state, 'area_title'=>$area_title) );
		if (preg_match("|," . preg_replace("/[\(\)]/", "", $o_area_name->get_area_name()) . ",|", preg_replace("/[\(\)]/", "", $o_account->get_string_area_names()))) {
?>
						<td style="font-weight: bold;"><input type="checkbox" name="area_name_checkbox[]" id="<?= $area_state ?>_checkbox" value="<?= $o_area_name->get_area_name() ?>" checked="checked" /><?= $area_title ?></td>
<?php
		}
		else {
?>
						<td><input type="checkbox" name="area_name_checkbox[]" id="<?= $checkbox_id ?>_checkbox" value="<?= $o_area_name->get_area_name() ?>" /><?= $area_title ?></td>
<?php
		}
		if ($count == 0) {
			$count = 0;
			print '</tr><tr>';
		}
		else {
			$count++;
		}
	}
?>
				</tr>
			</table>
			</fieldset>
<?php
}
?>
			<input type="submit" value="Save Areas" onClick="return confirm('Are you sure you want to update your areas?');" />
			</form>
		</td>
	</tr>
</table>
<?php
include_once($_SESSION['web_interface']->get_server_path('office/global/bottom.php'));
?>

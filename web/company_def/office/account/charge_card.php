<?php
if ($_SESSION['office']->account->get_account_type() != 'ADMIN') {
	$_SESSION['web_interface']->destroySession();
	exit;
}
$_PAGE_TITLE = 'Charge Card';

require_once('office/account/class.account.php');
$o_account = new account( array('accountid'=>$_REQUEST['accountid']) );
if (!$o_account->isOwner()) {
	$_SESSION['web_interface']->destroySession();
	exit;
}

require_once('office/account/class.billing.php');
$o_billing = new billing( array('billingid'=>$_REQUEST['billingid']) );

include_once($_SESSION['web_interface']->get_server_path('office/global/top.php'));
if ($_SESSION['office']->account->is_admin()) {
?>
<script type="text/javascript">
	function confirm_submit() {
		if (confirm('ARE YOU SURE YOU WANT TO SUBMIT?')) {
			document.getElementById('credit_form').submit();
		}
	}
</script>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td class="bodyNav" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('office/account/left_nav_' . $_SESSION['office']->account->get_account_type() . '.php')); ?>
		</td>
		<td class="bodyMain" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('other/misc/status_error.php')); ?>
			<form method="post" id="credit_form" name="credit_form" action="/office/account/handle.php">
			<input type="hidden" name="cmd" value="charge_card" />
			<input type="hidden" name="accountid" value="<?= $o_account->get_accountid() ?>" />
			<input type="hidden" name="billingid" value="<?= $o_billing->get_billingid() ?>" />
<?php
// you are trying to CREDIT a charge
if ($o_billing->get_billingid() && $o_billing->get_type() != 'CREDIT') {
?>
			<input type="hidden" name="amount" value="<?= $o_billing->get_amount() ?>" />
			<input type="hidden" name="type_of_charge" value="CREDIT" />
			<input type="hidden" name="x_trans_id" value="<?= $o_billing->get_transaction_id() ?>" />
			<div style="color: #FF0000; font-size: 14pt; font-weight: bold;">Credit for: <a href="/office/account/account.php?accountid=<?= $o_account->get_accountid() ?>"><?= $o_account->get_first_name() ?> <?= $o_account->get_last_name() ?></a></div>
<?php
}
else {
?>
			<input type="hidden" name="type_of_charge" value="AUTH_CAPTURE" />
			<div style="font-size: 14pt; font-weight: bold;">Charge for: <a href="/office/account/account.php?accountid=<?= $o_account->get_accountid() ?>"><?= $o_account->get_first_name() ?> <?= $o_account->get_last_name() ?></a></div>
<?php
}
?>
			<table border="0" cellpadding="3" cellspacing="3">
				<tr><td>Name on Card: </td><td><?= $o_account->get_card_name() ?></td></tr>
				<tr><td>Credit Card Type: </td><td><?= $o_account->get_card_type() ?></td></tr>
				<tr><td>Expiration mmyy: </td><td><?= $o_account->get_card_exp_date() ?></td></tr>
				<tr><td>Address: </td><td><?= $o_account->o_business_address->get_line1() . ' ' . $o_account->o_business_address->get_line2() ?></td></tr>
				<tr><td>City: </td><td><?= $o_account->o_business_address->get_city() ?></td></tr>
				<tr><td>State: </td><td><?= $o_account->o_business_address->get_state() ?></td></tr>
				<tr><td <?= $_SESSION['web_interface']->missing_required_label('amount') ?>>Amount:* </td><td>
<?php
// you are trying to CREDIT a charge
if ($o_billing->get_billingid() && $o_billing->get_type() != 'CREDIT') {
	print '$' . sprintf("%.2f", $o_billing->get_amount());
}
else {
?>
				<input type="text" name="amount" value="<?= $_REQUEST['amount'] ?>"  <?= $_SESSION['web_interface']->missing_required_input('amount') ?>/></td></tr>
<?php
}
?>
				<tr><td <?= $_SESSION['web_interface']->missing_required_label('notes') ?>>Notes: </td><td><textarea name="notes" cols="50" rows="3" <?= $_SESSION['web_interface']->missing_required_label('notes') ?>><?= $_REQUEST['notes'] ?></textarea></td></tr>
				<tr><td colspan="2"><input type="button" onClick="confirm_submit();" value="Submit" /></td></tr>
			</table>
		</td>
	</tr>
</table>
<?php
}
include_once($_SESSION['web_interface']->get_server_path('office/global/bottom.php'));
?>

<?php
if ($_SESSION['office']->account->get_account_type() != 'ADMIN') {
	$_SESSION['web_interface']->destroySession();
	exit;
}
$_PAGE_TITLE = 'Account';

require_once('office/account/class.letter.php');
$o_account = new account( array('accountid'=>$_REQUEST['accountid']) );
if (!$o_account->isOwner()) {
	$_SESSION['web_interface']->destroySession();
	exit;
}

require_once('office/account/class.letter.php');
$o_letter = new letter( array('letterid'=>$_REQUEST['letterid']) );
$email_segments = $o_letter->merge_segments( array('o_account'=>$o_account) );

$_REQUEST['bcc_list'] = ($_REQUEST['bcc_list']) ? $_REQUEST['bcc_list'] : $email_segments['bcc_list'];
$_REQUEST['body'] = ($_REQUEST['body']) ? $_REQUEST['body'] : $email_segments['body'];
$_REQUEST['cc_list'] = ($_REQUEST['cc_list']) ? $_REQUEST['cc_list'] : $email_segments['cc_list'];
$_REQUEST['from'] = ($_REQUEST['from']) ? $_REQUEST['from'] : $email_segments['from_email'];
$_REQUEST['subject'] = ($_REQUEST['subject']) ? $_REQUEST['subject'] : $email_segments['subject'];
$_REQUEST['to'] = ($_REQUEST['to']) ? $_REQUEST['to'] : $o_account->get_main_email_address();

include_once($_SESSION['web_interface']->get_server_path('office/global/top.php'));
?>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td class="bodyNav" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('office/account/left_nav_' . $_SESSION['office']->account->get_account_type() . '.php')); ?>
		</td>
		<td class="bodyMain" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('other/misc/status_error.php')); ?>
			<h1>Create Email</h1>
			<p>Here you may create and send an email to: <a href="/office/account/account.php?accountid=<?= $o_account->get_accountid() ?>"><?= $o_account->get_first_name() ?> <?= $o_account->get_last_name() ?></a>.</p>
			<form method="post" action="/office/account/handle.html">
			<input type="hidden" name="cmd" value="send_email" />
			<input type="hidden" name="accountid" value="<?= $o_account->get_accountid() ?>" />
			<input type="hidden" name="letterid" value="<?= $o_letter->get_letterid() ?>" />
			<table border="0" cellpadding="0" cellspacing="2">
				<tr>
					<td <?= $_SESSION['web_interface']->missing_required_label('to') ?>>To:</td>
					<td><input type="text" name="to" value="<?= $_REQUEST['to'] ?>" size="75" <?= $_SESSION['web_interface']->missing_required_input('to') ?> /></td>
				</tr>
				<tr>
					<td <?= $_SESSION['web_interface']->missing_required_label('cc_list') ?>>Cc List:</td>
					<td><input type="text" name="cc_list" value="<?= $_REQUEST['cc_list'] ?>" size="75" <?= $_SESSION['web_interface']->missing_required_input('cc_list') ?> /></td>
				</tr>
				<tr>
					<td <?= $_SESSION['web_interface']->missing_required_label('bcc_list') ?>>Bcc List:</td>
					<td><input type="text" name="bcc_list" value="<?= $_REQUEST['bcc_list'] ?>" size="75" <?= $_SESSION['web_interface']->missing_required_input('bcc_list') ?> /></td>
				</tr>
				<tr>
					<td <?= $_SESSION['web_interface']->missing_required_label('from') ?>>From:</td>
					<td><input type="text" name="from" value="<?= $_REQUEST['from'] ?>" size="75" <?= $_SESSION['web_interface']->missing_required_input('from') ?> /></td>
				</tr>
				<tr>
					<td <?= $_SESSION['web_interface']->missing_required_label('subject') ?>>Subject:</td>
					<td><input type="text" name="subject" value="<?= $_REQUEST['subject'] ?>" size="75" <?= $_SESSION['web_interface']->missing_required_input('subject') ?> /></td>
				</tr>
				<tr><td colspan="2" <?= $_SESSION['web_interface']->missing_required_label('body') ?>>Body:</td></tr>
				<tr><td colspan="2"><textarea name="body" cols="64" rows="25" <?= $_SESSION['web_interface']->missing_required_input('body') ?>><?= $_REQUEST['body'] ?></textarea></td></tr>
			</table>
			<input type="submit" value="Submit" />
			</form>
		</td>
	</tr>
</table>
<?php
include_once($_SESSION['web_interface']->get_server_path('office/global/bottom.php'));
?>
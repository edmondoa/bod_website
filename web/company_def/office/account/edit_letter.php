<?php
if ($_SESSION['office']->account->get_account_type() != 'ADMIN') {
	$_SESSION['web_interface']->destroySession();
	exit;
}
$_PAGE_TITLE = 'Account';

require_once('office/account/class.letter.php');
$o_letter = new letter( array('letterid'=>$_REQUEST['letterid']) );
$o_letter->initialize($_REQUEST);

include_once($_SESSION['web_interface']->get_server_path('office/global/top.php'));
?>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td class="bodyNav" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('office/account/left_nav_' . $_SESSION['office']->account->get_account_type() . '.php')); ?>
		</td>
		<td class="bodyMain" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('other/misc/status_error.php')); ?>
			<h1>Edit / Create Letter</h1>
			<p>Here you may edit / create a letter to be available to send to an account. Below the form is a list of the mergefields that are available to be used in the subject or body of the email.  All mergefields should be enclosed by {}. All fields (i.e. Title, Subject, Body) are required.</p>
			<form method="post" action="/office/account/handle.html">
			<input type="hidden" name="cmd" value="save_letter" />
			<input type="hidden" name="letterid" value="<?= $o_letter->get_letterid() ?>" />
			<table border="0" cellpadding="0" cellspacing="2">
				<tr>
					<td <?= $_SESSION['web_interface']->missing_required_label('title') ?>>Title:</td>
					<td><input type="text" name="title" value="<?= $o_letter->get_title() ?>" size="75" <?= $_SESSION['web_interface']->missing_required_input('title') ?> /></td>
				</tr>
				<tr>
					<td <?= $_SESSION['web_interface']->missing_required_label('subject') ?>>Subject:</td>
					<td><input type="text" name="subject" value="<?= $o_letter->get_subject() ?>" size="75" <?= $_SESSION['web_interface']->missing_required_input('subject') ?> /></td>
				</tr>
				<tr>
					<td <?= $_SESSION['web_interface']->missing_required_label('from_email') ?>>From Email:</td>
					<td><input type="text" name="from_email" value="<?= $o_letter->get_from_email() ?>" size="75" <?= $_SESSION['web_interface']->missing_required_input('from_email') ?> /></td>
				</tr>
				<tr>
					<td <?= $_SESSION['web_interface']->missing_required_label('cc_list') ?>>Cc List:</td>
					<td><input type="text" name="cc_list" value="<?= $o_letter->get_cc_list() ?>" size="75" <?= $_SESSION['web_interface']->missing_required_input('cc_list') ?> /></td>
				</tr>
				<tr>
					<td <?= $_SESSION['web_interface']->missing_required_label('bcc_list') ?>>Bcc List:</td>
					<td><input type="text" name="bcc_list" value="<?= $o_letter->get_bcc_list() ?>" size="75" <?= $_SESSION['web_interface']->missing_required_input('bcc_list') ?> /></td>
				</tr>
				<tr><td colspan="2" <?= $_SESSION['web_interface']->missing_required_label('body') ?>>Body:</td></tr>
				<tr><td colspan="2"><textarea name="body" cols="64" rows="25" <?= $_SESSION['web_interface']->missing_required_input('body') ?>><?= $o_letter->get_body() ?></textarea></td></tr>
			</table>
			<input type="submit" value="Submit" />
			</form>
			<h1 style="padding-top: 15px;">Mergefields</h1>
			<p style="font-weight: bold;">The following merge fields are special and have the listed properties:</p>
			<table border="0" cellpadding="0" cellspacing="3">
				<tr><td>secure_card_number: shows the card number with all numbers as X's except the last 4</td></tr>
				<tr><td>welcome_letter_pricing: i.e. Monthly pricing: $19.00 for 1 area<br/>(Add an additional area for only $6/month!)</td></tr>
				<tr><td>next_bill_day, next_bill_month, next_bill_year: the appropriate part of the next bill date</td></tr>
				<tr><td>next_bill_date_string: i.e. 'the 4th day of each month' or 'May 3rd of each year'</td></tr>
				<tr><td>subscription_areas: the areas they are subscribed to</td></tr>
				<tr><td>plural_areas: inserts an s if there are multiple areas</td></tr>
				<tr><td>monthly_pricing_equivalent: monthly pricing equivalent</td></tr>
				<tr><td>most_recent_charge: most recent charge</td></tr>
				<tr><td>number_of_areas: number of areas</td></tr>
				<tr><td>today_date, today_day, today_month, today_year: parts of or entire todays date</td></tr>
				<tr><td>csv_attachment:  (with comma delimited (.csv) file attached)</td></tr>
				<tr><td>start_end_day_difference: day difference between start and end dates</td></tr>
			</table>
			<p style="font-weight: bold;">You may merge any fields from the account table as is:</p>
			<table border="0" cellpadding="0" cellspacing="3">
				<tr>
<?php
$count = 0;
foreach ($_SESSION['office']->account->get_table_fields() as $field) {
	if ($field == 'password') { continue; }
	print '<td style="padding-right: 10px;">' . $field . '</td>';
	$count++;
	if ($count == 2) {
		$count = 0;
		print '</tr><tr>';
	}
} 
?>
				</tr>
			</table>
			<p style="font-weight: bold;">You may merge any fields from the company table by preceeding the below field with company (i.e. {company_main_email_address}):</p>
			<table border="0" cellpadding="0" cellspacing="3">
				<tr>
<?php
$count = 0;
foreach ($_SESSION['office']->account->o_company->get_table_fields() as $field) {
	print '<td style="padding-right: 10px;">' . $field . '</td>';
	$count++;
	if ($count == 2) {
		$count = 0;
		print '</tr><tr>';
	}
} 
?>
				</tr>
			</table>
		</td>
	</tr>
</table>
<?php
include_once($_SESSION['web_interface']->get_server_path('office/global/bottom.php'));
?>
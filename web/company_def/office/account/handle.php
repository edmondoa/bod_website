<?php
#
# @copyright Copyright (c) 2017 Green Seed Technologies, Inc. All rights reserved.
#
# @author David Hoover
# @author David McLaughlin <david@greenseedtech.com>
#

require_once('office/account/class.account.php');

switch (strtolower($_REQUEST['cmd'])) {

	// handle an activate_account request
	case 'activate_account':
		// try to charge card on file
		try {
			$_SESSION['office']->account->activate_account();
			$_SESSION['status']['message'] = 'Your Credit Card has been charged $' . sprintf("%.2f", $_SESSION['office']->account->get_rate()) . '. Your subscription has been renewed and your next billing date will be ' . date('F j, Y', strtotime($_SESSION['office']->account->get_next_bill_date())) . '.';
		}
		catch (Exception $exception) {
			$_SESSION['error']['message'] = $exception->getMessage();
		}
		include_once($_SESSION['web_interface']->get_server_path('office/account/edit.php'));
	break;
	
	// handle a cancel_account request
	case 'cancel_account':
		$_SESSION['office']->account->cancel_by_account();
		$body = 'Account ' . $_SESSION['office']->account->get_accountid() . ' has requested their account be cancelled.';
		mail('admin@byownerdaily.com', 'Request for Account Cancellation: ' . $_SESSION['office']->account->get_first_name() . ' ' . $_SESSION['office']->account->get_last_name(), $body, "From: billing@byownerdaily.com");
		include_once($_SESSION['web_interface']->get_server_path('office/account/cancel_account_confirmed.php'));
	break;
	
	// handle a CHARGE_CARD request
	case 'charge_card':
		require_once('office/account/class.billing.php');
		try {
			$o_account = new account( array('accountid'=>$_REQUEST['accountid']) );
			if (!$o_account->isOwner()) {
				$_SESSION['web_interface']->destroySession();
				exit;
			}
			if (!$_SESSION['office']->account->is_admin()) {
				throw new Exception('You are not authorized to perform this action.');
			}
			$_SESSION['web_interface']->check_required( array(required=>'type_of_charge,amount') );
			if (isset($_SESSION['error'])) {
				throw new Exception($_SESSION['error']['message']);
			}
			
			require_once('common/class.authorize_net.php');
			$o_authorize_net = new authorize_net();
			$r_one_time_charge = $o_authorize_net->one_time_charge(
				array(
					'x_type'=>$_REQUEST['type_of_charge'],
					'x_trans_id'=>$_REQUEST['x_trans_id'],
					'x_cust_id'=>$o_account->get_customer_number(),
					'x_token'=>$o_account->getToken(),
					'x_exp_date'=>$o_account->get_card_exp_date(),
					'x_description'=>$_REQUEST['notes'],
					'x_amount'=>$_REQUEST['amount'],
					'x_first_name'=>$o_account->get_first_name(),
					'x_last_name'=>$o_account->get_last_name(),
					'x_address'=>$o_account->o_business_address->get_line1() . ' ' . $o_account->o_business_address->get_line2(),
					'x_city'=>$o_account->o_business_address->get_city(),
					'x_state'=>$o_account->o_business_address->get_state(),
					'x_zip'=>$o_account->o_business_address->get_postal_code()
				)
			);
			if ($r_one_time_charge['status'] != 1) {
				throw new Exception('There was an error processing the transaction:<br/>' . $r_one_time_charge['message']);
			}
			
			$o_billing = new billing(
				array(
					'accountid'=>$o_account->get_accountid(),
					'amount'=>$_REQUEST['amount'],
					'type'=>$_REQUEST['type_of_charge'],
					'approval_code'=>$r_one_time_charge['approval_code'],
					'transaction_id'=>$r_one_time_charge['transaction_id'],
					'notes'=>$_REQUEST['notes']
				)
			);
			$o_billing->save();
			if ($_REQUEST['type_of_charge'] == 'AUTH_CAPTURE') {
				$_SESSION['status']['message'] = 'The Credit Card has been charged $' . sprintf("%.2f", $_REQUEST['amount']) . '.';
			}
			else if ($_REQUEST['type_of_charge'] == 'CREDIT') {
				$_SESSION['status']['message'] = 'The Credit Card has been credited $' . sprintf("%.2f", $_REQUEST['amount']) . '.';
			}
			unset($_REQUEST['notes']);
			include_once($_SESSION['web_interface']->get_server_path('office/account/account.php'));
		}
		catch (Exception $exception) {
			$_SESSION['error']['message'] = $exception->getMessage();
			include_once($_SESSION['web_interface']->get_server_path('office/account/charge_card.php'));
		}
	break;
	
	case 'continue_subscription':
		$_SESSION['office']->account->continue_subscription();
		$_SESSION['status']['message'] = 'Your account is no longer scheduled to be cancelled.';
		include_once($_SESSION['web_interface']->get_server_path('office/account/edit.php'));
	break;
	
	// handle a CREATE_EMAIL request
	case 'create_email':
		include_once($_SESSION['web_interface']->get_server_path('office/account/create_email.php'));
	break;
	
	//handle a DELETE request
	case 'delete':
		try {
			$o_account = new account( array('accountid'=>$_REQUEST['accountid']) );
			if (!$o_account->isOwner()) {
				$_SESSION['web_interface']->destroySession();
				exit;
			}
			$o_account->delete();
			$_SESSION['status']['message'] = 'The account has been deleted.';
			include_once($_SESSION['web_interface']->get_server_path('office/account/search.php'));
		}
		catch (Exception $exception) {
			throw $exception;
		}
	break;
	
	//handle a DELETE_LETTER request
	case 'delete_letter':
		try {
			require_once('office/account/class.letter.php');
			$o_letter = new letter( array('letterid'=>$_REQUEST['letterid']) );
			$o_letter->delete();
			$_SESSION['status']['message'] = 'The letter has been deleted.';
			include_once($_SESSION['web_interface']->get_server_path('office/account/letter.php'));
		}
		catch (Exception $exception) {
			throw $exception;
		}
	break;
	
	// handle a DOWNLOAD_ALL request
	case 'download_all':
		header("Content-type: application/vnd.ms-excel");
		header("Content-disposition: attachment; filename=contacts.csv");
		print '"accountid","affiliateid","account_type","username","first_name","last_name","business_name","main_email_address","billing_email_address","alert_email_address","email_attachment","personal_phone","business_phone","fax_phone","cell_phone","personal_addressid","business_addressid","logo_path","pass_question","pass_answer","login_count","last_login","login_attempt","created","billing_day","billing_status","billing_status_date","billing_type","billing_type_other","next_bill_date","card_name","card_type","card_exp_date","start_date","end_date","customer_number","customer_type","customer_type_other","invoice_number","authorize_net_number","subscription_type","subscription_type_other","license_redistribute","license_how_many","rate","monthly_average","referral_source","referral_source_other","converted_trial","url","notes","prospecting_notes","month_additional_area_pricing","month_one_area_pricing","month_two_area_pricing","year_additional_area_pricing","year_one_area_pricing","year_two_area_pricing","companyid"';
		print "\n";
				
		$o_account_utilities = new account_utilities();
		$list_account = $o_account_utilities->get_list( array('expecting'=>'hash') );
		foreach ($list_account as $accountid=>$account) {
			$notes = preg_replace("/\n/", " ", $account['notes']);
			$notes = preg_replace("/\r/", "", $notes);
			$prospecting_notes = preg_replace("/\n/", " ", $account['prospecting_notes']);
			$prospecting_notes = preg_replace("/\r/", "", $prospecting_notes);
			
			print '"' . $account['accountid'] . '","' . $account['affiliateid'] . '","' . $account['account_type'] . '","' . $account['username'] . '","' . $account['first_name'] . '","' . $account['last_name'] . '","' . $account['business_name'] . '","' . $account['main_email_address'] . '","' . $account['billing_email_address'] . '","' . $account['alert_email_address'] . '","' . $account['email_attachment'] . '","' . $account['personal_phone'] . '","' . $account['business_phone'] . '","' . $account['fax_phone'] . '","' . $account['cell_phone'] . '","' . $account['personal_addressid'] . '","' . $account['business_addressid'] . '","' . $account['logo_path'] . '","' . $account['pass_question'] . '","' . $account['pass_answer'] . '","' . $account['login_count'] . '","' . $account['last_login'] . '","' . $account['login_attempt'] . '","' . $account['created'] . '","' . $account['billing_day'] . '","' . $account['billing_status'] . '","' . $account['billing_status_date'] . '","' . $account['billing_type'] . '","' . $account['billing_type_other'] . '","' . $account['next_bill_date'] . '","' . $account['card_name'] . '","' . $account['card_type'] . '","' . $account['card_exp_date'] . '","' . $account['start_date'] . '","' . $account['end_date'] . '","' . $account['customer_number'] . '","' . $account['customer_type'] . '","' . $account['customer_type_other'] . '","' . $account['invoice_number'] . '","' . $account['authorize_net_number'] . '","' . $account['subscription_type'] . '","' . $account['subscription_type_other'] . '","' . $account['license_redistribute'] . '","' . $account['license_how_many'] . '","' . $account['rate'] . '","' . $account['monthly_average'] . '","' . $account['referral_source'] . '","' . $account['referral_source_other'] . '","' . $account['trial_to_customer'] . '","' . $account['url'] . '","' . $notes . '","' . $prospecting_notes . '","' . $account['month_additional_area_pricing'] . '","' . $account['month_one_area_pricing'] . '","' . $account['month_two_area_pricing'] . '","' . $account['year_additional_area_pricing'] . '","' . $account['year_one_area_pricing'] . '","' . $account['year_two_area_pricing'] . '","' . $account['companyid'] . "\"\n";
		}
		exit;
	break;
	
	// handle a reprocess_credit_card request
	case 'reprocess_credit_card':
		try {
			$_SESSION['office']->account->reprocess_credit_card();
			$_SESSION['status']['message'] = 'Your Credit Card has been charged $' . sprintf("%.2f", $_SESSION['office']->account->get_rate()) . ' and your next billing date has been set to ' . date('F j, Y', strtotime($_SESSION['office']->account->get_next_bill_date())) . '.';
		}
		catch (Exception $exception) {
			$_SESSION['error']['message'] = $exception->getMessage();
		}
		include_once($_SESSION['web_interface']->get_server_path('office/account/edit.php'));
	break;
	
	//handle a SAVE request
	case 'save':
		$_REQUEST['email_attachment'] = ($_REQUEST['email_attachment']) ? $_REQUEST['email_attachment'] : 0;
		
		$_SESSION['web_interface']->check_required( array(required=>'username,first_name,last_name,personal_phone,personal_line1,personal_city,personal_country,personal_state,personal_postal_code,business_name,business_phone,business_line1,business_city,business_country,business_state,business_postal_code,main_email_address') );
		// check password
		if ($_REQUEST['new_password'] && $_SESSION['web_interface']->getHashString( array('salt'=>$_SESSION['office']->account->get_accountid(), 'string'=>$_REQUEST['current_password']) ) != $_SESSION['office']->account->get_password()) {
			error_log($_SESSION['web_interface']->getHashString( array('salt'=>$_SESSION['office']->account->get_accountid(), 'string'=>$_REQUEST['current_password']) ) . ' - ' . $_SESSION['office']->account->get_password());
			$_SESSION['error']['message'] = 'You have tried to change your password but your current password did not match that stored in our system.  Please try again.  If you did not mean to try to update your password simply leave both the Current Password and New Password fields blank.';
		}
		else if ($_REQUEST['new_password'] && $_SESSION['web_interface']->getHashString( array('salt'=>$_SESSION['office']->account->get_accountid(), 'string'=>$_REQUEST['current_password']) ) == $_SESSION['office']->account->get_password()) {
			$_REQUEST['password'] = $_SESSION['web_interface']->getHashString( array('salt'=>$_SESSION['office']->account->get_accountid(), 'string'=>$_REQUEST['new_password']) );
		}
		else {
			$_REQUEST['password'] = $_SESSION['office']->account->get_password();
		}
		// check username
		if ($_SESSION['office']->account->invalid_username($_REQUEST['username'])) {
			$_SESSION['error']['message'] = 'The username you have entered is invalid.  Usernames must only contain letters, numbers, or be an email address, and be more than 2 characters in length.';
		} 
		if ($_SESSION['office']->account->is_username_taken($_REQUEST['username'])) {
			$_SESSION['error']['message'] = 'The username you have entered is already in use.  Please choose another.';
		}
		
		// check to make sure they have put in a valid card_exp_date
		if (!preg_match("/^\d{4}$/", $_REQUEST['card_exp_date'])) {
			$_SESSION['error']['badfield']['card_exp_date'] = TRUE;
			$_SESSION['error']['message'] .= '<br/>Please enter a valid Credit Card Expiration Date.';
		}
		else {
			$month = preg_replace("/^(\d\d)\d\d$/", "$1", $_REQUEST['card_exp_date']);
			$year = '20' . preg_replace("/^\d\d(\d\d)$/", "$1", $_REQUEST['card_exp_date']);
			if (strtotime($year . '-' . $month . '-01') < strtotime(date('Y-m-01'))) {
				$_SESSION['error']['badfield']['card_exp_date'] = TRUE;
				$_SESSION['error']['message'] .= '<br/>Please enter a valid Credit Card Expiration Date.';
			}
		}
		
		if (isset($_SESSION['error'])) {
			include_once($_SESSION['web_interface']->get_server_path('office/account/edit.php'));
		}
		else {
			try {
				// don't change card number if have X's in it
				if (preg_match("/X/", $_REQUEST['card_number'])) {
					$_REQUEST['card_number'] = $_SESSION['office']->account->get_card_number();
				}
				else if ($_REQUEST['card_number']) {
					$_REQUEST['card_number'] = $_SESSION['web_interface']->getEncrypt($_REQUEST['card_number']);
				}
				
				// if they change credit card info send email
				// log it for now since some are apparently slipping through
				$log_info = "##### " . $_SESSION['office']->account->get_accountid() . " - " . date('Y-m-d H:i:s') . " #####\n";
				$log_info .= "Name: " . $_SESSION['office']->account->get_card_name() . " - " . $_REQUEST['card_name'] . "\n";
				$log_info .= "Type: " . $_SESSION['office']->account->get_card_type() . " - " . $_REQUEST['card_type'] . "\n";
				$log_info .= "Date: " . $_SESSION['office']->account->get_card_exp_date() . " - " . $_REQUEST['card_exp_date'] . "\n";
				$log_info .= "Email: " . $_SESSION['office']->account->get_main_email_address() . " - " . $_REQUEST['main_email_address'] . "\n\n";
				file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/scripts/backup/account_change.log', $log_info, FILE_APPEND);
				if ($_REQUEST['card_number'] != $_SESSION['office']->account->get_card_number( array('account_type'=>'ADMIN') ) || $_REQUEST['card_name'] != $_SESSION['office']->account->get_card_name() || $_REQUEST['card_type'] != $_SESSION['office']->account->get_card_type() || $_REQUEST['card_exp_date'] != $_SESSION['office']->account->get_card_exp_date() || $_REQUEST['main_email_address'] != $_SESSION['office']->account->get_main_email_address()) {
					$body = "The following account has changed their account information:\n" . 
						"Name: " . $_SESSION['office']->account->get_first_name() . ' ' . $_SESSION['office']->account->get_last_name() . "\n" . 
						"Business Name: " . $_SESSION['office']->account->get_business_name() . "\n" .
						"\n" . 
						"Information Changed:\n"
					;
					if ($_REQUEST['main_email_address'] != $_SESSION['office']->account->get_main_email_address()) {
						$body .= "Old Email Address: " . $_SESSION['office']->account->get_main_email_address() . "\n" . "New Email Address: " . $_REQUEST['main_email_address'] . "\n\n";
					}
					if ($_REQUEST['card_name'] != $_SESSION['office']->account->get_card_name()) {
						$body .= "Old Card Name: " . $_SESSION['office']->account->get_card_name() . "\n" . "New Card Name: " . $_REQUEST['card_name'] . "\n\n";
					}
					if ($_REQUEST['card_number'] != $_SESSION['office']->account->get_card_number()) {
						$body .= "Card Number Changed\n\n";
					}
					if ($_REQUEST['card_type'] != $_SESSION['office']->account->get_card_type()) {
						$body .= "Old Card Type: " . $_SESSION['office']->account->get_card_type() . "\n" . "New Card Type: " . $_REQUEST['card_type'] . "\n\n";
					}
					if ($_REQUEST['card_exp_date'] != $_SESSION['office']->account->get_card_exp_date()) {
						$body .= "Old Card Exp Date: " . $_SESSION['office']->account->get_card_exp_date() . "\n" . "New Card Exp Date: " . $_REQUEST['card_exp_date'] . "\n\n";
					}
					mail('admin@byownerdaily.com', 'Account Information Changed: ' . $_SESSION['office']->account->get_first_name() . ' ' . $_SESSION['office']->account->get_last_name(), $body, "From: billing@byownerdaily.com");
				}
				
				$current_main_email_address = $_SESSION['office']->account->get_main_email_address(); // so I know to send email or not
				
				$_SESSION['office']->account->initialize($_REQUEST);
				$_SESSION['office']->account->save();
				
				if ($_REQUEST['main_email_address'] != $current_main_email_address) {
					// send the automated letter if they change their email address
					require_once('office/account/class.letter.php');
					$o_letter = new letter( array('title'=>'Email Address Change Request Update') );
					$email_segments = $o_letter->merge_segments( array('o_account'=>$_SESSION['office']->account) );
					$headers = '';
	     		if ($email_segments['cc_list']) {
	     			$headers .= "Cc: " . $email_segments['cc_list'] . "\r\n";
	     		}
	     		if ($email_segments['bcc_list']) {
	     			$headers .= "Bcc: " . $email_segments['bcc_list'] . "\r\n";
	     		}
	     		$headers .= "From: " . $email_segments['from_email'];
					mail($_REQUEST['main_email_address'], $email_segments['subject'], $email_segments['body'], $headers);
				}
				
				include_once($_SESSION['web_interface']->get_server_path('office/account/account_updated.php'));
			}
			catch (Exception $exception) {
				throw $exception;
			}
		}
	break;
	
	//handle a SAVE ACCOUNT request
	case 'save_account':
		try {
			// make sure the notes have a hard return on the end
			$_REQUEST['notes'] = preg_replace("/\n$/", "", $_REQUEST['notes']);
			$_REQUEST['notes'] .= "\n"; 
			$_REQUEST['email_attachment'] = ($_REQUEST['email_attachment']) ? $_REQUEST['email_attachment'] : 0;
			$_REQUEST['created'] = $_REQUEST['created_year'] . '-' . $_REQUEST['created_month'] . '-' . $_REQUEST['created_day'];
			if ($_REQUEST['start_year'] && $_REQUEST['start_month'] && $_REQUEST['start_day']) {
				$_REQUEST['start_date'] = $_REQUEST['start_year'] . '-' . $_REQUEST['start_month'] . '-' . $_REQUEST['start_day'];
			}
			else {
				$_REQUEST['start_date'] = '';
			}
			if ($_REQUEST['end_year'] && $_REQUEST['end_month'] && $_REQUEST['end_day']) {
				$_REQUEST['end_date'] = $_REQUEST['end_year'] . '-' . $_REQUEST['end_month'] . '-' . $_REQUEST['end_day'];
			}
			else {
				$_REQUEST['end_date'] = '';
			}
			
			$o_account = new account( array('accountid'=>$_REQUEST['accountid']) );
			if (!$o_account->isOwner()) {
				$_SESSION['web_interface']->destroySession();
				exit;
			}
			$_SESSION['web_interface']->check_required( array(required=>'first_name,last_name') );
			// check username if they put one in
			if ($_REQUEST['username']) {
				if ($o_account->invalid_username($_REQUEST['username'])) {
					$_SESSION['error']['message'] = 'The username you have entered is invalid.  Usernames must only contain letters, numbers, or be an email address, and must be more than 2 characters in length.';
				} 
				if ($o_account->is_username_taken($_REQUEST['username'])) {
					$_SESSION['error']['message'] = 'The username you have entered is already in use.  Please choose another.';
				}
			}
			if (isset($_SESSION['error'])) {
				throw new Exception($_SESSION['error']['message']);		
			}
			if ($_REQUEST['account_type'] == 'TRIAL' && !($_REQUEST['start_date'] || $_REQUEST['end_date'])) {
				throw new Exception('TRIAL accounts must have a start and end date.');		
			}
			// check the next bill date
			if ($_REQUEST['next_bill_day'] || $_REQUEST['next_bill_month'] || $_REQUEST['next_bill_year']) {
				if (!$_REQUEST['next_bill_day'] || !$_REQUEST['next_bill_month'] || !$_REQUEST['next_bill_year']) {
					throw new Exception('You must enter a Next Bill Day, Month, and Year.');
				}
				$_REQUEST['next_bill_date'] = $_REQUEST['next_bill_year'] . '-' . $_REQUEST['next_bill_month'] . '-' . $_REQUEST['next_bill_day'];
			}
			// check to make sure they have selected a product
			if (count($_REQUEST['product']) < 1) {
				throw new Exception('You must select at least one product.');
			}
			// existing account
			if ($o_account->get_accountid()) {
				// do the update to billing_status first
				if ($_REQUEST['billing_status'] != $o_account->get_billing_status()) {
					// update the billing_status and billing_status_date
					$o_account->update_status($_REQUEST['billing_status']);
					$_REQUEST['notes'] .= date('Y-m-d') . ": Changed Status from " . strtoupper($o_account->get_billing_status()) . " to " . strtoupper($_REQUEST['billing_status']) . "\n";
				}
				// don't change card number if have X's in it
				if (preg_match("/X/", $_REQUEST['card_number'])) {
					$_REQUEST['card_number'] = $o_account->get_card_number();
				}
				else if ($_REQUEST['card_number']) {
					$_REQUEST['card_number'] = $_SESSION['web_interface']->getEncrypt($_REQUEST['card_number']);
				}
				// hash password if needed
				if ($_REQUEST['password']) {
					$_REQUEST['password'] = $_SESSION['web_interface']->getHashString( array('salt'=>$o_account->get_accountid(), 'string'=>$_REQUEST['password']) );
				}
				else {
					$_REQUEST['password'] = $o_account->get_password();
				}
				$o_account->initialize($_REQUEST);
				$o_account->save();
			}
			else {
				if ($_REQUEST['card_number']) {
					$_REQUEST['card_number'] = $_SESSION['web_interface']->getEncrypt($_REQUEST['card_number']);
				}
				if ($_REQUEST['password']) {
					$_REQUEST['password'] = $_SESSION['web_interface']->getHashString( array('salt'=>$o_account->get_accountid(), 'string'=>$_REQUEST['password']) );
				}
				$o_account->initialize($_REQUEST);
				$o_account->o_business_address->save();
				$o_account->set_business_addressid($o_account->o_business_address->get_addressid());
				$o_account->o_personal_address->save();
				$o_account->set_personal_addressid($o_account->o_personal_address->get_addressid());
				$o_account->create();
				$_REQUEST['accountid'] = $o_account->get_accountid();
				if (!$o_account->get_customer_number()) {
					$o_account->initialize( array('customer_number'=>$o_account->get_accountid()) );
					$o_account->save();
				}
			}
			// save their product type (hard coded for now)
			$o_account->saveProduct( array('product'=>$_REQUEST['product']) );
			header('Location: /office/account/account.html?accountid=' . $o_account->get_accountid() . '&status_message=' . urlencode('The account information has been saved.'));
		}
		catch (Exception $exception) {
			$_SESSION['error']['message'] = $exception->getMessage();
			include_once($_SESSION['web_interface']->get_server_path('office/account/account.php'));
		}
	break;

	// handle a SAVE AREA NAMES
	case 'save_area_names':
		try {
			if ($_SESSION['office']->account->is_admin() || $_SESSION['office']->account->is_private()) {
				$o_account = new account( array('accountid'=>$_REQUEST['accountid']) );
				if (!$o_account->isOwner()) {
					$_SESSION['web_interface']->destroySession();
					exit;
				}
				$o_account->save_area_names($_REQUEST['area_name_checkbox']);
				$_SESSION['status']['message'] = 'Areas have been saved.';
				if ($_SESSION['office']->account->is_private()) {
					$body = $_SESSION['office']->account->get_first_name() . ' ' . $_SESSION['office']->account->get_last_name() . " has changed the area names on the following account:\n" . 
						"Account ID: " . $o_account->get_accountid() . "\n" .
						"Name: " . $o_account->get_first_name() . ' ' . $o_account->get_last_name() . "\n" . 
						"Business Name: " . $o_account->get_business_name() . "\n" .
						"Customer Number: " . $o_account->get_customer_number() . "\n" .
						"\n" . 
						"Area(s):\n"
					;
					foreach ($_REQUEST['area_name_checkbox'] as $area_name) {
						$body .= $area_name . "\n";
					}
					if ($_SESSION['office']->account->get_companyid() != 9) { // FSBO Hotsheet
						mail('admin@byownerdaily.com', $_SESSION['office']->account->get_first_name() . ' ' . $_SESSION['office']->account->get_last_name() . ' Changed Areas for: ' . $o_account->get_first_name() . ' ' . $o_account->get_last_name(), $body, "From: admin@byownerdaily.com");
					}
				}
				include_once($_SESSION['web_interface']->get_server_path('office/account/account.php'));
			}
			else if ($_SESSION['office']->account->is_private() && $_SESSION['office']->account->get_accountid() != $_REQUEST['accountid']) {
				$_SESSION['status']['message'] = 'Your request to change the areas to which this account is currently subscribed has been sent to the support department.  The request will be processed within 2 business days. Thank you.';
				// send email to support
				$o_account = new account( array('accountid'=>$_REQUEST['accountid']) );
				if (!$o_account->isOwner()) {
					$_SESSION['web_interface']->destroySession();
					exit;
				}
				$body = "The following account would like to change their areas:\n" . 
					"Account ID: " . $o_account->get_accountid() . "\n" .
					"Name: " . $o_account->get_first_name() . ' ' . $o_account->get_last_name() . "\n" . 
					"Business Name: " . $o_account->get_business_name() . "\n" .
					"\n" . 
					"They would like the following area(s):\n"
				;
				foreach ($_REQUEST['area_name_checkbox'] as $area_name) {
					$body .= $area_name . "\n";
				}
				if ($o_account->get_companyid() != 9) { // FSBO Hotsheet
					mail('admin@byownerdaily.com', 'Areas Changed: ' . $o_account->get_first_name() . ' ' . $o_account->get_last_name(), $body, "From: admin@byownerdaily.com");
				}
				include_once($_SESSION['web_interface']->get_server_path('office/account/edit.php'));
			}
			else {
				$_SESSION['status']['message'] = 'Your request to change the areas to which you are currently subscribed has been sent to the support department.  The request will be processed within 2 business days. Thank you.';
				// send email to support
				$body = "The following account would like to change their areas:\n" . 
					"Account ID: " . $_SESSION['office']->account->get_accountid() . "\n" .
					"Name: " . $_SESSION['office']->account->get_first_name() . ' ' . $_SESSION['office']->account->get_last_name() . "\n" . 
					"Business Name: " . $_SESSION['office']->account->get_business_name() . "\n" .
					"\n" . 
					"They would like the following area(s):\n"
				;
				foreach ($_REQUEST['area_name_checkbox'] as $area_name) {
					$body .= $area_name . "\n";
				}
				if ($_SESSION['office']->account->get_companyid() != 9) { // FSBO Hotsheet
					mail('admin@byownerdaily.com', 'Areas Changed: ' . $_SESSION['office']->account->get_first_name() . ' ' . $_SESSION['office']->account->get_last_name(), $body, "From: admin@byownerdaily.com");
				}
				include_once($_SESSION['web_interface']->get_server_path('office/account/edit.php'));
			}
		}
		catch (Exception $exception) {
			throw $exception;
		}
	break;
	
	// handle a SAVE LETTER request
	case 'save_letter':
		try {
			$_SESSION['web_interface']->check_required( array(required=>'title,subject,body') );
			if (isset($_SESSION['error'])) {
				throw new Exception($_SESSION['error']['message']);		
			}
			require_once('office/account/class.letter.php');
			$o_letter = new letter( array('letterid'=>$_REQUEST['letterid']) );
			$o_letter->initialize($_REQUEST);
			$o_letter->save();
			$_SESSION['status']['message'] = 'Your letter has been saved.';
			include_once($_SESSION['web_interface']->get_server_path('office/account/letter.php'));
		}
		catch (Exception $exception) {
			$_SESSION['error']['message'] = $exception->getMessage();
			include_once($_SESSION['web_interface']->get_server_path('office/account/edit_letter.php'));
		}
	break;
	
	// handle a SAVE LISTING FIELDS
	case 'save_listing_fields':
		try {
			if ($_SESSION['office']->account->is_admin()) {
				$_REQUEST['listing_field_checkbox'] = array();
				foreach ($_REQUEST['default_listing_field_checkbox'] as $key=>$value) {
					$_REQUEST['listing_field_checkbox'][] = $value;
				}
				foreach ($_REQUEST['other_listing_field_checkbox'] as $key=>$value) {
					$_REQUEST['listing_field_checkbox'][] = $value;
				}
				$o_account = new account( array('accountid'=>$_REQUEST['accountid']) );
				if (!$o_account->isOwner()) {
					$_SESSION['web_interface']->destroySession();
					exit;
				}
				$o_account->save_listing_fields($_REQUEST['listing_field_checkbox']);
				$_SESSION['status']['message'] = 'Listing fields have been saved.';
			}
			include_once($_SESSION['web_interface']->get_server_path('office/account/account.php'));
		}
		catch (Exception $exception) {
			throw $exception;
		}
	break;
	
	// handle a SAVE PROPERTY TYPE FIELDS
	case 'save_property_type_fields':
		try {
			// check to see if they have checked them all
			require_once($_SESSION['web_interface']->get_server_path('other/misc/property_type_fields.php'));
			if (count($propertyTypeFields) == count($_REQUEST['property_type_checkbox'])) {
				unset($_REQUEST['property_type_checkbox']);
			}
			else {
				$_REQUEST['property_type_field_checkbox'] = array();
				foreach ($_REQUEST['property_type_checkbox'] as $key=>$value) {
					$_REQUEST['property_type_field_checkbox'][] = $value;
				}
			}
			if ($_SESSION['office']->account->is_admin() && $_REQUEST['accountid']) {
				$o_account = new account( array('accountid'=>$_REQUEST['accountid']) );
				if (!$o_account->isOwner()) {
					$_SESSION['web_interface']->destroySession();
					exit;
				}
				$o_account->save_property_type_fields($_REQUEST['property_type_field_checkbox']);
				$_SESSION['status']['message'] = 'Property Types have been saved.';
				include_once($_SESSION['web_interface']->get_server_path('office/account/account.php'));
			}
			else {
				$_SESSION['office']->account->save_property_type_fields($_REQUEST['property_type_field_checkbox']);
				include_once($_SESSION['web_interface']->get_server_path('office/account/edit.php'));
			}
		}
		catch (Exception $exception) {
			throw $exception;
		}
	break;
	
	// handle a SAVE ROBOT NAMES
	case 'save_robot_names':
		try {
			$_SESSION['status']['message'] = 'Your robots have been saved.';
			if ($_SESSION['office']->account->is_admin()) {
				$o_account = new account( array('accountid'=>$_REQUEST['accountid']) );
				if (!$o_account->isOwner()) {
					$_SESSION['web_interface']->destroySession();
					exit;
				}
				$o_account->save_robot_names($_REQUEST['robot_name_checkbox']);
				include_once($_SESSION['web_interface']->get_server_path('office/account/account.php'));
			}
		}
		catch (Exception $exception) {
			throw $exception;
		}
	break;
	
	// handle a SAVE SEARCH FIELDS
	case 'save_search_fields':
		try {
			if ($_SESSION['office']->account->is_admin()) {
				foreach ($_POST as $key=>$value) {
					if ($key == 'cmd' || $key == 'accountid') {
						continue;
					}
					if ($value) {
						$searchFields[$key] = $value;
					}
				}
				$o_account = new account( array('accountid'=>$_REQUEST['accountid']) );
				if (!$o_account->isOwner()) {
					$_SESSION['web_interface']->destroySession();
					exit;
				}
				$o_account->save_search_fields($searchFields);
				$_SESSION['status']['message'] = 'Search fields have been saved.';
			}
			include_once($_SESSION['web_interface']->get_server_path('office/account/account.php'));
		}
		catch (Exception $exception) {
			throw $exception;
		}
	break;
	
	// handle a SEND_EMAIL request
	case 'send_email':
		try {
			$_SESSION['web_interface']->check_required( array('required'=>'to,from,subject,body') );
			if (isset($_SESSION['error'])) {
				throw new Exception($_SESSION['error']['message']);		
			}
			
			$headers = '';
			if ($_REQUEST['cc_list']) {
				$headers .= "Cc: " . $_REQUEST['cc_list'] . "\r\n";
			}
			if ($_REQUEST['bcc_list']) {
				$headers .= "Bcc: " . $_REQUEST['bcc_list'] . "\r\n";
			}
			$headers .= "From: " . $_REQUEST['from'];
			
			mail($_REQUEST['to'], $_REQUEST['subject'], $_REQUEST['body'], $headers);
			$_SESSION['status']['message'] = 'Your email has been sent.';
			include_once($_SESSION['web_interface']->get_server_path('office/account/account.php'));
		}
		catch (Exception $exception) {
			$_SESSION['error']['message'] = $exception->getMessage();
			include_once($_SESSION['web_interface']->get_server_path('office/account/create_email.php'));
		}
	break;
}

?>

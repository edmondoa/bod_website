<?php
if ($_SESSION['office']->account->get_account_type() != 'ADMIN') {
	$_SESSION['web_interface']->destroySession();
	exit;
}
$_PAGE_TITLE = 'Account';

require_once($_SESSION['web_interface']->get_server_path('other/misc/select_options.php'));
include_once($_SESSION['web_interface']->get_server_path('office/global/top.php'));
?>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td class="bodyNav" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('office/account/left_nav_' . $_SESSION['office']->account->get_account_type() . '.php')); ?>
		</td>
		<td class="bodyMain" valign="top">
			<h1>Search Accounts</h1>
			<p>Search by Customer Number, Personal or Business Name, Email Address, Username, or Account ID. You may also optionally search by Account Type and / or Billing Status.</p>
			<form action="/office/account/search_results.php" method="post">
			<fieldset>
				<table border="0" cellpadding="3" cellspacing="3">
					<tr>
						<td>Keyword:</td>
						<td><input type="text" name="keyword" value="<?= $_REQUEST['keyword'] ?>" size="45" /></td>
					</tr>
					<tr>
						<td>Account Type:</td>
						<td>
							<select name="account_type">
								<option value="">All</option>
								<?= select_options( array('type'=>'account_type', 'selected'=>$_REQUEST['account_type']) ) ?>
								</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Billing Status:</td>
						<td>
							<select name="billing_status">
								<option value="">All</option>
								<?= select_options( array('type'=>'billing_status', 'selected'=>$_REQUEST['billing_status']) ) ?>
							</select>
						</td>
					</tr>
					<tr><td colspan="2"><input type="submit" value="Search" /></td></tr>
				</table>
			</fieldset>
			</form>
		</td>
	</tr>
</table>
<?php
include_once($_SESSION['web_interface']->get_server_path('office/global/bottom.php'));
?>
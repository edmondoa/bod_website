<?php
if ($_SESSION['office']->account->get_account_type() != 'ADMIN') {
	$_SESSION['web_interface']->destroySession();
	exit;
}
$_PAGE_TITLE = 'Account';

require_once('office/account/class.letter.php');
$o_letter_utilities = new letter_utilities();
$list_letter = $o_letter_utilities->get_list();

require_once($_SESSION['web_interface']->get_server_path('other/misc/select_options.php'));
include_once($_SESSION['web_interface']->get_server_path('office/global/top.php'));
?>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td class="bodyNav" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('office/account/left_nav_' . $_SESSION['office']->account->get_account_type() . '.php')); ?>
		</td>
		<td class="bodyMain" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('other/misc/status_error.php')); ?>
			<h1>Manage Letters</h1>
			<a href="/office/account/edit_letter.html">New Letter</a>
<?php
if (!empty($list_letter)) {
?>
			<div class="boxHeader">
				<table border="0" cellpadding="0" cellspacing="0" style="padding-top: 20px;">
					<tr class="head">
						<th width="100">Created</th>
						<th width="500">Title</th>
						<th width="50">&nbsp;</th>
					</tr>
<?php
	foreach ($list_letter as $item_letter) {
?>
					<tr class="row<?= $_SESSION['web_interface']->get_row_class($row_num) ?>">
						<td><?= $item_letter->get_created() ?></td>
						<td><?= $item_letter->get_title() ?></td>
						<td>
							<a href="/office/account/edit_letter.html?letterid=<?= $item_letter->get_letterid() ?>"><img src="<?= $_SESSION['web_interface']->get_path('img/editBtn.gif') ?>" border="0" /></a>&nbsp;
							<a href="javascript: if (confirm('ARE YOU SURE YOU WANT TO DELETE THIS LETTER?')) { document.location.href='/office/account/handle.html?cmd=delete_letter&letterid=<?= $item_letter->get_letterid() ?>'; }"><img src="<?= $_SESSION['web_interface']->get_path('img/deleteBtn.gif') ?>" border="0" /></a>
						</td>
					</tr>
<?php
	}
?>
				</table>
			</div>
<?php
}
?>
		</td>
	</tr>
</table>
<?php
include_once($_SESSION['web_interface']->get_server_path('office/global/bottom.php'));
?>

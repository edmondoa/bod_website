<?php
$_PAGE_TITLE = 'Listing Fields';

require_once('office/account/class.account.php');
try {
	$o_account = new account( array('accountid'=>$_REQUEST['accountid']) );
	if (!$o_account->isOwner()) {
		$_SESSION['web_interface']->destroySession();
		exit;
	}
	$account_listing_fields = $o_account->get_array_listing_fields();
}
catch (Exception $exception) {
	throw $exception;
}

require_once($_SESSION['web_interface']->get_server_path('other/misc/listing_fields.php'));
include_once($_SESSION['web_interface']->get_server_path('office/global/top.php'));
?>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td class="bodyNav" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('office/account/left_nav_' . $_SESSION['office']->account->get_account_type() . '.php')); ?>
		</td>
		<td class="bodyMain" valign="top">
			<h1>Manage Listing Fields</h1>
			Select the listing fields that you would like.
			<form method="post" action="/office/account/handle.php">
			<input type="hidden" name="cmd" value="save_listing_fields" />
			<input type="hidden" name="accountid" value="<?= $o_account->get_accountid() ?>" />
			<table border="0" cellspacing="10" cellpadding="10">
				<tr>
<?php
$checkbox_id = 'default_listing_field';
?>
					<td valign="top">
      			<fieldset>
      				<legend>Default Listing Fields</legend>
      				<table border="0" cellspacing="0" cellpadding="0">
      					<tr><td><input type="checkbox" onclick="checkAll(this, '<?= $checkbox_id ?>');"/>Check All</td></tr>
      					<tr>
<?php
foreach ($defaultListingFields as $databaseField=>$displayField) {
	if ($account_listing_fields[$databaseField]) {
?>
									<td style="font-weight: bold;"><input type="checkbox" name="<?= $checkbox_id ?>_checkbox[]" id="<?= $checkbox_id ?>_checkbox" value="<?= $databaseField ?>" checked="checked" /><?= $displayField ?></td>
<?php
	}
	else {
?>
									<td><input type="checkbox" name="<?= $checkbox_id ?>_checkbox[]" id="<?= $checkbox_id ?>_checkbox" value="<?= $databaseField ?>" /><?= $displayField ?></td>
<?php
	}
?>
								</tr>
<?php
}
?>
							</table>
						</fieldset>
					</td>
<?php
$checkbox_id = 'other_listing_field';
?>
					<td valign="top">
						<fieldset>
      				<legend>Other Listing Fields</legend>
      				<table border="0" cellspacing="0" cellpadding="0">
      					<tr><td><input type="checkbox" onclick="checkAll(this, '<?= $checkbox_id ?>');"/>Check All</td></tr>
      					<tr>
<?php
foreach ($allListingFields as $databaseField=>$displayField) {
	if ($defaultListingFields[$databaseField]) {
		continue;
	}
	if ($account_listing_fields[$databaseField]) {
?>
									<td style="font-weight: bold;"><input type="checkbox" name="<?= $checkbox_id ?>_checkbox[]" id="<?= $checkbox_id ?>_checkbox" value="<?= $databaseField ?>" checked="checked" /><?= $displayField ?></td>
<?php
	}
	else {
?>
									<td><input type="checkbox" name="<?= $checkbox_id ?>_checkbox[]" id="<?= $checkbox_id ?>_checkbox" value="<?= $databaseField ?>" /><?= $displayField ?></td>
<?php
	}
?>
								</tr>
<?php
}
?>
							</table>
						</fieldset>
					</td>
				</tr>
			</table>
			<input type="submit" value="Save Listing Fields" onClick="return confirm('Are you sure you want to update your listing fields?');" />
			</form>
		</td>
	</tr>
</table>
<?php
include_once($_SESSION['web_interface']->get_server_path('office/global/bottom.php'));
?>

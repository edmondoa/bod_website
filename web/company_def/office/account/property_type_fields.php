<?php
$_PAGE_TITLE = 'Property Type Fields';

require_once('office/account/class.account.php');
$o_account = new account( array('accountid'=>$_REQUEST['accountid']) );
if (!$o_account->isOwner()) {
	$_SESSION['web_interface']->destroySession();
	exit;
}
if ($_REQUEST['accountid']) {
	$account_property_type_fields = $o_account->get_array_property_type_fields();
}
else {
	$account_property_type_fields = $_SESSION['office']->account->get_array_property_type_fields();
}

require_once($_SESSION['web_interface']->get_server_path('other/misc/property_type_fields.php'));
if (count($account_property_type_fields) < 1) { // they have all of them selected
	foreach ($propertyTypeFields as $field) {
		$account_property_type_fields[$field] = TRUE;
	}
}

include_once($_SESSION['web_interface']->get_server_path('office/global/top.php'));
?>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td class="bodyNav" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('office/account/left_nav_' . $_SESSION['office']->account->get_account_type() . '.php')); ?>
		</td>
		<td class="bodyMain" valign="top">
			<h1>Manage Property Types</h1>
			Select the property type fields that you would like.
			<form method="post" action="/office/account/handle.php">
			<input type="hidden" name="cmd" value="save_property_type_fields" />
			<input type="hidden" name="accountid" value="<?= $o_account->get_accountid() ?>" />
			<table border="0" cellspacing="10" cellpadding="10">
				<tr>
					<td valign="top">
      			<fieldset>
      				<legend>Property Type Fields</legend>
      				<table border="0" cellspacing="0" cellpadding="0">
								<tr><td><input type="checkbox" onclick="checkAll(this, 'property_type');"/>Check All</td></tr>
<?php
foreach ($propertyTypeFields as $field) {
?>
								<tr>
<?php
	if ($account_property_type_fields[$field]) {
?>
									<td style="font-weight: bold;"><input type="checkbox" name="property_type_checkbox[]" id="property_type_checkbox" value="<?= $field ?>" checked="checked" /><?= $field ?></td>
<?php
	}
	else {
?>
									<td><input type="checkbox" name="property_type_checkbox[]" id="property_type_checkbox" value="<?= $field ?>" /><?= $field ?></td>
<?php
	}
?>
								</tr>
<?php
}
?>
							</table>
						</fieldset>
					</td>
				</tr>
			</table>
			<input type="submit" value="Save Property Type Fields" onClick="return confirm('Are you sure you want to update your property type fields?');" />
			</form>
		</td>
	</tr>
</table>
<?php
include_once($_SESSION['web_interface']->get_server_path('office/global/bottom.php'));
?>

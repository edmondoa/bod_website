<?php
$_PAGE_TITLE = 'Account';

require_once('office/account/class.account.php');
try {
	$o_account = new account( array('accountid'=>$_REQUEST['accountid']) );
	if (!$o_account->isOwner()) {
		$_SESSION['web_interface']->destroySession();
		exit;
	}
}
catch (Exception $exception) {
	throw $exception;
}

require_once('office/listing/class.listing.php');
try {
	$o_listing_utilities = new listing_utilities();
}
catch (Exception $exception) {
	throw $exception;
}

include_once($_SESSION['web_interface']->get_server_path('office/global/top.php'));
?>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td class="bodyNav" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('office/account/left_nav_' . $_SESSION['office']->account->get_account_type() . '.php')); ?>
		</td>
		<td class="bodyMain" valign="top">
			<h1>Manage Robots</h1>
			Select the robots that <a href="/office/account/account.php?accountid=<?= $o_account->get_accountid() ?>"><?= $o_account->get_first_name() ?> <?= $o_account->get_last_name() ?></a> should not be receiving listings from.
			<form method="post" action="/office/account/handle.php">
			<input type="hidden" name="cmd" value="save_robot_names" />
			<input type="hidden" name="accountid" value="<?= $o_account->get_accountid() ?>" />
			<fieldset>
				<table border="0" cellspacing="0" cellpadding="0" width="100%">
					<tr>
<?php
$count = 0;
foreach ($o_listing_utilities->get_list_robot_names() as $robot_name) {
	if (preg_match("|," . $robot_name . ",|", $o_account->get_string_robot_names())) {
?>
						<td style="font-weight: bold; width: 200px;"><input type="checkbox" name="robot_name_checkbox[]" id="robot_name_checkbox" value="<?= $robot_name ?>" checked="checked" /><?= $robot_name ?></td>
<?php
	}
	else {
?>
						<td width="200"><input type="checkbox" name="robot_name_checkbox[]" id="robot_name_checkbox" value="<?= $robot_name ?>" /><?= $robot_name ?></td>
<?php
	}
	if ($count == 1) {
		$count = 0;
		print '</tr><tr>';
	}
	else {
		$count++;
	}
}
?>
				</tr>
			</table>
			</fieldset>
			<input type="submit" value="Save Robots" onClick="return confirm('Are you sure you want to update the robots?');" />
			</form>
		</td>
	</tr>
</table>
<?php
include_once($_SESSION['web_interface']->get_server_path('office/global/bottom.php'));
?>
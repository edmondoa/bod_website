<?php
if ($_SESSION['office']->account->get_account_type() != 'ADMIN' && $_SESSION['office']->account->get_account_type() != 'BILLING' && $_SESSION['office']->account->get_account_type() != 'PRIVATE_LABEL') {
	$_SESSION['web_interface']->destroySession();
	exit;
}
$_PAGE_TITLE = 'Account';

require_once('office/account/class.area_name.php');
$o_area_name_utilities = new area_name_utilities();
$list_area_names = $o_area_name_utilities->get_list_area_names();

$o_account_utilities = new account_utilities();

require_once($_SESSION['web_interface']->get_server_path('other/misc/select_options.php'));
include_once($_SESSION['web_interface']->get_server_path('office/global/top.php'));
?>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td class="bodyNav" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('office/account/left_nav_' . $_SESSION['office']->account->get_account_type() . '.php')); ?>
		</td>
		<td class="bodyMain" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('other/misc/status_error.php')); ?>
			<h1>Search Accounts</h1>
			<p>You may enter a Customer Number, Personal or Business Name, Email Address, Username, or Account ID in the keyword field. You may also search by Account Type, Billing Status, and Created range.</p>
			<form action="/office/account/search_results.php" method="post">
			<fieldset>
				<label for="keyword">Keyword:</label>
				<input type="text" name="keyword" value="<?= $_REQUEST['keyword'] ?>" size="45" />
				<br/>
<?php
if ($_SESSION['office']->account->is_admin()) {
?>
				<label for="companyid">Private Label:</label>
				<select name="companyid">
					<option value="-1">None</option>
					<option value="">All</option>
<?php
	foreach ($o_account_utilities->get_list( array('order_by'=>' business_name ', 'where_plus'=>" AND account_type='PRIVATE_LABEL' ") ) as $item_account) {
?>
					<option value="<?= $item_account->get_companyid() ?>" <?php if ($_REQUEST['companyid'] == $item_account->get_companyid()) { print 'selected="selected"'; } ?>><?= $item_account->get_business_name() ?></option>
<?php
	}
?>
				</select>
				<br/>
<?php
}
?>
				<label for="account_type">Account Type:</label>
				<select name="account_type">
					<option value="">All</option>
<?php
if ($_SESSION['office']->account->is_private()) {
	select_options( array('type'=>'private_account_type', 'selected'=>$_REQUEST['account_type']) );
}
else {
	select_options( array('type'=>'account_type', 'selected'=>$_REQUEST['account_type']) );
}
?>

				</select>
				<br/>
				<label for="billing_status">Billing Status:</label>
				<select name="billing_status">
					<option value="">All</option>
					<?= select_options( array('type'=>'billing_status', 'selected'=>$_REQUEST['billing_status']) ) ?>
				</select>
				<br/>
<?php
if ($_SESSION['office']->account->is_admin()) {
?>
				<label for="area_name">Area:</label>
				<select name="area_name">
					<option value="">All</option>
<?php
	foreach ($list_area_names as $area_name) {
?>
					<option value="<?= $area_name ?>"><?= $area_name ?></option>
<?php
	}
?>
				</select>
				<br/>
<?php
}
?>
				<label for="start_date">Start Date:</label>
				<select name="start_month">
					<?= select_options( array(type=>'month') ) ?>
				</select>
				<select name="start_day">
					<?= select_options( array(type=>'day') ) ?>
				</select>
				<select name="start_year">
					<?= select_options( array(type=>'past_year', 'selected'=>'2002') ) ?>
				</select>
				<br/>
				<label for="last_name">End Date:</label>
				<select name="end_month">
					<?= select_options( array(type=>'month', selected=>date('m')) ) ?>
				</select>
				<select name="end_day">
					<?= select_options( array(type=>'day', selected=>date('d')) ) ?>
				</select>
				<select name="end_year">
					<?= select_options( array(type=>'past_year', selected=>date('Y')) ) ?>
				</select>
			</fieldset>
			<input type="submit" value="Search" />
			</form>
		</td>
	</tr>
</table>
<?php
include_once($_SESSION['web_interface']->get_server_path('office/global/bottom.php'));
?>

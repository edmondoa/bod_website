<?php
$_PAGE_TITLE = 'Search Fields';

require_once('office/account/class.account.php');
try {
	$o_account = new account( array('accountid'=>$_REQUEST['accountid']) );
	if (!$o_account->isOwner()) {
		$_SESSION['web_interface']->destroySession();
		exit;
	}
	$account_search_fields = $o_account->get_hash_search_fields();
	foreach ($account_search_fields as $key=>$value) {
		$_REQUEST[$key] = $value;
	}
}
catch (Exception $exception) {
	throw $exception;
}

require_once($_SESSION['web_interface']->get_server_path('other/misc/select_options.php'));
include_once($_SESSION['web_interface']->get_server_path('office/global/top.php'));
?>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td class="bodyNav" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('office/account/left_nav_' . $_SESSION['office']->account->get_account_type() . '.php')); ?>
		</td>
		<td class="bodyMain" valign="top">
			<h1>Manage Search Fields</h1>
			Select the search fields that you would like.
			<form method="post" action="/office/account/handle.php">
			<input type="hidden" name="cmd" value="save_search_fields" />
			<input type="hidden" name="accountid" value="<?= $o_account->get_accountid() ?>" />
			<table border="0" cellspacing="10" cellpadding="10">
				<tr>
					<td>
						<table border="0" cellpadding="2" cellspacing="0">
							<tr>
								<td>Bed:</td>
								<td>
									<select name="bedrooms">
										<option value="">Any</option>
										<?= select_options( array('type'=>'number', 'max'=>5, 'plus_sign'=>TRUE, 'selected'=>$_REQUEST['bedrooms']) ) ?>
									</select>
								</td>
								<td>Bath:</td>
								<td>
									<select name="bathrooms">
										<option value="">Any</option>
										<?= select_options( array('type'=>'number', 'max'=>5, 'plus_sign'=>TRUE, 'selected'=>$_REQUEST['bathrooms']) ) ?>
									</select>
								</td>
								<td>Square Feet:</td>
								<td>
									<select name="square_feet">
										<option value="">Any</option>
										<?= select_options( array('type'=>'number', 'min'=>500, 'max'=>3000, 'increment'=>250, 'plus_sign'=>TRUE, 'selected'=>$_REQUEST['bathrooms']) ) ?>
									</select>
								</td>
								<td>Garage:</td>
								<td>
									<select name="garage">
										<option value="">Any</option>
										<?= select_options( array('type'=>'number', 'max'=>5, 'plus_sign'=>TRUE, 'selected'=>$_REQUEST['garage']) ) ?>
									</select>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table border="0" cellpadding="2" cellspacing="0">
							<tr>
								<td>Property Type:</td>
								<td><input type="text" name="property_type" value="<?= $_REQUEST['property_type'] ?>" size="10"/></td>
								<td>Year Built</td>
								<td>
									<select name="year_built">
										<option value="">Any</option>
										<?= select_options( array('type'=>'number', 'min'=>1950, 'max'=>date('Y'), 'increment'=>5, 'plus_sign'=>TRUE, 'selected'=>$_REQUEST['year_built']) ) ?>
									</select>
								</td>
								<td>Lot Size:</td>
								<td>
									<select name="lot_size">
										<option value="">Any</option>
										<?= select_options( array('type'=>'number', 'min'=>'0.25', 'max'=>5, 'increment'=>'0.25', 'plus_sign'=>TRUE, 'selected'=>$_REQUEST['lot_size']) ) ?>
									</select>
								</td>
							</tr>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table border="0" cellpadding="2" cellspacing="0">
							<tr>
								<td>Keyword</td>
								<td><input type="text" name="description_ad" value="<?= $_REQUEST['description_ad'] ?>" size="25" /></td>
								<td>Price Range:</td>
								<td>
									<input type="text" name="minPrice" value="<?= $_REQUEST['minPrice'] ?>" /> (min) 
									<input type="text" name="maxPrice" value="<?= $_REQUEST['maxPrice'] ?>" /> (max: leave blank if no upper limit)
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table border="0" cellpadding="2" cellspacing="0">
							<tr>
								<td>Scrubber:</td>
								<td><input type="text" name="scrub_edit" value="<?= $_REQUEST['scrub_edit'] ?>" /></td>
								<td>Lead ID:</td>
								<td><input type="text" name="lead_id" value="<?= $_REQUEST['lead_id'] ?>" /></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table border="0" cellpadding="2" cellspacing="0">
							<tr>
								<td>Contact Phone:</td>
								<td><font style="font-size: 9px;">Use format (XXX-XXX-XXXX, e.g. 303-555-1111)</font><br/><input type="text" name="contact_phone" value="<?= $_REQUEST['contact_phone'] ?>" /></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr> 
					<td>
						<table border="0" cellpadding="2" cellspacing="0" width="100%">
							<tr><td><input type="checkbox" name="no_dnc" value="1" <?php if ($_REQUEST['no_dnc']) { print 'checked="checked"'; } ?> /> Is NOT listed with the "U.S. Do Not Call Registry"</td></tr>
							<tr><td><input type="checkbox" name="with_address_only" value="1" <?php if ($_REQUEST['with_address_only']) { print 'checked="checked"'; } ?> /> Must have a Mailable Address</td></tr>
							<tr><td><input type="checkbox" name="with_phone_only" value="1" <?php if ($_REQUEST['with_phone_only']) { print 'checked="checked"'; } ?> /> Must have a Phone</td></tr>
							<tr><td><input type="checkbox" name="with_email_only" value="1" <?php if ($_REQUEST['with_email_only']) { print 'checked="checked"'; } ?> /> Must have an Email</td></tr>
						</table>
					</td>
				</tr>
			</table>
			<input type="submit" value="Save Search Fields" onClick="return confirm('Are you sure you want to update your search fields?');" />
			</form>
		</td>
	</tr>
</table>
<?php
include_once($_SESSION['web_interface']->get_server_path('office/global/bottom.php'));
?>

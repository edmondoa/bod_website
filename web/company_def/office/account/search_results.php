<?php
if ($_SESSION['office']->account->get_account_type() != 'ADMIN' && $_SESSION['office']->account->get_account_type() != 'BILLING' && $_SESSION['office']->account->get_account_type() != 'PRIVATE_LABEL') {
	$_SESSION['web_interface']->destroySession();
	exit;
}
$_PAGE_TITLE = 'Account';

require_once('office/account/class.area_name.php');
$o_area_name_utilities = new area_name_utilities();
$list_area_names = $o_area_name_utilities->get_list_area_names();

$_SESSION['keyword'] = $_REQUEST['keyword'];
require_once('office/account/class.account.php');
try {
	$o_account_utilities = new account_utilities();
	$list_account = $o_account_utilities->get_list_for_search($_REQUEST);
	if ($_REQUEST['area_name']) {
		$new_list_account = array();
		foreach ($list_account as $o_account) {
			if (preg_match("/" . preg_quote($_REQUEST['area_name']) . ",/", $o_account->get_string_area_names())) {
				$new_list_account[] = $o_account;
			}
		}
		$list_account = $new_list_account;
	}
	if (count($list_account) == 1) {
		$o_account = $list_account[0];
		header("Location: /office/account/account.php?accountid=" . $o_account->get_accountid());
	}
	$list_private_label = $o_account_utilities->get_list( array('order_by'=>' business_name ', 'where_plus'=>" AND account_type='PRIVATE_LABEL' ") );
	$companyid_private_label_hash = array();
	foreach ($list_private_label as $item_account) {
		$companyid_private_label_hash[$item_account->get_companyid()] = $item_account->get_business_name();
	}
}
catch (Exception $exception) {
	throw $exception;
}

require_once($_SESSION['web_interface']->get_server_path('other/misc/select_options.php'));
include_once($_SESSION['web_interface']->get_server_path('office/global/top.php'));
?>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td class="bodyNav" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('office/account/left_nav_' . $_SESSION['office']->account->get_account_type() . '.php')); ?>
		</td>
		<td class="bodyMain" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('other/misc/status_error.php')); ?>
			<h1>Search Accounts</h1>
			<p>You may enter a Customer Number, Personal or Business Name, Email Address, Username, or Account ID in the keyword field. You may also search by Account Type, Billing Status, and Created range.</p>
			<form action="/office/account/search_results.php" method="post">
			<fieldset>
				<label for="keyword">Keyword:</label>
				<input type="text" name="keyword" value="<?= $_REQUEST['keyword'] ?>" size="45" />
				<br/>
<?php
if ($_SESSION['office']->account->is_admin()) {
?>
				<label for="companyid">Private Label:</label>
				<select name="companyid">
					<option value="-1">None</option>
					<option value="">All</option>
<?php
	foreach ($list_private_label as $item_account) {
?>
					<option value="<?= $item_account->get_companyid() ?>" <?php if ($_REQUEST['companyid'] == $item_account->get_companyid()) { print 'selected="selected"'; } ?>><?= $item_account->get_business_name() ?></option>
<?php
	}
?>
				</select>
				<br/>
<?php
}
?>
				<label for="account_type">Account Type:</label>
				<select name="account_type">
					<option value="">All</option>
<?php
if ($_SESSION['office']->account->is_private()) {
	select_options( array('type'=>'private_account_type', 'selected'=>$_REQUEST['account_type']) );
}
else {
	select_options( array('type'=>'account_type', 'selected'=>$_REQUEST['account_type']) );
}
?>

				</select>
				<br/>
				<label for="billing_status">Billing Status:</label>
				<select name="billing_status">
					<option value="">All</option>
					<?= select_options( array('type'=>'billing_status', 'selected'=>$_REQUEST['billing_status']) ) ?>
				</select>
				<br/>
<?php
if ($_SESSION['office']->account->is_admin()) {
?>
				<label for="area_name">Area:</label>
				<select name="area_name">
					<option value="">All</option>
<?php
	foreach ($list_area_names as $area_name) {
?>
					<option value="<?= $area_name ?>"><?= $area_name ?></option>
<?php
	}
?>
				</select>
				<br/>
<?php
}
?>
				<label for="start_date">Start Date:</label>
				<select name="start_month">
					<?= select_options( array(type=>'month', selected=>$_REQUEST['start_month']) ) ?>
				</select>
				<select name="start_day">
					<?= select_options( array(type=>'day', selected=>$_REQUEST['start_day']) ) ?>
				</select>
				<select name="start_year">
					<?= select_options( array(type=>'past_year', selected=>$_REQUEST['start_year']) ) ?>
				</select>
				<br/>
				<label for="last_name">End Date:</label>
				<select name="end_month">
					<?= select_options( array(type=>'month', selected=>$_REQUEST['end_month']) ) ?>
				</select>
				<select name="end_day">
					<?= select_options( array(type=>'day', selected=>$_REQUEST['end_day']) ) ?>
				</select>
				<select name="end_year">
					<?= select_options( array(type=>'past_year', selected=>$_REQUEST['end_year']) ) ?>
				</select>
			</fieldset>
			<input type="submit" value="Search" />
			</form>
<?php
if (!isset($_SESSION['error'])) {
?>
			<h1>Search Results</h1>
<?php
	if (empty($list_account)) {
?>
			There are no results at this time.
<?php
	}
	else {
?>
			Listed below are the results of the search terms you entered above.
			<div class="boxHeader">
				<table cellpadding="0" cellspacing="0" border="0">
					<colgroup>
						<col width="75"/>
						<col width="150"/>
						<col width="125"/>
						<col width="110"/>
						<col width="110"/>
					</colgroup>
					<tbody>
					<tr class="head">
						<th>Customer<br/>Number</th>
						<th>Personal<br/>Name</th>
						<th>Billing<br/>Status</th>
						<th>Account<br/>Type</th>
						<th>Private<br/>Label</th>
				</tr>
					</tbody>
				</table>
			</div>
			<div class="boxScroll">
				<table cellpadding="0" cellspacing="0" border="0">
					<colgroup>
						<col width="75"/>
						<col width="150"/>
						<col width="125"/>
						<col width="110"/>
						<col width="110"/>
					</colgroup>
					<tbody>
<?php
		$hash_billing_status = select_options( array('expecting'=>'array', 'type'=>'billing_status') );
		foreach ($list_account as $item_account) {
?>
					<tr class="row<?= $_SESSION['web_interface']->get_row_class($row_num) ?>">
						<td><a href="/office/account/account.php?accountid=<?= $item_account->get_accountid() ?>"><?= $item_account->get_customer_number() ?></a></td>
						<td>
							<a href="/office/account/account.php?accountid=<?= $item_account->get_accountid() ?>">
								<?= $item_account->get_full_name_short() ?>
<?php
			if ($item_account->get_account_type() == 'PRIVATE_LABEL') {
				print ' (' . $o_account_utilities->get_num( array('where_plus'=>" AND companyid='" . $_SESSION['data_access']->db_quote($item_account->get_companyid()) . "' ") ) . ')';
			}
?>
							</a>
						</td>
						<td><?= $hash_billing_status[$item_account->get_billing_status()] ?></td>
						<td><?= $item_account->get_account_type() ?></td>
						<td><?= $companyid_private_label_hash[$item_account->get_companyid()] ?></td>
					</tr>
<?php
		}
?>
					</tbody>
				</table>
			</div>
<?php
	}
}
?>
		</td>
	</tr>
</table>
<?php
include_once($_SESSION['web_interface']->get_server_path('office/global/bottom.php'));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title><?= $_PAGE_TITLE ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<META NAME="description" CONTENT="For Sale by Owner automated email delivery service of FSBO leads.">
<META NAME="keywords" CONTENT="FSBO database, FSBO lead, fsbo leads, daily fsbo, For Sale By Owner, for sale by owner, fsbo, FSBO, homes for sale, for-sale-by-owner, realtor, real estate agent, real estate broker, real estate, mortgage, by owner daily, for sale by owner list, www.byownerdaily.com">
<link href="<?= $_SESSION['web_interface']->get_path('css/office.css') ?>" rel="stylesheet" type="text/css"/>
<script src="<?= $_SESSION['web_interface']->get_path('js/global.js') ?>" type="text/javascript"></script>
</head>
<body background="/web/company_def/img/bg_px.jpg">
<div class="top">
	<div class="topCompanyLogo"><img src="<?= $_SESSION['web_interface']->get_path('img/company_logo.gif') ?>"  alt="Logo" border="0" /></div>
	<div class="topNav">
		<ul>
<?php include_once($_SESSION['web_interface']->get_server_path('office/global/top_nav_' . strtoupper($_SESSION['office']->account->get_account_type()) . '.php')); ?>
		</ul>
		<a href="/other/login.php?status_message=You+have+been+logged+out." style="color: #7E7E7E; float: right; font-family: Tahoma; padding: 10px 10px 0 0; text-decoration: none;">Logout</a>
	</div>
	<div class="topBottom"><img src="/web/company_def/img/grey_px.jpg" /></div>
</div>
<div class="body">

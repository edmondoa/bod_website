<?php
$_PAGE_TITLE = 'Office';

require_once('office/account/class.account.php');
try {
	$o_account_utilities = new account_utilities();
	$list_account = $o_account_utilities->get_list_new();
	$list_billing = $o_account_utilities->get_list_overdue();
	$list_overdue_nontrial = array();
	$list_overdue_trial = array();
	if (count($list_billing) > 0) {
		foreach ($list_billing as $item_account) {
			if ($item_account->get_account_type() == 'TRIAL') {
				$list_overdue_trial[] = $item_account;
			}
			else {
				$list_overdue_nontrial[] = $item_account;
			}
		}
	}
}
catch (Exception $exception) {
	throw $exception;
}
	
include_once($_SESSION['web_interface']->get_server_path('office/global/top.php'));
include_once($_SESSION['web_interface']->get_server_path('other/misc/status_error.php'));
?>
<div class="bodyMain">
	Welcome to your office <?= $_SESSION['office']->account->get_first_name() ?>!
<?php
if ($_SESSION['office']->account->get_account_type() == 'ADMIN') {
?>
	<p>
		<strong>New Accounts</strong><br/>
<?php
	if (empty($list_account)) {
?>
		No NEW Accounts at this time.
<?php
	}
	else {
?>
		<div class="boxHeader">
				<table cellpadding="0" cellspacing="0" border="0">
					<colgroup>
						<col width="100"/>
						<col width="200"/>
						<col width="200"/>
						<col width="100"/>
					</colgroup>
					<tbody>
					<tr class="head">
						<th>Customer Number</th>
						<th>Personal Name</th>
						<th>Business Name</th>
						<th>Account Type</th>
				</tr>
					</tbody>
				</table>
			</div>
			<div class="boxScroll" style="height: 200px;">
				<table cellpadding="0" cellspacing="0" border="0">
					<colgroup>
						<col width="100"/>
						<col width="200"/>
						<col width="200"/>
						<col width="100"/>
					</colgroup>
					<tbody>
<?php					
		foreach ($list_account as $item_account) {
?>
					<tr class="row<?= $_SESSION['web_interface']->get_row_class($row_num) ?>">
						<td><a href="/office/account/account.php?accountid=<?= $item_account->get_accountid() ?>"><?= $item_account->get_customer_number() ?></a></td>
						<td><a href="/office/account/account.php?accountid=<?= $item_account->get_accountid() ?>"><?= $item_account->get_full_name_short() ?></a></td>
						<td><?= $item_account->get_business_name_short() ?></td>
						<td><?= $item_account->get_account_type() ?></td>
					</tr>
<?php
		}
?>
					</tbody>
				</table>
			</div>
<?php
	}
?>
	</p>
	<p>
		<strong>Active Trial Accounts</strong><br/>
<?php
	if (empty($list_overdue_trial)) {
?>
		No active trial accounts at this time.
<?php
	}
	else {
?>
		<div class="boxHeader">
				<table cellpadding="0" cellspacing="0" border="0">
					<colgroup>
						<col width="100"/>
						<col width="200"/>
						<col width="200"/>
						<col width="150"/>
						<col width="150"/>
					</colgroup>
					<tbody>
					<tr class="head">
						<th>Customer Number</th>
						<th>Personal Name</th>
						<th>Business Name</th>
						<th>Next Bill Date</th>
						<th>Card Exp Date</th>
				</tr>
					</tbody>
				</table>
			</div>
			<div class="boxScroll" style="height: 200px;">
				<table cellpadding="0" cellspacing="0" border="0">
					<colgroup>
						<col width="100"/>
						<col width="200"/>
						<col width="200"/>
						<col width="150"/>
						<col width="150"/>
					</colgroup>
					<tbody>
<?php					
		foreach ($list_overdue_trial as $item_account) {
?>
					<tr class="row<?= $_SESSION['web_interface']->get_row_class($row_num) ?>">
						<td><a href="/office/account/account.php?accountid=<?= $item_account->get_accountid() ?>"><?= $item_account->get_customer_number() ?></a></td>
						<td><a href="/office/account/account.php?accountid=<?= $item_account->get_accountid() ?>"><?= $item_account->get_full_name_short() ?></a></td>
						<td><?= $item_account->get_business_name_short() ?></td>
						<td><?= $item_account->get_next_bill_date() ?></td>
						<td><?= preg_replace("/^(\d{2})(\d{2})$/", "$1/$2", $item_account->get_card_exp_date()) ?></td>
					</tr>
<?php
		}
?>
					</tbody>
				</table>
			</div>
<?php
	}
?>
	</p>
	
	<p>
		<strong>Overdue Accounts</strong><br/>
<?php
	if (empty($list_overdue_nontrial)) {
?>
		No overdue accounts at this time.
<?php
	}
	else {
?>
		<div class="boxHeader">
				<table cellpadding="0" cellspacing="0" border="0">
					<colgroup>
						<col width="100"/>
						<col width="200"/>
						<col width="200"/>
						<col width="150"/>
						<col width="150"/>
					</colgroup>
					<tbody>
					<tr class="head">
						<th>Customer Number</th>
						<th>Personal Name</th>
						<th>Business Name</th>
						<th>Next Bill Date</th>
						<th>Card Exp Date</th>
				</tr>
					</tbody>
				</table>
			</div>
			<div class="boxScroll" style="height: 200px;">
				<table cellpadding="0" cellspacing="0" border="0">
					<colgroup>
						<col width="100"/>
						<col width="200"/>
						<col width="200"/>
						<col width="150"/>
						<col width="150"/>
					</colgroup>
					<tbody>
<?php					
		foreach ($list_overdue_nontrial as $item_account) {
?>
					<tr class="row<?= $_SESSION['web_interface']->get_row_class($row_num) ?>">
						<td><a href="/office/account/account.php?accountid=<?= $item_account->get_accountid() ?>"><?= $item_account->get_customer_number() ?></a></td>
						<td><a href="/office/account/account.php?accountid=<?= $item_account->get_accountid() ?>"><?= $item_account->get_full_name_short() ?></a></td>
						<td><?= $item_account->get_business_name_short() ?></td>
						<td><?= $item_account->get_next_bill_date() ?></td>
						<td><?= preg_replace("/^(\d{2})(\d{2})$/", "$1/$2", $item_account->get_card_exp_date()) ?></td>
					</tr>
<?php
		}
?>
					</tbody>
				</table>
			</div>
<?php
	}
?>
	</p>
<?php
}
?>
</div>
<?php
include_once($_SESSION['web_interface']->get_server_path('office/global/bottom.php'));
?>

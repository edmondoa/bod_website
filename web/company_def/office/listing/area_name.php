<?php
if ($_SESSION['office']->account->get_account_type() != 'ADMIN') {
	$_SESSION['web_interface']->destroySession();
	exit;
}
$_PAGE_TITLE = 'Area Names';

require_once('office/account/class.area_name.php');
try {
	$o_area_name_utilities = new area_name_utilities();
	$list_area_name = $o_area_name_utilities->get_list();
}
catch (Exception $exception) {
	throw $exception;
}

include_once($_SESSION['web_interface']->get_server_path('office/global/top.php'));
?>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td class="bodyNav" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('office/listing/left_nav_' . $_SESSION['office']->account->get_account_type() . '.php')); ?>
		</td>
		<td class="bodyMain" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('other/misc/status_error.php')); ?>
			<h1>Area Names</h1>
			<a href="/office/listing/edit_area_name.html">Add New Area</a>
<?php
if (empty($list_area_name)) {
?>
			There are no area names at this time.
<?php
}
else {
?>
			<form method="post" action="/office/listing/handle.php">
			<input type="hidden" name="cmd" value="save_area_names" />
			<div class="boxHeader">
				<table cellpadding="0" cellspacing="0" border="0">
					<colgroup>
						<col width="200"/>
						<col width="325"/>
						<col width="50"/>
					</colgroup>
					<tbody>
					<tr class="head">
						<th align="center">Area State</a></th>
						<th align="center">Area Name</th>
						<th>&nbsp;</th>
					</tr>
					</tbody>
				</table>
			</div>
			<div class="boxScroll">
				<table cellpadding="0" cellspacing="0" border="0">
					<colgroup>
						<col width="200"/>
						<col width="325"/>
						<col width="50"/>
					</colgroup>
					<tbody>
<?php
	foreach ($list_area_name as $item_area_name) {
?>
					<tr class="row<?= $_SESSION['web_interface']->get_row_class($row_num) ?>">
						<td align="center"><input type="text" name="area_state_<?= $item_area_name->get_area_nameid() ?>" value="<?= $item_area_name->get_area_state() ?>" /></td>
						<td align="center"><input type="text" name="area_name_<?= $item_area_name->get_area_nameid() ?>" value="<?= $item_area_name->get_area_name() ?>" size="50" /></td>
						<td align="center">
<?php
		if ($_SESSION['office']->account->is_admin()) {
?>
							<a href="/office/listing/edit_area_name.html?area_nameid=<?= $item_area_name->get_area_nameid() ?>"><img src="<?= $_SESSION['web_interface']->get_path('img/editBtn.gif') ?>" border="0" /></a>&nbsp;
							<a href="javascript: void(0);" onClick="if (confirm('Are you sure you want to delete thise Area Name? Any listings associated with this Area Name will also be deleted!')) { document.location.href='/office/listing/handle.php?cmd=delete_area_name&area_nameid=<?= $item_area_name->get_area_nameid() ?>'; } else { return false; }"><img src="<?= $_SESSION['web_interface']->get_path('img/deleteBtn.gif') ?>" border="0" /></a>
<?php
		}
?>
						</td>
					</tr>
<?php
	}
?>
					</tbody>
				</table>
			</div>
			<input type="submit" value="Update" />
			</form>
<?php
}
?>
		</td>
	</tr>
</table>
<?php
include_once($_SESSION['web_interface']->get_server_path('office/global/bottom.php'));
?>

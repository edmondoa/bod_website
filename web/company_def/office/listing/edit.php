<?php
if ($_SESSION['office']->account->get_account_type() != 'ADMIN' && $_SESSION['office']->account->get_account_type() != 'SUPPORT') {
	$_SESSION['web_interface']->destroySession();
	exit;
}
$_PAGE_TITLE = 'Listings';

require_once('office/listing/class.listing.php');
try {
	$o_listing = new listing( array('listingid'=>$_REQUEST['listingid']) );
	$_SESSION['type'] = 'previous';
	$previous_listingid = $o_listing->get_next_or_previous_listingid($_SESSION);
	$_SESSION['type'] = 'next';
	$next_listingid = $o_listing->get_next_or_previous_listingid($_SESSION);
	unset($_SESSION['type']);
}
catch (Exception $exception) {
 throw $exception;
}

include_once($_SESSION['web_interface']->get_server_path('office/global/top.php'));
?>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td class="bodyNav" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('office/listing/left_nav_' . $_SESSION['office']->account->get_account_type() . '.php')); ?>
		</td>
		<td class="bodyMain" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('other/misc/status_error.php')); ?>
			<h1 style="text-align: center;">
				<?php if ($next_listingid) { ?><a href="/office/listing/view.php?listingid=<?= $next_listingid ?>" style="float: right;">Next Listing</a> <?php } ?>
				<?php if ($previous_listingid) { ?><a href="/office/listing/view.php?listingid=<?= $previous_listingid ?>">Previous Listing</a> <?php } ?>
				<?= $o_listing->get_area_name() ?> - <?= date('F j, Y', strtotime($o_listing->get_date_posted())) ?>
			</h1>
			<form method="post" action="/office/listing/handle.php">
			<input type="hidden" name="cmd" value="save" />
			<input type="hidden" name="listingid" value="<?= $o_listing->get_listingid() ?>" />
			<fieldset>
				<legend>Listing Information</legend>
				<label for="lead_id">Lead ID:</label>
				<input type="text" name="lead_id" value="<?= $o_listing->get_lead_id() ?>" /><br/>
				<label for="state">Area State:</label>
				<input type="text" name="area_st" value="<?= $o_listing->get_area_st() ?>" /><br/>
				<label for="date_delivery">Date Posted:</label>
				<input type="text" name="date_posted" value="<?= $o_listing->get_date_posted() ?>" /><br/>
				<label for="date_delivery">Property Type:</label>
				<input type="text" name="property_type" value="<?= $o_listing->get_property_type() ?>" /><br/>
				<label for="contact_phone1">Phone 1:</label>
				<input type="text" name="contact_phone1" value="<?= $o_listing->get_contact_phone1() ?>" /><br/>
				<label for="contact_name1">Contact Name 1:</label>
				<input type="text" name="contact_name1" value="<?= $o_listing->get_contact_name1() ?>" /><br/>
				<label for="contact_street1">Contact Street 1:</label>
				<input type="text" name="contact_street1" value="<?= $o_listing->get_contact_street1() ?>" /><br/>
				<label for="contact_city1">Contact City 1:</label>
				<input type="text" name="contact_city1" value="<?= $o_listing->get_contact_city1() ?>" /><br/>
				<label for="contact_st1">Contact State 1:</label>
				<input type="text" name="contact_st1" value="<?= $o_listing->get_contact_st1() ?>" /><br/>
				<label for="contact_zip1">Contact Zip 1:</label>
				<input type="text" name="contact_zip1" value="<?= $o_listing->get_contact_zip1() ?>" /><br/>
				<label for="contact_phone2">Phone 2:</label>
				<input type="text" name="contact_phone2" value="<?= $o_listing->get_contact_phone2() ?>" /><br/>
				<label for="contact_name2">Contact Name 2:</label>
				<input type="text" name="contact_name2" value="<?= $o_listing->get_contact_name2() ?>" /><br/>
				<label for="contact_street2">Contact Street 2:</label>
				<input type="text" name="contact_street2" value="<?= $o_listing->get_contact_street2() ?>" /><br/>
				<label for="contact_city2">Contact City 2:</label>
				<input type="text" name="contact_city2" value="<?= $o_listing->get_contact_city2() ?>" /><br/>
				<label for="contact_st2">Contact State 2:</label>
				<input type="text" name="contact_st2" value="<?= $o_listing->get_contact_st2() ?>" /><br/>
				<label for="contact_zip2">Contact Zip 2:</label>
				<input type="text" name="contact_zip2" value="<?= $o_listing->get_contact_zip2() ?>" /><br/>
				
				<label for="property_street">Property Street 1:</label>
				<input type="text" name="property_street" value="<?= $o_listing->get_property_street() ?>" /><br/>
				<label for="property_street2">Property Street 2:</label>
				<input type="text" name="property_street2" value="<?= $o_listing->get_property_street2() ?>" /><br/>
				<label for="property_city">Property City:</label>
				<input type="text" name="property_city" value="<?= $o_listing->get_property_city() ?>" /><br/>
				<label for="property_st">Property State:</label>
				<input type="text" name="property_st" value="<?= $o_listing->get_property_st() ?>" /><br/>
				<label for="property_zip">Property Zip:</label>
				<input type="text" name="property_zip" value="<?= $o_listing->get_property_zip() ?>" /><br/>
				
				<label for="area_name">Area Name:</label>
				<input type="text" name="area_name" value="<?= $o_listing->get_area_name() ?>" /><br/>
				<label for="data_source_name">Data Source Name:</label>
				<input type="text" name="data_source_name" value="<?= $o_listing->get_data_source_name() ?>" /><br/>
				<label for="data_source_namepublish">Data Source Name Publish:</label>
				<input type="text" name="data_source_namepublish" value="<?= $o_listing->get_data_source_namepublish() ?>" /><br/>
				<label for="description_ad">Description Ad:</label>
				<textarea name="description_ad" cols="50" rows="10"><?= $o_listing->get_description_ad() ?></textarea><br/>
				<label for="bedrooms">Bed:</label>
				<input type="text" name="bedrooms" value="<?= $o_listing->get_bedrooms() ?>" /><br/>
				<label for="bathrooms">Bath:</label>
				<input type="text" name="bathrooms" value="<?= $o_listing->get_bathrooms() ?>" /><br/>
				<label for="square_feet">Square Feet:</label>
				<input type="text" name="square_feet" value="<?= $o_listing->get_square_feet() ?>" /><br/>
				<label for="year_built">Year Built:</label>
				<input type="text" name="year_built" value="<?= $o_listing->get_year_built() ?>" /><br/>
				<label for="lot_size">Lot Size:</label>
				<input type="text" name="lot_size" value="<?= $o_listing->get_lot_size() ?>" /><br/>
				<label for="garage">Garage:</label>
				<input type="text" name="garage" value="<?= $o_listing->get_garage() ?>" /><br/>
				<label for="price">Price:</label>
				<input type="text" name="price" value="<?= $o_listing->get_price() ?>" /><br/>
				<label for="contact_email">Email:</label>
				<input type="text" name="contact_email" value="<?= $o_listing->get_contact_email() ?>" /><br/>
				<label for="phone1_lookupsource">Phone 1 Lookup Source:</label>
				<input type="text" name="phone1_lookupsource" value="<?= $o_listing->get_phone1_lookupsource() ?>" /><br/>
				<label for="phone2_lookupsource">Phone 2 Lookup Source:</label>
				<input type="text" name="phone2_lookupsource" value="<?= $o_listing->get_phone2_lookupsource() ?>" /><br/>
				<label for="phone1_dnc">Phone 1 DNC:</label>
				<input type="text" name="phone1_dnc" value="<?= $o_listing->get_phone1_dnc() ?>" /><br/>
				<label for="phone2_dnc">Phone 2 DNC:</label>
				<input type="text" name="phone2_dnc" value="<?= $o_listing->get_phone2_dnc() ?>" /><br/>
				<label for="phone1_prefixfinder">Phone 1 PrefixFinder:</label>
				<input type="text" name="phone1_prefixfinder" value="<?= $o_listing->get_phone1_prefixfinder() ?>" /><br/>
				<label for="phone1_pf_areacode">Phone 1 PF Areacode:</label>
				<input type="text" name="phone1_pf_areacode" value="<?= $o_listing->get_phone1_pf_areacode() ?>" /><br/>
				<label for="phone1_pf_prefix">Phone 1 PF Prefix:</label>
				<input type="text" name="phone1_pf_prefix" value="<?= $o_listing->get_phone1_pf_prefix() ?>" /><br/>
				<label for="phone1_pf_city">Phone 1 PF City:</label>
				<input type="text" name="phone1_pf_city" value="<?= $o_listing->get_phone1_pf_city() ?>" /><br/>
				<label for="phone1_pf_st">Phone 1 PF State:</label>
				<input type="text" name="phone1_pf_st" value="<?= $o_listing->get_phone1_pf_st() ?>" /><br/>
				<label for="phone2_prefixfinder">Phone 2 PrefixFinder:</label>
				<input type="text" name="phone2_prefixfinder" value="<?= $o_listing->get_phone2_prefixfinder() ?>" /><br/>
				<label for="phone2_pf_areacode">Phone 2 PF Areacode:</label>
				<input type="text" name="phone2_pf_areacode" value="<?= $o_listing->get_phone2_pf_areacode() ?>" /><br/>
				<label for="phone2_pf_prefix">Phone 2 PF Prefix:</label>
				<input type="text" name="phone2_pf_prefix" value="<?= $o_listing->get_phone2_pf_prefix() ?>" /><br/>
				<label for="phone2_pf_city">Phone 2 PF City:</label>
				<input type="text" name="phone2_pf_city" value="<?= $o_listing->get_phone2_pf_city() ?>" /><br/>
				<label for="phone2_pf_st">Phone 2 PF State:</label>
				<input type="text" name="phone2_pf_st" value="<?= $o_listing->get_phone2_pf_st() ?>" /><br/>
				<label for="misc1">Misc 1:</label>
				<input type="text" name="misc1" value="<?= $o_listing->get_misc1() ?>" /><br/>
				<label for="zip_approximate">Zip Approximate:</label>
				<input type="text" name="zip_approximate" value="<?= $o_listing->get_zip_approximate() ?>" /><br/>
				<label for="zip_confidence">Zip Confidence:</label>
				<input type="text" name="zip_confidence" value="<?= $o_listing->get_zip_confidence() ?>" /><br/>
				<label for="latitude">Latitude:</label>
				<input type="text" name="latitude" value="<?= $o_listing->get_latitude() ?>" /><br/>
				<label for="longitude">Longitude:</label>
				<input type="text" name="longitude" value="<?= $o_listing->get_longitude() ?>" /><br/>
				<label for="map_url">Map URL:</label>
				<input type="text" name="map_url" value="<?= $o_listing->get_map_url() ?>" /><br/>
				<label for="map_url">Product:</label>
				<input type="text" name="product" value="<?= $o_listing->get_product() ?>" /><br/>
				<label for="map_url">Status:</label>
				<input type="text" name="status" value="<?= $o_listing->get_status() ?>" /><br/>
			</fieldset>
			<input type="submit" value="Save" />
			<input type="button" name="Delete" value="Delete" onClick="if (confirm('Are you sure you want to delete this listing?')) { document.location.href='/office/listing/handle.php?cmd=delete&listingid=<?= $o_listing->get_listingid() ?>'; }" />
			</form>
		</td>
	</tr>
</table>
<?php
include_once($_SESSION['web_interface']->get_server_path('office/global/bottom.php'));
?>
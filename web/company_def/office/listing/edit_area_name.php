<?php
if ($_SESSION['office']->account->get_account_type() != 'ADMIN') {
	$_SESSION['web_interface']->destroySession();
	exit;
}
$_PAGE_TITLE = 'Area Names';

require_once('office/account/class.area_name.php');
try {
	$o_area_name = new area_name( array('area_nameid'=>$_REQUEST['area_nameid']) );
	$o_area_name->initialize($_REQUEST);
	// initialize country if not there
	if (!$o_area_name->get_area_country()) {
		$o_area_name->initialize( array('area_country'=>'US') );
	}
}
catch (Exception $exception) {
	throw $exception;
}

include_once($_SESSION['web_interface']->get_server_path('office/global/top.php'));
?>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td class="bodyNav" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('office/listing/left_nav_' . $_SESSION['office']->account->get_account_type() . '.php')); ?>
		</td>
		<td class="bodyMain" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('other/misc/status_error.php')); ?>
			<h1>Add / Edit Area Name</h1>
			Fill out the form below to add / edit the Area Name.
			<form method="post" action="/office/listing/handle.php">
			<input type="hidden" name="cmd" id="cmd" value="save_area_name" />
			<input type="hidden" name="area_nameid" id="area_nameid" value="<?= $o_area_name->get_area_nameid() ?>" />
			<fieldset>
				<table border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td <?= $_SESSION['web_interface']->missing_required_label('area_country') ?>>Area Country:</td>
						<td><input type="text" name="area_country" value="<?= $o_area_name->get_area_country() ?>" <?= $_SESSION['web_interface']->missing_required_input('area_country') ?> /> (e.g. US)</td>
					</tr>
					<tr>
						<td <?= $_SESSION['web_interface']->missing_required_label('area_state') ?>>Area State:</td>
						<td><input type="text" name="area_state" value="<?= $o_area_name->get_area_state() ?>" <?= $_SESSION['web_interface']->missing_required_input('area_state') ?> /> (e.g. Arizona)</td>
					</tr>
					<tr>
						<td <?= $_SESSION['web_interface']->missing_required_label('area_name') ?>>Area Name:</td>
						<td><input type="text" name="area_name" value="<?= $o_area_name->get_area_name() ?>" <?= $_SESSION['web_interface']->missing_required_input('area_name') ?> /> (e.g. AZ-Tucson Area)</td>
					</tr>
					<tr>
						<td valign="top" <?= $_SESSION['web_interface']->missing_required_label('description') ?>>Description:</td>
						<td valign="top"><textarea name="description" cols="40" rows="3" <?= $_SESSION['web_interface']->missing_required_input('description') ?> /><?= $o_area_name->get_description() ?></textarea></td>
					</tr>
					<tr>
						<td valign="top" <?= $_SESSION['web_interface']->missing_required_label('cities') ?>>Cities:</td>
						<td valign="top"><textarea name="cities" cols="40" rows="5" <?= $_SESSION['web_interface']->missing_required_input('cities') ?> /><?= $o_area_name->get_cities() ?></textarea></td>
					</tr>
					<tr><td colspan="2"><input type="submit" value="Submit" /></td></tr>
				</table>
			</fieldset>
			</form>
		</td>
	</tr>
</table>
<?php
include_once($_SESSION['web_interface']->get_server_path('office/global/bottom.php'));
?>
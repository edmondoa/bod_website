<?php
require_once('office/account/class.area_name.php');
require_once('office/listing/class.listing.php');

switch (strtolower($_REQUEST['cmd'])) {

	//handle a DELETE AREA NAME request
	case 'delete_area_name':
		try {
			$o_area_name = new area_name( array('area_nameid'=>$_REQUEST['area_nameid']) );
			$o_area_name->delete();
		}
		catch (Exception $exception) {
			throw $exception;
		}
		$_SESSION['status']['message'] = 'Your Area has been deleted.';
		include_once($_SESSION['web_interface']->get_server_path('office/listing/area_name.php'));
	break;
	
	//handle a DELETE request
	case 'delete':
		try {
			$o_listing = new listing( array('listingid'=>$_REQUEST['listingid']) );
			$o_listing->delete();
			$_SESSION['status']['message'] = 'The listing has been deleted.';
		}
		catch (Exception $exception) {
			throw $exception;
		}
		$_REQUEST['page'] = ($_REQUEST['page']) ? $_REQUEST['page'] : 'search';
		include_once($_SESSION['web_interface']->get_server_path('office/listing/' . $_REQUEST['page'] . '.php'));
	break;
	
	//handle a DELETE MULTIPLE request
	case 'delete_multiple':
		try {
			foreach ($_REQUEST['listingid_checkbox'] as $listingid) {
				$o_listing = new listing( array('listingid'=>$listingid) );
				$o_listing->delete();
			}
			$_SESSION['status']['message'] = 'The listings have been deleted.';
		}
		catch (Exception $exception) {
			throw $exception;
		}
		$_REQUEST['cmd'] = 'search';
		include_once($_SESSION['web_interface']->get_server_path('office/listing/search.php'));
	break;
	
	//handle a SAVE request
	case 'save':
		try {
			$o_listing = new listing( array('listingid'=>$_REQUEST['listingid']) );
			$o_listing->initialize($_REQUEST);
			$_SESSION['status']['message'] = $o_listing->save();
		}
		catch (Exception $exception) {
			throw $exception;
		}
		include_once($_SESSION['web_interface']->get_server_path('office/listing/edit.php'));
	break;
	
	//handle a SAVE AREA NAME request
	case 'save_area_name':
		$_SESSION['web_interface']->check_required( array(required=>'area_country,area_state,area_name,description') );
		if (!preg_match("/^\w\w-.*$/", $_REQUEST['area_name'])) {
			$_SESSION['error']['message'] = 'Area Names must be in the form of {Two Letter State}-{Name}, e.g. UT-Utah County.';
		}
		if (isset($_SESSION['error'])) {
			include_once($_SESSION['web_interface']->get_server_path('office/listing/edit_area_name.php'));
		}
		else {
			$_REQUEST['area_title'] = preg_replace("/^\w\w-/", "", $_REQUEST['area_name']);
			try {
				$o_area_name = new area_name( array('area_nameid'=>$_REQUEST['area_nameid']) );
				$o_area_name->initialize($_REQUEST);
				$_SESSION['status']['message'] = $o_area_name->save();
			}
			catch (Exception $exception) {
				throw $exception;
			}
			include_once($_SESSION['web_interface']->get_server_path('office/listing/area_name.php'));
		}
	break;
	
	//handle a SAVE AREA NAMES request
	case 'save_area_names':
		$o_area_name_utilities = new area_name_utilities();
		$list_area_name = $o_area_name_utilities->get_list();
		foreach ($list_area_name as $item_area_name) {
			if ($_REQUEST['area_state_' . $item_area_name->get_area_nameid()] && $_REQUEST['area_name_' . $item_area_name->get_area_nameid()]) {
				$area_title = preg_replace("/^\w\w-/", "", $_REQUEST['area_name_' . $item_area_name->get_area_nameid()]);
				$item_area_name->initialize(
					array(
						'area_name'=>$_REQUEST['area_name_' . $item_area_name->get_area_nameid()],
						'area_state'=>$_REQUEST['area_state_' . $item_area_name->get_area_nameid()],
						'area_title'=>$area_title
					)
				);
				$item_area_name->save();
			}
		}
		$_SESSION['status']['message'] = 'All Areas have been updated.';
		include_once($_SESSION['web_interface']->get_server_path('office/listing/area_name.php'));
	break;
	
	//handle a SEND LISTINGS request
	case 'send_listings':
		$_SESSION['web_interface']->check_required( array(required=>'accountid|time_zone') );
		if ($_REQUEST['noEmptyEmails'] && !$_REQUEST['product']) {
			$_SESSION['error']['message'] .= '<br/>You must select a Product to be able to check the No Empty Emails box.';
		}
		if (isset($_SESSION['error'])) {
			include_once($_SESSION['web_interface']->get_server_path('office/listing/send_listings_confirm.php'));
		}
		else {
			// if they are sending to an account use normal method
			if ($_REQUEST['accountid']) {
				$o_listing_utilities = new listing_utilities();
				$o_listing_utilities->send_listings($_REQUEST);
				$_SESSION['status']['message'] = 'The listings have been sent.';
			}
			else { // fork the process
				$results = file_get_contents('http://' . $_SERVER['HTTP_HOST'] . '/scripts/send_listings.php?cmd=fork&time_zone=' . $_REQUEST['time_zone'] . '&product=' . $_REQUEST['product'] . '&noEmptyEmails=' . $_REQUEST['noEmptyEmails']);
				if ($results == 'success') {
					$_SESSION['status']['message'] = 'The listings have been sent.';
				}
				else {
					$_SESSION['status']['message'] = 'There was an error please try again.';
				}
			}
			include_once($_SESSION['web_interface']->get_server_path('office/listing/send_listings_sent.php'));
		}
	break;
	
	//handle a UPLOAD request
	case 'upload':
		$totalNumRecords = 0;
		$totalDuplicateRecords = 0;
		$_REQUEST['file1'] = $_FILES['file1']['name'];
		$_SESSION['web_interface']->check_required( array(required=>'file1') );
		if (isset($_SESSION['error'])) {
			include_once($_SESSION['web_interface']->get_server_path('office/listing/upload.php'));
		}
		else {
			for ($count = 1; $count <= 10; $count++) {
				$_REQUEST['file' . $count] = $_FILES['file' . $count]['name'];
				try {
					$o_listing_utilities = new listing_utilities();
					list($num_records, $num_duplicate_records) = $o_listing_utilities->upload( array('file_location'=>$_FILES['file' . $count]['tmp_name']) );
					$totalNumRecords += $num_records;
					$totalDuplicateRecords += $num_duplicate_records;
				}
				catch (Exception $exception) {
					throw $exception;
				}
			}
		}
		$_SESSION['status']['message'] = 'Your file(s) have been updated.<br/>There were ' . $totalNumRecords . ' records in the file(s) of which ' . $totalDuplicateRecords . ' were duplicates.<br/>To send these latest listings to all accounts <a href="/office/listing/send_listings_confirm.php">click here</a>.';
		include_once($_SESSION['web_interface']->get_server_path('office/listing/upload.php'));
	break;
	
	//handle a upload_cl_file request
	case 'upload_cl_file':
		try {
			if ($_FILES['file']['tmp_name']) {
				$o_listing_utilities = new listing_utilities();
				$num_records = $o_listing_utilities->upload_cl_file( array('file_location'=>$_FILES['file']['tmp_name']) );
				
				$_SESSION['status']['message'] = 'Your file has been uploaded.<br/>There were ' . $num_records . ' records in the file.';
			}
		}
		catch (Exception $exception) {
		}
		include_once($_SESSION['web_interface']->get_server_path('office/listing/uploadFile.php'));
	break;

	//handle a upload_copy_paste_file request
	case 'upload_copy_paste_file':
		try {
			if ($_FILES['file']['tmp_name']) {
				$o_listing_utilities = new listing_utilities();
				$num_records = $o_listing_utilities->upload_copy_paste_file( array('file_location'=>$_FILES['file']['tmp_name']) );
				
				$_SESSION['status']['message'] = 'Your file has been uploaded.<br/>There were ' . $num_records . ' records in the file.';
			}
		}
		catch (Exception $exception) {
		}
		include_once($_SESSION['web_interface']->get_server_path('office/listing/uploadFile.php'));
	break;
	
}

?>

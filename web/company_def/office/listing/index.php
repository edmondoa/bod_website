<?php
if ($_SESSION['office']->account->get_account_type() != 'ADMIN') {
	$_SESSION['web_interface']->destroySession();
	exit;
}
$_PAGE_TITLE = 'Listings';

include_once($_SESSION['web_interface']->get_server_path('office/global/top.php'));
?>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td class="bodyNav" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('office/listing/left_nav_' . $_SESSION['office']->account->get_account_type() . '.php')); ?>
		</td>
		<td class="bodyMain" valign="top">
			<h1>Listings Section</h1>
<?php
if (!$_SESSION['office']->account->is_upload()) {
?>
			In this section you can <a href="/office/listing/search.php">search</a> the listings.
<?php
}
if ($_SESSION['office']->account->is_admin()) {
?>
			<br/>You may also <a href="/office/listing/area_name.php">manage the Area Names</a>, <a href="/office/listing/sent_listings.php">view the listings that were sent today</a>, <a href="/office/listing/send_listings_confirm.php">send the latest listings to all accounts</a>, or <a href="/office/listing/upload.php">upload the new listings</a>.
<?php
}
?>
		</td>
	</tr>
</table>
<?php
include_once($_SESSION['web_interface']->get_server_path('office/global/bottom.php'));
?>

<?php
if ($_SESSION['office']->account->get_billing_status() != 'active' && $_SESSION['office']->account->get_billing_status() != 'queued') {
	header('Location: /office/index.php');
}
$_PAGE_TITLE = 'Listings';

require_once($_SESSION['web_interface']->get_server_path('other/misc/listing_fields.php'));

if ($_REQUEST['accountid']) {
	require_once('office/account/class.account.php');
	$o_account = new account( array('accountid'=>$_REQUEST['accountid']) );
	if (!$o_account->isOwner()) {
		$_SESSION['web_interface']->destroySession();
		exit;
	}
	$_REQUEST['string_area_names'] = $o_account->get_string_area_names();
	$account_search_fields = $o_account->get_hash_search_fields();
	foreach ($account_search_fields as $key=>$value) {
		$_REQUEST[$key] = $value;
	}
	
	$_REQUEST['where_plus'] = '';
	// only search for products they are signed up for
	$_REQUEST['where_plus'] .= " AND product IN ('',";
	foreach ($o_account->getArrayProducts() as $productId=>$name) {
		$_REQUEST['where_plus'] .= "'" . $_SESSION['data_access']->db_quote($name) . "',";
	}
	$_REQUEST['where_plus'] = preg_replace("/,$/", ") ", $_REQUEST['where_plus']);
	
	// only search for property_types they want
	$array_account_property_type_fields = $o_account->get_array_property_type_fields();
	if (count($array_account_property_type_fields) > 0) {
		$_REQUEST['where_plus'] .= " AND property_type IN (";
		foreach ($array_account_property_type_fields as $propertyType=>$boolean) {
			$_REQUEST['where_plus'] .= "'" . $_SESSION['data_access']->db_quote($propertyType) . "',";
		}
		$_REQUEST['where_plus'] = preg_replace("/,$/", ") ", $_REQUEST['where_plus']);
	}
}

$fields = array('start_month','start_day','start_year','end_month','end_day','end_year','area_name','string_area_names','minPrice','maxPrice','bedrooms','bathrooms','garage','square_feet','year_built','lot_size','description_ad','scrub_edit','lead_id','contact_phone1','contact_phone2','no_dnc','with_address_only','with_phone_only','with_email_only','lead_id','contact_phone');
foreach ($fields as $field) {
	if (isset($_REQUEST[$field])) {
		$_REQUEST[$field] = $_REQUEST[$field];
	}
	else if ($_REQUEST['cmd']) {
		unset($_SESSION[$field]);
	}
	else if ($_SESSION[$field]) {
		$_REQUEST[$field] = $_SESSION[$field];
	}
}

if ($_REQUEST['cmd']) {
	if ($_REQUEST['area_name'] || $_REQUEST['area_name_checkbox'] || $_REQUEST['minPrice'] || $_REQUEST['bedrooms'] || $_REQUEST['bathrooms'] || $_REQUEST['square_feet'] || $_REQUEST['year_built'] || $_REQUEST['lot_size'] || $_REQUEST['description_ad'] || ($_REQUEST['start_month'] && $_REQUEST['start_day'] && $_REQUEST['start_year'] && $_REQUEST['end_month'] && $_REQUEST['end_day'] && $_REQUEST['end_year']) || $_REQUEST['no_dnc'] || $_REQUEST['scrub_edit'] || $_REQUEST['lead_id'] || $_REQUEST['contact_phone1'] || $_REQUEST['contact_phone2'] || $_REQUEST['with_address_only'] || $_REQUEST['with_phone_only'] || $_REQUEST['with_email_only'] || $_REQUEST['lead_id'] || $_REQUEST['contact_phone']) {
		// add the search items to the session
		foreach ($_REQUEST as $key=>$value) {
			if ($key == 'area_name_checkbox') {
				$_REQUEST['string_area_names'] = ',';
				foreach ($_REQUEST[$key] as $index=>$area_name) {
					$_REQUEST['string_area_names'] .= $area_name . ',';
				}
				$_SESSION['string_area_names'] = $_REQUEST['string_area_names'];
			}
			else {
				$_SESSION[$key] = $value;
			}
		}
		// get the listings
		require_once('office/listing/class.listing.php');
		try {
			$o_listing_utilities = new listing_utilities( array('order_by'=>$_REQUEST['order_by']) );
			$list_listing = $o_listing_utilities->get_list($_REQUEST);
		}
		catch (Exception $exception) {
			throw $exception;
		}
		
		// set the date range
		if ($_REQUEST['start_month'] && $_REQUEST['start_day'] && $_REQUEST['start_year'] && $_REQUEST['end_month'] && $_REQUEST['end_day'] && $_REQUEST['end_year']) {
			$_REQUEST['start_date'] = $_REQUEST['start_year'] . '-' . $_REQUEST['start_month'] . '-' . $_REQUEST['start_day'];
			$_REQUEST['end_date'] = $_REQUEST['end_year'] . '-' . $_REQUEST['end_month'] . '-' . $_REQUEST['end_day'];
			if ($_REQUEST['start_date'] == $_REQUEST['end_date']) {
				$date_range = date('F j, Y', strtotime($_REQUEST['start_date']));
			}
			else {
				$date_range = date('F j, Y', strtotime($_REQUEST['start_date'])) . ' - ' . date('F j, Y', strtotime($_REQUEST['end_date']));
			}
		}
		else {
			$date_range = date('F j, Y');
		}
			
		if ($_REQUEST['cmd'] == 'download' || $_REQUEST['cmd'] == 'send_to') {
			$attachment_rows = '';
			// get the listing fields for the account if there are any
			if ($_REQUEST['accountid']) {
				$array_account_listings = $o_account->get_array_listing_fields();
			}
			else {
				$array_account_listings = $_SESSION['office']->account->get_array_listing_fields();
			}
			// if there are then build the attachment otherwise use default
			if (count($array_account_listings) > 0) {
				$attachment_rows = '';
				foreach ($allListingFields as $databaseField=>$displayField) {
					if ($array_account_listings[$databaseField]) {
						$attachment_rows .= $displayField . ',';
						$arrayListingFields[$databaseField] = $displayField;
					}
				}
				$attachment_rows = substr($attachment_rows, 0, -1) . "\n";
				foreach ($list_listing as $item_listing) {
					$row = '';
					$item_listing = new listing( array('listingid'=>$item_listing->get_listingid()) );
					$phone1_dnc = ($item_listing->get_phone1_dnc() == 'T') ? '(Do Not Call Registry)' : '';
					$phone2_dnc = ($item_listing->get_phone2_dnc() == 'T') ? '(Do Not Call Registry)' : '';
					foreach ($arrayListingFields as $databaseField=>$displayField) {
						if ($databaseField == 'phone1_dnc') {
							$row .= '"' . $phone1_dnc . '",';
						}
						else if ($databaseField == 'phone2_dnc') {
							$row .= '"' . $phone2_dnc . '",';
						}
						else {
	          	$function_name = 'get_' . $databaseField;
          		$row .= '"' . $item_listing->$function_name() . '",';
						}
					}
					$row = substr($row, 0, -1);
					$attachment_rows .= $row . "\n";
				}	
			}
			// handle the sending of the file to real property management
			else if ($_SESSION['office']->account->get_accountid() == 5374) {
				$attachment_rows = '';
				$attachment_rows = 'Franchise ID,Lead First Name,Lead Last Name,Lead Phone,Lead Email,Lead Source,Comments,Address,City,State,Zip,Lead Business Name,Owner Type,Number of Properties,Occupancy Type,Priority Status,Corporate Campaign,Decision Time Frame,Hot Button,Management Type,Management Fee %,Management Flat Fee,Lease Fee %,Setup Fee,Presentation Reviewed,Referral First Name,Referral Last Name,Referral Phone,Referral Email,Referral Address,Referral City,Referral State,Referral Zip,Referral Business Name,Referral Type,Realtor License #,Rental Property 1 Address,Rental Property 1 City,Rental Property 1 State,Rental Property 1 Zip,Rental Property 1 Occupancy Type,Rental Property 1 Date Available,Rental Property 1 Desired Rent,Rental Property 1 Market Rent (CMA),Rental Property 1 RPM Suggested Rent,Rental Property 1 Property Comment,Rental Property 1 Bedrooms,Rental Property 1 Bathrooms,Rental Property 1 Square Footage,Rental Property 1 Pets,Rental Property 1 Year Built,phone1_dnc' . "\n";
				foreach ($list_listing as $item_listing) {
					$item_listing = new listing( array('listingid'=>$item_listing->get_listingid()) );
					list($firstName, $lastName) = explode(" ", $item_listing->get_contact_name1(), 2);
					$attachment_rows .= '"CallCenter","' . $firstName . '","' . $lastName . '","' . $item_listing->get_contact_phone1() . '","' . $item_listing->get_contact_email() . '","DD Leads","' . $item_listing->get_description_ad() . '","' . $item_listing->get_contact_street1() . '","' . $item_listing->get_contact_city1() . '","' . $item_listing->get_contact_st1() . '","' . $item_listing->get_contact_zip1() . '","","","","","","","","","","","","","","","' . $item_listing->get_lead_id() . '","","","","","","","","","","","' . $item_listing->get_property_street() . '","' . $item_listing->get_property_city() . '","' . $item_listing->get_property_st() . '","' . $item_listing->get_property_zip() . '","","","' . $item_listing->get_price() . '","","","","' . $item_listing->get_bedrooms() . '","' . $item_listing->get_bathrooms() . '","' . $item_listing->get_square_feet() . '","","' . $item_listing->get_year_built() . '","' . $item_listing->get_phone1_dnc() . '"' . "\n";
				}
			}
			else {
				$attachment_rows = '';
				foreach ($defaultListingFields as $databaseField=>$displayField) {
					$attachment_rows .= $displayField . ',';
				}
				$attachment_rows = substr($attachment_rows, 0, -1) . "\n";
				foreach ($list_listing as $item_listing) {
					$row = '';
					$item_listing = new listing( array('listingid'=>$item_listing->get_listingid()) );
					$phone1_dnc = ($item_listing->get_phone1_dnc() == 'T') ? '(Do Not Call Registry)' : '';
					$phone2_dnc = ($item_listing->get_phone2_dnc() == 'T') ? '(Do Not Call Registry)' : '';
					foreach ($defaultListingFields as $databaseField=>$displayField) {
						if ($databaseField == 'phone1_dnc') {
							$row .= '"' . $phone1_dnc . '",';
						}
						else if ($databaseField == 'phone2_dnc') {
							$row .= '"' . $phone2_dnc . '",';
						}
						else {
	          	$function_name = 'get_' . $databaseField;
          		$row .= '"' . $item_listing->$function_name() . '",';
						}
					}
					$row = substr($row, 0, -1);
					$attachment_rows .= $row . "\n";
				}
			}
			if ($_REQUEST['cmd'] == 'download') {
				header("Content-type: application/vnd.ms-excel");
				header("Content-disposition: attachment; filename=listings.csv");
				print $attachment_rows;
				exit;
			}
			else if ($_REQUEST['cmd'] == 'send_to') {
				// set up the email body stuff
				$email_body = file_get_contents( $o_account->get_server_path('office/listing/email_generic_body.txt') ) . file_get_contents( $o_account->get_server_path('office/listing/email_bottom.txt') );
				$email_body = preg_replace("/<<company_url>>/", $o_account->o_company->get_url(), $email_body);
				$email_body = preg_replace("/<<company_title>>/", $o_account->o_company->get_title(), $email_body);
				$email_body = preg_replace("/<<date_range>>/", $date_range, $email_body);
				$email_body = preg_replace("/<<today>>/", date('F j, Y'), $email_body);
				$email_body = preg_replace("/<<today_weekday>>/", date('l'), $email_body);
				if ($o_account->get_account_type() != 'TRIAL') {
					$email_body = preg_replace("/<<company_address>>/", "105 South State St.  #204\nOrem, UT  84058", $email_body);
				}
				// build entire email
				$boundary = '_____' . date('Ymd') . '_____';
				$from_email = ($o_account->o_company->get_from_email()) ? $o_account->o_company->get_from_email() : 'fsbo@' . $o_account->o_company->get_url();
				$reply_to_email = ($o_account->o_company->get_reply_to_email()) ? $o_account->o_company->get_reply_to_email() : 'info@' . $o_account->o_company->get_url();
				$headers = "From: " . $o_account->o_company->get_title() . " <" . $from_email . ">\r\n" . 
					"Reply-To: " . $reply_to_email . "\r\n" . 
					"MIME-Version: 1.0\r\n" . 
					"Content-Type: multipart/mixed; boundary=\"" . $boundary . "\"\r\n" . 
					//"Content-Transfer-Encoding: 8bit\r\n\r\n" . 
					"--" . $boundary . "\r\n" . 
					"Content-Type: text/plain; charset=iso-8859-1\r\n" . 
					"Content-Transfer-Encoding: 8bit\r\n" . 
					"Content-Disposition: inline\r\n\r\n" . 
					$email_body . "\r\n"
				;
				$headers .= "--" . $boundary . "\r\n" . 
					"Content-Type: application/vnd.ms-excel; name=\"" . date('Y_m_d') . "_FSBO_Leads.csv\"\r\n" . 
					"Content-Transfer-Encoding: base64\r\n" . 
					"Content-Disposition: inline; filename=\"" . date('Y_m_d') . "_FSBO_Leads.csv\"\r\n\r\n" . 
					chunk_split(base64_encode($attachment_rows)) . "\r\n"
				;
				$headers .= "--" . $boundary . "--\r\n";
				$headers = preg_replace("/<<[^>].+?>>/", "", $headers);
				mail($o_account->get_main_email_address(), $date_range . " Leads for " . $o_account->get_first_name() . ' ' . $o_account->get_last_name(), '', $headers, '-f fsbo@' . $o_account->o_company->get_url());
			}
		}
		else if ($_REQUEST['cmd'] == 'download_all') {
			$rows = '';
			foreach ($allListingFields as $databaseField=>$displayField) {
				$rows .= $displayField . ',';
			}
			$rows = substr($rows, 0, -1) . "\n";
			foreach ($list_listing as $item_listing) {
				$row = '';
				$item_listing = new listing( array('listingid'=>$item_listing->get_listingid()) );
				foreach ($allListingFields as $databaseField=>$displayField) {
					$function_name = 'get_' . $databaseField;
					$row .= '"' . $item_listing->$function_name() . '",';
				}
				$row = substr($row, 0, -1);
				$rows .= $row . "\n";
			}
			header("Content-type: application/vnd.ms-excel");
			header("Content-disposition: attachment; filename=listings.csv");
			print $rows;
			exit;
		}
	}
	else {
		$_SESSION['error']['message'] = 'You must enter in at least one field to search by.  If you want to select by Date Delivery then you must enter starting and ending dates.';
	}
}
else {
/*
	$_REQUEST['start_month'] = ($_REQUEST['start_month']) ? $_REQUEST['start_month'] : date('m');
	$_REQUEST['start_day'] = ($_REQUEST['start_day']) ? $_REQUEST['start_day'] : date('d');
	$_REQUEST['start_year'] = ($_REQUEST['start_year']) ? $_REQUEST['start_year'] : date('Y');
	$_REQUEST['end_month'] = ($_REQUEST['end_month']) ? $_REQUEST['end_month'] : date('m');
	$_REQUEST['end_day'] = ($_REQUEST['end_day']) ? $_REQUEST['end_day'] : date('d');
	$_REQUEST['end_year'] = ($_REQUEST['end_year']) ? $_REQUEST['end_year'] : date('Y');
*/
}
 
require_once($_SESSION['web_interface']->get_server_path('other/misc/select_options.php'));
include_once($_SESSION['web_interface']->get_server_path('office/global/top.php'));
?>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td class="bodyNav" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('office/listing/left_nav_' . $_SESSION['office']->account->get_account_type() . '.php')); ?>
		</td>
		<td class="bodyMain" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('other/misc/status_error.php')); ?>
			<h1>Search Listings</h1>
<?php
if ($_REQUEST['accountid']) {
?>
			You will be able to send your search results to: <a href="/office/account/account.php?accountid=<?= $o_account->get_accountid() ?>"><?= $o_account->get_first_name() ?> <?= $o_account->get_last_name() ?></a><br/>
			<br/>
<?php
}
?>
			Search by any or all of the options below. Please note that not all ads will contain all of the below information.  Use date search option to return ALL records in the desired date range.
<?php
if ($_SESSION['office']->account->get_account_type() == 'CUSTOMER') {
?>
			<br/>
			<span style="color: #FF0000;">Note: Only the last three months of data will be shown.</span>
<?php
}
?>
			<form method="post" action="/office/listing/search.php" name="searchForm" id="searchForm">
			<input type="hidden" name="cmd" id="cmd" value="search" />
			<input type="hidden" name="order_by" value="" />
			<input type="hidden" name="accountid" value="<?= $_REQUEST['accountid'] ?>" />
			<div style="background-color: #F8F7F1; font-weight: bold; margin: 5px; padding: 10px; white-space: nowrap; width: 95%;">
				<?php include_once($_SESSION['web_interface']->get_server_path('office/listing/search_form_' . $_SESSION['office']->account->get_account_type() . '.php')); ?>
			</div>
			<input type="button" name="Show Count" value="Show Count" onClick="document.getElementById('cmd').value='count'; document.getElementById('searchForm').submit()" />&nbsp;
			<input type="button" name="Show Results" value="Show Results" onClick="document.getElementById('cmd').value='search'; document.getElementById('searchForm').submit()" />&nbsp;
<?php
if ($_SESSION['office']->account->is_admin() || $_SESSION['office']->account->is_support() || $_SESSION['office']->account->is_customer()) {
?>
			<input type="button" name="Download" value="Download" onClick="document.getElementById('cmd').value='download'; document.getElementById('searchForm').submit()" />
<?php
}
if ($_SESSION['office']->account->is_admin() || $_SESSION['office']->account->is_support()) {
?>
			<input type="button" name="Download All" value="Download All" onClick="document.getElementById('cmd').value='download_all'; document.getElementById('searchForm').submit()" />
<?php
}
if ($_SESSION['office']->account->is_admin() && $_REQUEST['accountid']) {
?>
			<input type="button" name="Send To <?= $o_account->get_first_name() ?> <?= $o_account->get_last_name() ?>" value="Send To <?= $o_account->get_first_name() ?> <?= $o_account->get_last_name() ?>" onClick="document.getElementById('cmd').value='send_to'; document.getElementById('searchForm').submit()" />
<?php
}
?>
			</form>
			<br/>
<?php
if (!isset($_SESSION['error']) && ($_REQUEST['cmd'] == 'search' || $_REQUEST['cmd'] == 'count') || $_REQUEST['cmd'] == 'send_to') {
?>
			<h1>Search Results</h1>
<?php
	if (empty($list_listing)) {
?>
			There are no results at this time.
<?php
	}
	else {
		if ($_REQUEST['cmd'] == 'send_to') {
?>
			The below results were sent to <?= $o_account->get_first_name() ?> <?= $o_account->get_last_name() ?> at <?= $o_account->get_main_email_address() ?>.<br/>
<?php
		}
		else {
?>
			Your search resulted in <strong><?php if ($_REQUEST['cmd'] == 'count') { print $list_listing['total_count']; } else { print count($list_listing); } ?></strong> matches. The results are listed below.
<?php
		}
		if ($_REQUEST['cmd'] == 'count') {
			foreach ($list_listing as $key=>$row) {
				if ($key && $key == 'total_count') {
					continue;
				}
?>
			<br/><strong><?= $row['area_name'] ?> Total</strong>: <?= $row['num'] ?>
<?php
			}
		}
		else {
			$area_totals = array();
			foreach ($list_listing as $item_listing) {
				$area_totals[$item_listing->get_area_name()]++;
			}
			ksort($area_totals);
			foreach ($area_totals as $area_name=>$total) {
?>
			<br/><strong><?= $area_name ?> Total</strong>: <?= $total ?>
<?php
			}
		}
		if ($_REQUEST['cmd'] == 'search' || $_REQUEST['cmd'] == 'send_to') {
?>
			<form method="post" action="/office/listing/handle.php">
			<input type="hidden" name="cmd" value="delete_multiple" />
			<div class="boxHeader">
				<table cellpadding="0" cellspacing="0" border="0">
					<colgroup>
						<col width="25"/>
						<col width="220"/>
						<col width="290"/>
						<col width="80"/>
						<col width="45"/>
					</colgroup>
					<tbody>
					<tr class="head">
						<th align="center"><?php if ($_SESSION['office']->account->get_account_type() == 'ADMIN') { ?><input type="checkbox" id="ck" name="ck" value="" onClick="checkAll(this);" /><?php } else { print '&nbsp;'; } ?></th>
						<th align="center" <?= $o_listing_utilities->get_order_by_class('area_name') ?>><a href="#" onClick="document.getElementById('searchForm').order_by.value='area_name, date_posted desc, contact_name1'; document.getElementById('searchForm').submit();">Area Name</a></th>
						<th>Description</th>
						<th align="center" <?= $o_listing_utilities->get_order_by_class('date_posted') ?>><a href="#" onClick="document.getElementById('searchForm').order_by.value='date_posted desc, area_name, contact_name1'; document.getElementById('searchForm').submit();">Date</a></th>
						<th>&nbsp;</th>
					</tr>
					</tbody>
				</table>
			</div>
			<div class="boxScroll">
				<table cellpadding="0" cellspacing="0" border="0">
					<colgroup>
						<col width="25"/>
						<col width="220"/>
						<col width="290"/>
						<col width="80"/>
						<col width="25"/>
					</colgroup>
					<tbody>
<?php
			$position = 0;
			foreach ($list_listing as $item_listing) {
?>
					<tr class="row<?= $_SESSION['web_interface']->get_row_class($row_num) ?>">
						<td align="center"><?php if ($_SESSION['office']->account->get_account_type() == 'ADMIN') { ?><input type="checkbox" id="listingid_checkbox_<?= $item_listing->get_listingid() ?>" name="listingid_checkbox[]" value="<?= $item_listing->get_listingid() ?>" /><?php } ?></td>
						<td align="center"><?= $item_listing->get_area_name() ?></td>
						<td><a href="/office/listing/view.php?listingid=<?= $item_listing->get_listingid() ?>&position=<?= $position ?>"><?= substr($item_listing->get_description_ad(), 0, 30) ?>..</a></td>
						<td align="center" style="white-space: nowrap;"><?= date('M j, y', strtotime($item_listing->get_date_posted())) ?></td>
						<td align="center">
<?php
				if ($_SESSION['office']->account->is_admin()) {
?>
							<a href="/office/listing/edit.php?listingid=<?= $item_listing->get_listingid() ?>"><img src="<?= $_SESSION['web_interface']->get_path('img/editBtn.gif') ?>" border="0" /></a>&nbsp;
<?php
				}
?>
						</td>
					</tr>
<?php
				$position++;
			}
?>
					</tbody>
				</table>
			</div>
			<?php if ($_SESSION['office']->account->get_account_type() == 'ADMIN') { ?><input type="submit" value="Delete Checked Listings" onclick="return (confirm('Are you sure you want to delete these listings?'));"/><?php } ?>
			</form>
<?php
		}
	}
}
?>
		</td>
	</tr>
</table>
<?php
include_once($_SESSION['web_interface']->get_server_path('office/global/bottom.php'));
?>

<?php
require_once('office/account/class.area_name.php');
$o_area_name_utilities = new area_name_utilities();

require_once('office/account/class.Product.php');
$ProductUtilities = new ProductUtilities( array('orderBy'=>'productId') );
$listProduct = $ProductUtilities->getList(); 
?>
<script type="text/javascript">
	function setDates() {
		document.getElementById("start_month").value = '<?= date('m') ?>';
		document.getElementById("start_day").value = '<?= date('d') ?>';
		document.getElementById("start_year").value = '<?= date('Y') ?>';
		document.getElementById("end_month").value = '<?= date('m') ?>';
		document.getElementById("end_day").value = '<?= date('d') ?>';
		document.getElementById("end_year").value = '<?= date('Y') ?>';
	}
	
	function checkAllCA(checkallbox) {
		var myForm = checkallbox.form;
<?php
foreach ($o_area_name_utilities->get_list_area_states( array('where_plus'=>" AND area_country='CA' ") ) as $area_state) {
?>
		checkAll(checkallbox, '<?= $area_state ?>_');
<?php
}
?>
	}
	
	function checkAllCST(checkallbox) {
		var myForm = checkallbox.form;
<?php
$string_states = $o_area_name_utilities->get_string_states_in_time_zone( array('mysql_formatted'=>TRUE, 'time_zone'=>'CST') );
foreach ($o_area_name_utilities->get_list_area_states( array('where_plus'=>" AND area_country='US' AND SUBSTR(area_name, 1, 2) IN (" . $string_states . ") ") ) as $area_state) {
?>
		checkAll(checkallbox, '<?= $area_state ?>_');
<?php
}
?>
	}
	
	function checkAllEST(checkallbox) {
		var myForm = checkallbox.form;
<?php
$string_states = $o_area_name_utilities->get_string_states_in_time_zone( array('mysql_formatted'=>TRUE, 'time_zone'=>'EST') );
foreach ($o_area_name_utilities->get_list_area_states( array('where_plus'=>" AND area_country='US' AND SUBSTR(area_name, 1, 2) IN (" . $string_states . ") ") ) as $area_state) {
?>
		checkAll(checkallbox, '<?= $area_state ?>_');
<?php
}
?>
	}
	
	function checkAllMST(checkallbox) {
		var myForm = checkallbox.form;
<?php
$string_states = $o_area_name_utilities->get_string_states_in_time_zone( array('mysql_formatted'=>TRUE, 'time_zone'=>'MST') );
foreach ($o_area_name_utilities->get_list_area_states( array('where_plus'=>" AND area_country='US' AND SUBSTR(area_name, 1, 2) IN (" . $string_states . ") ") ) as $area_state) {
?>
		checkAll(checkallbox, '<?= $area_state ?>_');
<?php
}
?>
	}
	
	function checkAllPST(checkallbox) {
		var myForm = checkallbox.form;
<?php
$string_states = $o_area_name_utilities->get_string_states_in_time_zone( array('mysql_formatted'=>TRUE, 'time_zone'=>'PST') );
foreach ($o_area_name_utilities->get_list_area_states( array('where_plus'=>" AND area_country='US' AND SUBSTR(area_name, 1, 2) IN (" . $string_states . ") ") ) as $area_state) {
?>
		checkAll(checkallbox, '<?= $area_state ?>_');
<?php
}
?>
	}
	
	function checkAllUS(checkallbox) {
		var myForm = checkallbox.form;
<?php
foreach ($o_area_name_utilities->get_list_area_states( array('where_plus'=>" AND area_country='US' ") ) as $area_state) {
?>
		checkAll(checkallbox, '<?= $area_state ?>_');
<?php
}
?>
	}
</script>
<table border="0" cellpadding="2" cellspacing="0" width="100%">
	<tr>
		<td>
			<table border="0" cellpadding="2" cellspacing="0" width="50%">
				<tr>
					<td>
						From Date:
						<select id="start_month" name="start_month">
							<option value="">Month</option>
							<?= select_options( array(type=>'short_month', selected=>$_REQUEST['start_month']) ) ?>
						</select>
						<select id="start_day" name="start_day">
							<option value="">Day</option>
							<?= select_options( array(type=>'day', selected=>$_REQUEST['start_day']) ) ?>
						</select>
						<select id="start_year" name="start_year">
							<option value="">Year</option>
							<?= select_options( array(type=>'number', min=>'2000', max=>'2025', selected=>$_REQUEST['start_year']) ) ?>
						</select>
					</td>
					<td>
						To Date:
						<select id="end_month" name="end_month">
							<option value="">Month</option>
							<?= select_options( array(type=>'short_month', selected=>$_REQUEST['end_month']) ) ?>
						</select>
						<select id="end_day" name="end_day">
							<option value="">Day</option>
							<?= select_options( array(type=>'day', selected=>$_REQUEST['end_day']) ) ?>
						</select>
						<select id="end_year" name="end_year">
							<option value="">Year</option>
							<?= select_options( array(type=>'number', min=>'2000', max=>'2025', selected=>$_REQUEST['end_year']) ) ?>
						</select>
					</td>
					<td><a href="#" onClick="setDates();">today</a></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" cellpadding="2" cellspacing="0" width="50%">
				<tr>
					<td>Bed:</td>
					<td>
						<select name="bedrooms">
							<option value="">Any</option>
							<?= select_options( array('type'=>'number', 'max'=>5, 'plus_sign'=>TRUE, 'selected'=>$_REQUEST['bedrooms']) ) ?>
						</select>
					</td>
					<td>Bath:</td>
					<td>
						<select name="bathrooms">
							<option value="">Any</option>
							<?= select_options( array('type'=>'number', 'max'=>5, 'plus_sign'=>TRUE, 'selected'=>$_REQUEST['bathrooms']) ) ?>
						</select>
					</td>
					<td>Square Feet:</td>
					<td>
						<select name="square_feet">
							<option value="">Any</option>
							<?= select_options( array('type'=>'number', 'min'=>500, 'max'=>3000, 'increment'=>250, 'plus_sign'=>TRUE, 'selected'=>$_REQUEST['bathrooms']) ) ?>
						</select>
					</td>
					<td>Garage:</td>
					<td>
						<select name="garage">
							<option value="">Any</option>
							<?= select_options( array('type'=>'number', 'max'=>5, 'plus_sign'=>TRUE, 'selected'=>$_REQUEST['garage']) ) ?>
						</select>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" cellpadding="2" cellspacing="0" width="50%">
				<tr>
					<td>Property Type:</td>
					<td><input type="text" name="property_type" value="<?= $_REQUEST['property_type'] ?>" size="10"/></td>
					<td>Year Built</td>
					<td>
						<select name="year_built">
							<option value="">Any</option>
							<?= select_options( array('type'=>'number', 'min'=>1950, 'max'=>date('Y'), 'increment'=>5, 'plus_sign'=>TRUE, 'selected'=>$_REQUEST['year_built']) ) ?>
						</select>
					</td>
					<td>Lot Size:</td>
					<td>
						<select name="lot_size">
							<option value="">Any</option>
							<?= select_options( array('type'=>'number', 'min'=>'0.25', 'max'=>5, 'increment'=>'0.25', 'plus_sign'=>TRUE, 'selected'=>$_REQUEST['lot_size']) ) ?>
						</select>
					</td>
				</tr>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" cellpadding="2" cellspacing="0" width="50%">
				<tr>
					<td>Keyword</td>
					<td><input type="text" name="description_ad" value="<?= $_REQUEST['description_ad'] ?>" size="25" /></td>
					<td>Price Range:</td>
					<td>
						<input type="text" name="minPrice" value="<?= $_REQUEST['minPrice'] ?>" /> (min) 
						<input type="text" name="maxPrice" value="<?= $_REQUEST['maxPrice'] ?>" /> (max: leave blank if no upper limit)
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" cellpadding="2" cellspacing="0" width="50%">
				<tr>
					<td>Scrubber:</td>
					<td><input type="text" name="scrub_edit" value="<?= $_REQUEST['scrub_edit'] ?>" /></td>
					<td>Lead ID:</td>
					<td><input type="text" name="lead_id" value="<?= $_REQUEST['lead_id'] ?>" /></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" cellpadding="2" cellspacing="0">
				<tr>
					<td>Contact Phone:</td>
					<td><font style="font-size: 9px;">Use format (XXX-XXX-XXXX, e.g. 303-555-1111)</font><br/><input type="text" name="contact_phone" value="<?= $_REQUEST['contact_phone'] ?>" /></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" cellpadding="2" cellspacing="0">
				<tr>
					<td>Product:</td>
					<td>
						<select name="productName">
<?php
foreach ($listProduct as $Product) {
?>
							<option value="<?= $Product->getName() ?>" <?php if ($_REQUEST['productName'] == $Product->getName()) { print 'selected="selected"'; } ?>><?= $Product->getTitle() ?></option>
<?php
}
?>
						</select>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr> 
		<td>
			<table border="0" cellpadding="2" cellspacing="0" width="100%">
				<tr><td><input type="checkbox" name="no_dnc" value="1" <?php if ($_REQUEST['no_dnc']) { print 'checked="checked"'; } ?> /> Is NOT listed with the "U.S. Do Not Call Registry"</td></tr>
				<tr><td><input type="checkbox" name="with_address_only" value="1" <?php if ($_REQUEST['with_address_only']) { print 'checked="checked"'; } ?> /> Must have a Mailable Address</td></tr>
				<tr><td><input type="checkbox" name="with_phone_only" value="1" <?php if ($_REQUEST['with_phone_only']) { print 'checked="checked"'; } ?> /> Must have a Phone</td></tr>
				<tr><td><input type="checkbox" name="with_email_only" value="1" <?php if ($_REQUEST['with_email_only']) { print 'checked="checked"'; } ?> /> Must have an Email</td></tr>
			</table>
		</td>
	</tr>
	<tr>
		<td style="font-weight: normal; white-space: normal;">
			<a href="javascript: void(0);" onClick="expandCollapse('areaNames');">Search by Specific Areas</a>
<?php
if (preg_match("/\w/", $_REQUEST['string_area_names'])) {
?>
			<div name="areaNames" id="areaNames">
<?php
}
else {
?>
			<div name="areaNames" id="areaNames" style="display: none;">
<?php
}
?>
				<input type="checkbox" onclick="checkAllUS(this);"/>Check All US<br/>
				&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" onclick="checkAllCST(this);"/>Check All CST<br/>
				&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" onclick="checkAllEST(this);"/>Check All EST<br/>
				&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" onclick="checkAllMST(this);"/>Check All MST<br/>
				&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" onclick="checkAllPST(this);"/>Check All PST<br/>
				<input type="checkbox" onclick="checkAllCA(this);"/>Check All CA
				<table border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td valign="top">
<?php
$column_count = 0;
foreach ($o_area_name_utilities->get_list_area_states() as $area_state) {
?>
							<fieldset>
								<legend><?= $area_state ?></legend>
								<table border="0" cellspacing="0" cellpadding="0">
									<tr><td><input type="checkbox" onclick="checkAll(this, '<?= $area_state ?>_');"/>Check All</td></tr>
									<tr>
<?php
	foreach ($o_area_name_utilities->get_list_area_titles( array('area_state'=>$area_state) ) as $area_title) {
		$o_area_name = new area_name( array('area_state'=>$area_state, 'area_title'=>$area_title) );
		if (preg_match("|," . preg_replace("/[\(\)]/", "", $o_area_name->get_area_name()) . ",|", preg_replace("/[\(\)]/", "", $_REQUEST['string_area_names']))) {
?>
										<td style="font-weight: bold;"><input type="checkbox" name="area_name_checkbox[]" id="<?= $area_state ?>_checkbox" value="<?= $o_area_name->get_area_name() ?>" checked="checked" /><?= $area_title ?></td>
<?php
		}
		else {
?>
										<td><input type="checkbox" name="area_name_checkbox[]" id="<?= $area_state ?>_checkbox" value="<?= $o_area_name->get_area_name() ?>" /><?= $area_title ?></td>
<?php
		}
		if ($count == 0) {
			$count = 0;
			print '</tr><tr>';
		}
		else {
			$count++;
		}
	}
?>
									</tr>
								</table>
							</fieldset>
						</td>
<?php
	if ($column_count > 0) {
		$column_count = 0;
?>
					</tr>
					<tr>	
						<td valign="top">
<?php
	}
	else {
		$column_count++;
?>
						<td valign="top">
<?php
	}
}
?>
					</tr>
				</table>
			</div>
		</td>
	</tr>
</table>

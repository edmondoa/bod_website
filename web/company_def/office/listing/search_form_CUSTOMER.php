<script type="text/javascript">
	function setDates() {
		document.getElementById("start_month").value = '<?= date('m') ?>';
		document.getElementById("start_day").value = '<?= date('d') ?>';
		document.getElementById("start_year").value = '<?= date('Y') ?>';
		document.getElementById("end_month").value = '<?= date('m') ?>';
		document.getElementById("end_day").value = '<?= date('d') ?>';
		document.getElementById("end_year").value = '<?= date('Y') ?>';
	}
</script>
<table border="0" cellpadding="2" cellspacing="0" width="100%">
	<tr>
		<td>
			<table border="0" cellpadding="2" cellspacing="0" width="50%">
				<tr>
					<td>
						From Date:
						<select id="start_month" name="start_month">
							<option value="">Month</option>
							<?= select_options( array(type=>'short_month', selected=>$_REQUEST['start_month']) ) ?>
						</select>
						<select id="start_day" name="start_day">
							<option value="">Day</option>
							<?= select_options( array(type=>'day', selected=>$_REQUEST['start_day']) ) ?>
						</select>
						<select id="start_year" name="start_year">
							<option value="">Year</option>
							<?= select_options( array(type=>'number', min=>'2000', max=>'2025', selected=>$_REQUEST['start_year']) ) ?>
						</select>
					</td>
					<td>
						To Date:
						<select id="end_month" name="end_month">
							<option value="">Month</option>
							<?= select_options( array(type=>'short_month', selected=>$_REQUEST['end_month']) ) ?>
						</select>
						<select id="end_day" name="end_day">
							<option value="">Day</option>
							<?= select_options( array(type=>'day', selected=>$_REQUEST['end_day']) ) ?>
						</select>
						<select id="end_year" name="end_year">
							<option value="">Year</option>
							<?= select_options( array(type=>'number', min=>'2000', max=>'2025', selected=>$_REQUEST['end_year']) ) ?>
						</select>
					</td>
					<td><a href="#" onClick="setDates();">today</a></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" cellpadding="2" cellspacing="0" width="100%">
				<tr>
					<td>Bed:</td>
					<td>
						<select name="bedrooms">
							<option value="">Any</option>
							<?= select_options( array('type'=>'number', 'max'=>5, 'plus_sign'=>TRUE, 'selected'=>$_REQUEST['bedrooms']) ) ?>
						</select>
					</td>
					<td>Bath:</td>
					<td>
						<select name="bathrooms">
							<option value="">Any</option>
							<?= select_options( array('type'=>'number', 'max'=>5, 'plus_sign'=>TRUE, 'selected'=>$_REQUEST['bathrooms']) ) ?>
						</select>
					</td>
					<td>Square Feet:</td>
					<td>
						<select name="square_feet">
							<option value="">Any</option>
							<?= select_options( array('type'=>'number', 'min'=>500, 'max'=>3000, 'increment'=>250, 'plus_sign'=>TRUE, 'selected'=>$_REQUEST['bathrooms']) ) ?>
						</select>
					</td>
					<td>Garage:</td>
					<td>
						<select name="garage">
							<option value="">Any</option>
							<?= select_options( array('type'=>'number', 'max'=>5, 'plus_sign'=>TRUE, 'selected'=>$_REQUEST['garage']) ) ?>
						</select>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" cellpadding="2" cellspacing="0" width="100%">
				<tr>
					<td>Year Built</td>
					<td>
						<select name="year_built">
							<option value="">Any</option>
							<?= select_options( array('type'=>'number', 'min'=>1950, 'max'=>date('Y'), 'increment'=>5, 'plus_sign'=>TRUE, 'selected'=>$_REQUEST['year_built']) ) ?>
						</select>
					</td>
					<td>Lot Size:</td>
					<td>
						<select name="lot_size">
							<option value="">Any</option>
							<?= select_options( array('type'=>'number', 'min'=>'0.25', 'max'=>5, 'increment'=>'0.25', 'plus_sign'=>TRUE, 'selected'=>$_REQUEST['lot_size']) ) ?>
						</select>
					</td>
				</tr>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" cellpadding="2" cellspacing="0" width="100%">
				<tr>
					<td>Keyword</td>
					<td><input type="text" name="description_ad" value="<?= $_REQUEST['description_ad'] ?>" size="25" /></td>
					<td>Price Range:</td>
					<td>
						<select name="price">
							<option value="">Price</option>
							<?= select_options( array(type=>'price', selected=>$_REQUEST['price']) ) ?>
						</select>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" cellpadding="2" cellspacing="0">
				<tr>
					<td>Contact Phone:</td>
					<td><font style="font-size: 9px;">Use format (XXX-XXX-XXXX, e.g. 303-555-1111)</font><br/><input type="text" name="contact_phone" value="<?= $_REQUEST['contact_phone'] ?>" /></td>
					<td>Lead ID:</td>
					<td><input type="text" name="lead_id" value="<?= $_REQUEST['lead_id'] ?>" /></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" cellpadding="2" cellspacing="0" width="100%">
				<tr><td><input type="checkbox" name="no_dnc" value="1" <?php if ($_REQUEST['no_dnc']) { print 'checked="checked"'; } ?> /> Is NOT listed with the "U.S. Do Not Call Registry"</td></tr>
				<tr><td><input type="checkbox" name="with_address_only" value="1" <?php if ($_REQUEST['with_address_only']) { print 'checked="checked"'; } ?> /> Must have a Mailable Address</td></tr>
				<tr><td><input type="checkbox" name="with_phone_only" value="1" <?php if ($_REQUEST['with_phone_only']) { print 'checked="checked"'; } ?> /> Must have a Phone</td></tr>
				<tr><td><input type="checkbox" name="with_email_only" value="1" <?php if ($_REQUEST['with_email_only']) { print 'checked="checked"'; } ?> /> Must have an Email</td></tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" cellpadding="2" cellspacing="0" width="100%">
				<tr>
					<td>
						<br/>
						<span style="font: bold 12pt Arial;">If you would like to LIMIT your search to a specific area(s),<br/>then check the corresponding area(s) below:</span><br/>
<?php
foreach ($_SESSION['office']->account->get_list_area_names() as $area_name) {
	$escaped_area_name = preg_replace("/\//", "\\/", $area_name);
?>
						<input type="checkbox" name="area_name_checkbox[]" value="<?= $area_name ?>" <?php if (preg_match("/," . $escaped_area_name . ",/", $_REQUEST['string_area_names'])) { print 'checked="checked"'; } ?> /><?= $area_name ?><br/>
<?php
}
?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

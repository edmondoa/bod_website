<?php
require_once('office/account/class.area_name.php');
$o_area_name_utilities = new area_name_utilities();
?>
<table border="0" cellpadding="2" cellspacing="0" width="100%">
	<tr>
		<td>
			<table border="0" cellpadding="2" cellspacing="0" width="100%">
				<tr>
					<td>
						From Date:
						<select name="start_month">
							<option value="">Month</option>
							<?= select_options( array(type=>'short_month', selected=>$_REQUEST['start_month']) ) ?>
						</select>
						<select name="start_day">
							<option value="">Day</option>
							<?= select_options( array(type=>'day', selected=>$_REQUEST['start_day']) ) ?>
						</select>
						<select name="start_year">
							<option value="">Year</option>
							<?= select_options( array(type=>'number', min=>'2000', max=>'2025', selected=>$_REQUEST['start_year']) ) ?>
						</select>
					</td>
					<td>
						To Date:
						<select name="end_month">
							<option value="">Month</option>
							<?= select_options( array(type=>'short_month', selected=>$_REQUEST['end_month']) ) ?>
						</select>
						<select name="end_day">
							<option value="">Day</option>
							<?= select_options( array(type=>'day', selected=>$_REQUEST['end_day']) ) ?>
						</select>
						<select name="end_year">
							<option value="">Year</option>
							<?= select_options( array(type=>'number', min=>'2000', max=>'2025', selected=>$_REQUEST['end_year']) ) ?>
						</select>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" cellpadding="2" cellspacing="0" width="100%">
				<tr>
					<td>Bed:</td>
					<td>
						<select name="bedrooms">
							<option value="">Any</option>
							<?= select_options( array('type'=>'number', 'max'=>5, 'plus_sign'=>TRUE, 'selected'=>$_REQUEST['bedrooms']) ) ?>
						</select>
					</td>
					<td>Bath:</td>
					<td>
						<select name="bathrooms">
							<option value="">Any</option>
							<?= select_options( array('type'=>'number', 'max'=>5, 'plus_sign'=>TRUE, 'selected'=>$_REQUEST['bathrooms']) ) ?>
						</select>
					</td>
					<td>Square Feet:</td>
					<td>
						<select name="square_feet">
							<option value="">Any</option>
							<?= select_options( array('type'=>'number', 'min'=>500, 'max'=>3000, 'increment'=>250, 'plus_sign'=>TRUE, 'selected'=>$_REQUEST['bathrooms']) ) ?>
						</select>
					</td>
					<td>Garage:</td>
					<td>
						<select name="garage">
							<option value="">Any</option>
							<?= select_options( array('type'=>'number', 'max'=>5, 'plus_sign'=>TRUE, 'selected'=>$_REQUEST['garage']) ) ?>
						</select>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" cellpadding="2" cellspacing="0" width="100%">
				<tr>
					<td>Property Type:</td>
					<td><input type="text" name="property_type" value="<?= $_REQUEST['property_type'] ?>" size="10"/></td>
					<td>Year Built</td>
					<td>
						<select name="year_built">
							<option value="">Any</option>
							<?= select_options( array('type'=>'number', 'min'=>1950, 'max'=>date('Y'), 'increment'=>5, 'plus_sign'=>TRUE, 'selected'=>$_REQUEST['year_built']) ) ?>
						</select>
					</td>
					<td>Lot Size:</td>
					<td>
						<select name="lot_size">
							<option value="">Any</option>
							<?= select_options( array('type'=>'number', 'min'=>'0.25', 'max'=>5, 'increment'=>'0.25', 'plus_sign'=>TRUE, 'selected'=>$_REQUEST['lot_size']) ) ?>
						</select>
					</td>
				</tr>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" cellpadding="2" cellspacing="0" width="100%">
				<tr>
					<td>Keyword</td>
					<td><input type="text" name="description_ad" value="<?= $_REQUEST['description_ad'] ?>" size="25" /></td>
					<td>Price Range:</td>
					<td>
						<select name="price">
							<option value="">Price</option>
							<?= select_options( array(type=>'price', selected=>$_REQUEST['price']) ) ?>
						</select>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td style="font-weight: normal; white-space: normal;">
			<a href="javascript: void(0);" onClick="expandCollapse('areaNames');">Search by Specific Areas</a>
<?php
if (preg_match("/\w/", $_REQUEST['string_area_names'])) {
?>
			<div name="areaNames" id="areaNames">
<?php
}
else {
?>
			<div name="areaNames" id="areaNames" style="display: none;">
<?php
}
foreach ($o_area_name_utilities->get_list_area_states() as $area_state) {
?>
				<fieldset>
					<legend><?= $area_state ?></legend>
					<table border="0" cellspacing="0" cellpadding="0">
						<tr><td><input type="checkbox" onclick="checkAll(this, '<?= $area_state ?>_');"/>Check All</td></tr>
						<tr>
<?php
	$count = 0;
	foreach ($o_area_name_utilities->get_list_area_titles( array('area_state'=>$area_state) ) as $area_title) {
		$o_area_name = new area_name( array('area_state'=>$area_state, 'area_title'=>$area_title) );
		if (preg_match("|," . preg_replace("/[\(\)]/", "", $o_area_name->get_area_name()) . ",|", preg_replace("/[\(\)]/", "", $_REQUEST['string_area_names']))) {
?>
							<td style="font-weight: bold;"><input type="checkbox" name="area_name_checkbox[]" id="<?= $area_state ?>_checkbox" value="<?= $o_area_name->get_area_name() ?>" checked="checked" /><?= $area_title ?></td>
<?php
		}
		else {
?>
							<td><input type="checkbox" name="area_name_checkbox[]" id="<?= $area_state ?>_checkbox" value="<?= $o_area_name->get_area_name() ?>" /><?= $area_title ?></td>
<?php
		}
		if ($count == 0) {
			$count = 0;
			print '</tr><tr>';
		}
		else {
			$count++;
		}
	}
?>
					</tr>
				</table>
				</fieldset>
<?php
}
?>
			</div>
		</td>
	</tr>
</table>

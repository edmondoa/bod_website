<?php
if ($_SESSION['office']->account->get_account_type() != 'ADMIN' && $_SESSION['office']->account->get_account_type() != 'BILLING' && $_SESSION['office']->account->get_account_type() != 'PRIVATE_LABEL') {
	$_SESSION['web_interface']->destroySession();
	exit;
}
$_PAGE_TITLE = 'Upload Listings';

require_once('office/account/class.account.php');
try {
	$o_account = new account( array('accountid'=>$_REQUEST['accountid']) );
	if (!$o_account->isOwner()) {
		$_SESSION['web_interface']->destroySession();
		exit;
	}
}
catch (Exception $exception) {
	throw $exception;
}

require_once('office/listing/class.listing.php');
$o_listing_utilities = new listing_utilities();
$most_recent_listing = $o_listing_utilities->get_most_recent_listing();

$_REQUEST['start_month'] = date('m', strtotime($most_recent_listing));
$_REQUEST['start_day'] = date('d', strtotime($most_recent_listing));
$_REQUEST['start_year'] = date('Y', strtotime($most_recent_listing));
$_REQUEST['end_month'] = date('m', strtotime($most_recent_listing));
$_REQUEST['end_day'] = date('d', strtotime($most_recent_listing));
$_REQUEST['end_year'] = date('Y', strtotime($most_recent_listing));

require_once($_SESSION['web_interface']->get_server_path('other/misc/select_options.php'));
include_once($_SESSION['web_interface']->get_server_path('office/global/top.php'));
?>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td class="bodyNav" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('office/listing/left_nav_' . $_SESSION['office']->account->get_account_type() . '.php')); ?>
		</td>
		<td class="bodyMain" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('other/misc/status_error.php')); ?>
			<h1>Send Listings</h1>
			Send listings for the below dates to: <?= $o_account->get_first_name() ?> <?= $o_account->get_last_name() ?>.<br/>
			<p style="font-weight: bold">The most recent listing is from: <?= date('F j, Y', strtotime($most_recent_listing)) ?></p>
			<form method="post" action="/office/listing/send_listings_confirm.php">
			<input type="hidden" name="accountid" value="<?= $o_account->get_accountid() ?>" />
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td align="right">From Date: </td>
					<td>
						<select name="start_month">
							<?= select_options( array(type=>'short_month', selected=>$_REQUEST['start_month']) ) ?>
						</select>
						<select name="start_day">
							<?= select_options( array(type=>'day', selected=>$_REQUEST['start_day']) ) ?>
						</select>
						<select name="start_year">
							<?= select_options( array(type=>'past_year', selected=>$_REQUEST['start_year']) ) ?>
						</select>
					</td>
				</tr>
				<tr>
					<td align="right">To Date: </td>
					<td>
						<select name="end_month">
							<?= select_options( array(type=>'short_month', selected=>$_REQUEST['end_month']) ) ?>
						</select>
						<select name="end_day">
							<?= select_options( array(type=>'day', selected=>$_REQUEST['end_day']) ) ?>
						</select>
						<select name="end_year">
							<?= select_options( array(type=>'past_year', selected=>$_REQUEST['end_year']) ) ?>
						</select>
					</td>
				</tr>
				<tr><td colspan="2"><input type="submit" value="Continue >>>" /></td></tr>
			</table>
			</form>
		</td>
	</tr>
</table>
<?php
include_once($_SESSION['web_interface']->get_server_path('office/global/bottom.php'));
?>	
<?php
if ($_SESSION['office']->account->get_account_type() != 'ADMIN' && $_SESSION['office']->account->get_account_type() != 'PRIVATE_LABEL' && $_SESSION['office']->account->get_account_type() != 'SUPPORT' && $_SESSION['office']->account->get_account_type() != 'UPLOAD') {
	$_SESSION['web_interface']->destroySession();
	exit;
}
$_PAGE_TITLE = 'Upload Listings';

require_once('office/account/class.account.php');
try {
	$o_account = new account( array('accountid'=>$_REQUEST['accountid']) );
	if (!$o_account->isOwner()) {
		$_SESSION['web_interface']->destroySession();
		exit;
	}
}
catch (Exception $exception) {
	throw $exception;
}

require_once('office/account/class.area_name.php');
$o_area_name_utilities = new area_name_utilities();

require_once('office/account/class.Product.php');
$ProductUtilities = new ProductUtilities();

require_once('office/listing/class.listing.php');
$o_listing_utilities = new listing_utilities();
if ($o_account->get_accountid()) {
	$account_search_fields = $o_account->get_hash_search_fields();
	foreach ($account_search_fields as $key=>$value) {
		$_REQUEST[$key] = $value;
	}
	
	$robot_names = explode(",", $o_account->get_string_robot_names());
	$string_robot_names = '';
	foreach ($robot_names as $robot_name) {
		if ($robot_name) {
			$string_robot_names .= "'" . $_SESSION['data_access']->db_quote($robot_name) . "',";
		}
	}
	$string_robot_names = preg_replace("/,$/", "", $string_robot_names);
	if ($string_robot_names) {
		$robot_where_plus = " AND robot_name NOT IN (" . $string_robot_names . ") ";
	}
	
	$area_names = $o_account->get_list_area_names();
	$string_area_names = '';
	foreach ($area_names as $area_name) {
		if ($area_name) {
			$string_area_names .= "'" . $_SESSION['data_access']->db_quote($area_name) . "',";
		}
	}
	$string_area_names = preg_replace("/,$/", "", $string_area_names);
	
	// only search for products they are signed up for
	$where_plus = " AND product IN ('',";
	foreach ($o_account->getArrayProducts() as $productId=>$name) {
		$where_plus .= "'" . $_SESSION['data_access']->db_quote($name) . "',";
	}
	$where_plus = preg_replace("/,$/", ") ", $where_plus);
	
	// only search for property_types they want
	$array_account_property_type_fields = $o_account->get_array_property_type_fields();
	if (count($array_account_property_type_fields) > 0) {
		$where_plus .= " AND property_type IN (";
		foreach ($array_account_property_type_fields as $propertyType=>$boolean) {
			$where_plus .= "'" . $_SESSION['data_access']->db_quote($propertyType) . "',";
		}
		$where_plus = preg_replace("/,$/", ") ", $where_plus);
	}
		
	if ($string_area_names) {
		$_REQUEST['where_plus'] = " AND area_name IN (" . $string_area_names . ") " . $robot_where_plus . $where_plus;
		$list_listing = $o_listing_utilities->get_list($_REQUEST);
	}
	else {
		$list_listing = array();
	}
}
else {
	$list_listing = $o_listing_utilities->get_list( array('where_plus'=>" AND (processed='' OR processed IS NULL OR processed='0000-00-00') ") );
}

// put together the current count of undelivered stuff in queue
$o_area_name_utilities = new area_name_utilities();
$hashStateTimeZone = $o_area_name_utilities->get_list_state_time_zone();
$queueHash = array();
if (count($list_listing) > 0) {
	foreach ($list_listing as $Listing) {
		if (!$hashStateTimeZone[$Listing->get_state()]) {
			continue;
		}
		$queueHash[$Listing->get_product()][$hashStateTimeZone[$Listing->get_state()]]++;
	}
}

include_once($_SESSION['web_interface']->get_server_path('office/global/top.php'));
?>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td class="bodyNav" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('office/listing/left_nav_' . $_SESSION['office']->account->get_account_type() . '.php')); ?>
		</td>
		<td class="bodyMain" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('other/misc/status_error.php')); ?>
			<form name="listingForm" id="listingForm" method="post" action="/office/listing/handle.php">
			<input type="hidden" name="cmd" value="send_listings" />
			<input type="hidden" name="accountid" value="<?= $o_account->get_accountid() ?>" />
			<input type="hidden" name="start_date" value="<?= $_REQUEST['start_year'] ?>-<?= $_REQUEST['start_month'] ?>-<?= $_REQUEST['start_day'] ?>" />
			<input type="hidden" name="end_date" value="<?= $_REQUEST['end_year'] ?>-<?= $_REQUEST['end_month'] ?>-<?= $_REQUEST['end_day'] ?>" />
			<h1>Send Listings</h1>
<?php
if ($o_account->get_accountid()) {
?>
			Send the following <strong><?= count($list_listing) ?></strong> listings to: <a href="/office/account/account.php?accountid=<?= $o_account->get_accountid() ?>"><?= $o_account->get_first_name() ?> <?= $o_account->get_last_name() ?></a>.
<?php
}
else if (!$_SESSION['office']->account->is_upload()) {
?>
			Send the applicable listings below to:<br/>
			<br/>
			Time Zone: 
			<select name="time_zone" <?= $_SESSION['web_interface']->missing_required_input('time_zone') ?>>
				<option value="">&nbsp;</option>
<?php
	foreach ($o_area_name_utilities->get_time_zones() as $time_zone) {
?>
				<option value="<?= $time_zone ?>"><?= $time_zone ?></option>
<?php
	}
?>
			</select><br/>
			Product: 
			<select name="product" <?= $_SESSION['web_interface']->missing_required_input('product') ?>>
				<option value="">All</option>
<?php
	foreach ($ProductUtilities->getList() as $Product) {
?>
				<option value="<?= $Product->getName() ?>"><?= $Product->getTitle() ?> (<?= $Product->getName() ?>)</option>
<?php
	}
?>
			</select>
			<br/>
			<input type="checkbox" name="noEmptyEmails" value="1" /> <span style="color: #FF0000; font-weight: bold;">If you do NOT want to send emails with no listings please check this box.<br/>This means only those people with listings will get an email.</span>
			<br/>
			<br/>
<?php
	if (count($queueHash) > 0) {
?>
			<span style="font-weight: bold;">Current Undelivered Queue Listing Count</span>
			<table cellpadding="5" cellspacing="5" border="0">
<?php
		foreach ($queueHash AS $product=>$productHash) {
?>
				<tr>
					<td style="font-weight: bold;"><?= $product ?></td>
<?php
			if (count($productHash) > 0) {
				foreach ($productHash as $timeZone=>$timeZoneCount) {
?>
					<td><?= $timeZone ?>: <?= $timeZoneCount ?></td>
<?php
				}
			}
?>
				</tr>
<?php
		}
?>
			</table>
<?php
	}
} // end else if (!$_SESSION['office']->account->is_upload()) {
if (empty($list_listing)) {
?>
			<br/><br/><strong>There are no listings to send.<br/><a href="/office/listing/send_listings.php?accountid=<?= $o_account->get_accountid() ?>">Please try again</a>.</strong>
<?php
}
else {	
?>
			<div class="boxHeader">
				<table cellpadding="0" cellspacing="0" border="0">
					<colgroup>
						<col width="25"/>
						<col width="220"/>
						<col width="275"/>
						<col width="100"/>
						<col width="75"/>
					</colgroup>
					<tbody>
					<tr class="head">
						<th>&nbsp;</th>
						<th align="center">Area Name</th>
						<th>Description</th>
						<th align="center">Date Posted</th>
						<th align="center">Product</th>
					</tr>
					</tbody>
				</table>
			</div>
			<div class="boxScroll">
				<table cellpadding="0" cellspacing="0" border="0">
					<colgroup>
						<col width="25"/>
						<col width="220"/>
						<col width="275"/>
						<col width="100"/>
						<col width="75"/>
					</colgroup>
					<tbody>
<?php					
	foreach ($list_listing as $item_listing) {
?>
					<tr class="row<?= $_SESSION['web_interface']->get_row_class($row_num) ?>">
						<td align="center"><a href="/office/listing/handle.php?cmd=delete&page=send_listings_confirm&listingid=<?= $item_listing->get_listingid() ?>"><img src="<?= $_SESSION['web_interface']->get_path('img/deleteBtn.gif') ?>" border="0" /></a></td>
						<td align="center"><?= $item_listing->get_area_name() ?></td>
						<td><a href="/office/listing/view.php?listingid=<?= $item_listing->get_listingid() ?>"><?= substr($item_listing->get_description_ad(), 0, 30) ?>...</a></td>
						<td align="center"><?= date('M j, y', strtotime($item_listing->get_date_posted())) ?></td>
						<td align="center"><?= $item_listing->get_product() ?></td>
					</tr>
<?php
	}
?>
					</tbody>
				</table>
			</div>
<?php
	if (!$_SESSION['office']->account->is_upload()) {
?>
			<input type="button" value="Send" onClick="if (confirm('ARE YOU SURE YOU WANT TO SEND THE ABOVE LISTINGS?')) { document.getElementById('listingForm').submit(); }" />
<?php
	}
}
?>
		</td>
	</tr>
</table>
<?php
include_once($_SESSION['web_interface']->get_server_path('office/global/bottom.php'));
?>	

<?php
$_PAGE_TITLE = 'Upload Listings';

include_once($_SESSION['web_interface']->get_server_path('office/global/top.php'));
?>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td class="bodyNav" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('office/listing/left_nav_' . $_SESSION['office']->account->get_account_type() . '.php')); ?>
		</td>
		<td class="bodyMain" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('other/misc/status_error.php')); ?>
			<h1>Listings Sent</h1>
			Your listings have been sent.
<?php
if ($_REQUEST['accountid']) {
?>
			<br/><a href="/office/account/account.php?accountid=<?= $_REQUEST['accountid'] ?>">Return to account.</a>
<?php
}
?>
		</td>
	</tr>
</table>
<?php
include_once($_SESSION['web_interface']->get_server_path('office/global/bottom.php'));
?>	
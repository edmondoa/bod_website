<?php
if ($_SESSION['office']->account->get_account_type() != 'ADMIN' && $_SESSION['office']->account->get_account_type() != 'SUPPORT') {
	$_SESSION['web_interface']->destroySession();
	exit;
}
$_PAGE_TITLE = 'Upload Listings';

require_once('office/listing/class.listing.php');
$o_listing_utilities = new listing_utilities();
$list_listing = $o_listing_utilities->get_list( array('where_plus'=>" AND processed=NOW() ") );

include_once($_SESSION['web_interface']->get_server_path('office/global/top.php'));
?>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td class="bodyNav" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('office/listing/left_nav_' . $_SESSION['office']->account->get_account_type() . '.php')); ?>
		</td>
		<td class="bodyMain" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('other/misc/status_error.php')); ?>
			<h1>Sent Listings</h1>
			The following listings were sent today.
<?php
if (empty($list_listing)) {
?>
			<br/><br/><strong>There were no listings sent today.</strong>
<?php
}
else {
?>
			<div class="boxHeader">
				<table cellpadding="0" cellspacing="0" border="0">
					<colgroup>
						<col width="220"/>
						<col width="320"/>
					</colgroup>
					<tbody>
					<tr class="head">
						<th align="center">Area Name</th>
						<th>Description</th>
					</tr>
					</tbody>
				</table>
			</div>
			<div class="boxScroll">
				<table cellpadding="0" cellspacing="0" border="0">
					<colgroup>
						<col width="220"/>
						<col width="320"/>
					</colgroup>
					<tbody>
<?php					
	foreach ($list_listing as $item_listing) {
?>
					<tr class="row<?= $_SESSION['web_interface']->get_row_class($row_num) ?>">
						<td align="center"><?= $item_listing->get_area_name() ?></td>
						<td><a href="/office/listing/view.php?listingid=<?= $item_listing->get_listingid() ?>"><?= substr($item_listing->get_description_ad(), 0, 35) ?>...</a></td>
					</tr>
<?php
	}
?>
					</tbody>
				</table>
			</div>
<?php
}
?>
		</td>
	</tr>
</table>
<?php
include_once($_SESSION['web_interface']->get_server_path('office/global/bottom.php'));
?>	
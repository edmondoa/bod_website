<?php
if ($_SESSION['office']->account->get_account_type() != 'ADMIN' && $_SESSION['office']->account->get_account_type() != 'SUPPORT' && $_SESSION['office']->account->get_account_type() != 'UPLOAD') {
	$_SESSION['web_interface']->destroySession();
	exit;
}
$_PAGE_TITLE = 'Upload Listings';

require_once('office/listing/class.listing.php');
$o_listing_utilities = new listing_utilities();

include_once($_SESSION['web_interface']->get_server_path('office/global/top.php'));
?>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td class="bodyNav" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('office/listing/left_nav_' . $_SESSION['office']->account->get_account_type() . '.php')); ?>
		</td>
		<td class="bodyMain" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('other/misc/status_error.php')); ?>
			<h1>Upload Listings</h1>
			<p class="headerText">Upload your new listings here.</p>
			<p>The fields should be in the following order and should be comma-delimited:<br/>firstextractiondate, refindkey, refind_value, robot_name, robot_nametemp, date_posted, data_source_name, data_source_namepublish, data_source_type, default_area_code, area_county, logo, logo_url, logo_classification, pt_review, property_type, fsbo_classification, keyword_review, area_st, area_name, area_misc, scrub_edit, description_ad, ad_url, contact_email, contact_phone1, phone1_lookupsource, scrub_edit1, contact_name1, contact_street1, contact_city1, contact_st1, contact_zip1, contact_biz, contact_bizname, scrub_edit2, contact_phone2, phone2_lookupsource, contact_name2, contact_street2, contact_city2, contact_st2, contact_zip2, property_street, property_street2, property_city, property_st, property_zip, zip_confidence, zip_approximate, latitude, longitude, map_url, square_feet, bedrooms, bathrooms, lot_size, year_built, garage, price, phone1_dnc, phone1_prefixfinder, phone1_pf_areacode, phone1_pf_prefix, phone1_pf_city, phone1_pf_st, phone1_pf_county, phone1_pf_county2, phone1_pf_county3, phone1_pf_county4, phone1_pf_msa, phone2_dnc, phone2_prefixfinder, phone2_pf_areacode, phone2_pf_prefix, phone2_pf_city, phone2_pf_st, phone2_pf_county, phone2_pf_county2, phone2_pf_county3, phone2_pf_county4, phone2_pf_msa, dup_phoneinfo, misc1, processed, lead_id, language, translate, date_ad, product, status</p>
<?php
if ($o_listing_utilities->get_most_recent_listing()) {
?>
			<p style="font-weight: bold">
				The most recent listing is from: <?= date('F j, Y', strtotime($o_listing_utilities->get_most_recent_listing())) ?>
<?php
	for ($count = 1; $count <= 10; $count++) {
		if ($_FILES['file' . $count]['name']) {
?>
				<br/>Your latest file you uploaded was: <?= $_FILES['file' . $count]['name'] ?>
<?php
		}
	}
?> 
			</p>
<?php
}
?>
			<form method="post" action="/office/listing/handle.php" enctype="multipart/form-data">
			<input type="hidden" name="cmd" value="upload" />
<?php
for ($count = 1; $count <= 10; $count++) {
?>
			<fieldset>
				<label for="file<?= $count ?>" <?= $_SESSION['web_interface']->missing_required_label('file' . $count) ?>>File <?= $count ?>:</label>
				<input type="file" name="file<?= $count ?>" />
			</fieldset>
<?php
}
?>
			<fieldset>
				<input type="submit" value="Upload" />
			</fieldset>
			</form>
		</td>
	</tr>
</table>
<?php
include_once($_SESSION['web_interface']->get_server_path('office/global/bottom.php'));
?>
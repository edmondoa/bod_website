<?php
if ($_SESSION['office']->account->get_account_type() != 'ADMIN' && $_SESSION['office']->account->get_account_type() != 'SUPPORT' && $_SESSION['office']->account->get_account_type() != 'UPLOAD') {
	$_SESSION['web_interface']->destroySession();
	exit;
}
$_PAGE_TITLE = 'Upload Listings';

require_once('office/listing/class.listing.php');
$o_listing_utilities = new listing_utilities();

include_once($_SESSION['web_interface']->get_server_path('office/global/top.php'));
?>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td class="bodyNav" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('office/listing/left_nav_' . $_SESSION['office']->account->get_account_type() . '.php')); ?>
		</td>
		<td class="bodyMain" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('other/misc/status_error.php')); ?>
			<h1>Upload File</h1>
			<p class="headerText">Upload your new file here.</p>
			<form method="post" action="/office/listing/handle.php" enctype="multipart/form-data">
			<input type="hidden" name="button" value="<?= $_REQUEST['button'] ?>" />
			<input type="hidden" name="cmd" value="<?= $_REQUEST['cmd'] ?>" />
			<fieldset>
				<label for="file" <?= $_SESSION['web_interface']->missing_required_label('file') ?>>File:</label>
				<input type="file" name="file" />
			</fieldset>
			<fieldset>
				<input type="submit" value="<?= $_REQUEST['button'] ?>" />
			</fieldset>
			</form>
		</td>
	</tr>
</table>
<?php
include_once($_SESSION['web_interface']->get_server_path('office/global/bottom.php'));
?>
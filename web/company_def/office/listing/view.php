<?php
$_PAGE_TITLE = 'Listings';

require_once('office/listing/class.listing.php');
try {
	$o_listing = new listing( array('listingid'=>$_REQUEST['listingid']) );
	$previous_listingid = $o_listing->get_next_or_previous_listingid( array('position'=>$_REQUEST['position'], 'query'=>$_SESSION['listing_search_query'], 'type'=>'previous') );
	$next_listingid = $o_listing->get_next_or_previous_listingid( array('position'=>$_REQUEST['position'], 'query'=>$_SESSION['listing_search_query'], 'type'=>'next') );
}
catch (Exception $exception) {
 throw $exception;
}

include_once($_SESSION['web_interface']->get_server_path('office/global/top.php'));
?>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td class="bodyNav" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('office/listing/left_nav_' . $_SESSION['office']->account->get_account_type() . '.php')); ?>
		</td>
		<td class="bodyMain" valign="top">
			<h1 style="text-align: center;">
				<?php if ($next_listingid) { ?><a href="/office/listing/view.php?listingid=<?= $next_listingid ?>&position=<?= $_REQUEST['position']+1 ?>" style="float: right;">Next Listing</a> <?php } ?>
				<?php if ($previous_listingid) { ?><a href="/office/listing/view.php?listingid=<?= $previous_listingid ?>&position=<?= $_REQUEST['position']-1 ?>">Previous Listing</a> <?php } ?>
				<?= $o_listing->get_area_name() ?> - <?= date('F j, Y', strtotime($o_listing->get_date_posted())) ?>
			</h1>
			<div class="displayInfo">
<?php
if ($_SESSION['office']->account->is_support() || $_SESSION['office']->account->is_developer() || $_SESSION['office']->account->is_admin()) {
?>
				<fieldset>
					<legend>Listing Information</legend>
					<table border="0" cellpadding="0" cellspacing="0">
						<tr><td class="left">listingid:</td><td class="right"><?= $o_listing->get_listingid() ?></td></tr>
						<tr><td class="left">lead_id:</td><td class="right"><?= $o_listing->get_lead_id() ?></td></tr>
						<tr><td class="left">firstextractiondate:</td><td class="right"><?= $o_listing->get_firstextractiondate() ?></td></tr>
						<tr><td class="left">refindkey:</td><td class="right"><?= $o_listing->get_refindkey() ?></td></tr>
						<tr><td class="left">refind_value:</td><td class="right"><?= $o_listing->get_refind_value() ?></td></tr>
						<tr><td class="left">robot_name:</td><td class="right"><?= $o_listing->get_robot_name() ?></td></tr>
						<tr><td class="left">robot_nametemp:</td><td class="right"><?= $o_listing->get_robot_nametemp() ?></td></tr>
						<tr><td class="left">date_posted:</td><td class="right"><?= $o_listing->get_date_posted() ?></td></tr>
						<tr><td class="left">data_source_name:</td><td class="right"><?= $o_listing->get_data_source_name() ?></td></tr>
						<tr><td class="left">data_source_namepublish:</td><td class="right"><?= $o_listing->get_data_source_namepublish() ?></td></tr>
						<tr><td class="left">data_source_type:</td><td class="right"><?= $o_listing->get_data_source_type() ?></td></tr>
						<tr><td class="left">default_area_code:</td><td class="right"><?= $o_listing->get_default_area_code() ?></td></tr>
						<tr><td class="left">area_county:</td><td class="right"><?= $o_listing->get_area_county() ?></td></tr>
						<tr><td class="left">logo:</td><td class="right"><?= $o_listing->get_logo() ?></td></tr>
						<tr><td class="left">logo_url:</td><td class="right"><?= $o_listing->get_logo_url() ?></td></tr>
						<tr><td class="left">logo_classification:</td><td class="right"><?= $o_listing->get_logo_classification() ?></td></tr>
						<tr><td class="left">pt_review:</td><td class="right"><?= $o_listing->get_pt_review() ?></td></tr>
						<tr><td class="left">property_type:</td><td class="right"><?= $o_listing->get_property_type() ?></td></tr>
						<tr><td class="left">fsbo_classification:</td><td class="right"><?= $o_listing->get_fsbo_classification() ?></td></tr>
						<tr><td class="left">keyword_review:</td><td class="right"><?= $o_listing->get_keyword_review() ?></td></tr>
						<tr><td class="left">area_st:</td><td class="right"><?= $o_listing->get_area_st() ?></td></tr>
						<tr><td class="left">area_name:</td><td class="right"><?= $o_listing->get_area_name() ?></td></tr>
						<tr><td class="left">area_misc:</td><td class="right"><?= $o_listing->get_area_misc() ?></td></tr>
						<tr><td class="left">scrub_edit:</td><td class="right"><?= $o_listing->get_scrub_edit() ?></td></tr>
						<tr><td class="left" valign="top">description_ad:</td><td class="right"><?= $o_listing->get_description_ad() ?></td></tr>
						<tr>
							<td class="left" valign="top">ad_url:</td>
							<td class="right">
<?php
$num = intval(strlen($o_listing->get_ad_url())/50);
$position = 0;
for($count = 0; $count <= $num; $count++) {
	print substr($o_listing->get_ad_url(), $position, 50) . '<br/>';
	$position += 50;
}
print substr($o_listing->get_ad_url(), $position, strlen($o_listing->get_ad_url()));
?>
							</td>
						</tr>
						<tr><td class="left">contact_email:</td><td class="right"><?= $o_listing->get_contact_email() ?></td></tr>
						<tr><td class="left">contact_phone1:</td><td class="right"><?= $o_listing->get_contact_phone1() ?></td></tr>
						<tr><td class="left">phone1_lookupsource:</td><td class="right"><?= $o_listing->get_phone1_lookupsource() ?></td></tr>
						<tr><td class="left">scrub_edit1:</td><td class="right"><?= $o_listing->get_scrub_edit1() ?></td></tr>
						<tr><td class="left">contact_name1:</td><td class="right"><?= $o_listing->get_contact_name1() ?></td></tr>
						<tr><td class="left">contact_street1:</td><td class="right"><?= $o_listing->get_contact_street1() ?></td></tr>
						<tr><td class="left">contact_city1:</td><td class="right"><?= $o_listing->get_contact_city1() ?></td></tr>
						<tr><td class="left">contact_st1:</td><td class="right"><?= $o_listing->get_contact_st1() ?></td></tr>
						<tr><td class="left">contact_zip1:</td><td class="right"><?= $o_listing->get_contact_zip1() ?></td></tr>
						<tr><td class="left">contact_biz:</td><td class="right"><?= $o_listing->get_contact_biz() ?></td></tr>
						<tr><td class="left">contact_bizname:</td><td class="right"><?= $o_listing->get_contact_bizname() ?></td></tr>
						<tr><td class="left">scrub_edit2:</td><td class="right"><?= $o_listing->get_scrub_edit2() ?></td></tr>
						<tr><td class="left">contact_phone2:</td><td class="right"><?= $o_listing->get_contact_phone2() ?></td></tr>
						<tr><td class="left">phone2_lookupsource:</td><td class="right"><?= $o_listing->get_phone2_lookupsource() ?></td></tr>
						<tr><td class="left">contact_name2:</td><td class="right"><?= $o_listing->get_contact_name2() ?></td></tr>
						<tr><td class="left">contact_street2:</td><td class="right"><?= $o_listing->get_contact_street2() ?></td></tr>
						<tr><td class="left">contact_city2:</td><td class="right"><?= $o_listing->get_contact_city2() ?></td></tr>
						<tr><td class="left">contact_st2:</td><td class="right"><?= $o_listing->get_contact_st2() ?></td></tr>
						<tr><td class="left">contact_zip2:</td><td class="right"><?= $o_listing->get_contact_zip2() ?></td></tr>
						<tr><td class="left">property_street:</td><td class="right"><?= $o_listing->get_property_street() ?></td></tr>
						<tr><td class="left">property_street2:</td><td class="right"><?= $o_listing->get_property_street2() ?></td></tr>
						<tr><td class="left">property_city:</td><td class="right"><?= $o_listing->get_property_city() ?></td></tr>
						<tr><td class="left">property_st:</td><td class="right"><?= $o_listing->get_property_st() ?></td></tr>
						<tr><td class="left">property_zip:</td><td class="right"><?= $o_listing->get_property_zip() ?></td></tr>
						<tr><td class="left">zip_confidence:</td><td class="right"><?= $o_listing->get_zip_confidence() ?></td></tr>
						<tr><td class="left">zip_approximate:</td><td class="right"><?= $o_listing->get_zip_approximate() ?></td></tr>
						<tr><td class="left">square_feet:</td><td class="right"><?= $o_listing->get_square_feet() ?></td></tr>
						<tr><td class="left">bedrooms:</td><td class="right"><?= $o_listing->get_bedrooms() ?></td></tr>
						<tr><td class="left">bathrooms:</td><td class="right"><?= $o_listing->get_bathrooms() ?></td></tr>
						<tr><td class="left">lot_size:</td><td class="right"><?= $o_listing->get_lot_size() ?></td></tr>
						<tr><td class="left">year_built:</td><td class="right"><?= $o_listing->get_year_built() ?></td></tr>
						<tr><td class="left">garage:</td><td class="right"><?= $o_listing->get_garage() ?></td></tr>
						<tr><td class="left">price:</td><td class="right"><?= $o_listing->get_price() ?></td></tr>
						<tr><td class="left">phone1_dnc:</td><td class="right"><?= $o_listing->get_phone1_dnc() ?></td></tr>
						<tr><td class="left">phone1_prefixfinder:</td><td class="right"><?= $o_listing->get_phone1_prefixfinder() ?></td></tr>
						<tr><td class="left">phone1_pf_areacode:</td><td class="right"><?= $o_listing->get_phone1_pf_areacode() ?></td></tr>
						<tr><td class="left">phone1_pf_prefix:</td><td class="right"><?= $o_listing->get_phone1_pf_prefix() ?></td></tr>
						<tr><td class="left">phone1_pf_city:</td><td class="right"><?= $o_listing->get_phone1_pf_city() ?></td></tr>
						<tr><td class="left">phone1_pf_st:</td><td class="right"><?= $o_listing->get_phone1_pf_st() ?></td></tr>
						<tr><td class="left">phone1_pf_county:</td><td class="right"><?= $o_listing->get_phone1_pf_county() ?></td></tr>
						<tr><td class="left">phone1_pf_county2:</td><td class="right"><?= $o_listing->get_phone1_pf_county2() ?></td></tr>
						<tr><td class="left">phone1_pf_county3:</td><td class="right"><?= $o_listing->get_phone1_pf_county3() ?></td></tr>
						<tr><td class="left">phone1_pf_county4:</td><td class="right"><?= $o_listing->get_phone1_pf_county4() ?></td></tr>
						<tr><td class="left">phone1_pf_msa:</td><td class="right"><?= $o_listing->get_phone1_pf_msa() ?></td></tr>
						<tr><td class="left">phone2_dnc:</td><td class="right"><?= $o_listing->get_phone2_dnc() ?></td></tr>
						<tr><td class="left">phone2_prefixfinder:</td><td class="right"><?= $o_listing->get_phone2_prefixfinder() ?></td></tr>
						<tr><td class="left">phone2_pf_areacode:</td><td class="right"><?= $o_listing->get_phone2_pf_areacode() ?></td></tr>
						<tr><td class="left">phone2_pf_prefix:</td><td class="right"><?= $o_listing->get_phone2_pf_prefix() ?></td></tr>
						<tr><td class="left">phone2_pf_city:</td><td class="right"><?= $o_listing->get_phone2_pf_city() ?></td></tr>
						<tr><td class="left">phone2_pf_st:</td><td class="right"><?= $o_listing->get_phone2_pf_st() ?></td></tr>
						<tr><td class="left">phone2_pf_county:</td><td class="right"><?= $o_listing->get_phone2_pf_county() ?></td></tr>
						<tr><td class="left">phone2_pf_county2:</td><td class="right"><?= $o_listing->get_phone2_pf_county2() ?></td></tr>
						<tr><td class="left">phone2_pf_county3:</td><td class="right"><?= $o_listing->get_phone2_pf_county3() ?></td></tr>
						<tr><td class="left">phone2_pf_county4:</td><td class="right"><?= $o_listing->get_phone2_pf_county4() ?></td></tr>
						<tr><td class="left">phone2_pf_msa:</td><td class="right"><?= $o_listing->get_phone2_pf_msa() ?></td></tr>
						<tr><td class="left">dup_phoneinfo:</td><td class="right"><?= $o_listing->get_dup_phoneinfo() ?></td></tr>
						<tr><td class="left">misc1:</td><td class="right"><?= $o_listing->get_misc1() ?></td></tr>
						<tr><td class="left">created:</td><td class="right"><?= $o_listing->get_created() ?></td></tr>
						<tr><td class="left">processed:</td><td class="right"><?= $o_listing->get_processed() ?></td></tr>
						<tr><td class="left">latitude:</td><td class="right"><?= $o_listing->get_latitude() ?></td></tr>
						<tr><td class="left">longitude:</td><td class="right"><?= $o_listing->get_longitude() ?></td></tr>
						<tr><td class="left">map_url:</td><td class="right"><?= $o_listing->get_map_url() ?></td></tr>
						<tr><td class="left">language:</td><td class="right"><?= $o_listing->get_language() ?></td></tr>
						<tr><td class="left">translate:</td><td class="right"><?= $o_listing->get_translate() ?></td></tr>
						<tr><td class="left">date_ad:</td><td class="right"><?= $o_listing->get_date_ad() ?></td></tr>
						<tr><td class="left">product:</td><td class="right"><?= $o_listing->get_product() ?></td></tr>
						<tr><td class="left">status:</td><td class="right"><?= $o_listing->get_status() ?></td></tr>
					</table>
				</fieldset>
<?php
}
else {
?>
				<fieldset>
					<legend>Ad Description</legend>
					<p style="padding-top: 10px;"><?= $o_listing->get_description_ad() ?></p>
				</fieldset>
				<fieldset>
					<legend>Contact 1 Info</legend>
					<table border="0" cellpadding="0" cellspacing="0">
						<tr><td class="left">Name:</td><td class="right"><?= $o_listing->get_contact_name1() ?></td></tr>
						<tr><td class="left">Address:</td><td class="right"><?= $o_listing->get_contact_street1() ?></td></tr>
						<tr><td class="left">City:</td><td class="right"><?= $o_listing->get_contact_city1() ?></td></tr>
						<tr><td class="left">State:</td><td class="right"><?= $o_listing->get_contact_st1() ?></td></tr>
						<tr><td class="left">Zip:</td><td class="right"><?= $o_listing->get_contact_zip1() ?></td></tr>
						<tr><td class="left" valign="top">Phone:</td><td class="right"><?= $o_listing->get_contact_phone1() ?> <?php if ($o_listing->get_phone1_dnc() == 'T') { print '<strong>(Do Not Call Registry)</strong>'; } ?></td></tr>
<?php
	if ($o_listing->get_phone1_prefixfinder() == 'Y') {
?>						
						<tr><td class="left">Prefix-Finder:</td><td class="right"><?= $o_listing->get_phone1_pf_areacode() ?>-<?= $o_listing->get_phone1_pf_prefix() ?>-xxxx area is <?= $o_listing->get_phone1_pf_city() ?>, <?= $o_listing->get_phone1_pf_st() ?></td></tr>
<?php
	}
?>
						<tr><td class="left">Email:</td><td class="right"><?= $o_listing->get_contact_email() ?></td></tr>
					</table>
				</fieldset>
				<fieldset>
					<legend>Contact 2 Info</legend>
					<table border="0" cellpadding="0" cellspacing="0">
						<tr><td class="left">Name:</td><td class="right"><?= $o_listing->get_contact_name2() ?></td></tr>
						<tr><td class="left">Address:</td><td class="right"><?= $o_listing->get_contact_street2() ?></td></tr>
						<tr><td class="left">City:</td><td class="right"><?= $o_listing->get_contact_city2() ?></td></tr>
						<tr><td class="left">State:</td><td class="right"><?= $o_listing->get_contact_st2() ?></td></tr>
						<tr><td class="left">Zip:</td><td class="right"><?= $o_listing->get_contact_zip2() ?></td></tr>
						<tr><td class="left" valign="top">Phone:</td><td class="right"><?= $o_listing->get_contact_phone2() ?> <?php if ($o_listing->get_phone2_dnc() == 'T') { print '<strong>(Do Not Call Registry)</strong>'; } ?></td></tr>
<?php
	if ($o_listing->get_phone2_prefixfinder() == 'Y') {
?>
						<tr><td class="left">Prefix-Finder:</td><td class="right"><?= $o_listing->get_phone2_pf_areacode() ?>-<?= $o_listing->get_phone2_pf_prefix() ?>-xxxx area is <?= $o_listing->get_phone2_pf_city() ?>, <?= $o_listing->get_phone2_pf_st() ?></td></tr>
<?php
	}
?>
					</table>
				</fieldset>
				<fieldset>
					<legend>Property Info</legend>
					<table border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td class="left">Address:</td>
							<td class="right">
								<?= $o_listing->get_property_street() ?>
								<?php if ($o_listing->get_property_street2()) { print '<br/>' . $o_listing->get_property_street2(); } ?>
							</td>
						</tr>
						<tr><td class="left">City:</td><td class="right"><?= $o_listing->get_property_city() ?></td></tr>
						<tr><td class="left">State:</td><td class="right"><?= $o_listing->get_property_st() ?></td></tr>
						<tr><td class="left">Zip:</td><td class="right"><?= $o_listing->get_property_zip() ?></td></tr>
						<tr><td class="left">Bed:</td><td class="right"><?= $o_listing->get_bedrooms() ?></td></tr>
						<tr><td class="left">Bath:</td><td class="right"><?= $o_listing->get_bathrooms() ?></td></tr>
						<tr><td class="left">Square Feet:</td><td class="right"><?= $o_listing->get_square_feet() ?></td></tr>
						<tr><td class="left">Year Built:</td><td class="right"><?= $o_listing->get_year_built() ?></td></tr>
						<tr><td class="left">Lot Size:</td><td class="right"><?= $o_listing->get_lot_size() ?></td></tr>
						<tr><td class="left">Garage:</td><td class="right"><?= $o_listing->get_garage() ?></td></tr>
						<tr><td class="left">Price:</td><td class="right">$<?= $o_listing->get_price() ?></td></tr>
					</table>
				</fieldset>
				<fieldset>
					<legend>Other Info</legend>
					<table border="0" cellpadding="0" cellspacing="0">
						<tr><td class="left">Lead ID:</td><td class="right"><?= $o_listing->get_lead_id() ?></td></tr>
						<tr><td class="left">Lead Source:</td><td class="right"><?= $o_listing->get_data_source_namepublish() ?></td></tr>
					</table>
				</fieldset>
<?php
}
if ($_SESSION['office']->account->is_admin() || $_SESSION['office']->account->is_support() || $_SESSION['office']->account->is_upload()) {
?>
				<input type="button" name="Edit" value="Edit" onClick="document.location.href='/office/listing/edit.php?listingid=<?= $o_listing->get_listingid() ?>';" />
				<input type="button" name="Delete" value="Delete" onClick="if (confirm('Are you sure you want to delete this listing?')) { document.location.href='/office/listing/handle.php?cmd=delete&listingid=<?= $o_listing->get_listingid() ?>'; }" />
<?php
}
?>
			</div>
		</td>
	</tr>
</table>
<?php
include_once($_SESSION['web_interface']->get_server_path('office/global/bottom.php'));
?>

<html>
<body>
<table border="0" cellspacing="0" cellpadding="0">
	<tr><td style="color: #3b56cd; font-size: 11pt; font-weight: bold;"><strong>HOW DO I CANCEL?</strong></td></tr>
	<tr><td><hr/></td></tr>
	<tr><td>Before the trial ends, you can immediately cancel your service at any time by logging in and going to your My Account page. Once there, simply click the cancel button and tell us why you have chosen to discontinue the service. If for any reason you can't login, feel free to <a href="/company.html" target="_blank">Contact Us</a> to cancel.</td></tr>
</table>
</body>
</html>
<?php
$_META_DESCRIPTION = '';
$_META_KEYWORDS = '';
$_PAGE_TITLE = '';

include_once($_SESSION['web_interface']->get_server_path('other/global/top.php'));
?>
<table width="699" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="7" valign="top"><IMG src="/web/company_def/img/spacer.gif" alt="" width="7" height="1"></td>
<td width="684" valign="top">
<table border="0" cellspacing="0" cellpadding="1" bgcolor="#DDDDDD">
<tr><td valign="top">

<table width="682" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
<tr>
<td valign="top" height="3" bgcolor="#D9E2E6"><IMG src="/web/company_def/img/spacer.gif" alt=""></td>
</tr>

<tr>
<td valign="top" height="350" style="padding:11px 17px 10px 15px; line-height:14px " class="big">
<h1 class="PageHead">Faster FSBO Leads, More FSBO Leads, Comprehensive FSBO Leads</h1>
<div style="height:1px; background-color:#A0A0A0 "><IMG src="/web/company_def/img/spacer.gif" alt=""></div>
<br style="line-height:10px ">

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
<td>

<h2 class="big2">Faster For Sale by Owner Leads</h2>
<span class="copy14">
Check us out--we are the fastest FSBO Lead service on the market. Same day published is the same 
day we distribute the For Sale by Owner leads.
</span>

<br><br><br>

<h2 class="big2">More For-Sale-by-Owner Leads</h2>
<span class="copy14">
We capture hundreds of thousands of ads every single day. The ads are processed and become
For-Sale-by-Owner Leads which are distributed.
</span>

<br><br><br>

<h2 class="big2">Comprehensive FSBO Leads</h2>
<span class="copy14">
We are the only FSBO Lead service to publish the ad as it was written. While we add additional
value we don't forget to include the ad as published.
</span>
<br>
</td>
</tr>
</table>

</td>
</tr>
<!--  <tr>
</tr> -->
</table>
</td></tr>
</table>

</td>
<td width="8" valign="top"><IMG src="/web/company_def/img/spacer.gif" alt="" width="8" height="1"></td>
</tr>
</table>

</TD>
</TR>
<TR>
<TD>
<IMG src="/web/company_def/img/spacer.gif" WIDTH=699 HEIGHT=12 ALT=""></TD>
</TR>
</TABLE>
<?php
include_once($_SESSION['web_interface']->get_server_path('other/global/bottom.php'));
?>
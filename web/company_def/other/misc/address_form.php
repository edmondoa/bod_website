<?php
require_once($_SESSION['web_interface']->get_server_path('other/misc/select_options.php'));
?>
<tr>
	<td class="left"><span <?= $_SESSION['web_interface']->missing_required_label($address->get_type() . '_line1'); ?>>Street Address:</span></td>
	<td class="right"><input type="text" name="<?= $address->get_type() ?>_line1" <?= $_SESSION['web_interface']->missing_required_input($address->get_type() . '_line1'); ?> id="street_address" value="<?= $address->get_line1() ?>" /></td>
</tr>
<tr>
	<td class="left">&nbsp;</td>
	<td class="right"><input type="text" name="<?= $address->get_type() ?>_line2" id="line2" value="<?= $address->get_line2() ?>" /></td>
</tr>
<tr>
	<td class="left"><span <?= $_SESSION['web_interface']->missing_required_label($address->get_type() . '_city'); ?>>City:</span></td>
	<td class="right"><input type="text" name="<?= $address->get_type() ?>_city" <?= $_SESSION['web_interface']->missing_required_input($address->get_type() . '_city'); ?> id="<?= $address->get_type() ?>_city" value="<?= $address->get_city() ?>"/></td>
</tr>
<tr>
	<td class="left"><span <?= $_SESSION['web_interface']->missing_required_label($address->get_type() . '_country'); ?>>Country:</span></td>
	<td class="right">
		<select name="<?= $address->get_type() ?>_country" <?= $_SESSION['web_interface']->missing_required_input($address->get_type() . '_country'); ?> id="<?= $address->get_type() ?>_country" onChange="javascript: get_<?= $address->get_type() ?>_states();">
			<?= select_options( array('type'=>'country', 'selected'=>$address->get_country()) ) ?>
		</select>
	</td>
</tr>
<tr>
	<td class="left"><span <?= $_SESSION['web_interface']->missing_required_label($address->get_type() . '_state'); ?>>State:</span></td>
	<td class="right">
		<select name="<?= $address->get_type() ?>_state" <?= $_SESSION['web_interface']->missing_required_input($address->get_type() . '_state'); ?> id="<?= $address->get_type() ?>_state">
		</select>
	</td>
</tr>
<tr>
	<td class="left"><span <?= $_SESSION['web_interface']->missing_required_label($address->get_type() . '_postal_code'); ?>>Zip Code:</span></td>
	<td class="right"><input type="text" name="<?= $address->get_type() ?>_postal_code" <?= $_SESSION['web_interface']->missing_required_input($address->get_type() . '_postal_code'); ?> id="<?= $address->get_type() ?>_postal_code" value="<?= $address->get_postal_code() ?>" size="10" /></td>
</tr>
<script language="javascript" type="text/javascript">
	function get_<?= $address->get_type() ?>_states() {
		var country = document.getElementById('<?= $address->get_type() ?>_country');
		var state = document.getElementById('<?= $address->get_type() ?>_state');

		if (country.value == 'US') {
			clearList(state, 'Select a State');
		
<?php
$count = 1;
$states = select_options( array(type=>'us_state', expecting=>'array') );
foreach ($states as $key=>$value) {	
?>
			state[<?= $count ?>] = new Option('<?= $value ?>','<?= $key ?>');
<?php
	if ($key == $address->get_state()) {
		print 'state.options[' . $count . '].selected = true;';
	}
	$count++;
}
?>
		}
		else if (country.value == 'CA') {
			clearList(state, 'Select a Province');
		
<?php
$count = 1;
$provinces = select_options( array(type=>'ca_state', expecting=>'array') );
foreach ($provinces as $key=>$value) {	
?>
			state[<?= $count ?>] = new Option('<?= $value ?>','<?= $key ?>');
<?php
	if ($key == $address->get_state()) {
		print 'state.options[' . $count . '].selected = true;';
	}
	$count++;
}
?>
		}
		else {
			clearList(state, 'Select State');
		}
	}
	get_<?= $address->get_type() ?>_states();
</script>
<?php
	function select_options($args = NULL) {
		//address stuff
		$country_options = array('US'=>'United States', 'CA'=>'Canada');
		$ca_state_options = array('AB'=>'Alberta','BC'=>'British Columbia','MB'=>'Manitoba','NB'=>'New Brunswick', 'NS'=>'Nova Scotia','ON'=>'Ontario','QC'=>'Quebec','SK'=>'Saskatchewan');
		$us_state_options = array('AL'=>'Alabama','AK'=>'Alaska','AZ'=>'Arizona','AR'=>'Arkansas','CA'=>'California','CO'=>'Colorado','CT'=>'Connecticut','DE'=>'Delaware','DC'=>'District of Columbia','FL'=>'Florida','GA'=>'Georgia','HI'=>'Hawaii','ID'=>'Idaho','IL'=>'Illinois','IN'=>'Indiana','IA'=>'Iowa','KS'=>'Kansas','KY'=>'Kentucky','LA'=>'Louisiana','ME'=>'Maine','MD'=>'Maryland','MA'=>'Massachusetts','MI'=>'Michigan','MN'=>'Minnesota','MS'=>'Mississippi','MO'=>'Missouri','MT'=>'Montana','NE'=>'Nebraska','NV'=>'Nevada','NH'=>'New Hampshire','NJ'=>'New Jersey','NM'=>'New Mexico','NY'=>'New York','NC'=>'North Carolina','ND'=>'North Dakota','OH'=>'Ohio','OK'=>'Oklahoma','OR'=>'Oregon','PA'=>'Pennsylvania','RI'=>'Rhode Island','SC'=>'South Carolina','SD'=>'South Dakota','TN'=>'Tennessee','TX'=>'Texas','UT'=>'Utah','VT'=>'Vermont','VA'=>'Virginia','WA'=>'Washington','WV'=>'West Virginia','WI'=>'Wisconsin','WY'=>'Wyoming');

		//billing stuff
		$account_type_options = array('AFFILIATE'=>'Affiliate','CUSTOMER'=>'Customer', 'NEW'=>'New', 'PRIVATE_LABEL'=>'Private Label', 'SUPPORT'=>'Support', 'TRIAL'=>'Trial', 'UPLOAD'=>'Upload');
		$billing_type_options = array('Credit Card'=>'Credit Card','Invoice'=>'Invoice','Other'=>'Other');
		$billing_month_options = array('01'=>'1 - January','02'=>'2 - February','03'=>'3 - March','04'=>'4 - April','05'=>'5 - May','06'=>'6 - June','07'=>'7 - July','08'=>'8 - August','09'=>'9 - September','10'=>'10 - October','11'=>'11 - November','12'=>'12 - December');
		$billing_status_options = array('active'=>'Active','on_hold'=>'On Hold','queued'=>'Queued to Cancel','canceled'=>'Canceled','archive'=>'Archived','not_processed'=>'Not Processed');
		$credit_card_options = array('4'=>'Visa','5'=>'Mastercard','3'=>'American Express');
		$customer_type_options = array('Affiliate'=>'Affiliate', 'Investor'=>'Investor', 'Mortgage'=>'Mortgage', 'Private Label'=>'Private Label', 'Property Manager'=>'Property Manager', 'Realtor'=>'Realtor', 'Title'=>'Title', 'Other'=>'Other');
		$paymethod_type_options = array('CreditCard'=>'Credit Card','CheckDraft'=>'Check Draft');
		$private_account_type_options = array('CUSTOMER'=>'Customer');
		$private_customer_type_options = array('Investor'=>'Investor', 'Mortgage'=>'Mortgage', 'Property Manager'=>'Property Manager', 'Realtor'=>'Realtor', 'Title'=>'Title', 'Other'=>'Other');
		$signup_customer_type_options = array('Investor'=>'Investor', 'Mortgage'=>'Mortgage', 'Property Manager'=>'Property Manager', 'Realtor'=>'Realtor', 'Title'=>'Title', 'Other'=>'Other');
		$subscription_type_options = array('Monthly'=>'Monthly','3-Month'=>'3-Month', '6-Month'=>'6-Month', 'Annual'=>'Annual','Other'=>'Other');
		

		//date_time stuff
		$day_options = array();
		for ($count = 1; $count<=31; $count++) {
			if ($count < 10) {
				$day_options['0' . $count] = $count;
			}
			else {
				$day_options[$count] = $count;
			}
		}
		
		$month_options = array('01'=>'January','02'=>'February','03'=>'March','04'=>'April','05'=>'May','06'=>'June','07'=>'July','08'=>'August','09'=>'September','10'=>'October','11'=>'November','12'=>'December');
		
		$short_month_options = array('01'=>'Jan','02'=>'Feb','03'=>'Mar','04'=>'Apr','05'=>'May','06'=>'Jun','07'=>'Jul','08'=>'Aug','09'=>'Sep','10'=>'Oct','11'=>'Nov','12'=>'Dec');

		$year = date("Y");
		$year_options = array($year=>$year);
		for ($count = 1; $count<=10; $count++) {
			$year++;
			$year_options[$year] = $year;
		}

		$year = date("Y");
		$past_year_options = array($year=>$year);
		while ($year >= 2002) {
			$year--;
			$past_year_options[$year] = $year;
		}

		$hour_options = array(1=>'1',2=>'2',3=>'3',4=>'4',5=>'5',6=>'6',7=>'7',8=>'8',9=>'9',10=>'10',11=>'11',12=>'12');
		$minute_options = array('00'=>'00','05'=>'05',10=>'10',15=>'15',20=>'20',25=>'25',30=>'30',35=>'35',40=>'40',45=>'45',50=>'50',55=>'55');
		
		// listing stuff
		$price_options = array(
			"< '100000'"=>'less than $100,000', ">= '100000' AND price < '125000'"=>'$100,000 to $125,000', ">= '125000' AND price < '150000'"=>'$125,000 to $150,000', ">= '150000' AND price < '175000'"=>'$150,000 to $175,000', ">= '175000' AND price < '200000'"=>'$175,000 to $200,000', ">= '200000' AND price < '250000'"=>'$200,000 to $250,000', ">= '250000' AND price <= '300000'"=>'$250,000 to $300,000', ">= '300000'"=>'greater than $300,000');
		$style_options = array('Residential'=>'Residential');
		
		// misc
		$number_options = array();
		if ($args['type'] == 'number' && $args['max']) {
			$args['min'] = ($args['min']) ? $args['min'] : 1;
			$args['increment'] = ($args['increment']) ? $args['increment'] : 1;
			if ($args['reverse']) {
				$count = $args['max'];
				while ($count >= $args['min']) {
					if ($args['plus_sign']) {
						$number_options["'" . $count . "'"] = $count . '+';
					}
					else {
						$number_options[$count] = $count;
					}
					$count -= $args['increment'];
				}
			}
			else {
				$count = $args['min'];
				while ($count <= $args['max']) {
					if ($args['plus_sign']) {
						$number_options["'" . $count . "'"] = $count . '+';
					}
					else {
						$number_options[$count] = $count;
					}
					$count += $args['increment'];
				}
			}
		}
		
		// actual code
		if ($args['expecting'] == 'array') {
			return ${$args['type']."_options"};
		}
		foreach (${$args['type']."_options"} as $key=>$value) {
			if ($args['selected'] == $key) {
				$selected = 'selected';
			}
			else {
				$selected = '';
			}
			print '<option value="' . $key . '" ' . $selected . '>' . $value . '</option>';
		}
	}
?>

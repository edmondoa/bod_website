<html>
<body>
<table border="0" cellspacing="0" cellpadding="0">
	<tr><td style="color: #3b56cd; font-size: 11pt; font-weight: bold;"><strong>PAYING SUBSCRIPTION?</strong></td></tr>
	<tr><td><hr/></td></tr>
	<tr>
		<td>
			Please refer to the subscription type information below:<br/>
			<br/>
			<strong>Monthly:</strong> At the end of your free trial period your account will automatically convert to a monthly subscription. This is a month to month agreement with no commitment. Pricing: 1 area $29/month. Add $6/month for a 2nd area. Add $5/month per area 3+. All months are prepaid in advance. There are no refunds or prorated refunds after the billing day. Billing is automatic on the same day each month*.  There are discounts available for annual payment.<br/>
			<br/>
			<span style="font-size: 9pt;">*subscriptions with start dates on the 29th, 30th, and 31st of the month are initially be billed on the start date, but then auto-billed on the 1st day of each month thereafter, skipping the subsequent month. (e.g. a signup on January 31st will be billed that day. The next billing day will be March 1st)</span><br/>
			<br/>
			<strong>Annual:</strong> At the end of your trial period your account will automatically convert to a paid subscription for 12 months. Pricing: 1 area $240/year. Add $60/year for a 2nd area. Add $48/year per area 3+. All Annual subscriptions are prepaid in advance. No refund/credit will be given except during the first 14 days.<br/>
			<br/>
			<strong>Refund Policy:</strong>  <?= $_SESSION['o_company']->get_title() ?> has a NO-Refund policy. If you would like instructions on how to cancel your account go to <?= $_SESSION['o_company']->get_url() ?> /cancel. If you cancel an annual agreement, no refund/credit will be given. All agreements are set to auto-renew unless changed by you, the account owner.
</td></tr>
</table>
</body>
</html>
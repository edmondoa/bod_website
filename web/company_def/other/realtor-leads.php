<?php
$_META_DESCRIPTION = '';
$_META_KEYWORDS = '';
$_PAGE_TITLE = '';

include_once($_SESSION['web_interface']->get_server_path('other/global/top.php'));
?>
<table width="699" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="7" valign="top"><IMG src="/web/company_def/img/spacer.gif" alt="" width="7" height="1"></td>
<td width="684" valign="top">
<table border="0" cellspacing="0" cellpadding="1" bgcolor="#DDDDDD">
<tr><td valign="top">

<table width="682" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
<tr>
<td valign="top" height="3" bgcolor="#D9E2E6"><IMG src="/web/company_def/img/spacer.gif" alt=""></td>
</tr>

<tr>
<td valign="top" height="350" style="padding:11px 17px 10px 15px; line-height:14px " class="big">
<h1 class="PageHead">Real Estate Professionals</h1>
<div style="height:1px; background-color:#A0A0A0 "><IMG src="/web/company_def/img/spacer.gif" alt=""></div>
<br style="line-height:10px ">

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
<td>

<h2 class="big2">More leads:</h2>
<span class="copy14">
To say the obvious, more leads is key to filling your sales pipeline. It's a numbers game 
and the more FSBO leads moving onto your plate translates into greater potential success. More 
real estate listings is the goal we help fulfil. Our FSBO listing service delivers you daily FSBO leads.
</span>

<br><br><br>
<h2 class="big2">FSBO Sellers can really use your help:</h2>
<span class="copy14">
<!--			National Association of Realtors� 2005 Home Sales report: -->
Excerpts from:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

A close look at "For-Sale-By-Owner" (FSBO) data from
<Center>NAR's 2005 Profile of Home Buyers and Sellers
</Center>
<ul>
- 85% of homes sold with an agent or broker, 13% FSBO, 2% Other.<br><br>
- Homes sold with help by a professional averaged 16% more:<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Median sale price with an agent $230,000 vs. FSBO $198,000.<br><br>
- FSBO sellers indicate they have the most trouble with:<br>
&nbsp;&nbsp;&nbsp; 1. Completing the "complicated" paperwork.<br>
&nbsp;&nbsp;&nbsp; 2. Preparing the home for the sale.<br>
&nbsp;&nbsp;&nbsp; 3. Finding potential buyers.<br>
&nbsp;&nbsp;&nbsp; 4. Setting the right price.<br>
<br><br>
"Since FSBOs don�t use a real estate professional, they have to do their own marketing to attract potential buyers.
<br><br>
But FSBOs have no access to fundamental marketing services, such as Multiple Listing Service (MLS) and cannot list their homes there. They also have no access to other marketing avenues such and REALTOR.com, Virtual Office Web sites and the Internet Data Exchange.
<br><br>
Real estate professionals are experts in marketing who can sell a home for more money in less time. 
<br><br>
Real estate professionals assist both sellers and buyers with a variety of the details surrounding a real estate transaction. Real estate pros can help a seller set a realistic price and ensure the proper paperwork and various disclosures and inspections are handled correctly.
<br><br>
In addition, real estate professionals are experts in attracting qualified buyers. A broker or sales associate also can show a home more objectively than can a seller who may be emotionally attached to the home, and who might become unnerved by prospective buyers� critical comments. The real estate pro also checks the financial capability and bona fides of buyers before allowing them onto a seller�s property."
</ul>
</span>

<br>
<h2 class="big2">Faster access to the Realtor Lead = More Listings</h2>
<span class="copy14">
Bottom line is that our FSBO Lead Service captures classifed ads and turns each one into a Realtor lead
usually the day they are published. If you want to get the jump on the competition, getting to 
the FSBO leads the same day they are published is critical. Our customers have this advantage!
</span>

<br><br><br>
<h2 class="big2">Realtor FSBO Leads turn into Sales Closes</h2>
<span class="copy14">
More Real Estate FSBO leads enable more closings. Our customers report:
</span>
<br>
<ul>
<li>Joan, a power agent with a team, reports that realtor leads from our FSBO Lead service
directly contributes to a 20% increase in closings. Our real estate lead service increases
their annual closing total by 24 to 36.</li>
<li>Issac, a FSBO warrior, says that 80% of his closes originate with a FSBO lead supplied by our service.</li>
</ul>


</td>
</tr>
</table>

</td>
</tr>
<!--  <tr>
</tr> -->
</table>
</td></tr>
</table>

</td>
<td width="8" valign="top"><IMG src="/web/company_def/img/spacer.gif" alt="" width="8" height="1"></td>
</tr>
</table>

</TD>
</TR>
<TR>
<TD>
<IMG src="/web/company_def/img/spacer.gif" WIDTH=699 HEIGHT=12 ALT=""></TD>
</TR>
</TABLE>
<?php
include_once($_SESSION['web_interface']->get_server_path('other/global/bottom.php'));
?>
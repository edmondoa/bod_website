<?php
$_META_DESCRIPTION = '';
$_META_KEYWORDS = '';
$_PAGE_TITLE = '';

include_once($_SESSION['web_interface']->get_server_path('other/global/top.php'));
?>
<table width="699" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="7" valign="top"><IMG src="/web/company_def/img/spacer.gif" alt="" width="7" height="1"></td>
<td width="684" valign="top">
<table border="0" cellspacing="0" cellpadding="1" bgcolor="#DDDDDD">
<tr><td valign="top">

<table width="682" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
<tr>
<td valign="top" height="3" bgcolor="#D9E2E6"><IMG src="/web/company_def/img/spacer.gif" alt=""></td>
</tr>

<tr>
<td valign="top" height="350" style="padding:11px 17px 10px 15px; line-height:14px " class="big">
<h1 class="PageHead">FSBO Sample</h1>
<div style="height:1px; background-color:#A0A0A0 "><IMG src="/web/company_def/img/spacer.gif" alt=""></div>
<br style="line-height:10px ">

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
<!--			<td> -->

<td align="left" valign="top" class="gray">
Below is a partial sample of FSBO leads from two counties in Utah. Our For Sale by Owner leads include 
the original ad description. You'll notice in our FSBO lists that newspaper sources are identified 
and phone numbers on the U.S. Do Not Call Registry are marked.
<br> (Note: Addresses, Phone numbers & dates on this sample have been changed to keep details confidential.)
<br><br><br>
<!--							</td> -->

</tr>

<tr>
<td align="left" valign="top">
(1 of 53) SOURCE: Salt Lake Tribune
<br>
DATE: July 10  &nbsp;&nbsp;COUNTY: Salt Lake, UT
<br><br>
MILLCREEK-  123 Any St. $196,900 2200sq.ft. Rmblr, 3bdrm 2 ba.,
<br>2 car gar., 2 frplc By Owner 951-9999

<br><br>
REVERSE LOOKUP 801-951-9999<b>* (Do Not Call Registry)</b>
<br>
SMITH, BRUCE & RUTHANNE
<br>
123 Any St.
<br>
SALT LAKE CITY, UT 84106
<br>
____________________
<br><br>
(2 of 53) SOURCE: Deseret News
<br>
DATE: July 10  &nbsp;&nbsp;COUNTY: Salt Lake, UT
<br><br>
SUGAR HOUSE brick Rmblr., 4bdrm, 2ba., possible mother-in-law bsmt.,
<br>must sell immediately $179,000 808-9999

<br><br>
REVERSE LOOKUP 801-808-9999&nbsp;&nbsp; NO RESULTS
<br>
Prefix Finder (801) 808-xxxx (SALT LAKE CITY, UT)
)
<br>
____________________
<br><br>
(3 of 53) SOURCE: Internet
<br>
DATE: July 10   &nbsp;&nbsp;COUNTY: Salt Lake, UT
<br><br>
FOR SALE: Herriman Multi-level Home 2200 sqft multi level design.
Fresh two tone paint, new carpet through
out whole house, home theater wired and ready, 250sqft
new tile, new lighting fixtures, recessed cans,
ambiance lighting, spec dimmers, perfect landscaped
yard, rv receptacles, new shed, huge back concrete
patio adjoining rv pad, Way to many extras and
upgrades to list. Call anytime or email for interior
photos. 801-253-9999 home. Bed: 4,
Bath: 3, Sqft: 2250, Price: $180,000, Property Address: 1000 Foothill
Rd. West Jordan, UT 84065
<br><br>
REVERSE LOOKUP 801-253-9999<b>* (Do Not Call Registry)</b>
<br>
HARMER, ROBERT & ARLENE
<br>
WEST JORDAN, UT 84084
<br>
____________________
<br><br>
(4 of 53) SOURCE: Daily Herald
<br>
DATE: July 10   &nbsp;&nbsp;COUNTY: Utah, UT
<br><br>
Orem Condo 1850sf,4bd 3ba, vltd, lots of Xtras! Must Sell! $127K John 221-9999
<br><br>
REVERSE LOOKUP 801-221-9999
<br>
JONES, JOHN
<br>
100W 100N
<br>
OREM, UT 84057
<br>

</td>
</tr>
</table>

</td>
</tr>

</table>
</td></tr>
</table>

</td>
<td width="8" valign="top"><IMG src="/web/company_def/img/spacer.gif" alt="" width="8" height="1"></td>
</tr>
</table>

</TD>
</TR>
<TR>
<TD>
<IMG src="/web/company_def/img/spacer.gif" WIDTH=699 HEIGHT=12 ALT=""></TD>
</TR>
</TABLE>
<?php
include_once($_SESSION['web_interface']->get_server_path('other/global/bottom.php'));
?>
<?php
$_META_DESCRIPTION = '';
$_META_KEYWORDS = '';
$_PAGE_TITLE = '';

include_once($_SESSION['web_interface']->get_server_path('other/global/top.php'));
?>
<table width="699" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="7" valign="top"><IMG src="/web/company_def/img/spacer.gif" alt="" width="7" height="1"></td>
<td width="684" valign="top">
<table border="0" cellspacing="0" cellpadding="1" bgcolor="#DDDDDD">
<tr><td valign="top">

<table width="682" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
<tr>
<td valign="top" height="3" bgcolor="#D9E2E6"><IMG src="/web/company_def/img/spacer.gif" alt=""></td>
</tr>

<tr>
<td valign="top" height="350" style="padding:11px 17px 10px 15px; line-height:14px " class="big">
<h1 class="PageHead">FSBO Services (Realtor-Title-Mortgage-Other Service Professionals)</h1>
<div style="height:1px; background-color:#A0A0A0 "><IMG src="/web/company_def/img/spacer.gif" alt=""></div>
<br style="line-height:10px ">

<!-- Main Inner Table -->
<table width="100%" cellspacing="0" cellpadding="2" border="0">
<tr>
<td width="80%" align="left" valign="top">
<!-- Table on Left of Main Inner table -->
<table width="100%" cellspacing="0" cellpadding="2" border="0">
<tr>
<td>
<h2><span class="big2"><?= $_SESSION['o_company']->get_title() ?> saves you time:</span></h2>
<ul>
<li><b>W</b>e scour For Sale By Owner information in your local newspapers, magazines, websites and other public sources.</li>
<li><b>W</b>e send leads to your email address the same day they appear in your local paper.</li>
<li><b>NO</b> repeat ads.</li>
<li><b>W</b>e do a reverse phone directory lookup on ALL ad phone numbers.</li>
<li><b>O</b>ur telephone Prefix Finder(tm) tells you the city source of mobile and land based phones.</li>
<li><b>W</b>e compare advertised phone numbers with the National Do Not Call Registry and identify registered phone numbers.</li>
</ul>

<br>
<h2><span class="big2"><?= $_SESSION['o_company']->get_title() ?> saves you money:</span></h2>
<ul>
<li><b>I</b>t won't be necessary to subscribe to multiple newspapers anymore! </li>
<li><b>F</b>ast, early and daily lead delivery helps your efficiency and effectiveness. </li>
<li><b>W</b>e save you time, and time is money. </li>
</ul>
<!--									<br><br>
<center><h2 class="big">We search public newspaper classified ads,
<br> COMPILE and DELIVER daily!</h2></center>
-->
</td>
</tr>
</table>

</td>

<td width="20%" align="left" valign="top">
<!-- Table on Right of Main Inner table -->
<table width="100%" cellspacing="0" cellpadding="2" class="blue_bg" border="0">
<tr>
<td class="gray" align="center"><b>Subscription Services</b></td>
</tr>
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="6" bgcolor="#ffffff" border="0">
<tr>
<td>
<a href="areas_main.html">Subscribe Now!</a><br><br>
<a href="sample.html">View sample</a><br>
<!--											<a href="ssl/request_sample.html">Free Trial</a> -->
<br><br>
<strong>Pricing:</strong><br>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
<td class="blacktxt" align="left" valign="top" nowrap><a href="areas_main.html">See Areas Covered<IMG src="/web/company_def/img/spacer.gif" alt="" width="25" height="1" border="0"></td>
<!--												     <td class="blacktxt" align="left" valign="top" nowrap>Month-to-month<IMG src="/web/company_def/img/spacer.gif" alt="" width="25" height="1" border="0"></td>
<td class="blacktxt" align="left" valign="top" nowrap>$29 month</td>
-->														 
</tr>
<!--											     <tr>
<td class="blacktxt" align="left" valign="top" nowrap>3-month<IMG src="/web/company_def/img/spacer.gif" alt="" width="25" height="1" border="0"></td>
<td class="blacktxt" align="left" valign="top" nowrap>$26 month*</td>
</tr>
<tr>
<td class="blacktxt" align="left" valign="top" nowrap>6-month<IMG src="/web/company_def/img/spacer.gif" alt="" width="25" height="1" border="0"></td>
<td class="blacktxt" align="left" valign="top" nowrap>$23 month*</td>
</tr>
<tr>
<td class="blacktxt" align="left" valign="top" nowrap>12-month<IMG src="/web/company_def/img/spacer.gif" alt="" width="25" height="1" border="0"></td>
<td class="blacktxt" align="left" valign="top" nowrap>$20 month*</td>
</tr>
-->													 
</table>

<br>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>

<table width="100%" cellspacing="0" cellpadding="2" border="0">
<tr>
<td>
<center>
<h2 class="top_txt_menu">We search public newspaper classified ads</h2>													
</center>
</td>
</tr>
</table>
<table width="100%" cellspacing="0" cellpadding="2" border="0">
<tr>
<td>
<center>
<h2 class="top_txt_menu">Collect and scrub relevant FSBO ads</h2>													
</center>
</td>
</tr>
</table>
<table width="100%" cellspacing="0" cellpadding="2" border="0">
<tr>
<td>
<center>
<h2 class="top_txt_menu">Delivery leads to you the same day!</h2>													
</center>
</td>
</tr>
</table>								

</td>
</tr>
</table>

</td>
</tr>
</table>

</td>
<td width="8" valign="top"><IMG src="/web/company_def/img/spacer.gif" alt="" width="8" height="1"></td>
</tr>
</table>

</TD>
</TR>
<TR>
<TD>
<IMG src="/web/company_def/img/spacer.gif" WIDTH=699 HEIGHT=12 ALT=""></TD>
</TR>
</TABLE>
<?php
include_once($_SESSION['web_interface']->get_server_path('other/global/bottom.php'));
?>
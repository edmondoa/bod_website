<?php
require_once($_SESSION['web_interface']->get_server_path('other/misc/select_options.php'));
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>
<HEAD>
	<LINK href="<?= $_SESSION['web_interface']->get_path('css/other.css') ?>" rel="stylesheet" type="text/css">
	<script src="<?= $_SESSION['web_interface']->get_path('js/global.js') ?>" type="text/javascript"></script>
	<script type="text/javascript">
		function submit_form() {
			if (checkEmail(document.getElementById('main_email_address'))) { 
				if (document.getElementById('accept_tandc').checked) {
					return true;
				} 
				else { 
					alert('You must agree to the Terms and Conditions.');
					return false;
				} 
			}
		}
	</script>
</HEAD>
<BODY bgColor="#ffffff">
<center>
<form name="thisForm" method="post" action="/ssl/handle.html" onSubmit="return submit_form();">
<input type="hidden" name="cmd" value="send_affiliate_request" />
<input type="hidden" name="page" value="affiliate_signup_email" />
<table border="0" cellspacing="0" cellpadding="1" bgcolor="#DDDDDD">
	<tr>
		<td valign="top">
			<table width="682" height="353" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
				<tr>
					<td valign="top" height="3" bgcolor="#D9E2E6"><img src="/web/company_def/img/spacer.gif"></td>
				</tr>
				<tr>
					<td valign="top" height="350" style="padding:11px 17px 10px 15px; line-height:14px " class="big">
						<strong class="blue">FAQ - <?= $_SESSION['o_company']->get_title() ?></strong><br>
						<br style="line-height:10px ">
						<div style="height:1px; background-color:#A0A0A0 "><img src="/web/company_def/img/spacer.gif"></div>
						<br style="line-height:10px ">
						<table width="100%" cellspacing="0" cellpadding="2" border="0">
							<tr>
								<td align="left" valign="top">
									<span class="header1">Please fill out the below form with your information. Affiliate payments will be mailed to the address you enter below. After hitting submit this information will be sent to us and we will be contacting you shortly. Thank you for your interest in <?= $_SESSION['o_company']->get_title() ?>.</span>
									<br><br>
									* indicates required field
									<?php include_once($_SESSION['web_interface']->get_server_path('other/misc/status_error.php')); ?>
									<TABLE width="85%" CELLSPACING="0" CELLPADDING="1" BORDER="0">
										<TR><TD COLSPAN="4" ALIGN="left"><span class="header1"><strong>1. Contact Information</strong></span></td></TR>
										<TR>
											<TD ALIGN="RIGHT"><span <?= $_SESSION['web_interface']->missing_required_label('first_name') ?>>First Name*</span></td>
											<TD><input type="text" name="first_name" size="20" value="<?= $_REQUEST['first_name'] ?>" <?= $_SESSION['web_interface']->missing_required_input('first_name') ?>></td>
											<TD align="RIGHT"><span <?= $_SESSION['web_interface']->missing_required_label('last_name') ?>>Last Name*</span></td>
											<TD><input type="text" name="last_name" size="25" value="<?= $_REQUEST['last_name'] ?>" <?= $_SESSION['web_interface']->missing_required_input('last_name') ?>></td>
										</tr>
										<TR>
											<TD ALIGN="RIGHT"><span <?= $_SESSION['web_interface']->missing_required_label('main_email_address') ?>>Email address*</span></td>
											<TD><input type="text" id="main_email_address" name="main_email_address" size="20" value="<?= $_REQUEST['main_email_address'] ?>" <?= $_SESSION['web_interface']->missing_required_input('main_email_address') ?>></td>
											<TD ALIGN="RIGHT">Email (verify)&nbsp;</td>
											<TD><input type="text" name="confirm_email_address" size="25" value="<?= $_REQUEST['confirm_email_address'] ?>"></td>
										</tr>
										<TR>
											<TD ALIGN="right"><span <?= $_SESSION['web_interface']->missing_required_label('personal_phone') ?>>Main Phone*</span></td>
											<TD><input type="text" name="personal_phone" size="20" value="<?= $_REQUEST['personal_phone'] ?>" <?= $_SESSION['web_interface']->missing_required_input('personal_phone') ?>></td>
											<TD ALIGN="RIGHT">Work Phone</td>
											<TD><input type="text" name="business_phone" size="25" value="<?= $_REQUEST['business_phone'] ?>"></td>
										</tr>
										<TR>
											<TD ALIGN="right">Company</td>
											<TD><input type="text" name="business_name" size="20" value="<?= $_REQUEST['business_name'] ?>"></td>
											<TD ALIGN="RIGHT">Address</td>
											<TD><input type="text" name="line1" size="25" value="<?= $_REQUEST['line1'] ?>" <?= $_SESSION['web_interface']->missing_required_input('line1') ?>></td>
										</tr>
										<TR>
											<TD ALIGN="right">City</td>
											<TD><input type="text" name="city" size="20" value="<?= $_REQUEST['city'] ?>" <?= $_SESSION['web_interface']->missing_required_input('city') ?>></td>
											<TD align="right">State&nbsp;</td>
											<TD align="left">
												<!-- Table for two cells of State and ZIP -->
												<TABLE CELLSPACING="0" CELLPADDING="0" BORDER="0">
													<tr>
														<TD>
															<select name="state" <?= $_SESSION['web_interface']->missing_required_input('state') ?>>
																<option value=""></option>
																<?= select_options( array('type'=>'us_state', 'selected'=>$_REQUEST['state']) ) ?>
															</select>
														</td>
														<TD align="right">&nbsp; Zip&nbsp;</td>
														<TD><input type="text" name="postal_code" size="3" value="<?= $_REQUEST['postal_code'] ?>" <?= $_SESSION['web_interface']->missing_required_input('postal_code') ?>></td>
													</tr>
												</TABLE>
											</TD>
										</tr>
									</table>
									<br>

									<TABLE width="75%" CELLSPACING="0" CELLPADDING="0" BORDER="0">
										<TR><TD COLSPAN="4" align="left"><span class="header1"><strong>2. Site Information</strong></span></td></TR>
										<tr><td colspan="4" align="left">Username/Password is needed for online access to view current customer and payment status. The Username you enter below will also be used to create a customized URL for people to access this site. For example if you enter joenobody as your Username, your URL will be http://<?= $_SESSION['o_company']->get_company_url() ?>/joenobody. The Password you enter is case-sensitive (e.g. donny123 is NOT the same as DonNy123).</td></tr>
										<tr><td>&nbsp;</td></tr>
										<TR>
											<TD ALIGN="right"><span <?= $_SESSION['web_interface']->missing_required_label('username') ?>>Username*</span></td>
											<TD><input type="text" name="username" size="20" value="<?= $_REQUEST['username'] ?>" <?= $_SESSION['web_interface']->missing_required_input('username') ?>></td>
											<TD ALIGN="RIGHT"><span <?= $_SESSION['web_interface']->missing_required_label('password') ?>>Password*</span></td>
											<TD><input type="password" name="password" size="20" value="<?= $_REQUEST['password'] ?>" <?= $_SESSION['web_interface']->missing_required_input('password') ?>></td>
										</tr>
									</TABLE>
									<br>

									<TABLE width="90%" CELLSPACING="0" CELLPADDING="0" BORDER="0">
										<TR><TD align="left"><span class="header1"><strong>3. Terms and Conditions</strong></span></td></TR>
										<tr><td align="left">By clicking the checkbox below you are agreeing to the Terms and Conditions as sent to you by email.</td></tr>
										<tr><td>&nbsp;</td></tr>
										<tr><td><input type="checkbox" id="accept_tandc" name="accept_tandc" value="1" />I accept the Terms and Conditions as stated in the email I received.</td></tr>
										<tr><td>&nbsp;</td></tr>
										<TR>
											<TD ALIGN="LEFT">
												<input type="submit" name="submit" value="Submit">
												<input type="reset" name="reset" value="Clear">
											</td>
										</tr>
									</TABLE>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
				</tr>
			</table>
		</td>
	</tr>
</table>
		</TD>
	</TR>
	<TR><TD><IMG SRC="/web/company_def/img/spacer.gif" WIDTH=699 HEIGHT=12 ALT=""></TD></TR>
</TABLE>
</form>
</center>
</body>
</html>

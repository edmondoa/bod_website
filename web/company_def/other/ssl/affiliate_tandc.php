<div style="width: 600px;">
FSBO Lead Service<br/>
Affiliate Agreement<br/>
(FSBOLeadsUSA)<br/>
<br/>
<br/>
1.	RECITALS<br/>
1.1	WHEREAS, Automated Info Solutions, Inc. ("AIS") collects, processes and distributes publicly available data;<br/>
1.2	WHEREAS, be it known that AIS has subsidiary companies (including ByOwnerDaily, Mortgage-FSBO-Leads and others) that collect, process and distribute publicly available Real Estate related data including For-Sale-by-Owner leads;<br/>
1.3	WHEREAS, the parties hereto desire to engage in a mutually non-exclusive Affiliate Agreement based upon all the terms and conditions contained herein;<br/>
1.4	WHEREAS, Affiliate agrees to the condition of an Email/DB-Only Product when working with Real Estate Professionals who are in the customary role of representing the buyer and/or the seller. No restrictions are placed on any other sales prospects including Real Estate Investors, Mortgage Professionals, etc.<br/>
1.5	NOW THEREFORE, in consideration of the conditions, agreements and obligations set forth herein and for good and valuable consideration, the receipt and sufficiency of which is hereby acknowledged and accepted, the parties hereto hereby agree as follows:<br/>
<br/>
2.	DEFINITIONS<br/>
2.1	"Affiliate Agreement or Agreement" shall mean this document and all responsibilities, obligations and bounds herein.<br/>
2.2	"AIS" shall mean Automated Info Solutions, Inc., a Utah (USA) registered corporation and all its subsidiaries.<br/>
2.3	"FSBOLeadsUSA" ("FLUSA") is a wholly owned subsidiary of AIS.<br/>
2.4	"Affiliate" shall mean any person or business entity, which, directly or indirectly, controls or is controlled by, or is under common control with the Affiliate to this agreement. Affiliate is a signatory to this agreement.<br/>
2.5	"Referral" refers to the status of those receiving a one-time payment for New Customer Acquisition rather than a monthly payment. This status change requires a separate Referral Agreement.<br/>
2.6	"Prospect" shall mean an individual or entity with whom the Affiliate seeks to send to FLUSA.<br/>
2.7	"Customer" shall mean a prospect supplied by Affiliate that has completely filled out a FLUSA Subscription Form, made a payment and has therefore become a FLUSA Customer. FLUSA will provide a monthly report of Affiliate referred Customers. "New Customer" refers to a prospect signing a Contract with FLUSA and does not refer to current customers adding additional areas.<br/>
2.8	"Customer Contract" shall refer to the Agreement between FLUSA and the Customer.<br/>
2.9	"Customer Term" shall mean the duration of a valid contract between FLUSA and Customer and includes a Customer start date. The Customer Term shall cease immediately if customer payment fails for any reason or if any part of the Customer contract is breached.<br/>
2.10	"Affiliate Link" shall refer to a specific link found on Affiliate's website that re-directs users to the FLUSA website.<br/>
2.11	"Affiliate ID" shall refer to an identification number assigned to Affiliate. The ID is also a part of the Affiliate Link. When users are redirected from Affiliate's website, the ID number is tracked by FLUSA.<br/>
2.12	"FSBO data" shall mean any communication from FLUSA about For-Sale-by-Owner information, knowledge, located on website, FSBO Leads and any other data that FLUSA distributes and/or makes available to the Affiliate and/or Prospect and/or Customer.<br/>
2.13	"FSBO Lead" shall refer to the For-Sale-by-Owner leads made available by FLUSA to the Affiliate and/or Prospect and/or Customer whether through email, database access or in any other manner.<br/>
2.14	"Referral fees" shall mean monthly consideration due to Affiliate.<br/>
2.15	"Confidential Information" shall mean the FSBO data, Affiliate Agreement, products, product information, direction/guidance, trade secrets, technical information, etc. disclosed in relation to this Agreement. Confidential information shall also include FSBO data and advice/information provided by FLUSA which is marked or identified as "confidential" or "proprietary;" or information which, by the nature of the circumstances surrounding the disclosure, ought in good faith to be treated as confidential.<br/>
2.16	"Effective Date" shall mean, with respect to the Affiliate Agreement, the date on which the Agreement is signed by FLUSA and listed at bottom of this Agreement.<br/>
2.17	"Email/DB-Only Product" Affiliate agrees that when working with Real Estate Professionals that FLUSA's email/db only product will be what is sold to Prospects. No restrictions are placed on any other sales prospects including Real Estate Investors, Mortgage Professionals, etc.<br/>
2.18	Free "30 Day Trial Period"  AIS allows Affiliate to extend a 30 day trial up to 3 areas. If greater than 3 areas required, Affiliate to send email notification to AIS. AIS does not require a free trial to be given to prospects.<br/>
<br/>
3.	RESPONSIBILITY OF FLUSA<br/>
3.1	FLUSA maintains a public facing website that contains general information and indicates geographical locations covered.<br/>
3.2	FLUSA will supply an HTML link to be located on Affiliate's website that redirects Affiliate's Prospects to FLUSA.<br/>
3.3	FLUSA has the sole responsibility to collect, process and distribute FSBO data to Customers provided by Affiliate. FLUSA will support the FSBO data and processes used for distribution.<br/>
3.4	FLUSA shall be responsible for all Custom interaction including billing, service and technical questions, etc. (All Customers are owned by FLUSA; see 4.4)<br/>
3.5	FLUSA is not restricted from using other affiliates, referrers, private label relationships, subsidiaries, AIS or directly soliciting Customers.<br/>
3.6	FLUSA updates its downloaded U.S. Do Not Call Registry database as required by the FTC. However, FLUSA Customers are responsible for 'Do not Call' compliance and this is indicated in the FLUSA/Customer contract. Customer and/or Affiliate assumes all liability when using phone numbers/addresses/email/etc. supplied by FLUSA including those a part of the U.S. National 'Do Not Call Registry'. Customer assumes all responsibility to identify phone numbers associated with the U.S. National 'Do Not Call Registry'.<br/>
<br/>
 <br/>
4.	RESPONSIBILITY OF AFFLIATE<br/>
4.1	Affiliate, using its own resources, is solely responsible to seek and to direct Prospects to FLUSA.<br/>
4.2	New Customer Acquisition requirements: Affiliate agrees to add a minimum of three New customers during the first full month after the Effective Date is established. Thereafter, Affiliate agrees to add a minimum of three New Customers each three months. If Affiliate does not meet these requirements then the Agreement will be considered in material breach and FLUSA may choose to change Affiliate's status to that of Referral (2.5) or to terminate the Agreement. (7.2)<br/>
4.3	Affiliate should display the FLUSA URL link and/or be prepared to make known to Prospects the specific Affiliate ID or remind Prospect to announce the Affiliate's name during a phone call to FLUSA. It is the responsibility of the Affiliate to educate its Prospects about the Affiliate ID.<br/>
4.4	Affiliate agrees that all Customer information shall be owned by FLUSA including the actual Customer. FLUSA is responsible for all Customer Contract related communication.<br/>
4.5	All FSBO data supplied is only for internal use of specific Customer and/or Prospect and/or Affiliate. Affiliate/Prospect/Customer may not forward, sell, lease, lend or give FSBO data to any other party without explicit written (email is fine) permission from FLUSA. Violation of this will result in an immediate termination of the Customer Contract. Unless agreed upon separately, Brokers/Managers may not forward FSBO Leads onto Agents/Lenders, etc. unless explicit permission in writing has been issued. A group Customer contract is required for non-individual situations.<br/>
4.6	Affiliate agrees to show/explain to FLUSA any and all marketing materials, approaches, or other means, etc. that Affiliate uses to present FLUSA to Prospects.<br/>
4.7	Affiliate is to disclose any illegal or unethical use of the FSBO data. FLUSA will not knowingly work with Affiliates/Customers that engage in illegal or unethical use of FSBO data. <br/>
4.8	Affiliate agrees to allow FLUSA to publish a public document (Press Release, Web Site announcement or other) describing in general terms the nature of the relationship and services rendered.<br/>
<br/>
5.	INDEMNITY/WARRANTIES, LIMITATION<br/>
5.1	Affiliate at its own cost and expense, will hereby indemnify FLUSA and agrees that no express or implied warranty is made and holds harmless FLUSA from any claim arising from searching and/or extracting and/or receiving/viewing/downloading FSBO data including claims of infringement of patent, copyright, trade secret, misappropriation, U.S. Do Not Call Registry claims or disputes, distribution of viruses, FLUSA website lack of access or slowness, trespass or similar proprietary rights of a third party in the United States.<br/>
5.2	FLUSA shall in no event be liable for loss of profit or other special or consequential damages suffered by the Affiliate and/or Customer or others as a result of FLUSA's performance or nonperformance whether deemed intentional or unintentional. FLUSA does not guarantee the correctness or completeness of any FSBO data or other information/advice furnished or timeliness of delivery.<br/>
<br/>
6.	RIGHTS AND DATA OWNERSHIP<br/>
6.1	FLUSA exclusively retains all rights and ownership associated with its data collection processes, distribution and website.<br/>
6.2	FLUSA exclusively retains all rights and ownership of the FSBO data. Affiliate and/or Customer may only use the data individually or as spelled out in a specific Customer contract.<br/>
<br/>
7.	TERM AND TERMINATION<br/>
7.1	Begin. The Term of this Agreement shall begin on the Effective Date (2.16) as set forth in this Agreement.<br/>
7.2	Affiliate Agreement Termination. Either party may terminate this Agreement for any reason provided thirty (30) days written notice is given. If termination is by FLUSA, referral fees will continue for ninety (90) days unless Affiliate caused material breach is the impetus for termination. If termination of this Agreement is by Affiliate through its own will or by causing a material breach, all referral fees to Affiliate will cease immediately. If Affiliate does not meet the New Customer Acquisition requirement, FLUSA will choose to create a probationary period or to change the Affiliate's status to that of a Referral (2.5) or to terminate the Agreement due to material breach. (4.2) All Customers are owned by FLUSA. (4.4)<br/>
7.3	Effect of Termination. Termination of this Agreement shall not prevent FLUSA from pursuing other remedies available to it, including injunctive relief, nor shall such termination relieve Affiliate from obligations that shall survive termination of this Agreement such as Confidentiality and all other applicable.<br/>
7.4	Customer Termination. Should the Customer Term (2.9) cease, Affiliate Consideration for the specific Customer will likewise immediately end.<br/>
<br/>
8.	GENERAL TERMS<br/>
8.1	Confidentiality. Except as otherwise noted in this Agreement or with written consent by FLUSA, Affiliate agrees not to disclose any proprietary information including, without limitation, the terms of this Agreement, information about FLUSA including but not limited to prospect or customer or partner/Affiliate lists, competitors, marketing and sales plans, technologies and methodologies employed.<br/>
8.2	Duration of Confidentiality. Affiliate agrees to hold FLUSA Confidential Information in confidence during the term of this Agreement and for a period of three (3) years after termination of this Agreement. The parties agree that, unless required by law, they shall not make each other's Confidential Information, including this Agreement, available in any form to any third party or to use each other's Confidential Information for any purpose other than the implementation of this Agreement. Each party agrees to take all reasonable steps to ensure that Confidential Information is not disclosed or distributed by its employees or affiliates or agents in violation of the terms of this Agreement. Furthermore, Affiliate agrees not to use any Confidential Information of FLUSA to create any program or user documentation that is substantially similar FLUSA services without explicit written permission.<br/>
8.3	Governing Law and Jurisdiction. This Agreement, and all matters arising out of or relating to this Agreement, shall be governed by the laws of the State of Utah, USA. FLUSA and Affiliate, by entering into this Agreement, submit to jurisdiction in the State of Utah, USA, for adjudication of any disputes and/or claims between the parties under this Agreement. Furthermore, the parties hereby agree that the State of Utah shall have exclusive jurisdiction over any disputes between the parties relative to this Agreement, whether said disputes sounds in contract, tort, or other areas of the law. Nevertheless, FLUSA, shall be entitled to seek injunctive and other redress in any court in relation to infringement of intellectual property rights in the FSBO data.<br/>
8.4	Notice. All notices, including notices of address change, required to be sent under this Agreement shall be in writing and shall be deemed to have been given when mailed by first class mail to the relevant address listed in the signature blocks of this Agreement or the address stated in any applicable notice of change of address. To expedite order processing, Affiliate agrees that FLUSA may treat documents faxed by Affiliate to FLUSA as original documents; nevertheless, either party may require the other to exchange original signed documents.<br/>
8.5	Severability. In the event any provision of this Agreement is held to be invalid or unenforceable, the remaining provisions of this Agreement shall remain in full force.<br/>
8.6	Waiver. The waiver by either party of any default or breach of this Agreement shall not constitute a waiver of any other or subsequent default or breach. No action, regardless of form, arising out of the Agreement may be brought by Affiliate more than six (6) months after the cause of action has occurred.<br/>
8.7	Disclaimer. FSBO data is provided "as is" without warranty of any kind. Further, FLUSA does not warrant, guarantee, or make any representations regarding the use, or the results of the use of the FSBO data or written materials in terms of correctness (including U.S. Do Not Call indications), accuracy, reliability, currency, or otherwise. The Affiliate and/or Customer assumes the entire risk to the results and performance of the FSBO data. No oral or written information or advice given by FLUSA, or its employees/consultants shall create a warranty or in any way increase the scope of this warranty, and the Affiliate may not rely on any such information or advice.<br/>
8.8	Force Majeure. FLUSA will not be responsible for failure of performance due to causes beyond its control, including, without limitation, acts of God or nature; labor disputes; sovereign acts of any federal, state or foreign governments; acts of or ramifications caused by terrorists or others; or shortage of materials including electricity, lack of internet connectivity or blockage, etc.<br/>
8.9	Relationship between the Parties. FLUSA and Affiliate are considered Independent Contractors. Nothing in this Agreement shall be construed to create a partnership, joint venture or agency relationship between the parties.<br/>
8.10	Assignment and Third Party Beneficiaries. FLUSA may assign this agreement and any of its rights or obligations under it to any third party, provided that it notifies Affiliate prior to doing so. Affiliate may not assign this agreement except with written permission submitted to FLUSA and acceptance acknowledged by FLUSA.<br/>
8.11	Entire Agreement. This Agreement constitutes the complete agreement between the parties and supersedes all prior agreements and representations, written or oral, concerning the subject matter of this Agreement. This Agreement may not be modified or amended except in a writing signed by a duly authorized representative of each party; no other act, document, usage or custom shall be deemed to amend or modify this Agreement.<br/>
<br/>
9.	CONSIDERATION/TAXES/REPORTING/PRICING<br/>
9.1	Consideration eligibility. Affiliate Prospects must use the supplied FLUSA link or enter in the Affiliates ID number directly on the FLUSA website or if phoning FLUSA, the Affiliate name must be indicated. There is no other eligibility option. Note: If a person signs up on the FLUSA website and does not enter the Affiliate ID number, FLUSA will not be held to accountable to pay an Affiliate.<br/>
9.2	Affiliate Consideration duration. Consideration will continue as long as this Affiliate Agreement is in good standing and at least one referred Customer is active. Should either condition lapse then Consideration and the Agreement will immediately cease. (7.2)<br/>
9.3	Taxes. As an Independent Contractor, Affiliate is responsible for all taxes and any fees.<br/>
9.4	Reporting. FLUSA will supply a monthly report of all Customers associated with Affiliate. The monthly report will contain appropriate Customer additions, modifications, termination, etc. <br/>
9.5	Customer Pricing. Affiliate's Prospects will use either the standard FLUSA pricing as found on the FLUSA website (www.fsboleadsusa.com) or an agreed preferred pricing as agreed upon by Affiliate and FLUSA. Pricing is automatically presented to Prospects on the FLUSA website. <br/>
 	Monthly pricing:<br/>
Preferred pricing: $_____ per Customer/1st area/monthly. (min. $25, no max., see 9.6)<br/>
Preferred pricing: $_____ per Customer/2+ area(s)/monthly. (min. of $5, no max., see 9.6)<br/>
 	Annual pricing:<br/>
Preferred pricing: $_____ per Customer/1st area/monthly. (min. $300, no max., see 9.6)<br/>
Preferred pricing: $_____ per Customer/2+ area(s)/monthly. (min. $60, no max., see 9.6)<br/>
9.6	Affiliate Payment. Affiliate will be paid monthly. Customer Term begin date and/or Termination of service will render a pro-rated payment (based on a 30 day month) to Affiliate as is appropriate. Any non-standard rates, such as a group or promotional rate, require written (email is sufficient) FLUSA approval and acceptance. After the Customer Term service starts, Affiliate will be paid the following month on or near the 15th.<br/>
<br/>
If a 1st area price of $39 is used, the following percentage revenue sharing applies:<br/>
1-9 Total Current Customers @ 50% of total subscription revenue to Affiliate.<br/>
10-19 Total Current Customers @ 55% of total subscription revenue to Affiliate.<br/>
20+ Total Current Customers @ 60% of total subscription revenue to Affiliate.<br/>
 	  Example: 23 Customers: First 1-9 (10) @50%, 10-19 (10) @55%, 20-23 (4) @60%.<br/>
 	  Total current customers is measured on the last business day of each month.<br/>
 	  If a refund must be given to a Customer, the cost will be subtracted from the next Affiliate payment. This applies to monthly, annual and any other type of Customer arrangement.<br/>
 	A 3% Credit Card charge will be assessed against your revenue share.<br/>
<br/>
By signature on this Agreement, FLUSA and Affiliate agree to all terms in this Affiliate Agreement.<br/>
<br/>
FLUSA<br/>
(An AIS subsidiary)<br/>
Phone/Fax: (801) 796-4636<br/>
105 South State Street<br/>
#204<br/>
Orem, UT  84058<br/>
</div>
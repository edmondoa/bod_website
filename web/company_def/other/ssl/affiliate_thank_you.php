<?php
require_once($_SESSION['web_interface']->get_server_path('other/misc/select_options.php'));
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>
<HEAD>
	<LINK href="<?= $_SESSION['web_interface']->get_path('css/other.css') ?>" rel="stylesheet" type="text/css">
	<script src="<?= $_SESSION['web_interface']->get_path('js/global.js') ?>" type="text/javascript"></script>
</HEAD>
<BODY bgColor="#ffffff">
<center>
<table width="699" height="355" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="7" valign="top"><img src="/web/company_def/img/spacer.gif" width="7" height="1"></td>
<td width="684" valign="top">
<table border="0" cellspacing="0" cellpadding="1" bgcolor="#DDDDDD">
<tr><td valign="top"><table width="682" height="353" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
<tr>
<td valign="top" height="3" bgcolor="#D9E2E6"><img src="/web/company_def/img/spacer.gif"></td>
</tr>
<tr>
<td valign="top" height="350" style="padding:11px 17px 10px 15px; line-height:14px " class="big">
<strong class="blue">Thank You</strong><br>
<br style="line-height:10px ">
<div style="height:1px; background-color:#A0A0A0 "><img src="/web/company_def/img/spacer.gif"></div>
<br style="line-height:10px ">
<table width="100%" cellspacing="0" cellpadding="2" border="0">
<tr>
<td align="left" valign="top">
<span class="big2">
Your information has been submitted and will be processed within 2 business days. If you have any questions or concerns please feel free to <a href="mailto: <?= $_SESSION['o_company']->get_main_email_address() ?>">email us</a> or call us at 801-494-0470.
</span></td>
</tr>
</table>
</td>
</tr>
<tr>
</tr>
</table>
</td></tr>
</table>
</td>
<td width="8" valign="top"><img src="/web/company_def/img/spacer.gif" width="8" height="1"></td>
</tr>
</table>
</TD>
</TR>
<TR>
<TD>
<IMG SRC="/web/company_def/img/spacer.gif" WIDTH=699 HEIGHT=12 ALT=""></TD>
</TR>
</TABLE>
</center>
</body>
</html>

<?php
require_once($_SESSION['web_interface']->get_server_path('other/misc/select_options.php'));
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>
<HEAD>
	<LINK href="<?= $_SESSION['web_interface']->get_path('css/other.css') ?>" rel="stylesheet" type="text/css">
	<script src="<?= $_SESSION['web_interface']->get_path('js/global.js') ?>" type="text/javascript"></script>
</HEAD>
<BODY bgColor="#ffffff">
<center>
<form name="myForm" id="myForm" method="post" action="/ssl/handle.html" return onSubmit="return checkEmail(document.getElementById('main_email_address'));">
<input type="hidden" name="cmd" value="update_credit_card" />
<table border="0" cellspacing="0" cellpadding="1" bgcolor="#DDDDDD">
	<tr>
		<td valign="top">
			<table width="682" height="353" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
				<tr>
					<td valign="top" height="3" bgcolor="#D9E2E6"><img src="/web/company_def/img/spacer.gif"></td>
				</tr>
				<tr>
					<td valign="top" height="350" style="padding:11px 17px 10px 15px; line-height:14px " class="big">
						<strong class="blue">Credit Card Update Form - <?= $_SESSION['o_company']->get_title() ?></strong><br>
						<br style="line-height:10px ">
						<div style="height:1px; background-color:#A0A0A0 "><img src="/web/company_def/img/spacer.gif"></div>
						<br style="line-height:10px ">
						<table width="100%" cellspacing="0" cellpadding="2" border="0">
							<tr>
								<td align="left" valign="top">
									<span class="header1">Thank you for updating your Credit Card information below:</span>
									<br><br>
									* indicates required field
									<?php include_once($_SESSION['web_interface']->get_server_path('other/misc/status_error.php')); ?>
									<TABLE width="85%" CELLSPACING="0" CELLPADDING="1" BORDER="0">
										<TR>
											<TD COLSPAN="4" ALIGN="left"><span class="header1"><strong>1. Contact Information</strong></span></td>
										</TR>
										<TR>
											<TD ALIGN="RIGHT"><span <?= $_SESSION['web_interface']->missing_required_label('first_name') ?>>First Name*</span></td>
											<TD><input type="text" name="first_name" size="20" value="<?= $_REQUEST['first_name'] ?>" <?= $_SESSION['web_interface']->missing_required_input('first_name') ?>></td>
											<TD align="RIGHT"><span <?= $_SESSION['web_interface']->missing_required_label('last_name') ?>>Last Name*</span></td>
											<TD><input type="text" name="last_name" size="25" value="<?= $_REQUEST['last_name'] ?>" <?= $_SESSION['web_interface']->missing_required_input('last_name') ?>></td>
										</tr>
										<TR>
											<TD ALIGN="right">Company</td>
											<TD><input type="text" name="business_name" size="20" value="<?= $_REQUEST['business_name'] ?>"></td>
											<TD ALIGN="RIGHT">Address</td>
											<TD><input type="text" name="line1" size="25" value="<?= $_REQUEST['line1'] ?>"></td>
										</tr>
										<TR>
											<TD ALIGN="right">City</td>
											<TD><input type="text" name="city" size="20" value="<?= $_REQUEST['city'] ?>"></td>
											<TD align="right">State&nbsp;</td>
											<TD align="left">
												<!-- Table for two cells of State and ZIP -->
												<TABLE CELLSPACING="0" CELLPADDING="0" BORDER="0">
													<tr>
														<TD>
															<select name="state" <?= $_SESSION['web_interface']->missing_required_input('state') ?>>
																<option value=""></option>
																<?= select_options( array('type'=>'us_state', 'selected'=>$_REQUEST['state']) ) ?>
															</select>
														</td>
														<TD align="right">&nbsp; Zip&nbsp;</td>
														<TD><input type="text" name="postal_code" size="3" value="<?= $_REQUEST['postal_code'] ?>"></td>
													</tr>
												</TABLE>
											</TD>
										</tr>
										<TR>
											<TD ALIGN="right"><span <?= $_SESSION['web_interface']->missing_required_label('personal_phone') ?>>Main Phone*</span></td>
											<TD><input type="text" name="personal_phone" size="20" value="<?= $_REQUEST['personal_phone'] ?>" <?= $_SESSION['web_interface']->missing_required_input('personal_phone') ?>></td>
											<TD ALIGN="RIGHT">Work Phone</td>
											<TD><input type="text" name="business_phone" size="25" value="<?= $_REQUEST['business_phone'] ?>"></td>
										</tr>
										<TR>
											<TD ALIGN="RIGHT"><span <?= $_SESSION['web_interface']->missing_required_label('main_email_address') ?>>Email address*</span></td>
											<TD><input type="text" name="main_email_address" size="20" value="<?= $_REQUEST['main_email_address'] ?>" <?= $_SESSION['web_interface']->missing_required_input('main_email_address') ?>></td>
											<TD ALIGN="RIGHT">Email (verify)&nbsp;</td>
											<TD><input type="text" name="confirm_email_address" size="25" value="<?= $_REQUEST['confirm_email_address'] ?>"></td>
										</tr>
									</table>
									<br>
									<!-- Credit Card Info Table -->
									<TABLE width="75%" CELLSPACING="0" CELLPADDING="0" BORDER="0">
										<TR>
											<TD COLSPAN="4" align="left"><span class="header1"><strong>2. Credit Card Information</strong></span></td>
										</TR>
										<TR>
											<TD ALIGN="right"><span <?= $_SESSION['web_interface']->missing_required_label('card_name') ?>>Name on Card*</span></td>
											<TD><input type="text" name="card_name" size="20" value="<?= $_REQUEST['card_name'] ?>" <?= $_SESSION['web_interface']->missing_required_input('card_name') ?>></td>
											<TD ALIGN="RIGHT"><span <?= $_SESSION['web_interface']->missing_required_label('card_type') ?>>Credit Card Type*</span></td>
											<TD>
												<select name="card_type" <?= $_SESSION['web_interface']->missing_required_input('card_type') ?>>
													<option value="Click to Select">Click to Select</option>
													<option value="Visa" <?php if ($_REQUEST['card_type'] == 'Visa') { print 'selected="selected"'; } ?>>Visa</option>
													<option value="MC" <?php if ($_REQUEST['card_type'] == 'MC') { print 'selected="selected"'; } ?>>MasterCard</option>
													<option value="Disc" <?php if ($_REQUEST['card_type'] == 'Disc') { print 'selected="selected"'; } ?>>Discover</option>
													<option value="AmEx" <?php if ($_REQUEST['card_type'] == 'AmEx') { print 'selected="selected"'; } ?>>AmEx</option>
												</select>
											</td>
										</tr>
										<TR>
											<TD ALIGN="right"><span <?= $_SESSION['web_interface']->missing_required_label('card_number') ?>>Credit Card #*</span></td>
											<TD><input type="text" name="card_number" size="20" value="<?= $_REQUEST['card_number'] ?>" <?= $_SESSION['web_interface']->missing_required_input('card_number') ?>></td>
											<TD ALIGN="RIGHT"><span <?= $_SESSION['web_interface']->missing_required_label('card_exp_date') ?>>Expiration mmyy*</span></td>
											<TD><input type="text" name="card_exp_date" size="5" value="<?= $_REQUEST['card_exp_date'] ?>" <?= $_SESSION['web_interface']->missing_required_input('card_exp_date') ?>></td>
										</tr>
										<TR>
											<TD>&nbsp;</td>
										</tr>
										<TR>
											<TD ALIGN="LEFT"><input type="submit" name="submit" value="Submit"></td>
											<TD ALIGN="LEFT"><input type="reset" name="reset" value="Clear"></td>
										</tr>
									</TABLE>
									<!-- Option 1 -->
									<input type="hidden" name="subject" value="Credit Card Update">
									<!--	Option 2  -->
									<input type="hidden" name="required_fields" value="credit_card_name">
									<!-- Option 3 -->
									<input type="hidden" name="next_url" value="http://byownerdaily.com/gracias_g.html">
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
				</tr>
			</table>
		</td>
	</tr>
</table>
		</TD>
	</TR>
	<TR><TD><IMG SRC="/web/company_def/img/spacer.gif" WIDTH=699 HEIGHT=12 ALT=""></TD></TR>
</TABLE>
</form>
</center>
</body>
</html>

<?php
$_META_DESCRIPTION = '';
$_META_KEYWORDS = '';
$_PAGE_TITLE = '';

include_once($_SESSION['web_interface']->get_server_path('other/global/top.php'));
include_once($_SESSION['web_interface']->get_server_path('other/misc/status_error.php'));
?>
<table width="699" height="355" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="7" valign="top"><img src="/web/company_def/img/spacer.gif" width="7" height="1"></td>
		<td width="684" valign="top">
			<table border="0" cellspacing="0" cellpadding="1" bgcolor="#DDDDDD">
				<tr>
					<td valign="top">
						<table width="682" height="353" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
							<tr><td valign="top" height="3" bgcolor="#D9E2E6"><img src="/web/company_def/img/spacer.gif"></td></tr>
							<tr>
								<td valign="top" height="350" style="padding:11px 17px 10px 15px; line-height:14px " class="big">
									<strong class="blue">FSBO Referral, FSBO Affiliate, FSBO Private Label</strong><br>
									<br style="line-height:10px ">
									<div style="height:1px; background-color:#A0A0A0 "><img src="/web/company_def/img/spacer.gif"></div>
									<br style="line-height:10px ">
									<table width="100%" cellspacing="0" cellpadding="2" border="0">
										<tr>
											<td align="left" valign="top">
												<h2 class="big14">Our FSBO sharing programs are the best in the industry--they pay the most <font color="GREEN"><b>$$$</b></font>.</h2>
												<h2 class="big14">FSBO Affiliate</h2>
												<h2 class="big14">FSBO Referral</h2>
												<h2 class="big14">FSBO Private Label</h2>		
												<font class="big14">Interested referring, being an Affiliate or running a Private Label FSBO Service?
												<br><br>Whether you represent a company or yourself, we have a program for you.
												<br>Let us know your interest or questions you have and lets start working together!</font>
												* indicates required field
												<br>
												<form method="post" action="/ssl/handle.html" return onSubmit="return checkEmail(document.getElementById('email'));">
												<input type="hidden" name="cmd" value="send_request_affiliate" />
												<TABLE CELLSPACING="0" CELLPADDING="0" BORDER="0" ALIGN="LEFT">
													<TR>
														<TD ALIGN="right" <?= $_SESSION['web_interface']->missing_required_label('program_selection') ?>><span style="font-size:14">Program Type*</span><img src="/web/company_def/img/spacer.gif" width="6" height="1" border="0"></td>
														<TD>
															<select name="program_selection" <?= $_SESSION['web_interface']->missing_required_input('program_selection') ?>>
																<option value="">Click to Select</option>
																<option value="Referral" <?php if ($_REQUEST['program_selection'] == 'Referral') { print 'selected="selected"'; } ?>>Referral</option>
																<option value="Affiliate" <?php if ($_REQUEST['program_selection'] == 'Affiliate') { print 'selected="selected"'; } ?>>Affiliate</option>
																<option value="Private Label" <?php if ($_REQUEST['program_selection'] == 'Private Label') { print 'selected="selected"'; } ?>>Private Label</option>
																<option value="Other" <?php if ($_REQUEST['program_selection'] == 'Other') { print 'selected="selected"'; } ?>>Other</option>
															</select>
														</td>
													</tr>
													<TR>
														<TD ALIGN="right" <?= $_SESSION['web_interface']->missing_required_label('name') ?>><span style="font-size:14">Name*</span><img src="/web/company_def/img/spacer.gif" width="6" height="1" border="0"></td>
														<TD><input type="text" name="name" size="30" value="<?= $_REQUEST['name'] ?>" <?= $_SESSION['web_interface']->missing_required_input('name') ?> /></td>
													</tr>
													<TR>
														<TD ALIGN="right" <?= $_SESSION['web_interface']->missing_required_label('company') ?>><span style="font-size:14">Company</span><img src="/web/company_def/img/spacer.gif" width="6" height="1" border="0"></td>
														<TD><input type="text" name="company" size="30" value="<?= $_REQUEST['company'] ?>" <?= $_SESSION['web_interface']->missing_required_input('company') ?> /></td>
													</tr>
													<TR>
														<TD ALIGN="right" <?= $_SESSION['web_interface']->missing_required_label('email') ?>><span style="font-size:14">Email*</span><img src="/web/company_def/img/spacer.gif" width="6" height="1" border="0"></td>
														<TD><input type="text" id="email" name="email" onBlur="checkEmail(this);" size="30" value="<?= $_REQUEST['email'] ?>" <?= $_SESSION['web_interface']->missing_required_input('email') ?> /></td>
													</tr>
													<TR>
														<TD ALIGN="right" <?= $_SESSION['web_interface']->missing_required_label('phone') ?>><span style="font-size:14">Phone*</span><img src="/web/company_def/img/spacer.gif" width="6" height="1" border="0"></td>
														<TD><input type="text" name="phone" size="30" value="<?= $_REQUEST['phone'] ?>" <?= $_SESSION['web_interface']->missing_required_input('phone') ?> /></td>
													</tr>
													<TR>
														<TD ALIGN="right" <?= $_SESSION['web_interface']->missing_required_label('geography') ?>><span style="font-size:14">Geography?</span><img src="/web/company_def/img/spacer.gif" width="6" height="1" border="0"></td>
														<TD><textarea cols="35" rows="2" name="geography" <?= $_SESSION['web_interface']->missing_required_input('geography') ?>><?= $_REQUEST['geography'] ?></textarea></td>
													</tr>
													<TR>
														<TD ALIGN="right" <?= $_SESSION['web_interface']->missing_required_label('additional_information') ?>><span style="font-size:14">Additional Information?</span><img src="/web/company_def/img/spacer.gif" width="6" height="1" border="0"></td>
														<TD><textarea cols="35" rows="2" name="additional_information" <?= $_SESSION['web_interface']->missing_required_input('additional_information') ?>><?= $_REQUEST['additional_information'] ?></textarea></td>
													</tr>
													<TR>
														<TD ALIGN="right" <?= $_SESSION['web_interface']->missing_required_label('questions') ?>><span style="font-size:14">Questions?</span><img src="/web/company_def/img/spacer.gif" width="6" height="1" border="0"></td>
														<TD><textarea cols="35" rows="2" name="questions" <?= $_SESSION['web_interface']->missing_required_input('questions') ?>><?= $_REQUEST['questions'] ?></textarea></td>
													</tr>
													<TR><TD>&nbsp;</td></tr>
													<TR><TD ALIGN="LEFT" colspan="2"><input type="submit" name="submit" value=" Submit ">&nbsp;<input type="reset" name="reset" value=" Clear "></td></tr>
												</TABLE>
												</form>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
		<td width="8" valign="top"><img src="/web/company_def/img/spacer.gif" width="8" height="1"></td>
	</tr>
</table>
		</TD>
	</TR>
	<TR><TD><IMG SRC="/web/company_def/img/spacer.gif" WIDTH=699 HEIGHT=12 ALT=""></TD></TR>
</TABLE>
<?php
include_once($_SESSION['web_interface']->get_server_path('other/global/bottom.php'));
?>
<?php
$_META_DESCRIPTION = '';
$_META_KEYWORDS = '';
$_PAGE_TITLE = '';

include_once($_SESSION['web_interface']->get_server_path('other/global/top.php'));
include_once($_SESSION['web_interface']->get_server_path('other/misc/status_error.php'));
?>
<table width="699" height="355" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="7" valign="top"><img src="/web/company_def/img/spacer.gif" width="7" height="1"></td>
		<td width="684" valign="top">
			<table border="0" cellspacing="0" cellpadding="1" bgcolor="#DDDDDD">
				<tr>
					<td valign="top">
						<table width="682" height="353" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
							<tr><td valign="top" height="3" bgcolor="#D9E2E6"><img src="/web/company_def/img/spacer.gif"></td></tr>
							<tr>
								<td valign="top" height="350" style="padding:11px 17px 10px 15px; line-height:14px " class="big">
									<strong class="blue">Can't Find Area &nbsp;or &nbsp;Requesting New Area Service</strong><br>
									<br style="line-height:10px ">
									<div style="height:1px; background-color:#A0A0A0 "><img src="/web/company_def/img/spacer.gif"></div>
									<br style="line-height:10px ">
									<table width="100%" cellspacing="0" cellpadding="0" border="0">
										<tr>
											<td>
												<table width="100%" cellspacing="0" cellpadding="2" border="0">
													<tr>
														<td align="left" valign="top">
															Either you couldn't find the area you were looking for or our services are not presently available in your area.
															<br>
															We can help research the appropriate Area to match that which you seek--just let us know. In addition, we are expanding quickly and will supply additional areas soon.
															<br><br>
															<?php include_once($_SESSION['web_interface']->get_server_path('other/misc/status_error.php')); ?>
															Please let us know what area(s) are of interest:
															<br>
															<b>*</b> indicates required field
															<br>
															<form method="post" action="/ssl/handle.html" return onSubmit="return checkEmail(document.getElementById('email'));">
															<input type="hidden" name="cmd" value="send_request_area" />
															<TABLE CELLSPACING="0" CELLPADDING="0" BORDER="0" ALIGN="LEFT">
																<TR>
																	<TD ALIGN="right" <?= $_SESSION['web_interface']->missing_required_label('name') ?>><span style="font-size:14">Name<b>*</b></span><img src="/web/company_def/img/spacer.gif" width="6" height="1" border="0"></td>
																	<TD><input type="text" name="name" size="30" value="<?= $_REQUEST['name'] ?>" <?= $_SESSION['web_interface']->missing_required_input('name') ?> /></td>
																</tr>
																<TR>
																	<TD ALIGN="right" <?= $_SESSION['web_interface']->missing_required_label('company') ?>><span style="font-size:14">Company or Self</span><img src="/web/company_def/img/spacer.gif" width="6" height="1" border="0"></td>
																	<TD><input type="text" name="company" size="30" value="<?= $_REQUEST['company'] ?>" <?= $_SESSION['web_interface']->missing_required_input('company') ?> /></td>
																</tr>
																<TR>
																	<TD ALIGN="right" <?= $_SESSION['web_interface']->missing_required_label('email') ?>><span style="font-size:14">Email*</span><img src="/web/company_def/img/spacer.gif" width="6" height="1" border="0"></td>
																	<TD><input type="text" id="email" name="email" onBlur="checkEmail(this);" size="30" value="<?= $_REQUEST['email'] ?>" <?= $_SESSION['web_interface']->missing_required_input('email') ?> /></td>
																</tr>
																<TR>
																	<TD ALIGN="right" <?= $_SESSION['web_interface']->missing_required_label('phone') ?>><span style="font-size:14">Phone<b>*</b></span><img src="/web/company_def/img/spacer.gif" width="6" height="1" border="0"></td>
																	<TD><input type="text" name="phone" size="30" value="<?= $_REQUEST['phone'] ?>" <?= $_SESSION['web_interface']->missing_required_input('phone') ?> /></td>
																</tr>
																<TR>
																	<TD ALIGN="right" <?= $_SESSION['web_interface']->missing_required_label('hear_about_us') ?>><span style="font-size:14">How did you hear about us?</span><img src="/web/company_def/img/spacer.gif" width="6" height="1" border="0"></td>
																	<TD><textarea cols="24" rows="2" name="hear_about_us" <?= $_SESSION['web_interface']->missing_required_input('hear_about_us') ?>><?= $_REQUEST['hear_about_us'] ?></textarea></td>
																</tr>
																<TR>
																	<TD ALIGN="right" <?= $_SESSION['web_interface']->missing_required_label('areas_of_request') ?>><span style="font-size:14">Area(s) of Request*</span><img src="/web/company_def/img/spacer.gif" width="6" height="1" border="0"></td>
																	<TD><textarea cols="24" rows="2" name="areas_of_request" <?= $_SESSION['web_interface']->missing_required_input('areas_of_request') ?>><?= $_REQUEST['areas_of_request'] ?></textarea></td>
																<TR><TD ALIGN="LEFT">&nbsp;</td></tr>
																<tr><td colspan="2"><input type="submit" name="submit" value=" Submit ">&nbsp;<input type="reset" name="reset" value=" Clear "></td></tr>
															</table>
															</form>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
		<td width="8" valign="top"><img src="/web/company_def/img/spacer.gif" width="8" height="1"></td>
	</tr>
</table>
		</TD>
	</TR>
	<TR><TD><IMG SRC="/web/company_def/img/spacer.gif" WIDTH=699 HEIGHT=12 ALT=""></TD></TR>
</TABLE>
<?php
include_once($_SESSION['web_interface']->get_server_path('other/global/bottom.php'));
?>
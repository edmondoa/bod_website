<?php
$_REQUEST['state'] = ($_REQUEST['state']) ? $_REQUEST['state'] : $_REQUEST['subscribe_state'];

require_once('office/account/class.area_name.php');
$o_area_name_utilities = new area_name_utilities();
$area_state = $o_area_name_utilities->get_area_state( array('state'=>$_REQUEST['subscribe_state']) );
$list_area_name = $o_area_name_utilities->get_list( array('where'=>" WHERE area_name LIKE '" . $_SESSION['data_access']->db_quote($_REQUEST['subscribe_state']) . "-%' ") );

$_META_DESCRIPTION = '';
$_META_KEYWORDS = '';
$_PAGE_TITLE = $area_state . ' FSBO Leads | ' . $area_state . ' For Sale By Owner Leads | ' . $area_state . ' Real Estate Leads';

if (isset($_SESSION['affiliate'])) {
	$_affiliate_month_additional_area_pricing = $_SESSION['affiliate']->get_month_additional_area_pricing();
	$_affiliate_month_one_area_pricing = $_SESSION['affiliate']->get_month_one_area_pricing();
	$_affiliate_month_two_area_pricing = $_SESSION['affiliate']->get_month_two_area_pricing();
	$_affiliate_year_additional_area_pricing = $_SESSION['affiliate']->get_year_additional_area_pricing();
	$_affiliate_year_one_area_pricing = $_SESSION['affiliate']->get_year_one_area_pricing();
	$_affiliate_year_two_area_pricing = $_SESSION['affiliate']->get_year_two_area_pricing();
}
else {
	$_affiliate_month_additional_area_pricing = $_SESSION['o_company']->get_month_additional_area_pricing();
	$_affiliate_month_one_area_pricing = $_SESSION['o_company']->get_month_one_area_pricing();
	$_affiliate_month_two_area_pricing = $_SESSION['o_company']->get_month_two_area_pricing();
	$_affiliate_year_additional_area_pricing = $_SESSION['o_company']->get_year_additional_area_pricing();
	$_affiliate_year_one_area_pricing = $_SESSION['o_company']->get_year_one_area_pricing();
	$_affiliate_year_two_area_pricing = $_SESSION['o_company']->get_year_two_area_pricing();
}

$_regular_month_additional_area_pricing = $_SESSION['o_company']->get_month_additional_area_pricing();
$_regular_month_one_area_pricing = $_SESSION['o_company']->get_month_one_area_pricing();
$_regular_month_two_area_pricing = $_SESSION['o_company']->get_month_two_area_pricing();
$_regular_year_additional_area_pricing = $_SESSION['o_company']->get_year_additional_area_pricing();
$_regular_year_one_area_pricing = $_SESSION['o_company']->get_year_one_area_pricing();
$_regular_year_two_area_pricing = $_SESSION['o_company']->get_year_two_area_pricing();

require_once($_SESSION['web_interface']->get_server_path('other/misc/select_options.php'));
include_once($_SESSION['web_interface']->get_server_path('other/global/top.php'));
include_once($_SESSION['web_interface']->get_server_path('other/misc/status_error.php'));
?>
<script language="javascript" type="text/javascript">
	if (!document.all)
		document.captureEvents(Event.MOUSEMOVE)
	
	document.onmousemove = getPosition; 
	var X = 0
	var Y = 0 

	function getPosition(args) {
	  if (document.all) {
	    X = event.clientX + document.body.scrollLeft
	    Y = event.clientY + document.body.scrollTop
	  }
	  else { 
	    X = args.pageX
	    Y = args.pageY
	  } 
	}

	function popUp(popUpName) {
    var div;
    if(document.getElementById)
	    div = document.getElementById(popUpName);
    else if(document.all)
	    div = document.all[popUpName];   
    if(div.style.display== '' && div.offsetWidth != undefined && div.offsetHeight != undefined) {
        div.style.display = (div.offsetWidth!=0 && elem.offsetHeight!=0)?'block':'none';
    }
    div.style.display = (div.style.display==''||div.style.display=='block')?'none':'block';
    X = X + 15;   
    div.style.left = X+'px';
    div.style.top = Y+'px';
	}
	
	function calculateTotals() {
		var $total_areas = 0;
		for (var i=0;i<myForm.elements.length;i++) {
			var myElement = myForm.elements[i];
			if (myElement.type == 'radio') {
				var pattern = /^area_nameid_radio/;
				if (pattern.test(myElement.id)) {
					if (myElement.checked == true) {
						$total_areas = $total_areas + 1;
					}
				}
			}
		}
		var $total_month_affiliate_price = 0;
		var $total_month_regular_price = 0;
		var $total_year_affiliate_price = 0;
		var $total_year_regular_price = 0;
		
		if ($total_areas == 1) {
			$total_month_affiliate_price = <?= $_affiliate_month_one_area_pricing ?>;
			$total_month_regular_price = <?= $_regular_month_one_area_pricing ?>;
			$total_year_affiliate_price = <?= $_affiliate_year_one_area_pricing ?> * 12;
			$total_year_regular_price = <?= $_regular_year_one_area_pricing ?> * 12;
		}
		else if ($total_areas == 2) {
			$total_month_affiliate_price = <?= $_affiliate_month_two_area_pricing ?>;
			$total_month_regular_price = <?= $_regular_month_two_area_pricing ?>;
			$total_year_affiliate_price = <?= $_affiliate_year_two_area_pricing ?> * 12;
			$total_year_regular_price = <?= $_regular_year_two_area_pricing ?> * 12;
		}
		else if ($total_areas > 2) {
			$over_areas = $total_areas - 2;
			$over_month_affiliate_price = $over_areas * <?= $_affiliate_month_additional_area_pricing ?>;
			$over_month_regular_price = $over_areas * <?= $_regular_month_additional_area_pricing ?>;
			$total_month_affiliate_price = <?= $_affiliate_month_two_area_pricing ?> + $over_month_affiliate_price;
			$total_month_regular_price = <?= $_regular_month_two_area_pricing ?> + $over_month_regular_price;
			$over_year_affiliate_price = $over_areas * <?= $_affiliate_year_additional_area_pricing ?>;
			$over_year_regular_price = $over_areas * <?= $_regular_year_additional_area_pricing ?>;
			$total_year_affiliate_price = (<?= $_affiliate_year_two_area_pricing ?> + $over_year_affiliate_price) * 12;
			$total_year_regular_price = (<?= $_regular_year_two_area_pricing ?> + $over_year_regular_price) * 12;
		}
		document.getElementById('total_areas_display').innerHTML = $total_areas;
		myForm.total_areas.value = $total_areas;
		document.getElementById('total_month_areas_display').innerHTML = $total_areas;
		document.getElementById('total_year_areas_display').innerHTML = $total_areas;

		if ($total_month_affiliate_price != $total_month_regular_price) {
			document.getElementById('total_month_affiliate_price_display').style.display = 'inline';
			document.getElementById('total_month_regular_price_display').style.textDecoration = 'line-through';
		}
		else {
			document.getElementById('total_month_affiliate_price_display').style.display = 'none';
			document.getElementById('total_month_regular_price_display').style.textDecoration = 'none';
		}
		document.getElementById('total_month_affiliate_price_display').innerHTML = '$' + $total_month_affiliate_price;
		document.getElementById('total_month_regular_price_display').innerHTML = '$' + $total_month_regular_price;
		myForm.total_month_affiliate_price.value = $total_month_affiliate_price;
		myForm.total_month_regular_price.value = $total_month_regular_price;

		if ($total_year_affiliate_price != $total_year_regular_price) {
			document.getElementById('total_year_affiliate_price_display').style.display = 'inline';
			document.getElementById('total_year_regular_price_display').style.textDecoration = 'line-through';
		}
		else {
			document.getElementById('total_year_affiliate_price_display').style.display = 'none';
			document.getElementById('total_year_regular_price_display').style.textDecoration = 'none';
		}
		document.getElementById('total_year_affiliate_price_display').innerHTML = '$' + $total_year_affiliate_price;
		document.getElementById('total_year_regular_price_display').innerHTML = '$' + $total_year_regular_price;
		myForm.total_year_affiliate_price.value = $total_year_affiliate_price;
		myForm.total_year_regular_price.value = $total_year_regular_price;
	}
</script>
<form name="myForm" id="myForm" method="post" action="/ssl/handle.html" return onSubmit="return checkEmail(document.getElementById('main_email_address'));">
<input type="hidden" name="cmd" value="trial" />
<input type="hidden" name="subscribe_state" value="<?= $_REQUEST['subscribe_state'] ?>" />
<input type="hidden" name="total_areas" value="" />
<input type="hidden" name="total_month_affiliate_price" value="" />
<input type="hidden" name="total_month_regular_price" value="" />
<input type="hidden" name="total_year_affiliate_price" value="" />
<input type="hidden" name="total_year_regular_price" value="" />
<table width="699" height="355" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="7" valign="top"><img src="/web/company_def/img/spacer.gif" width="7" height="1"></td>
		<td width="684" valign="top">

			<!-- Main body Table -->
			<table border="0" cellspacing="0" cellpadding="1" bgcolor="#DDDDDD">
				<tr>
					<td valign="top">

						<!-- Inner1 Main body Table -->
						<table width="682" height="353" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
							<tr><td valign="top" height="3" bgcolor="#D9E2E6"><img src="/web/company_def/img/spacer.gif"></td></tr>
							<tr>
								<td valign="top" height="350" style="padding:11px 17px 10px 15px; line-height:14px " class="big">
									<strong class="blue">Free Trial - <?= $area_state ?> FSBO Leads</strong><br>
									<br style="line-height:10px ">
									<div style="height:1px; background-color:#A0A0A0 "><img src="/web/company_def/img/spacer.gif"></div>
									<br style="line-height:10px">

									<!-- Inner2 Main body Table -->
									<table width="100%" cellspacing="0" cellpadding="2" border="0">
										<tr>
											<td valign="top" class="seal_<?= $_REQUEST['state'] ?>_bg">
												<!--
												<span class="big2">Questions about the trial? <a href="/trial_faq.html">Click here for FAQ's</a></span>
												<br><br/>
												-->
												<span class="gray">
													For subscription pricing information, <a href="" onMouseOver="popUp('priceInfo');" onMouseOut="popUp('priceInfo');">mouse over here.</a>
													<div class="priceInfo" id="priceInfo" name="priceInfo" style="display: none;">
														<div class="priceInfoHeader">Subscription Pricing</div>

												<!-- Inner2 Subscription Pricing Table -->
												<TABLE class="priceInfoContent" CELLSPACING="0" CELLPADDING="5" BORDER="1">
													<TR>
														<TD align="CENTER">Areas</td>
														<TD align="center">Monthly</td>
														<TD align="center">Annual</td>
													</TR>
													<TR>
														<TD align="center">1</td>
														<td align="center"><span style="color: #FF0000; text-decoration: line-through;">$39</span> <strong>$<?= sprintf("%.0f", $_regular_month_one_area_pricing) ?></strong></td>
														<td align="center"><span style="color: #FF0000; text-decoration: line-through;">$348</span> <strong>$<?= sprintf("%.0f", (12*$_regular_year_one_area_pricing)) ?></strong></td>
													</tr>
													<TR>
														<TD align="center">2</td>
														<td align="center"><span style="color: #FF0000; text-decoration: line-through;">$39</span> <strong>$<?= sprintf("%.0f", $_regular_month_two_area_pricing) ?></strong></td>
														<td align="center"><span style="color: #FF0000; text-decoration: line-through;">$348</span> <strong>$<?= sprintf("%.0f", (12*$_regular_year_two_area_pricing)) ?></strong></td>
													</tr>
													<TR>
														<TD align="center">3</td>
														<td align="center"><span style="color: #FF0000; text-decoration: line-through;">$49</span> <strong>$<?= sprintf("%.0f", ($_regular_month_two_area_pricing+$_regular_month_additional_area_pricing)) ?></strong></td>
														<td align="center"><span style="color: #FF0000; text-decoration: line-through;">$468</span> <strong>$<?= sprintf("%.0f", (12*($_regular_year_two_area_pricing+$_regular_year_additional_area_pricing))) ?></strong></td>
													</tr>
													<TR>
														<TD align="center">4</td>
														<td align="center"><span style="color: #FF0000; text-decoration: line-through;">$59</span> <strong>$<?= sprintf("%.0f", ($_regular_month_two_area_pricing+(2*$_regular_month_additional_area_pricing))) ?></strong></td>
														<td align="center"><span style="color: #FF0000; text-decoration: line-through;">$588</span> <strong>$<?= sprintf("%.0f", (12*($_regular_year_two_area_pricing+(2*$_regular_year_additional_area_pricing)))) ?></strong></td>
													</tr>
												</TABLE>
												</div>
												<br/><br/>Get started today! Your first 14 days are FREE!
												<br/>If you don't want to continue using <?= $_SESSION['o_company']->get_title() ?>,<br/>just cancel before the trial ends and you will not be charged.<br/><a href="" onMouseOver="popUp('cancelTrial');" onMouseOut="popUp('cancelTrial');">How do I cancel trial?</a> 
												<div class="priceInfo" id="cancelTrial" name="cancelTrial" style="display: none;">
													<div class="priceInfoHeader">How do I cancel trial?</div>
													<TABLE class="priceInfoContent" CELLSPACING="0" CELLPADDING="5" BORDER="0">
														<tr><td>Continue the signup process below. You will be asked for your email address and password. You will use these to login to your account to cancel your trial anytime.</td></tr>
													</table>
												</div>
												</span>
												<br><br>
												
												<!-- Select Service Areas Table -->
												<TABLE width="75%" CELLSPACING="0" CELLPADDING="0" BORDER="0">
													<TR>
														<TD COLSPAN="2" ALIGN="LEFT">
															<span class="header1"><strong>1. Choose 1 area for the trial period</strong></span><br/>
															&nbsp;&nbsp;&nbsp&nbsp;If you would like to try more than 1 area, please contact us at (801) 494-0470.<br/>&nbsp;&nbsp;&nbsp;&nbsp;For area details, click the area name or mouseover
														</td>
													</TR>
												</TABLE>

												<!-- Service Areas Table -->
												<TABLE width="75%" CELLSPACING="0" CELLPADDING="0" BORDER="0">
<?php
$count = 0;
foreach ($list_area_name as $item_area_name) {
	if (strtolower($item_area_name->get_area_title()) == 'other') {
		continue;
	}
?>
														<tr>
															<TD ALIGN="left">
																&nbsp;<input type="radio" id="area_nameid_radio" name="area_nameid_radio" onClick="calculateTotals();" style="border:0px;" value="<?= $item_area_name->get_area_nameid() ?>" <?php if ($_REQUEST['area_nameid_radio'] == $item_area_name->get_area_nameid()) { print 'checked="checked"'; } ?> /><a href="/areas_ind_<?= $item_area_name->get_area_nameid() ?>.html" onMouseOver="popUp('areaInfo<?= $item_area_name->get_area_nameid() ?>');" onMouseOut="popUp('areaInfo<?= $item_area_name->get_area_nameid() ?>');"><?= $item_area_name->get_area_title() ?></a>
																<div class="areaInfo" id="areaInfo<?= $item_area_name->get_area_nameid() ?>" name="areaInfo<?= $item_area_name->get_area_nameid() ?>" style="display: none;">
																	<div class="areaInfoHeader"><?= $item_area_name->get_area_title() ?></div>
																	<table class="areaInfoContent">
<?php
	if (!preg_match("/county\s?+$/i", $item_area_name->get_area_title()) && !preg_match("/counties\s?+$/i", $item_area_name->get_area_title())) {
?>
																		<tr><td>Description:</td><td><?= $item_area_name->get_description() ?></td></tr>
<?php
	}
?>
																		<tr><td>Cities:</td><td><?= $item_area_name->get_cities() ?></td></tr>
																	</table>
																</div>
															</TD>
														</tr>
<?php
	$count++;
}
?>
													<TR><TD>&nbsp;</td></tr>
												</TABLE>

												<TABLE WIDTH="95%" CELLSPACING="1" CELLPADDING="1" BORDER="0>			
													<TR>
														<TD><input type="checkbox" name="year_price_terms" value="I Accept Terms of 12 month Agreement" style="border:0px;" <?php if ($_REQUEST['year_price_terms']) { print 'checked="checked"'; } ?>>
														<b>** Due to low price for a prepaid 12-Month Agreement, I understand that
														<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														no refund/credit will be given except during the first 14 days.</b></td>
													</tr>
												</TABLE>
												<br>

												<!-- Inner2 Subscription Pricing Table -->				
												<TABLE WIDTH="77%" CELLSPACING="1" CELLPADDING="1" BORDER="1>
													<TR>
														<TD COLSPAN="1"><span class="header1"><strong>2. Subscription Pricing</strong></span>&nbsp;&nbsp;<span style="font-size: 11pt; font-weight: bold;">SPECIAL PROMOTION: 14 DAYS FREE&nbsp;&nbsp;&nbsp;Final Price: $0.00</span>
															<BR>&nbsp;&nbsp;&nbsp;&nbsp;(Choose your billing preference)
														</td>
													</TR>
													<TR>
														<TD align="CENTER">Areas</td>
														<TD align="center">12-Month Agreement**<b><font color="#0000ff"> Best Value!</font></b></td>
														<TD align="center">Month-to-Month<b><font color="#0000ff"> Good Value!</font></b></td>
													</TR>
													<TR>
														<TD align="center"><div id="total_areas_display" id="total_areas_display" style="display: inline; font-weight: bold;">0</div></td>
														<TD style="padding-left: 10px;">
															<input type="radio" name="subscription_type" value="Annual" style="border:0px;" onClick="alert('Note: To receive a 12-Month Agreement low price, you are required to select the Checkbox below labeled:\n**Due to low price for a prepaid 12-Month Agreement, I understand that no refund/credit will be given.')" <?php if ($_REQUEST['subscription_type'] == 'Annual') { print 'checked="checked"'; } ?> />
															<div id="total_year_regular_price_display" name="total_year_regular_price_display" style="display: inline; font-weight: bold;">$0</div>
															<div id="total_year_affiliate_price_display" name="total_year_affiliate_price_display" style="display: none; font-weight: bold; padding-left: 10px;">$0</div>
															<b>/Year</b> for <div id="total_year_areas_display" name="total_year_areas_display" style="display: inline; font-weight: bold;">0</div>&nbsp;areas
														</td>
														<TD style="padding-left: 10px;">
															<input type="radio" name="subscription_type" value="Monthly" style="border:0px;" <?php if ($_REQUEST['subscription_type'] == 'Monthly') { print 'checked="checked"'; } ?> />
															<div id="total_month_regular_price_display" name="total_month_regular_price_display" style="display: inline; font-weight: bold;">$0</div>
															<div id="total_month_affiliate_price_display" name="total_month_affiliate_price_display" style="display: none; font-weight: bold; padding-left: 10px;">$0</div>
															<b>/Month</b> for <div id="total_month_areas_display" name="total_month_areas_display" style="display: inline; font-weight: bold;">0</div>&nbsp;areas
														</td>
													</TR>
												</TABLE>
												<br>
												
												<TABLE WIDTH="95%" CELLSPACING="1" CELLPADDING="1" BORDER="0>			
													<TR><TD COLSPAN="1">**Prepaid billing - you will be charged this amount each Agreement period.</TD></TR>			
													<TR><TD COLSPAN="1">**12-Month Agreements receive a 14-day money back guarantee.</TD></TR>
													<TR><TD COLSPAN="1">Month-to-Month Agreements may cancel at any time.</TD></TR>
													<TR>
														<TD><input type="checkbox" name="year_price_terms" value="I Accept Terms of 12 month Agreement" style="border:0px;" <?php if ($_REQUEST['year_price_terms']) { print 'checked="checked"'; } ?>>
														<b>** Due to low price for a prepaid 12-Month Agreement, I understand that
														<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														no refund/credit will be given except during the first 14 days.</b></td>
													</tr>
												</TABLE>
												<br>
												
												<!-- Contact Info Table -->
												<TABLE width="85%" CELLSPACING="0" CELLPADDING="1" BORDER="0">
													<TR><TD COLSPAN="4" ALIGN="left"><span class="header1"><strong>3. Contact Information</strong></span></td></TR>
													<TR>
														<TD ALIGN="RIGHT" <?= $_SESSION['web_interface']->missing_required_label('first_name') ?>>First Name*</td>
														<TD><input type="text" name="first_name" tabindex="1" value="<?= $_REQUEST['first_name'] ?>" <?= $_SESSION['web_interface']->missing_required_input('first_name') ?>></td>
														<TD ALIGN="RIGHT" <?= $_SESSION['web_interface']->missing_required_label('line1') ?>>Address</td>
														<TD><input type="text" name="line1" tabindex="9" value="<?= $_REQUEST['line1'] ?>" <?= $_SESSION['web_interface']->missing_required_input('line1') ?>></td>
													</tr>
													<TR>
														<TD ALIGN="RIGHT" <?= $_SESSION['web_interface']->missing_required_label('last_name') ?>>Last Name*</td>
														<TD><input type="text" name="last_name" tabindex="2" value="<?= $_REQUEST['last_name'] ?>" <?= $_SESSION['web_interface']->missing_required_input('last_name') ?>></td>
														<TD ALIGN="RIGHT" <?= $_SESSION['web_interface']->missing_required_label('line2') ?>>&nbsp;</td>
														<TD><input type="text" name="line2" tabindex="10" value="<?= $_REQUEST['line2'] ?>" <?= $_SESSION['web_interface']->missing_required_input('line2') ?>></td>
													</tr>
													<TR>
														<TD ALIGN="RIGHT" <?= $_SESSION['web_interface']->missing_required_label('main_email_address') ?>>Email address*</td>
														<TD><input type="text" id="main_email_address" name="main_email_address" tabindex="3" value="<?= $_REQUEST['main_email_address'] ?>" <?= $_SESSION['web_interface']->missing_required_input('main_email_address') ?>></td>
														<TD ALIGN="right" <?= $_SESSION['web_interface']->missing_required_label('city') ?>>City</td>
														<TD><input type="text" name="city" tabindex="11" value="<?= $_REQUEST['city'] ?>" <?= $_SESSION['web_interface']->missing_required_input('city') ?>></td>
													</tr>
													<TR>
														<TD ALIGN="right" <?= $_SESSION['web_interface']->missing_required_label('personal_phone') ?>>Contact Phone*</td>
														<TD><input type="text" name="personal_phone" tabindex="4" value="<?= $_REQUEST['personal_phone'] ?>" size="12" <?= $_SESSION['web_interface']->missing_required_input('personal_phone') ?>> ext. <input type="text" name="personal_phone_ext" tabindex="5" value="<?= $_REQUEST['personal_phone_ext'] ?>" size="5" <?= $_SESSION['web_interface']->missing_required_input('personal_phone_ext') ?>></td>
														<TD align="right" <?= $_SESSION['web_interface']->missing_required_label('state') ?>>State</td>
														<TD>
															<select name="state" <?= $_SESSION['web_interface']->missing_required_input('state') ?> tabindex="12">
																<option value=""></option>
																<option value="">United States</option>
																<option value="">----------</option>
																<?= select_options( array('type'=>'us_state', 'selected'=>$_REQUEST['state']) ) ?>
																<option value=""></option>
																<option value="">Canada</option>
																<option value="">----------</option>
																<?= select_options( array('type'=>'ca_state', 'selected'=>$_REQUEST['state']) ) ?>
															</select>
														</td>
													</tr>
													<TR>
														<TD ALIGN="RIGHT" <?= $_SESSION['web_interface']->missing_required_label('business_phone') ?>>Work Phone</td>
														<TD><input type="text" name="business_phone" tabindex="6" value="<?= $_REQUEST['business_phone'] ?>" size="12" <?= $_SESSION['web_interface']->missing_required_input('business_phone') ?>> ext. <input type="text" name="business_phone_ext" tabindex="7" value="<?= $_REQUEST['business_phone_ext'] ?>" size="5" <?= $_SESSION['web_interface']->missing_required_input('business_phone_ext') ?>></td>
														<TD align="right" <?= $_SESSION['web_interface']->missing_required_label('postal_code') ?>>Zip</td>
														<TD><input type="text" name="postal_code" tabindex="13" value="<?= $_REQUEST['postal_code'] ?>" <?= $_SESSION['web_interface']->missing_required_input('postal_code') ?>></td>
													</tr>
													<tr>
														<TD ALIGN="right" <?= $_SESSION['web_interface']->missing_required_label('business_name') ?>>Company*</td>
														<TD><input type="text" name="business_name" tabindex="8" value="<?= $_REQUEST['business_name'] ?>" <?= $_SESSION['web_interface']->missing_required_input('business_name') ?>></td>
													</tr>
												</table>
												<br>

												<strong>Why do I need a credit card for a free trial?</strong><br/>
												We limit free trials to those willing to provide a credit card as it is the only reliable way for us to verify that you have a sincere interest in our service. If you don't want to continue using <?= $_SESSION['o_company']->get_title() ?>, just cancel before the trial ends and you will not be charged.<br/>
												<br/>
												
												<!-- Credit Card Info Table -->
												<TABLE width="85%" CELLSPACING="0" CELLPADDING="0" BORDER="0">
													<TR><TD COLSPAN="4" align="left"><span class="header1"><strong>4. Credit Card Information</strong></span></td></TR>
													<TR>
														<TD ALIGN="right" <?= $_SESSION['web_interface']->missing_required_label('card_name') ?>>Name on Card*</td>
														<TD><input type="text" name="card_name" size="20" tabindex="14" value="<?= $_REQUEST['card_name'] ?>" <?= $_SESSION['web_interface']->missing_required_input('card_name') ?>></td>
														<TD ALIGN="RIGHT" <?= $_SESSION['web_interface']->missing_required_label('card_type') ?>>Credit Card Type*</td>
														<TD>
															<select name="card_type" <?= $_SESSION['web_interface']->missing_required_input('card_type') ?> tabindex="16">
																<option value="">Click to Select</option>
																<option value="VISA" <?php if ($_REQUEST['card_type'] == 'VISA') { print 'selected="selected"'; } ?>>Visa</option>
																<option value="MC" <?php if ($_REQUEST['card_type'] == 'MC') { print 'selected="selected"'; } ?>>MasterCard</option>
																<option value="DISC" <?php if ($_REQUEST['card_type'] == 'DISC') { print 'selected="selected"'; } ?>>Discover</option>
																<option value="AMEX" <?php if ($_REQUEST['card_type'] == 'AMEX') { print 'selected="selected"'; } ?>>AmEx</option>
															</select><br/>
															<img src="/web/company_def/img/credit_card_logos_10.gif" style="margin: 5px;" width="150" />
														</td>
													</tr>
													<TR>
														<TD ALIGN="right" <?= $_SESSION['web_interface']->missing_required_label('card_number') ?>>Credit Card #*</td>
														<TD><input type="text" name="card_number" size="20" tabindex="15" value="<?php if ($_REQUEST['card_number']) { print $_SESSION['web_interface']->getDecrypt($_REQUEST['card_number']); } ?>" <?= $_SESSION['web_interface']->missing_required_input('card_number') ?>></td>
														<TD ALIGN="RIGHT" <?= $_SESSION['web_interface']->missing_required_label('card_exp_date') ?>>Expiration mmyy*</td>
														<TD><input type="text" name="card_exp_date" maxlength="4" size="5" tabindex="17" value="<?= $_REQUEST['card_exp_date'] ?>" <?= $_SESSION['web_interface']->missing_required_input('card_exp_date') ?>></td>
													</tr>
													<TR><TD COLSPAN="4">&nbsp;</td></tr>
												</TABLE>
												
												<!-- Submit, Clear, Checkboxes Table -->		
												<TABLE width="100%" CELLSPACING="0" CELLPADDING="0" BORDER="0">		
													<TR><TD COLSPAN="4" ALIGN="left"><span class="header1"><strong>5. File Attachment</strong></span></td></TR>		
													<TR>
														<TD>
															<input type="checkbox" name="email_attachment" tabindex="18" value="1" style="border:0px;" <?php if ($_REQUEST['email_attachment']) { print 'checked="checked"'; } ?> />&nbsp;In addition to the daily email, would you like a .csv (comma delimited) file attachment sent? <a href="#" onMouseOver="popUp('whatIsCsv');" onMouseOut="popUp('whatIsCsv');">What is this?</a>
															<div class="areaInfo" id="whatIsCsv" name="whatIsCsv" style="display: none;">
																<div class="areaInfoHeader">CSV File Attachment</div>
																<table class="areaInfoContent">
																	<tr><td>The .CSV ("Comma Separated Value", also called "Comma Delimited") is a widely supported database format that can be included in the daily email lead file as an attachment. This format, with lead data divided into fields, is commonly used to import lead information into auto dialers, spreadsheets and prospecting/marketing software and systems, and for creating mailing labels. This option is included for you at no extra cost.<br/><br/>This same lead data file can also be accessed and downloaded by logging in online. However, this download option is available only to paying subscribers. </td></tr>
																</table>
															</div>
														</td>
													</tr>
													<TR><TD COLSPAN="4">&nbsp;</td></tr>
													<TR><TD COLSPAN="4" ALIGN="left"><span class="header1"><strong>6. Online Access</strong></span></td></TR>		
													<TR>
														<TD>
															Online access allows for historical lead access (up to 90-days back) for paid subscriptions only. Trial customer login access can be used to upgrade to <a href="/ssl/subscribe_<?= $_REQUEST['subscribe_state'] ?>.html">subscription</a>, add or change billing information, add or change area/s, and to<br/>cancel trial.<br/><br/>
															Password *<input type="password" name="password" tabindex="20" value="" <?= $_SESSION['web_interface']->missing_required_input('password') ?>>
															Confirm Password *<input type="password" name="confirm_password" tabindex="21" value="" <?= $_SESSION['web_interface']->missing_required_input('confirm_password') ?>>
														</td>
													</tr>
													<tr><td>&nbsp;</td></tr>
													<TR><TD COLSPAN="4" ALIGN="left"><span class="header1"><strong>7. Terms and Conditions</strong></span></td></TR>		
													<TR>
														<TD>
															<input type="checkbox" name="terms_conditions" tabindex="19" value="1" style="border:0px;" <?php if ($_REQUEST['terms_conditions']) { print 'checked="checked"'; } ?> />
															<b>*I have read the <a href="#" onClick="MyWindow=window.open('/terms_main.html','MyWindow','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=725,height=600'); return false;"><b>Terms & Conditions</b></a> and agree to its terms. I understand the free trial will be converted to a <a href="#" onClick="MyWindow=window.open('/paying_subscription.html','MyWindow','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=600,height=475'); return false;"><b>Paying Subscription</b></a> account unless <a href="#" onClick="MyWindow=window.open('/cancel_trial.html','MyWindow','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=300,height=225'); return false;"><b>cancelled</b></a>. 
														</td>
													</tr>
													<TR><TD COLSPAN="4">&nbsp;</td></tr>
													<TR><TD COLSPAN="4" ALIGN="left"><span class="header1"><strong>8. Other</strong></span></td></TR>
													<TR><TD COLSPAN="2" ALIGN="left" <?= $_SESSION['web_interface']->missing_required_label('referral_source') ?>>How did you hear about us? *</td></TR>
													<TR>
														<TD COLSPAN="2" ALIGN="left">
															<select name="referral_source" tabindex="22" <?= $_SESSION['web_interface']->missing_required_input('referral_source') ?>>
															<option value="">Click to Select</option>
<?php
$source_hash = array('Email','Facebook','Google','Yahoo','MSN','Other Search Engine','Flyer','Magazine','Referral','Sales Call','Convention','Word of Mouth','Other');
foreach ($source_hash as $source) {
?>
																<option value="<?= $source ?>" <?php if ($_REQUEST['referral_source'] == $source) { print 'selected="selected"'; } ?>><?= $source ?></option>
<?php
}
?>
															</select>
															Other: <input type="text" name="referral_source_other" tabindex="23" value="<?= $_REQUEST['referral_source_other'] ?>" />
														</td>
													</tr>
													<TR><TD>&nbsp;</td></tr>
													<TR><TD COLSPAN="2" ALIGN="left">Customer Type:</td></TR>
													<tr>
														<td align="left" colspan="2">
															<select name="customer_type" style="width: 155px;" tabindex="24">
																<option value="">&nbsp;</option>
																<?= select_options( array('type'=>'signup_customer_type', 'selected'=>$_REQUEST['customer_type']) ) ?>
															</select>
															Other: <input type="text" name="customer_type_other" tabindex="25" value="<?= $_REQUEST['customer_type_other'] ?>" />
														</td>
													</tr>
													<TR><TD>&nbsp;</td></tr>
													<tr><td colspan="4">To ensure successful email delivery, please add info@<?= preg_replace("/^www\./", "", $_SESSION['o_company']->get_company_url()) ?> to your Address Book now.</td></tr>
													<TR><TD>&nbsp;</td></tr>
													<TR ALIGN="CENTER">
														<TD align="CENTER">
															<input type="submit" name="submit" tabindex="26" value=" Submit Order ">&nbsp;
														</td>
													</TR>				
													<TR><TD COLSPAN="4" ALIGN="CENTER"><b>Note, items with a * are required.</b></td></tr>
													<TR><TD COLSPAN="4" ALIGN="CENTER">(Be sure to check Accept Terms)</td></tr>
													<tr>
														<td colspan="4" align="center">
															<br/>32 West 200 South #134<br/>
															Salt Lake City, UT 84101 <br/>
															USA
														</td>
													</tr>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
		</TD>
	</TR>
	<TR><TD><IMG SRC="/web/company_def/img/spacer.gif" WIDTH=699 HEIGHT=12 ALT=""></TD></TR>
</TABLE>
</form>
<script language="javascript" type="text/javascript">
	calculateTotals();
	
<?php
if ($_REQUEST['online_access']) {
?>
	document.getElementById('online_access_fields').style.display='inline';
<?php
}
?>
</script>
<?php
include_once($_SESSION['web_interface']->get_server_path('other/global/bottom.php'));
?>

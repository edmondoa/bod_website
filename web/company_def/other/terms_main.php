﻿<html>
<body>
		<table width="699" height="355" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="7" valign="top"><img src="/web/company_def/img/spacer.gif" width="7" height="1"></td>
    <td width="684" valign="top">
	<table border="0" cellspacing="0" cellpadding="1" bgcolor="#DDDDDD">
  <tr><td valign="top"><table width="682" height="353" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
  <tr>
    <td valign="top" height="3" bgcolor="#D9E2E6"><img src="/web/company_def/img/spacer.gif"></td>
  </tr>

  <tr>
    <td valign="top" height="350" style="padding:11px 17px 10px 15px; line-height:14px " class="big">
<strong class="blue">Terms & Conditions</strong><br>
<br style="line-height:10px ">
<div style="height:1px; background-color:#A0A0A0 "><img src="/web/company_def/img/spacer.gif"></div>
<br style="line-height:10px ">


<table width="100%" cellspacing="0" cellpadding="10" border="0">
<tr>
	<td align="left" valign="top" class="blacktxt">
You must agree to the Terms & Conditions to enable a subscription/sample from 

<b><?= $_SESSION['o_company']->get_title() ?></b> to you ("Customer"). Customer is exclusively owned by <b><?= $_SESSION['o_company']->get_title() ?></b> and not by an Affiliate or other.
		<br> 	(Customer refers to anyone receiving the leads.)
		<br><br>
Customer understands that all public data is derived from freely-available public sources. All data supplied is only for internal use of specific Customer and Customer may not forward, sell, lease, give, etc. to any other party or post data anywhere including websites/written materials, without explicit written permission from <b><?= $_SESSION['o_company']->get_title() ?></b> .

		<br><br>

Customer understands that this contract represents an obligation of services and payment (if appropriate) until the complete designated time-interval has expired. <b><?= $_SESSION['o_company']->get_title() ?></b>  reserves the right to deny any petition of service. Customer understands that by agreeing to pay by credit card and supplying credit card information that <b><?= $_SESSION['o_company']->get_title() ?></b> will automatically charge the credit card once per designated time-interval. Customer understands that <b><?= $_SESSION['o_company']->get_title() ?></b> will automatically renew an identical time-interval contract upon expiration unless Customer provides sufficient written notice (mailed correspondence or email is fine), 10 days prior to the expiration of the time-interval date, to either terminate this contract or alter the time-interval. Customer understands and agrees that Customer is responsible for all charges for the service until such notice of termination is received and acknowledged by <b><?= $_SESSION['o_company']->get_title() ?></b>. <b><?= $_SESSION['o_company']->get_title() ?></b> reserves the right to terminate service of this contract for any reason at any time including prior to the expiration of the time-interval and will give 10 days notice.

		<br><br>

<b><?= $_SESSION['o_company']->get_title() ?></b>  shall in no event be liable for loss of profit, Do Not Call Registry claims/disputes, goodwill or other special or consequential damages suffered by the Customer or others, as a result of <b><?= $_SESSION['o_company']->get_title() ?></b> s performance or nonperformance according to these Terms & Conditions and as agreed to by the Customer.

		<br><br>
Customer hereby indemnifies <b><?= $_SESSION['o_company']->get_title() ?></b> from any and all claims. Customer agrees that no express or implied warranty is made concerning the data (via email, website access or other) including ad contact information (phone numbers, address/city/state, etc.), details (description, bed, bath, lot size, etc.). Customer holds <b><?= $_SESSION['o_company']->get_title() ?></b> harmless from any claim arising from searching and/or extracting actual data and that which is delivered to Customer including claims of infringement of patent, copyright, U.S. National Do Not Call Registry, trade secret, trespass, misrepresentation, misappropriation, or similar proprietary rights of any party. <b><?= $_SESSION['o_company']->get_title() ?></b> exclusively retains all intellectual property rights associated with its data collection processes. <b><?= $_SESSION['o_company']->get_title() ?></b> and only the specific Customer both obtain ownership rights to the data.

		<br><br>

Customer acknowledges that all Agreements, including 12-Month Agreements, are to be prepaid and customer's credit card will be charged immediately. Customer agrees that no refund/credit will be given under any circumstance. 12-Month Agreement Customers are required to check the box on the sign up page indicating acknowledgement of no-refund policy. 

		<br><br>
By entering into this Contract both parties submit to exclusive jurisdiction in the State of Utah, USA, for adjudication of any disputes and/or claims between the parties under this Contract.
<br><br>
Version - </b>June 30, 2008
	</td>
</tr>
</table>


	</td>
  </tr>
  <tr>

  </tr>
</table>
</td></tr>
</table>

	</td>
    <td width="8" valign="top"><img src="/web/company_def/img/spacer.gif" width="8" height="1"></td>
  </tr>
</table>

		</TD>
	</TR>

	<TR>
		<TD>
			<IMG SRC="/web/company_def/img/spacer.gif" WIDTH=699 HEIGHT=12 ALT=""></TD>
	</TR>
</TABLE>
</body>
</html>
<?php
$_META_DESCRIPTION = '';
$_META_KEYWORDS = '';
$_PAGE_TITLE = '';

include_once($_SESSION['web_interface']->get_server_path('other/global/top.php'));
?>
<table width="699" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="7" valign="top"><IMG src="/web/company_def/img/spacer.gif" alt="" width="7" height="1"></td>
<td width="684" valign="top">
<table border="0" cellspacing="0" cellpadding="1" bgcolor="#DDDDDD">
<tr><td valign="top">

<table width="682" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
<tr>
<td valign="top" height="3" bgcolor="#D9E2E6"><IMG src="/web/company_def/img/spacer.gif" alt=""></td>
</tr>

<tr>
<td valign="top" height="350" style="padding:11px 17px 10px 15px; line-height:14px " class="big">
<h1 class="PageHead">Title Professionals: More FSBO Leads for you and your partners</h1>
<div style="height:1px; background-color:#A0A0A0 "><IMG src="/web/company_def/img/spacer.gif" alt=""></div>
<br style="line-height:10px ">

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
<td>

<h2 class="big2">More For-Sale-by-Owner Leads</h2>
<span class="copy14">
To say the obvious, more leads is key to filling your partners and your own sales pipelines. It's a numbers game 
and the more FSBO leads distributed to your partners and within your own staff, the better. Happy, productive partners  
and effective internal lead campaigns are the goal FSBO Leads Inc. helps fulfil.
</span>

<br><br><br>
<h2 class="big2">Faster For Sale by Owner Leads=More Listings</h2>
<span class="copy14">
Bottom line is that FSBO Leads Inc. captures classifed ads and turns them into leads
the day they are published. If you want to get the jump on the competition, getting to 
leads the day they are published is critical. FSBO Leads Inc. customers have this advantage!
</span>

<br><br><br>
<h2 class="big2">FSBO Leads turn into Sales Closes</h2>
<span class="copy14">
More FSBO leads enable more closings for your partners and internal sales teams.
<br>FSBO Leads Inc. customers report:
</span>
<br>
<ul>
<li>Pam, a power agent with a team, reports that FSBO Leads Inc. has been directly responsible for a 20% increase in close.</li>
<li>Debbie, a part time agent, attributes 80% of her closes to the FSBO Leads Inc. FSBO lead service.</li>
</ul>


</td>
</tr>
</table>

</td>
</tr>
<!--  <tr>
</tr> -->
</table>
</td></tr>
</table>

</td>
<td width="8" valign="top"><IMG src="/web/company_def/img/spacer.gif" alt="" width="8" height="1"></td>
</tr>
</table>

</TD>
</TR>
<TR>
<TD>
<IMG src="/web/company_def/img/spacer.gif" WIDTH=699 HEIGHT=12 ALT=""></TD>
</TR>
</TABLE>
<?php
include_once($_SESSION['web_interface']->get_server_path('other/global/bottom.php'));
?>
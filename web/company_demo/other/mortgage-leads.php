<?php
$_META_DESCRIPTION = '';
$_META_KEYWORDS = '';
$_PAGE_TITLE = '';

include_once($_SESSION['web_interface']->get_server_path('other/global/top.php'));
?>
<table width="699" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="7" valign="top"><IMG src="/web/company_def/img/spacer.gif" alt="" width="7" height="1"></td>
<td width="684" valign="top">
<table border="0" cellspacing="0" cellpadding="1" bgcolor="#DDDDDD">
<tr><td valign="top">

<table width="682" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
<tr>
<td valign="top" height="3" bgcolor="#D9E2E6"><IMG src="/web/company_def/img/spacer.gif" alt=""></td>
</tr>

<tr>
<td valign="top" height="350" style="padding:11px 17px 10px 15px; line-height:14px " class="big">
<h1 class="PageHead">Mortgage Lenders, Mortgage Brokers</h1>
<div style="height:1px; background-color:#A0A0A0 "><IMG src="/web/company_def/img/spacer.gif" alt=""></div>
<br style="line-height:10px ">

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
<td>

<h2 class="big2">More Lender FSBO Leads</h2>
<span class="copy14">
For Sale By Owners (FSBO�s) are a great source for mortgage leads!
&nbsp;Help For Sale By Owners with the mortgage on their new purchase.
&nbsp;You can also help FSBO�s by pre-qualifying potential buyers of their own home.
&nbsp;Or help the For Sale By Owner market their home with signage, flyers, 
and web�use these sources to find and capture more potential buyers!
</span>

<br><br><br>
<h2 class="big2">Faster For Sale by Owner Leads=More Loans Closed</h2>
<span class="copy14">
Close more loans by tapping into this growing market�For Sale By Owners are a great way to increase your existing business!
Bottom line is that FSBO Leads Inc. captures classifed ads and turns them into mortgage leads
the day they are published. If you want to get the jump on the competition, getting to 
the mortgage leads the day they are published is critical. FSBO Leads Inc. customers have this advantage!
</span>

<br><br><br>
<h2 class="big2">Mortgage FSBO Leads turn into Loan Closes</h2>
<ul>
<li>G. J. of Benchmark Mortgage; �I used to receive leads from a Title company but the leads were stale--days or weeks old. Thank goodness I found ByOwnerDAILY and their fresh and fast lead service.�</li>
<li>Debbie, a part time agent, attributes 80% of her closes to the FSBO Leads Inc. FSBO lead service.</li>
</ul>


</td>
</tr>
</table>

</td>
</tr>
<!--  <tr>
</tr> -->
</table>
</td></tr>
</table>

</td>
<td width="8" valign="top"><IMG src="/web/company_def/img/spacer.gif" alt="" width="8" height="1"></td>
</tr>
</table>

</TD>
</TR>
<TR>
<TD>
<IMG src="/web/company_def/img/spacer.gif" WIDTH=699 HEIGHT=12 ALT=""></TD>
</TR>
</TABLE>
<?php
include_once($_SESSION['web_interface']->get_server_path('other/global/bottom.php'));
?>
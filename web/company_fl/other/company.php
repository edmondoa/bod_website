<?php
$_META_DESCRIPTION = '';
$_META_KEYWORDS = '';
$_PAGE_TITLE = '';

include_once($_SESSION['web_interface']->get_server_path('other/global/top.php'));
?>
<!-- dave s. -->
<table width="699" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="7" valign="top"><IMG src="/web/company_def/img/spacer.gif" alt="" width="7" height="1"></td>
<td width="684" valign="top">
<table border="0" cellspacing="0" cellpadding="1" bgcolor="#DDDDDD">
<tr><td valign="top">

<table width="682" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
<tr>
<td valign="top" height="3" bgcolor="#D9E2E6"><IMG src="/web/company_def/img/spacer.gif" alt=""></td>
</tr>

<tr>
<td valign="top" height="350" style="padding:11px 17px 10px 15px; line-height:14px " class="big">
<h1 class="PageHead">Company and Contact</h1>
<div style="height:1px; background-color:#A0A0A0 "><IMG src="/web/company_def/img/spacer.gif" alt=""></div>
<br style="line-height:10px ">

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
<td>

<h2 class="big2">FSBO Lead Service</h2>
<span class="copy14">
<?= $_SESSION['o_company']->get_title() ?> was formed in 2003 by a select group of software technology Information Extraction experts
and Real Estate/Mortgage visionaries.</span>

<br><br><br>
<h2 class="big2">Largest national "Non MLS" database</h2>
<span class="copy14">
<?= $_SESSION['o_company']->get_title() ?> was created with one goal in mind: To build the largest National Residential Real Estate ("Non MLS") database in existence. Composed of unique and daily refreshed leads, <?= $_SESSION['o_company']->get_title() ?>  uses advanced cutting edge web and database technologies to capture millions of leads annually.
Realtors, Title, Mortgage Companies and other Service Professionals typically use the FSBO leads to prospect new business. Looking for a competitive,
expanded Inventory? We offer the edge.</span>

<br><br><br>
<h2 class="big2">FSBO Sales Lead Sources</h2>
<span class="copy14">
<?= $_SESSION['o_company']->get_title() ?>  sales lead generation solution collects thousands of leads daily from local newspapers (covering the entire U.S.) 
and other public online sources. Our lead-generation solution extracts leads in their entirety, eliminates  duplications 
and augments the lead with external contact information. Leads with phone numbers are compared against the U.S. 
National Do Not Call Registry and a reverse phone number lookup is performed to obtain contact name and address information.</span>

<br><br><br>
<h2 class="big2">FSBO Sales, Service, Business Development, Investor</h2>
<table width="100%" cellspacing="0" cellpadding="2" border="0">
<tr>
<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>

<td align="left" valign="top">
<strong>Sales-Service-Leads</strong>
<br><strong>Phone:</strong> (801) 494-0470
<br><strong>Email:</strong>  <a href="mailto:info@fsboleadsusa.com">info@fsboleadsusa.com</a>
</td>

<td align="left" valign="top">
<strong>Business Development-Investor Relations</strong>
<br><strong>Phone:</strong> (801) 494-0470
<br><strong>Email:</strong>  <a href="mailto:BizDev@fsboleadsusa.com">BizDev@fsboleadsusa.com</a>
</td>
</tr>
</table>


</td>
</tr>
</table>

</td>
</tr>

</table>
</td></tr>
</table>

</td>
<td width="8" valign="top"><IMG src="/web/company_def/img/spacer.gif" alt="" width="8" height="1"></td>
</tr>
</table>

</TD>
</TR>
<TR>
<TD>
<IMG src="/web/company_def/img/spacer.gif" WIDTH=699 HEIGHT=12 ALT=""></TD>
</TR>
</TABLE>
<?php
include_once($_SESSION['web_interface']->get_server_path('other/global/bottom.php'));
?>

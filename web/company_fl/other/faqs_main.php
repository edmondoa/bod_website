<?php
$_META_DESCRIPTION = '';
$_META_KEYWORDS = '';
$_PAGE_TITLE = '';

include_once($_SESSION['web_interface']->get_server_path('other/global/top.php'));
?>
<table width="699" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="7" valign="top"><IMG src="/web/company_def/img/spacer.gif" alt="" width="7" height="1"></td>
<td width="684" valign="top">
<table border="0" cellspacing="0" cellpadding="1" bgcolor="#DDDDDD">
<tr><td valign="top">

<table width="682" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
<tr>
<td valign="top" height="3" bgcolor="#D9E2E6"><IMG src="/web/company_def/img/spacer.gif" alt=""></td>
</tr>

<tr>
<td valign="top" height="350" style="padding:11px 17px 10px 15px; line-height:14px " class="big">
<h1 class="PageHead">Frequently Asked Questions ("FAQ")</h1>
<div style="height:1px; background-color:#A0A0A0 "><IMG src="/web/company_def/img/spacer.gif" alt=""></div>
<br style="line-height:10px ">

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
<td>

<h2 class="big2">What is the source of your For Sale By Owner leads?</h2>
<span class="copy14">
We scan and search newspapers, magazines, websites, and various other public sources FSBO postings.
In fact, we consult with real estate professionals in each area to determine the best information
sources so we can provide the best quality and maximum quantity of leads for the lowest price.</span>

<h2 class="big2">How current are the leads you provide?</h2>
<span class="copy14">
Usually same day posted! Daily we scan and search all current 'For Sale By Owner' ads,
7 days a week. We know how important it is to you that the leads are
'fresh' and delivered quickly. <?= $_SESSION['o_company']->get_title() ?> collects and delivers most ads the same
day.</span>

<h2 class="big2">How often do you deliver the leads?</h2>
<span class="copy14">
Monday-Sunday. We send most leads the same day they are posted!</span>

<h2 class="big2">Explain your free Prefix Finder(tm) service.</h2>
<span class="copy14">
We know that the more information you have at your fingertips, the better.
Many FSBO ads contain cell phone and unlisted numbers which retrieve NO RESULTS from a
reverse directory search.  This is why we created Prefix Finder(tm) which tells you the CITY
SOURCE of the telephone prefix or prefixes in an ad.  This can be especially helpful in
situations when reverse directory searches retrieve no result, when the city is not listed
in an ad, and when tracking non-occupant sellers for properties.</span>

<h2 class="big2">How accurate are your leads?</h2>
<span class="copy14">
99.99% accurate.  We obtain our leads from public sources and deliver them to you as
they are advertised.  We make little to no changes to the ads.  If there are any inaccuracies
it typically originates from the source, such as the property seller or newspaper.  In the
case of a 'wrong' phone number from a reverse directory search, again, the source of the error
typically originates from 'old or inaccurate' data from the directory providers.
</span>

<h2 class="big2">How do you screen out 'blind ads'?</h2>
<span class="copy14">
The 'blind ad' is one that does not identify the broker or agent placing the ad.
We use a system of tracking and eliminating most blind ads.  However, because
there is no way to determine the source of all blind ads, we cannot catch them all.
</span>

<h2 class="big2">Does we do anything to help its customers understand which phone numbers are on the U.S. National Do Not Call Registry?</h2>
<span class="copy14">
Yes. As a service to our customers, all phone numbers are compared against the U.S. "Do Not
Call Registry" database. If a match is found, the phone number will be indicated as being a part of
the Do Not Call Registry. For more information about the U.S. Do Not Call Registry
see <a href="https://www.donotcall.gov/default.aspx">U.S. Do Not Call Registry</a>.
</span>

<h2 class="big2">What is the difference between <?= $_SESSION['o_company']->get_title() ?> and competitors?</h2>
<span class="copy14">
Our biggest 'competitor' are those people who continue to spend their valuable time looking/farming for leads rather than working leads and working intelligently.
Compared to other national services, <?= $_SESSION['o_company']->get_title() ?> specializes in delivering the highest quality FSBO leads. 
Usually you will receive 20%-40% MORE LEADS and get leads 1-3 days FASTER using our service!</span>

</td>
</tr>
</table>

</td>
</tr>
<!--  <tr>
</tr> -->
</table>
</td></tr>
</table>

</td>
<td width="8" valign="top"><IMG src="/web/company_def/img/spacer.gif" alt="" width="8" height="1"></td>
</tr>
</table>

</TD>
</TR>
<TR>
<TD>
<IMG src="/web/company_def/img/spacer.gif" WIDTH=699 HEIGHT=12 ALT=""></TD>
</TR>
</TABLE>
<?php
include_once($_SESSION['web_interface']->get_server_path('other/global/bottom.php'));
?>
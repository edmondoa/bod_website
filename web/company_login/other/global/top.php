<?php
$_META_DESCRIPTION = ($_META_DESCRIPTION) ? $_META_DESCRIPTION : 'For Sale by Owner automated delivery service of FSBO leads.';
$_META_KEYWORDS = ($_META_KEYWORDS) ? $_META_KEYWORDS : 'FSBO, FSBO Leads, For Sale By Owner, for-sale-by-owner, agent, broker, realtor leads, real estate leads, mortgage, fsbo list, Lender';
$_PAGE_TITLE = ($_PAGE_TITLE) ? $_PAGE_TITLE : 'FSBO Leads | For Sale by Owner Leads | Real Estate Leads';
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>
<HEAD>
	<TITLE><?= $_PAGE_TITLE ?></TITLE>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<META NAME="description" CONTENT="<?= $_META_DESCRIPTION ?>">
	<META NAME="keywords" CONTENT="<?= $_META_KEYWORDS ?>">
	<META NAME="AUTHOR" CONTENT="<?= $_SESSION['o_company']->get_title() ?>">
	<META NAME="COPYRIGHT" CONTENT="www.<?= $_SESSION['o_company']->get_title() ?>.com">
	<META NAME="CONTACT_ADDR" CONTENT="info@<?= $_SESSION['o_company']->get_title() ?>.com">
	<META NAME="RATING" CONTENT="General">
	<META NAME="robots" CONTENT="all">
<?php
if ($_SERVER['SERVER_PORT'] == 443) {
?>
	<link rel="shortcut icon" href="https://bod84058.securesites.net/web/company_def/other/favicon.ico" type="image/x-icon">
<?php
}
?>
	<LINK href="<?= $_SESSION['web_interface']->get_path('css/other.css') ?>" rel="stylesheet" type="text/css">
	<script src="<?= $_SESSION['web_interface']->get_path('js/global.js') ?>" type="text/javascript"></script>
</HEAD>

<BODY bgColor="#ffffff" background=<?= $_SESSION['web_interface']->get_path('img/bg.jpg') ?> topMargin=0 MARGINWIDTH="0" MARGINHEIGHT="0">
<!-- ImageReady Slices (01_feb009.psd - Slices: 02, 04, 05, 06, 08, 11, bot_left, bot_left_corn, bot_right, bot_right_corn, top_left_corn, top_right_corn) --><!-- Outermost table: -->
<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
	<TBODY>
	<TR>
		<TD style="BACKGROUND-POSITION: right top" width="50%"><IMG src="/web/company_def/img/spacer.gif" ALT="fsbo leads"></TD>
		<TD vAlign=top>
			<!-- Main table: -->
			<TABLE cellSpacing=0 cellPadding=0 width=766 align=center bgColor="#ffffff" border=0>
				<TBODY>
				<!-- Top row, Main table: -->
				<TR>
					<!-- Top left image -->
					<TD><IMG src="<?= $_SESSION['web_interface']->get_path('img/top_left_corn.jpg') ?>" height="10" width=34 alt="for sale by owner"></TD>
					<!-- Top middle image -->
					<TD style="BACKGROUND-IMAGE: url(<?= $_SESSION['web_interface']->get_path('img/top_px.jpg') ?>)"></TD>
					<!-- Top right image -->
					<TD><IMG src="<?= $_SESSION['web_interface']->get_path('img/top_right_corn.jpg') ?>" height=10 width=33 alt="fsbo leads" ></TD>
				</TR>
					
				<TR>
					<TD style="BACKGROUND-IMAGE: url(<?= $_SESSION['web_interface']->get_path('img/left_px.jpg') ?>)"><IMG src="/web/company_def/img/spacer.gif" height=50 width=34 alt=""></TD>
					<TD vAlign=top width=699 height=508>
					
					<!-- Header and Body but not Footer table: -->
						<TABLE cellSpacing=0 cellPadding=0 width=699 border=0>
							<TBODY>
								<!-- First row, above Nav  -->


							<!-- 5th Row   -->
							<TR>
								<TD vAlign=top width=699 height=355>
									<!-- Main Body table: -->

<?php
#
# @copyright Copyright (c) 2017 Green Seed Technologies, Inc. All rights reserved.
#
# @author David Hoover
# @author David McLaughlin <david@greenseedtech.com>
#

$_PAGE_TITLE = 'Account';

 // unset card_number if XXXX
if (preg_match("/X/", $_REQUEST['card_number'])) {
	unset($_REQUEST['card_number']);
}
$_SESSION['office']->account->initialize($_REQUEST);
include_once($_SESSION['web_interface']->get_server_path('office/global/top.php'));
?>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td class="bodyNav" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('office/account/left_nav_' . $_SESSION['office']->account->get_account_type() . '.php')); ?>
		</td>
		<td class="bodyMain" valign="top">
			<?php include_once($_SESSION['web_interface']->get_server_path('other/misc/status_error.php')); ?>
			<h1>Edit Your Account</h1>
<?php
if (($_SESSION['office']->account->get_billing_status() == 'active' || $_SESSION['office']->account->get_billing_status() == 'queued') && $_SESSION['office']->account->get_account_type() == 'CUSTOMER' && time() > strtotime($_SESSION['office']->account->get_next_bill_date())) {
?>
			Your account is currently past due. We tried to charge your credit card for $<?= sprintf("%.2f", $_SESSION['office']->account->get_rate()) ?> on <?= date('F j, Y', strtotime($_SESSION['office']->account->get_next_bill_date())) ?> but we were not successful. To avoid your subscription from being cancelled please review your credit card information below and click the 'Re-Process Credit Card' button.
<?php
}
else if ($_SESSION['office']->account->get_account_type() == 'CUSTOMER' && $_SESSION['office']->account->get_billing_status() == 'queued') {
?>
			Your account is scheduled to be cancelled on <?= date('F j, Y', strtotime($_SESSION['office']->account->get_end_date())) ?>. If you would like to continue your subscription please <a href="/office/account/handle.html?cmd=continue_subscription">click here</a>.
<?php
}
else if ($_SESSION['office']->account->get_billing_status() == 'canceled' && $_SESSION['office']->account->get_rate() > 0) {
?>
			Your account is currently cancelled. To activate your account you will be charged $<?= sprintf("%.2f", $_SESSION['office']->account->get_rate()) ?>. <a href="javascript: if (confirm('Are you sure you would like to activate your subscription?')) { document.location.href='/office/account/handle.php?cmd=activate_account'; }">Click here</a> to activate your account.
<?php
}
?>
			<form id="myForm" name="myForm" method="post" action="/office/account/handle.php">
			<input type="hidden" name="cmd" id="cmd" value="save" />
			<div class="displayInfo">
				<fieldset>
					<legend>Areas</legend>
					You are subscribed to the following areas:<br/>
<?php
foreach ($_SESSION['office']->account->get_list_area_names() as $area_name) {
	print '<strong>' . $area_name . '</strong><br/>';
}
if ($_SESSION['office']->account->get_account_type() != 'TRIAL' && $_SESSION['office']->account->get_billing_status() == 'active') {
?>
					<a href="/office/account/area_names.php?accountid=<?= $_SESSION['office']->account->get_accountid() ?>">Modify Areas</a>
				</fieldset>
<?php
}
?>
				<fieldset>
					<legend>Property Types</legend>
					You are receiving information about the following property types:<br/>
<?php
	$property_type_fields = $_SESSION['office']->account->get_array_property_type_fields();
	if (count($property_type_fields) > 0) {
		foreach ($property_type_fields as $property_type_field=>$boolean) {
			print '<strong>' . $property_type_field . '</strong><br/>';
		}
	}
	else {
		print 'Your account is receiving ALL property types.<br/>';
	}
?>
					<br/><a href="/office/account/property_type_fields.php">Modify Property Type Fields</a>
				</fieldset>
				<fieldset>
					<legend>Login Info</legend>
					<table border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td class="left"><span <?= $_SESSION['web_interface']->missing_required_label('username') ?>>Username:</span></td>
							<td class="right"><input type="text" name="username" value="<?= $_SESSION['office']->account->get_username() ?>" <?= $_SESSION['web_interface']->missing_required_input('username') ?> /></td>
						</tr>
						<tr>
							<td class="left">Current Password:</td>
							<td class="right"><input type="password" name="current_password" /></td>
						</tr>
						<tr>
							<td class="left">New Password:</td>
							<td class="right"><input type="password" name="new_password" /></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td class="right"><span style="font-size: 8pt;">To change your password, type in your current password and your new password above and click the 'Update' button below.</span></td>
						</tr>
					</table>
				</fieldset>
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td width="50%">
							<fieldset>
								<legend>Personal Info</legend>
								<table border="0" cellpadding="0" cellspacing="0" width="100%">
									<tr>
										<td class="left"><span <?= $_SESSION['web_interface']->missing_required_label('first_name') ?>>First Name:</span></td>
										<td class="right"><input type="text" name="first_name" value="<?= $_SESSION['office']->account->get_first_name() ?>" <?= $_SESSION['web_interface']->missing_required_input('first_name') ?> /></td>
									</tr>
									<tr>
										<td class="left"><span <?= $_SESSION['web_interface']->missing_required_label('last_name') ?>>Last Name:</span></td>
										<td class="right"><input type="text" name="last_name" value="<?= $_SESSION['office']->account->get_last_name() ?>" <?= $_SESSION['web_interface']->missing_required_input('last_name') ?> /></td>
									</tr>
									<tr>
										<td class="left"><span <?= $_SESSION['web_interface']->missing_required_label('personal_phone') ?>>Phone:</span></td>
										<td class="right"><input type="text" name="personal_phone" value="<?= $_SESSION['office']->account->get_personal_phone() ?>" <?= $_SESSION['web_interface']->missing_required_input('personal_phone') ?> /></td>
									</tr>
<?php
$address = $_SESSION['office']->account->o_personal_address;
include($_SESSION['web_interface']->get_server_path('other/misc/address_form.php'));
?>
								</table>
							</fieldset>
						</td>
						<td width="50%">
							<fieldset>
								<legend>Business Info</legend>
								<table border="0" cellpadding="0" cellspacing="0" width="100%">
									<tr>
										<td class="left"><span <?= $_SESSION['web_interface']->missing_required_label('business_name') ?>>Company Name:</span></td>
										<td class="right"><input type="text" name="business_name" value="<?= $_SESSION['office']->account->get_business_name() ?>" <?= $_SESSION['web_interface']->missing_required_input('business_name') ?> /></td>
									</tr>
									<tr>
										<td class="left"><span <?= $_SESSION['web_interface']->missing_required_label('business_phone') ?>>Phone:</span></td>
										<td class="right"><input type="text" name="business_phone" value="<?= $_SESSION['office']->account->get_business_phone() ?>" <?= $_SESSION['web_interface']->missing_required_input('business_phone') ?> /></td>
									</tr>
<?php
$address = $_SESSION['office']->account->o_business_address;
include($_SESSION['web_interface']->get_server_path('other/misc/address_form.php'));
?>
								</table>
							</fieldset>
						</td>
					</tr>
				</table>
				<fieldset>
					<legend>Contact Info</legend>
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tr>
							<td class="left"><span <?= $_SESSION['web_interface']->missing_required_label('main_email_address') ?>>Email:</span></td>
							<td class="right"><input type="text" name="main_email_address" value="<?= $_SESSION['office']->account->get_main_email_address() ?>" size="35" <?= $_SESSION['web_interface']->missing_required_input('main_email_address') ?> /></td>
						</tr>
						<tr>
							<td class="left" valign="top">CSV Attachment:</td>
							<td class="right">
								<input type="checkbox" name="email_attachment" value="1" <?php if ($_SESSION['office']->account->get_email_attachment()) { print 'checked="checked"'; } ?> /><br/>
								Note: If you check this box you will receive a csv attachment that contains the same listings as in the email.
							</td>
						</tr>
					</table>
				</fieldset>
				<fieldset>
					<legend>Billing Info</legend>
					<table border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td class="left" <?= $_SESSION['web_interface']->missing_required_label('card_name') ?>>Name on Card:</td>
								<td class="right"><input type="text" name="card_name" size="20" value="<?= $_SESSION['office']->account->get_card_name() ?>" <?= $_SESSION['web_interface']->missing_required_input('card_name') ?>></td>
							</tr>
							<tr>
								<TD class="left" <?= $_SESSION['web_interface']->missing_required_label('card_type') ?>>Credit Card Type:</td>
								<TD class="right">
									<select name="card_type" <?= $_SESSION['web_interface']->missing_required_input('card_type') ?>>
										<option value="">Click to Select</option>
										<option value="VISA" <?php if ($_SESSION['office']->account->get_card_type() == 'VISA') { print 'selected="selected"'; } ?>>Visa</option>
										<option value="MC" <?php if ($_SESSION['office']->account->get_card_type() == 'MC') { print 'selected="selected"'; } ?>>MasterCard</option>
										<option value="DISC" <?php if ($_SESSION['office']->account->get_card_type() == 'DISC') { print 'selected="selected"'; } ?>>Discover</option>
										<option value="AMEX" <?php if ($_SESSION['office']->account->get_card_type() == 'AMEX') { print 'selected="selected"'; } ?>>AmEx</option>
										<option value="Invoice" <?php if ($_SESSION['office']->account->get_card_type() == 'Invoice') { print 'selected="selected"'; } ?>>Invoice</option>
									</select>
								</td>
							</tr>
							<TR>
								<TD class="left" <?= $_SESSION['web_interface']->missing_required_label('card_number') ?>>Credit Card #:</td>
								<TD class="right"><input type="text" name="card_number" size="20" value=""></td>
							</tr>
							<tr>
								<TD class="left" <?= $_SESSION['web_interface']->missing_required_label('card_exp_date') ?>>Expiration mmyy:</td>
								<TD class="right"><input type="text" name="card_exp_date" size="5" value="<?= $_SESSION['office']->account->get_card_exp_date() ?>" <?= $_SESSION['web_interface']->missing_required_input('card_exp_date') ?>></td>
							</tr>
<?php
if ($_SESSION['office']->account->get_billing_status() == 'active') {
?>
							<tr><td>&nbsp;</td></tr>
							<tr><td>If you would like to cancel your service <a href="javascript: if (confirm('Are you sure you would like to cancel your subscription?')) { document.location.href='/office/account/handle.php?cmd=cancel_account'; }">click here</a>.</td></tr>
<?php
}
?>
						</table>
					</fieldset>
				<input type="submit" value="Update" />
<?php
if ($_SESSION['office']->account->get_account_type() == 'CUSTOMER' && $_SESSION['office']->account->get_billing_status() == 'active' && time() > strtotime($_SESSION['office']->account->get_next_bill_date())) {
?>
				&nbsp;&nbsp;<button name="reprocess" onClick="document.getElementById('cmd').value = 'reprocess_credit_card'; document.getElementById('myForm').submit();">Re-Process Credit Card</button>
<?php
}
?>
			</div>
			</form>
		</td>
	</tr>
</table>
<?php
include_once($_SESSION['web_interface']->get_server_path('office/global/bottom.php'));
?>

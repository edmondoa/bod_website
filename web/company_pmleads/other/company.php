<!DOCTYPE html>
<html>
<head>
<TITLE>FRBO Leads | For Rent by Owner Leads | Property Manager Leads| Landlord Leads | Property Management Leads | Property Manager | FRBO</TITLE>

<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<META NAME="description" CONTENT="FRBO Leads for Property Managers">
	<META NAME="keywords" CONTENT="FRBO Leads, For Rent by Owner Leads, For Rent Leads, Landlord Leads, Property Management Leads, Property Manager Leads, Property Manager, FRBO">
	<META NAME="AUTHOR" CONTENT="PMLeads">
	<META NAME="COPYRIGHT" CONTENT="www.PMLeads.com">
	<META NAME="CONTACT_ADDR" CONTENT="info@PMLeads.com">
	<META NAME="RATING" CONTENT="General">
	<META NAME="robots" CONTENT="all">
	<meta name="google-site-verification" content="10jby2I1JfPVKWnEZnua1dzuM50yg0J8LMFD8HSa72Q" />
	<meta name="msvalidate.01" content="E3A36D47253BBBF9C14C14996A278B68" />


<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- //for-mobile-apps -->
<!-- <link href="/web/company_pmleads/other/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" /> -->
<link href="/web/company_pmleads/other/css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" /> -->
<!-- <link href="css/style.css" rel="stylesheet" type="text/css" media="all" /> -->

 
<!-- js -->
<script src="/web/company_pmleads/other/js/jquery-1.11.1.min.js"></script>
<!-- <script src="js/jquery-1.11.1.min.js"></script> -->
<!-- //js -->


<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Abril+Fatface' rel='stylesheet' type='text/css'>

</head>

	
<body>

<!-- header1 -->
	<div class="header1">
		<div class="container">
			<div class="header1-right">
				<p>
				&nbsp;&nbsp;&nbsp;<a href="services_main.html">Contact</a>
				&nbsp;&nbsp;&nbsp;<a href="login.html">Login</a>
				<!-- &nbsp;&nbsp;(801) 494-0470 -->
				&nbsp;&nbsp;&nbsp;<a href="areas_main_trial.html"><span>Free Trial</span></a></p>
			</div>

			<div class="clearfix"> </div>
			</div>
	</div>


<!-- header2 -->
	<div class="header2">
		<div class="container">
			<div class="navbar1-default">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar1-header">

<!-- 
				<button type="button" class="navbar1-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar1-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
 -->					
					
					<div class="logo1">  <!-- This is the logo top left -->
						<h1><a class="navbar1-brand" href="index.html">PM Leads</a></h1>
					</div>
				</div>

						<div class="drop1">
							<ul class="drop_menu1">
								<li><a href="index.html">HOME</a></li>
								<!-- <li><a href="index.html" class="activePage">HOME</a></li> -->
								
								<li><a href="frbo-leads.html" class="menu_spacer">LEADS</a>
								<!-- <li><a href="frbo-leads.html">PRODUCTS</a> -->								
									<ul>
										<li><a href="landlord-leads.html">for PMs & Realtors (FRBOs)</a></li>
										<li><a href="activePM-leads.html">Sell to: <br> PMs and <br>Realtors</a></li>
									</ul>
								</li>

								<li><a href="areas_main.html" class="menu_spacer">AREAS COVERED</a></li>
								<!-- <li><a href="areas_main.html">AREAS COVERED</a></li> -->
								
								<li><a href="areas_main_trial.html" class="menu_spacer">TRIAL</a></li>
								<!-- li><a href="areas_main_trial.html">TRIAL</a></li> -->
								
								<li><a href="faqs_main.html" class="menu_spacer">FAQs</a></li>
								<!-- <li><a href="faqs_main.html">FAQs</a></li> -->

								<li><a href="company.html" class="menu_spacer">COMPANY</a>
								<!-- <li><a href="company.html">COMPANY</a> -->								
									<ul>
										<li><a href="company.html">History</a></li>
										<li><a href="investor-leads.html">Leadership</a></li>
										<li><a href="for-rent-by-owner.html">Jobs</a></li>
									</ul>
								</li>

							</ul>
						</div>

 
			<div class="clearfix"> </div>
			
			</div>
		</div>
	</div>


<!-- bannner1 -->
	<div class="banner1">
		<div class="container">
			<!-- <div class="logo"> -->
				<!-- <a href="index.html">PM Leads</a> -->  <!-- This located in top middle of top graphic. -->
				<!-- <a href="index.html">PM Leads<span>Find Your HOME</span></a>  -->
			<!-- </div> -->
			
			<!--
			<div class="navigation">
				<nav class="navbar navbar-default">
					<div class="navbar-header">
					  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					  </button>
					</div>

					<div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
						<nav class="cl-effect-13" id="cl-effect-13">
							<ul class="nav navbar-nav">
								<li><a href="index.html" class="active">HOME</a></li>
								<li><a href="http://www.pmleads.com/areas_main.html">AREAS COVERED</a></li>
								<li><a href="http://www.pmleads.com/areas_main_trial.html">Trial</a></li>
								<li><a href="http://www.activePM-leads.html">FAQs</a></li>
								<li><a href="http://www.pmleads.com/company_history.html">COMPANY</a></li>
							</ul>
						</nav>
					</div>
				</nav>
			</div>
			-->


 
			<div class="banner1-info">
				<h3>History</h3>
<!-- 				
				<h2>
					<a href="http://www.pmleads.com/areas_main.html">Subscribe</a>
				</h2>
 -->				
			</div>

	 	</div>
	</div>
<!-- //bannner1 -->


<br>


<!-- banner-bottom -->
	<div class="faq">
		<div class="container">

				<h2>Our Story</h2>
				<h3><ul><li>In 2003 the parent company of PM Leads brought together a Real Estate Visionary and a Software Information Extraction Expert.</li></ul></h3>


				<h4><ul><li>In 2003 we launched our Real Estate - For Sale by Owner ("FSBO") Leads product.</li></ul></h4>
					<h5><ul><li>In 2003 we delivered FSBO leads covering 4 states.</li></ul></h5>
					<h5><ul><li>In 2005 we expanded and delivered FSBO leads for the entire US.</li></ul></h5>
					<h5><ul><li>In 2007 we added FSBO Leads for Canada.</li></ul></h5>
					<h5><ul><li>Since 2008 we have serviced more FSBO customers (including partner totals) than any other competitor.</li></ul></h5>
					<h5><ul><li>See our FSBO site here: <a href="http://www.fsboleadsusa.com/" target="_blank">FSBO Leads</a></li></ul></h5>
				<h4><ul><li>In 2010 we began investigating leads for Residential Property Managers and Realtors.</li></ul></h4>
					<h5><ul><li>In 2012 we began a beta program of For Rent by Owner ("FRBO") leads also known as Landlord leads.</li></ul></h5>
					<h5><ul><li>In 2014 we delivered FRBO leads covering 4 states.</li></ul></h5>
					<h5><ul><li>In 2015 we expanded and delivered FRBO leads for the majority of the US.</li></ul></h5>
					<h5><ul><li>In 2016 we added FRBO leads for Canada.</li></ul></h5>
				<h4><ul><li>In 2016 we are introducing "PM leads" for those servicing Property Managers/Realtors.</li></ul></h4>
				<h4><ul><li>In late 2016 we will roll out Vacation by Owner ("VRBO") leads.</li></ul></h4>


				<h2>Largest national PM/Owner/Landlord Leads database</h2>
				<h3><ul><li>PMLeads was created with one goal in mind: To build the largest Property Manager/Owner/Landlord databases in existence. Composed of unique and daily refreshed leads, PMLeads uses advanced cutting edge web and database technologies to capture hundreds of thousands of leads annually. Property Managers, Realtors, Mortgage Lenders, Title, and other Service Professionals typically use the PM Leads products to prospect new business. Looking for a competitive, expanded sales-lead inventory? We offer the edge.</li></ul></h3>
				
				<h2>PM Leads - Sources</h2>
				<h3><ul><li>Our lead-generation solutions, on a national basis, collects thousands of daily residental rental leads from local newspapers and other public online sources.</li></ul></h3>

				<!-- <h3><ul><li>Our lead-generation solution, on a national basis, collects thousands of daily rental leads from local newspapers and other public online sources. Our lead-generation solution extracts rental leads in their entirety, eliminates duplications and augments the lead with external contact information. Leads with phone numbers are compared against the U.S. National Do Not Call Registry and a reverse phone number lookup is performed to obtain contact name and address information.</li></ul></h3> -->
				
<br>
				<center><h2>Inman Innovator Award Finalist<h2>

						<img src="http://www.pmleads.com/web/company_pmleads/other/images/Inman_award.jpg" alt="" class="img-responsive" />
						<!-- <img src="web/company_pmleads/other/images/Inman_award.jpg" alt="" class="img-responsive" /> -->
						<!-- <img src="images/Inman_award.jpg" alt="" class="img-responsive" /> -->
<br>
						<h3>The Inman News referred to the founders as:</h3>
						<h4>"The Lennon and McCartney of online lead generation."</h4>
				</center>
				
<br>
				<h2><a href="investor-leads.html">To learn about Company Leadership, click here.</a></h2>

				<h2><a href="for-rent-by-owner.html">See Company job openings, click here.</a></h2>

		</div>
	</div>
<!-- //banner-bottom -->

<br>

<!-- footer1 -->
	<div class="footer1">
		<div class="container">
			<div class="footer1-grids">
<!--				<div class="col-md-4 footer1-grid-left">
					<h3>twitter feed</h3>
					<ul>
						<li><a href="single.html">It is a long established fact that a reader will 
						be distracted by the readable content of a page when looking at 
						its layout.</a><span>15 minutes ago</span></li>
						<li><a href="mailto:info@example.com" class="cols">@NASA</a> & <a href="mailto:info@example.com" class="cols">
							@orbital science</a> <a href="single.html">readable content of a page when looking at 
							its layout</a><span>45 minutes ago</span></li>
					</ul>
				</div>
				<div class="col-md-4 footer1-grid-left">
					<h3>Newsletter</h3>
					<form>
						<input type="email" value="enter your email address" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'enter your email address';}" required="">
						<input type="submit" value="Submit" >
					</form>
				</div>
				<div class="col-md-4 footer1-grid-left">
					<h3>about us</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do 
						eiusmod tempor incididunt ut labore et dolore magna aliqua. 
						Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.
						<span>But I must explain to you how all this mistaken idea of denouncing
						pleasure and praising pain was born and I will give you a complete 
						account of the system, and expound the actual teachings of the 
						great explorer.</span>
						</p>
				</div>
				<div class="clearfix"> </div>
			</div>
-->

			<div class="footer1-bottom">
				<div class="footer1-bottom-left">
					<p>&copy2003 - <?= date('Y') ?>
					&nbsp;&nbsp;PMLeads 
					<!-- &nbsp;&nbsp;<a href="http://www.pmleads.com/sitemap.html">sitemap</a>  -->
					&nbsp;&nbsp;(801) 494-0470
					&nbsp;&nbsp;<a href="mailto:info@byownerdaily.com">info@pmleads.com</a></p></p>
				</div>

				<!-- <div class="footer1-bottom-right"> -->
				<div class="header1-right">

					<p><a href="areas_main_trial.html"><span>Free Trial</span></a></p>

<!-- 					<ul>
						<li><a href="#" class="icon-button twitter"><i class="icon-twitter"></i><span></span></a></li>
						<li><a href="#" class="icon-button google"><i class="icon-google"></i><span></span></a></li>
						<li><a href="#" class="icon-button v"><i class="icon-v"></i><span></span></a></li>
					</ul>
 -->
				</div>

				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
<!-- //footer1 -->

<!-- for bootstrap working -->
	<script src="js/bootstrap.js"></script>
<!-- //for bootstrap working -->

</body>
</html>

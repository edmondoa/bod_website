<?php
$_META_DESCRIPTION = '';
$_META_KEYWORDS = '';
$_PAGE_TITLE = '';

include_once($_SESSION['web_interface']->get_server_path('other/global/top.php'));
?>
<table width="699" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="7" valign="top"><IMG src="/web/company_def/img/spacer.gif" alt="" width="7" height="1"></td>
<td width="684" valign="top">
<table border="0" cellspacing="0" cellpadding="1" bgcolor="#DDDDDD">
<tr><td valign="top">

<table width="682" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
<tr>
<td valign="top" height="3" bgcolor="#D9E2E6"><IMG src="/web/company_def/img/spacer.gif" alt=""></td>
</tr>

<tr>
<td valign="top" height="350" style="padding:11px 17px 10px 15px; line-height:14px " class="big">
<h1 class="PageHead">Frequently Asked Questions ("FAQ")</h1>
<div style="height:1px; background-color:#A0A0A0 "><IMG src="/web/company_def/img/spacer.gif" alt=""></div>
<br style="line-height:10px ">

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
<td>

<h2 class="big2">What is PM Leads?</h2>
<span class="copy14">
PM Leads is a "For Rent by Owner' (FRBO) lead service. We specialize in tracking all of the Do-It-Yourself landlords in your area who are currently advertising vacancies for their properties. We email to you a daily list with the property ad information, including landlord contact information.
</span>


<br><br>
<h2 class="big2">What is the source of your Property Manager/Owner/Landlord Leads?</h2>
<span class="copy14">
We scan Residential Rental ads found in newspapers, magazines, public websites and various other public sources.
In fact, we consult with professionals in each area to determine the best information
sources so we can provide the best quality and maximum quantity of leads.
</span>


<br><br>
<h2 class="big2">How current are the Property Manager/Owner/Landlord Leads you provide?</h2>
<span class="copy14">
FRESH! Daily we scan and search hundreds of thousands of rental ads.
We apply the process seven days a week. We know how important it is to you that the leads are
fresh and delivered quickly. After daily processing, <?= $_SESSION['o_company']->get_title() ?> delivers most ads to your inbox by the next morning.
</span>


<br><br>
<h2 class="big2">How often do you deliver the Leads?</h2>
<span class="copy14">
Every morning, Monday-Sunday.</span>

<!--
<h2 class="big2">Explain your free Prefix Finder(tm) service.</h2>
<span class="copy14">
We know that the more information you have at your fingertips, the better.
Many FSBO ads contain cell phone and unlisted numbers which retrieve NO RESULTS from a
reverse directory search.  This is why we created Prefix Finder(tm) which tells you the CITY
SOURCE of the telephone prefix or prefixes in an ad.  This can be especially helpful in
situations when reverse directory searches retrieve no result, when the city is not listed
in an ad, and when tracking non-occupant sellers for properties.</span>
-->

<br><br>
<h2 class="big2">What does your service cost?</h2>
<span class="copy14">
Pricing is determined by the number of areas you receive as a subscriber. To see pricing, click the 'Areas Covered' link above (or here: <a href="areas_main.html">Areas Covered</a>), select your state, then check the area or areas you want. As you check areas, the pricing matrix below will dynamically display both Monthly and Annual pricing.
</span>


<br><br>
<h2 class="big2">How accurate are your leads?</h2>
<span class="copy14">
99.99% accurate.  We obtain our leads from public sources and deliver them to you as
they are advertised.  We make little to no changes to the ads.  If there are any inaccuracies
it typically originates from the source, such as the property seller or newspaper.  In the
case of a "wrong" phone number from a reverse directory search, again, the source of the error
typically originates from "old" or "inaccurate" data from the directory providers.
</span>

<!--
<br><br>
<h2 class="big2">How are these leads used?</h2>
<span class="copy14">
Our leads have a variety of users. Property managers use them to track landlords who are interested in using their property management services. Realtors use the leads to query the landlords about selling (and listing) the property. Realtors can also assist landlords in purchasing other rental properties. Investors use the leads to pursue potential investments ...  Lenders help landlords with loans for rental property financing and future purchases...
</span>
-->

<!--
<h2 class="big2">How do you screen out 'blind ads'?</h2>
<span class="copy14">
The 'blind ad' is one that does not identify the broker or agent placing the ad.
We use a system of tracking and eliminating most blind ads.  However, because
there is no way to determine the source of all blind ads, we cannot catch them all.
</span>
-->

<br><br>
<h2 class="big2">Does PM Leads do anything to help customers understand which phone numbers are on the U.S. National Do Not Call Registry?</h2>
<span class="copy14">
Yes. As a free service to our customers, all phone numbers are compared against the U.S. "Do Not
Call Registry" database. If a match is found, the phone number will be indicated as being a part of
the "Do Not Call Registry".
</span>

<br><br>
<h2 class="big2">What is the difference between <?= $_SESSION['o_company']->get_title() ?> and competitors?</h2>
<span class="copy14">
Our competitors seem to be people who continue to spend hours of their valuable time looking/farming for leads rather than intelligently working FRESH Leads.
<?= $_SESSION['o_company']->get_title() ?> is an innovator and the first to offer a national Property Manager/Owner/Landlord Leads service.</span>


</td>
</tr>
</table>


<br>
<h2 class="big2" align="center">For Rent by Owner Leads, For Rent Leads, Landlord Leads 
<br>Property Management Leads, Property Manager Leads</h2>


</td>
</tr>
<!--  <tr>
</tr> -->
</table>
</td></tr>
</table>

</td>
<td width="8" valign="top"><IMG src="/web/company_def/img/spacer.gif" alt="" width="8" height="1"></td>
</tr>
</table>

</TD>
</TR>
<TR>
<TD>
<IMG src="/web/company_def/img/spacer.gif" WIDTH=699 HEIGHT=12 ALT=""></TD>
</TR>
</TABLE>
<?php
include_once($_SESSION['web_interface']->get_server_path('other/global/bottom.php'));
?>
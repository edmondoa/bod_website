<!DOCTYPE html>
<html>
<head>
<TITLE>FRBO Leads | For Rent by Owner Leads | Property Manager Leads| Landlord Leads | Property Management Leads | Property Manager | FRBO</TITLE>

<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<META NAME="description" CONTENT="FRBO Leads for Property Managers">
	<META NAME="keywords" CONTENT="FRBO Leads, For Rent by Owner Leads, For Rent Leads, Landlord Leads, Property Management Leads, Property Manager Leads, Property Manager, FRBO">
	<META NAME="AUTHOR" CONTENT="PMLeads">
	<META NAME="COPYRIGHT" CONTENT="www.PMLeads.com">
	<META NAME="CONTACT_ADDR" CONTENT="info@PMLeads.com">
	<META NAME="RATING" CONTENT="General">
	<META NAME="robots" CONTENT="all">
	<meta name="google-site-verification" content="10jby2I1JfPVKWnEZnua1dzuM50yg0J8LMFD8HSa72Q" />
	<meta name="msvalidate.01" content="E3A36D47253BBBF9C14C14996A278B68" />


<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- //for-mobile-apps -->
<!-- <link href="/web/company_pmleads/other/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" /> -->
<link href="/web/company_pmleads/other/css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" /> -->
<!-- <link href="css/style.css" rel="stylesheet" type="text/css" media="all" /> -->

 
<!-- js -->
<script src="/web/company_pmleads/other/js/jquery-1.11.1.min.js"></script>
<!-- <script src="js/jquery-1.11.1.min.js"></script> -->
<!-- //js -->


<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Abril+Fatface' rel='stylesheet' type='text/css'>

</head>

	
<body>

<!-- header1 -->
	<div class="header1">
		<div class="container">
			<div class="header1-right">
				<p>
				&nbsp;&nbsp;&nbsp;<a href="services_main.html">Contact</a>
				&nbsp;&nbsp;&nbsp;<a href="login.html">Login</a>
				<!-- &nbsp;&nbsp;(801) 494-0470 -->
				&nbsp;&nbsp;&nbsp;<a href="areas_main_trial.html"><span>Free Trial</span></a></p>
			</div>

			<div class="clearfix"> </div>
			</div>
	</div>


<!-- header2 -->
	<div class="header2">
		<div class="container">
			<div class="navbar1-default">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar1-header">

<!-- 
				<button type="button" class="navbar1-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar1-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
 -->					
					
					<div class="logo1">  <!-- This is the logo top left -->
						<h1><a class="navbar1-brand" href="index.html">PM Leads</a></h1>
					</div>
				</div>

						<div class="drop1">
							<ul class="drop_menu1">
								<li><a href="index.html">HOME</a></li>
								<!-- <li><a href="index.html" class="activePage">HOME</a></li> -->
								
								<li><a href="frbo-leads.html" class="menu_spacer">LEADS</a>
								<!-- <li><a href="frbo-leads.html">PRODUCTS</a> -->								
									<ul>
										<li><a href="landlord-leads.html">for PMs & Realtors (FRBOs)</a></li>
										<li><a href="activePM-leads.html">Sell to: <br> PMs and <br>Realtors</a></li>
									</ul>
								</li>

								<li><a href="areas_main.html" class="menu_spacer">AREAS COVERED</a></li>
								<!-- <li><a href="areas_main.html">AREAS COVERED</a></li> -->
								
								<li><a href="areas_main_trial.html" class="menu_spacer">TRIAL</a></li>
								<!-- li><a href="areas_main_trial.html">TRIAL</a></li> -->
								
								<li><a href="faqs_main.html" class="menu_spacer">FAQs</a></li>
								<!-- <li><a href="faqs_main.html">FAQs</a></li> -->

								<li><a href="company.html" class="menu_spacer">COMPANY</a>
								<!-- <li><a href="company.html">COMPANY</a> -->								
									<ul>
										<li><a href="company.html">History</a></li>
										<li><a href="investor-leads.html">Leadership</a></li>
										<li><a href="for-rent-by-owner.html">Jobs</a></li>
									</ul>
								</li>

							</ul>
						</div>

 
			<div class="clearfix"> </div>
			
			</div>
		</div>
	</div>


<!-- bannner1 -->
	<div class="banner1">
		<div class="container">
			<!-- <div class="logo"> -->
				<!-- <a href="index.html">PM Leads</a> -->  <!-- This located in top middle of top graphic. -->
				<!-- <a href="index.html">PM Leads<span>Find Your HOME</span></a>  -->
			<!-- </div> -->
			
			<!--
			<div class="navigation">
				<nav class="navbar navbar-default">
					<div class="navbar-header">
					  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					  </button>
					</div>

					<div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
						<nav class="cl-effect-13" id="cl-effect-13">
							<ul class="nav navbar-nav">
								<li><a href="index.html" class="active">HOME</a></li>
								<li><a href="http://www.pmleads.com/areas_main.html">AREAS COVERED</a></li>
								<li><a href="http://www.pmleads.com/areas_main_trial.html">Trial</a></li>
								<li><a href="http://www.activePM-leads.html">FAQs</a></li>
								<li><a href="http://www.pmleads.com/company_history.html">COMPANY</a></li>
							</ul>
						</nav>
					</div>
				</nav>
			</div>
			-->


 
			<div class="banner1-info">
				<h3>FRBO Samples</h3>

			</div>

	 	</div>
	</div>
<!-- //bannner1 -->


<br>


<!-- banner-bottom -->
	<div class="faq">
		<div class="container">


			<h1>Property Managers/Realtors/Investors</h1>	
				<h2><center>For Rent by Owner (FRBO) Leads in email and/or in Spreadsheet (CSV) format
				<br> Here are five sample owner/landlord leads:</center></h2>
				
<!-- Ad  -->



<b>Lead 1.
<br>DATE: September 20   &nbsp;&nbsp;FL: Gainesville Area</b>
<br>PROPERTY TYPE: House
<br><br>
$1200 / 4br - 2400ft² - GREAT HOUSE IN A GREAT LOCATION FOR RENT!! (Gainesville) -- Great peaceful House for family!!! Great location in a friendly neighborhood! 4 bed / 3 bath / 2 car garage $ 1200 rent!! Please contact me if you are interested for more information before it's gone! 352-500-9999

<br><br>
Rental Property Information
<br>
STREET: 
33 W Delaware Place

<br>
CITY:
Gainesville

<br>
STATE:
FL

<br>
ZIP:
32608

<br><br>
Owner Information
<br>
EMAIL: 

<br>
PHONE 1 REVERSE LOOKUP: 352-500-9999* (Do Not Call Registry)
<br>
Adam
<br>
Area Code/Prefix Lookup: (352) 500-xxxx (Gainesville, FL)
<br><br>

____________________

<!-- Ad  -->

<br><br>
<b>Lead 2.
<br>DATE: September 20   &nbsp;&nbsp;OH: Butler County</b>
<br>PROPERTY TYPE: Townhome
<br><br>
$500 / 2br - Townhome for Rent (Middletown) -- This is a corner unit in the building and the townhomes have recently been remodeled. I supply the stove and refrigerator and I also pay the water and trash, you are responsible for the gas and electric. I put new a new furnace two years ago so the heat is very efficient. I do require a credit check, deposit, a one year lease and I will check previous references. Please call 513-967-9999 if you are interested. 500 Sutphin St.


<br><br>
Rental Property Information

<br>
STREET: 500 Sutphin St.

<br>
CITY: Middletown

<br>
STATE: OH

<br>
ZIP: 45043
<br><br>


Owner Information

<br>
EMAIL: 

<br>
PHONE 1 REVERSE LOOKUP: 513-967-9999

<br>


<br>
Area Code/Prefix Lookup: (513) 967-xxxx (Middletown, OH)
<br><br>

____________________
<!-- Ad  -->

<br><br>
<b>Lead 3.
<br>DATE: September 20  &nbsp;&nbsp;OR: Portland Area</b>
<br>PROPERTY TYPE: Condo

<br><br>
$1100 3 bedroom 2 bath unit with private wooded view on third floor 2 parking spaces no pets allowed no smoking allowed tenant pays electric and water must apply to Association and agree to background check Call to schedule a showing: Cathy 239.404.9999


<br><br>
Rental Property Information

<br>
STREET: 123 Elm

<br>
CITY: Portland

<br>
STATE: OR

<br>
ZIP: 97201


<br><br>
Owner Information

<br>
EMAIL: 

<br>
PHONE 1 REVERSE LOOKUP: 503-404-9999

<br>
Smith, Cathy

<br>
Area Code/Prefix Lookup: (503) 404-xxxx (Portland, OR)
<br><br>

____________________
<!-- Ad  -->

<br><br>
<b>Lead 4.
<br>DATE: September 20   &nbsp;&nbsp;AZ: Phoenix NE</b>
<br>PROPERTY TYPE: Apartment

<br><br>
2/1 APARTMENT UNFURNISHED MONTHLY RENT $499.00 CENTRAL HEAT AND AIR CONVENIENT TO SHOPPING,BANKING,FOOD AND BUS ROUTE AND I-295 EXCELLENT CONDITION, VANITY IN MASTERBEDROOM, MINI BLINDS, SPACIOUS WELL MAINTAINED AND MANAGED APARTMENTS VANITY IN MASTER BEDROOM QUALIFICATIONS: MONTHLY INCOME MINIMUM OF $1600. GOOD RENTAL HISTORY, NO CRIMINAL RECORD DEPOSIT $350.00 PRIVATE PROPERTY, APPOINTMENT NEEDED CALL PAUL AT 904-875-9999.


<br><br>
Rental Property Information

<br>
STREET: 1253 Westchase Ct.

<br>
CITY: Phoenix

<br>
STATE: AZ

<br>
ZIP: 85001


<br><br>
Owner Information

<br>
EMAIL: 

<br>
PHONE 1 REVERSE LOOKUP: 480-875-9999

<br>
Henry

<br>
Area Code/Prefix Lookup: (480) 875-xxxx (Scottsdale, AZ)
<br><br>



____________________
<!-- Ad  -->

<br><br>
<b>Lead 5.
<br>DATE: September 20  &nbsp;&nbsp;FL: Orlando Area</b>
<br>PROPERTY TYPE: House

<br><br>
$6000 / 5br - 3860ft² - Windermere Lakefront (Windermere) -- Luxury Executive Home for Lease- 5 bedroom, 4 full baths, 2 half baths plus a bonus room. Home features include a 3 car garage, screen enclosed salt water pool/spa, boat dock with boat & jet ski lift. Excellent location...walking distance to Publix and K-12 private school. Home is available for a December 1st move-in. Can be rented unfurnished for $5500 per month. 12 month lease or longer required. First, last & security deposit. Contact Phil @ 321-663-9999 for inquiries or to arrange a showing.


<br><br>
Rental Property Information

<br>
STREET: 11706 S Lake Sawyer Lane

<br>
CITY: Windermere

<br>
STATE: FL

<br>
ZIP: 34786


<br><br>
Owner Information

<br>
EMAIL: 

<br>
PHONE 1 REVERSE LOOKUP: 312-622-7519

<br>
Ed Barbar

<br>
Area Code/Prefix Lookup: (312) 622-xxxx (Windermere, FL)
<br><br>





<span class="copy14">
<br>
or delivered in Spreadsheet (CSV) format - only partial data shown
</span>

<br>
			<img src="http://www.pmleads.com/web/company_pmleads/other/images/LeadPM1.gif" style="border:thin solid black;"  class="img-responsive" />
			<!-- <img src="images/LeadPM1.gif" style="border:thin solid black;" alt="" class="img-responsive" /> -->

<br>

<!-- <IMG style="MARGIN-LEFT: 1px; MARGIN-RIGHT: 15px" 
			src="<?= $_SESSION['web_interface']->get_path('img/LeadPM1.gif') ?>" 
			align="left" vspace="1" border="1" alt="">
</img> -->


<br>
(Note: Addresses, Phone numbers & dates in these samples have been changed to keep details confidential.)
</span>

<span class="copy14">
<br><br>
Leads include: Property Type, Description, Bed, Bath, Sq. Feet, Price, Lot Size, Year Built, Garage Size, Latitude, Longitude and more fields.

<br><br>
When contact information is available, leads contain: Contact Name, Property/Contact Address, City, State/Province, Zip/Postal, Email, Phone Numbers (U.S. Do Not Call Registry indicated) and more fields.



		</div>
	</div>
<!-- //banner-bottom -->

<br><br><br><br>
					<center>
					<img src="http://www.pmleads.com/web/company_pmleads/other/images/house-for-rent6.jpg" alt="" class="img-responsive" />
					<!-- <img src="web/company_pmleads/other/images/house-for-rent6.jpg" alt="" class="img-responsive" /> -->
					<!-- <img src="images/house-for-rent6.jpg" style="border:thin solid black;" alt="" class="img-responsive" /> -->
					</center>	<!-- height="75%" width="75%" -->
<br>

<!-- footer1 -->
	<div class="footer1">
		<div class="container">
			<div class="footer1-grids">
<!--				<div class="col-md-4 footer1-grid-left">
					<h3>twitter feed</h3>
					<ul>
						<li><a href="single.html">It is a long established fact that a reader will 
						be distracted by the readable content of a page when looking at 
						its layout.</a><span>15 minutes ago</span></li>
						<li><a href="mailto:info@example.com" class="cols">@NASA</a> & <a href="mailto:info@example.com" class="cols">
							@orbital science</a> <a href="single.html">readable content of a page when looking at 
							its layout</a><span>45 minutes ago</span></li>
					</ul>
				</div>
				<div class="col-md-4 footer1-grid-left">
					<h3>Newsletter</h3>
					<form>
						<input type="email" value="enter your email address" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'enter your email address';}" required="">
						<input type="submit" value="Submit" >
					</form>
				</div>
				<div class="col-md-4 footer1-grid-left">
					<h3>about us</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do 
						eiusmod tempor incididunt ut labore et dolore magna aliqua. 
						Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.
						<span>But I must explain to you how all this mistaken idea of denouncing
						pleasure and praising pain was born and I will give you a complete 
						account of the system, and expound the actual teachings of the 
						great explorer.</span>
						</p>
				</div>
				<div class="clearfix"> </div>
			</div>
-->

			<div class="footer1-bottom">
				<div class="footer1-bottom-left">
					<p>&copy2003 - <?= date('Y') ?>
					&nbsp;&nbsp;PMLeads 
					<!-- &nbsp;&nbsp;<a href="http://www.pmleads.com/sitemap.html">sitemap</a>  -->
					&nbsp;&nbsp;(801) 494-0470
					&nbsp;&nbsp;<a href="mailto:info@byownerdaily.com">info@pmleads.com</a></p></p>
				</div>

				<!-- <div class="footer1-bottom-right"> -->
				<div class="header1-right">

					<p><a href="areas_main_trial.html"><span>Free Trial</span></a></p>

<!-- 					<ul>
						<li><a href="#" class="icon-button twitter"><i class="icon-twitter"></i><span></span></a></li>
						<li><a href="#" class="icon-button google"><i class="icon-google"></i><span></span></a></li>
						<li><a href="#" class="icon-button v"><i class="icon-v"></i><span></span></a></li>
					</ul>
 -->
				</div>

				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
<!-- //footer1 -->

<!-- for bootstrap working -->
	<script src="js/bootstrap.js"></script>
<!-- //for bootstrap working -->

</body>
</html>

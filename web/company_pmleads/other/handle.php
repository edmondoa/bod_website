<?php

switch (strtolower($_REQUEST['cmd'])) {

	//handle a LOGIN request
	case 'login':
		$_SESSION['web_interface']->check_required( array(required=>'main_email_address,password') );
		if (isset($_SESSION['error'])) {
			include_once($_SESSION['web_interface']->get_server_path('other/login.php'));
		}
		else {
			try {
				$_SESSION['office']->account = new account( array('main_email_address'=>$_REQUEST['main_email_address'], 'password'=>$_REQUEST['password']) );
				if ($_SESSION['office']->account->get_accountid()) {
					/*
					if ($_SESSION['office']->account->get_billing_status() == 'canceled') {
						$_SESSION['error']['message'] = 'Your account has been canceled.';
						include_once($_SESSION['web_interface']->get_server_path('other/login.php'));
					}
					else if ($_SESSION['office']->account->get_account_type() == 'TRIAL') {
						$_SESSION['error']['message'] = 'You currently have a Trial account and therefore cannot login.';
						include_once($_SESSION['web_interface']->get_server_path('other/login.php'));
					}
					else {
					*/
						include_once($_SESSION['web_interface']->get_server_path('office/index.php'));
					//}
				}
				else {
					$_SESSION['error']['message'] = 'That Email and/or Password could not be found.<br/>Please try again.';
					include_once($_SESSION['web_interface']->get_server_path('other/login.php'));
				}
			}
			catch (Exception $exception) {
				throw $exception;
			}
		}
	break;

	// handle a PASSWORD request
	case 'password':
		try {
			$_SESSION['web_interface']->check_required( array(required=>'main_email_address') );
			if (isset($_SESSION['error'])) {
				throw new Exception($_SESSION['error']['message']);
			}
			$o_account = new account( array('main_email_address'=>$_REQUEST['main_email_address']) );
			if (!$o_account->get_accountid()) {
				throw new Exception('We were unable to find an account with that information. Please check and try again.');
			}
			$newPassword = $_SESSION['web_interface']->generateRandomString();
			$newPasswordHashed = $_SESSION['web_interface']->getHashString( array('salt'=>$o_account->get_accountid(), 'string'=>$newPassword) );
			if ($newPasswordHashed) {
				$o_account->initialize( array('password'=>$newPasswordHashed) );
				$o_account->save();
				mail($o_account->get_main_email_address(), 'Member Account Password', 'Your Password is: ' . $newPassword, "From: support@" . $o_account->o_company->get_url());
				$_SESSION['status']['message'] = 'Your password has been sent to: ' . $o_account->get_main_email_address();
			}
			else {
				$_SESSION['error']['message'] = 'There was a problem with generating a new password. Please try again.';
			}
		}
		catch (Exception $exception) {
			$_SESSION['error']['message'] = $exception->getMessage();
		}
		include_once($_SESSION['web_interface']->get_server_path('other/password.php'));
	break;
	
	// handle a SIGNUP request
	case 'signup':
		unset($_SESSION['error']);
		$o_account = new account( array('main_email_address'=>$_REQUEST['main_email_address']) );
		
		// check to make sure requireds
		$_SESSION['web_interface']->check_required( array(required=>'first_name,last_name,personal_phone,main_email_address,business_name') );
		
		// upfirst the first_name and last_name
		$_REQUEST['first_name'] = trim(ucwords($_REQUEST['first_name']));
		$_REQUEST['last_name'] = trim(ucwords($_REQUEST['last_name']));
		$_REQUEST['business_name'] = trim(ucwords($_REQUEST['business_name']));
		
		// check and process phone number
		$personal_phone = preg_replace("/[^\d]/", "", $_REQUEST['personal_phone']);
		if (strlen($personal_phone) != 10) {
			$_SESSION['error']['message'] .= '<br/>Please enter your phone number including area code (i.e. 801-494-0470).';
			$_SESSION['error']['badfield']['personal_phone'] = TRUE;
		}
		else {
			$_REQUEST['personal_phone'] = preg_replace("/^(\d{3})(\d{3})(\d{4})$/", "$1-$2-$3", $personal_phone);
		}
		
		// see if an account with that email address already exists
		if ($o_account->get_accountid() && $o_account->get_account_type() != 'TRIAL') {
			$_SESSION['error']['message'] .= '<br/>An account with that Email address already exists. To reactivate your account please call us at (801) 494-0470. Thank you.';
		}
		else if ($o_account->get_accountid() && $o_account->get_account_type() == 'TRIAL') {
			$o_account->set_notes(date('Y-m-d') . ': Trial account signed up. Original Start Date: ' . $o_account->get_start_date() . ' Original End Date: ' . $o_account->get_end_date() . "\n" . $o_account->get_notes());
		}
		
		// check to make sure they have selected at least one area
		$chosen_area = ($_REQUEST['area_nameid_radio']) ? TRUE : FALSE;
		if (!$chosen_area) {
			$_SESSION['error']['message'] .= '<br/>You must select an area.';
		}

		// set subscription type to monthly
		$_REQUEST['subscription_type'] = 'Monthly';
		
		// make sure they have agreed to the terms
		if (!$_REQUEST['terms_conditions']) {
			$_SESSION['error']['message'] .= '<br/>You must agree to the Terms and Conditions.';
		}
		
		// check the property types if applicable
		if (isset($_REQUEST['property_type_checkbox'])) {
			// auto add the General and Residential property_type
			$_REQUEST['property_type_checkbox'][] = 'General';
			$_REQUEST['property_type_checkbox'][] = 'Residential';
			// check to see if they have checked them all
			require_once($_SESSION['web_interface']->get_server_path('other/misc/property_type_fields.php'));
			if (count($propertyTypeFields) == count($_REQUEST['property_type_checkbox'])) {
				unset($_REQUEST['property_type_checkbox']);
			}
			else {
				$_REQUEST['property_type_field_checkbox'] = array();
				foreach ($_REQUEST['property_type_checkbox'] as $key=>$value) {
					$_REQUEST['property_type_field_checkbox'][] = $value;
				}
			}
		}
		// by this point if no error then should be ok
		if (!isset($_SESSION['error'])) {
			// set some defaults
			$_REQUEST['billing_type'] = 'Credit Card';
			
			$_REQUEST['affiliateid'] = $_COOKIE['AFFILIATEID'];
			$_REQUEST['account_type'] = 'TRIAL';
			$_REQUEST['companyid'] = $_SESSION['o_company']->get_companyid();
			$_REQUEST['email_attachment'] = ($_REQUEST['email_attachment']) ? $_REQUEST['email_attachment'] : '0';
			
			// set start and end dates
			$_REQUEST['start_date'] = date('Y-m-d');
			$_REQUEST['end_date'] = date('Y-m-d', strtotime("+14 DAYS"));
			
			// calculate next bill date
			$month = date('n');
			$day = date('j');
			$year = date('Y');
			if ($day > 28) {
				$day = 1;
				$month++;
			}
			if ($month > 12) {
				$month = 1;
				$year++;
			}
			
			$next_bill_date = date('Y-m-d', strtotime("+1 MONTH", strtotime($year . '-' . $month . '-' . $day)));
			if (isset($_SESSION['affiliate'])) {
				$_REQUEST['rate'] = $_REQUEST['total_month_affiliate_price'];
			}
			else {
				$_REQUEST['rate'] = $_REQUEST['total_month_regular_price'];
			}
			
			// set username = main_email_address
			$_REQUEST['username'] = $_REQUEST['main_email_address'];
			
			// add the extension on to phones if needed
			if ($_REQUEST['personal_phone_ext']) {
				$_REQUEST['personal_phone'] .= ' ext. ' . $_REQUEST['personal_phone_ext'];
			}
			
			$o_account->initialize($_REQUEST);
			$o_account->o_business_address->save();
			$o_account->o_personal_address->save();
			
			// create account
			$o_account->set_business_addressid($o_account->o_business_address->get_addressid());
			$o_account->set_personal_addressid($o_account->o_personal_address->get_addressid());
			if (!$o_account->get_accountid()) {
				$o_account->create();
				// need to do the customer number
				$o_account->set_customer_number($o_account->get_accountid());
			}
			$_REQUEST['password'] = $_SESSION['web_interface']->getHashString( array('salt'=>$o_account->get_accountid(), 'string'=>'p@$$word') );
			$o_account->initialize( array('password'=>$_REQUEST['password']) );
			$o_account->save();
			
			// save their product type (hard coded for now)
			if (isset($_REQUEST['productName'])) {
				$o_account->setProduct( array('productName'=>$_REQUEST['productName']) );
			}
			
			if ($o_account->get_accountid()) {
				if (!isset($_SESSION['error']['message'])) {
					// build hash of area_names
					require_once('office/account/class.area_name.php');
					$o_area_name_utilities = new area_name_utilities();
					$list_area_name = $o_area_name_utilities->get_list();
					$hash_area_name = array();
					foreach ($list_area_name as $item_area_name) {
						$hash_area_name[$item_area_name->get_area_nameid()] = $item_area_name->get_area_name();
					}
					
					$area_names = array($hash_area_name[$_REQUEST['area_nameid_radio']]);
					$o_account->save_area_names($area_names);
					
					// save property types
					$o_account->save_property_type_fields($_REQUEST['property_type_field_checkbox']);
					
					// send email to admin
					$body = "A New Short Signup has occured with the following information:\n" . 
						"Account ID: " . $o_account->get_accountid() . "\n" . 
						"Name: " . $o_account->get_first_name() . ' ' . $o_account->get_last_name() . "\n" . 
						"Email Address: " . $o_account->get_main_email_address() . "\n" . 
						"Main Phone: " . $o_account->get_personal_phone() . "\n" . 
						"Business Name: " . $o_account->get_business_name() . "\n\n"
					;
					$body .= "Areas\n";
					foreach ($o_account->get_list_area_names() as $area_name) {
						$body .= $area_name . "\n";
					}
					if (isset($_SESSION['affiliate'])) {
						$body .= "Affiliate Total: $" . $_REQUEST['total_month_affiliate_price'] . "/Month for " . $_REQUEST['total_areas'] . " area(s)\n";
						$body .= "Regular Total: $" . $_REQUEST['total_month_regular_price'] . "/Month for " . $_REQUEST['total_areas'] . " area(s)\n\n";
					}
					else {
						$body .= "Total: $" . $_REQUEST['total_month_regular_price'] . "/Month for " . $_REQUEST['total_areas'] . " area(s)\n\n";
					}
					$body .= "Other Information\n";
					if ($_REQUEST['email_attachment']) {
						$body .= "Email Attachment: Yes\n";
					}
					mail('admin@byownerdaily.com', 'New Trial Signup: ' . $o_account->get_first_name() . ' ' . $o_account->get_last_name(), $body, "From: signup@" . $_SESSION['o_company']->get_url());

					// send email to new signup
					require_once('office/account/class.letter.php');
					$o_letter = new letter( array('title'=>'Short Signup Welcome (Standard)') );
					$email_segments = $o_letter->merge_segments( array('o_account'=>$o_account) );
					$headers = '';
     			if ($email_segments['cc_list']) {
     				$headers .= "Cc: " . $email_segments['cc_list'] . "\r\n";
     			}
     			if ($email_segments['bcc_list']) {
     				$headers .= "Bcc: " . $email_segments['bcc_list'] . "\r\n";
     			}
     			$headers .= "From: " . $email_segments['from_email'];
					//mail($_REQUEST['main_email_address'], $email_segments['subject'], $email_segments['body'], $headers);

					header('Location: /confirm_signup.php');
				}
			}
			else {
				$_SESSION['error']['message'] = 'There was an error creating your account. Please contact Customer Support.';
			}
		}
		include_once($_SESSION['web_interface']->get_server_path('other/signup.php'));
	break;

	// handle a UPDATE_CREDIT_CARD request
	case 'update_credit_card':
		unset($_SESSION['error']);
		try {
			$_SESSION['web_interface']->check_required( array(required=>'first_name,last_name,personal_phone,main_email_address,card_name,card_type,card_number,card_exp_date') );
			if (isset($_SESSION['error']['message'])) {
				throw new Exception($_SESSION['error']['message']);
			}
			
			// check email matches
			if ($_REQUEST['main_email_address'] != $_REQUEST['confirm_email_address']) {
				$_SESSION['error']['badfield']['confirm_email_address'] = TRUE;
				$_SESSION['error']['badfield']['main_email_address'] = TRUE;
				$_SESSION['error']['message'] = 'Please verify your email addresses match.';
				throw new Exception($_SESSION['error']['message']);
			}
			
			// check to make sure they have put in a valid card_exp_date
			if (!preg_match("/^\d{4}$/", $_REQUEST['card_exp_date'])) {
				$_SESSION['error']['badfield']['card_exp_date'] = TRUE;
				$_SESSION['error']['message'] = 'Please enter a valid Credit Card Expiration Date.';
			}
			else {
				$month = preg_replace("/^(\d\d)\d\d$/", "$1", $_REQUEST['card_exp_date']);
				$year = '20' . preg_replace("/^\d\d(\d\d)$/", "$1", $_REQUEST['card_exp_date']);
				if (strtotime($year . '-' . $month . '-01') < strtotime(date('Y-m-01'))) {
					$_SESSION['error']['badfield']['card_exp_date'] = TRUE;
					$_SESSION['error']['message'] = 'Please enter a valid Credit Card Expiration Date.';
				}
			}
			if (isset($_SESSION['error']['message'])) {
				throw new Exception($_SESSION['error']['message']);
			}
			
			// send email to admin
			$body = "Billing Update:\n" . 
				"Name: " . $_REQUEST['first_name'] . ' ' . $_REQUEST['last_name'] . "\n" . 
				"Email Address: " . $_REQUEST['main_email_address'] . "\n" . 
				"Main Phone: " . $_REQUEST['personal_phone'] . "\n" . 
				"Work Phone: " . $_REQUEST['business_phone'] . "\n" . 
				"Business Name: " . $_REQUEST['business_name'] . "\n\n"
			;
			$body .= "Street Address\n" . 
				$_REQUEST['line1'] . "\n"
			;
			$body .= $_REQUEST['city'] . ', ' . $_REQUEST['state'] . ' ' . $_REQUEST['postal_code'] . "\n\n";
			$body .= "Credit Card Information\n" . 
				"Name on Card: " . $_REQUEST['card_name'] . "\n" . 
				"Card Type: " . $_REQUEST['card_type'] . "\n" . 
				"Expiration Date: " . $_REQUEST['card_exp_date'] . "\n\n"
			;
			mail('admin@byownerdaily.com', 'Billing Update: ' . $_REQUEST['first_name'] . ' ' . $_REQUEST['last_name'], $body, "From: billing@" . $_SESSION['o_company']->get_url());
			include_once($_SESSION['web_interface']->get_server_path('other/ssl/cc_thank_you.php'));
		}
		catch (Exception $exception) {
			$_SESSION['error']['message'] = $exception->getMessage();
			include_once($_SESSION['web_interface']->get_server_path('other/ssl/cc_update.php'));
		}
	break;

}

?>
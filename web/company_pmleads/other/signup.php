<?php
$_REQUEST['state'] = ($_REQUEST['state']) ? $_REQUEST['state'] : $_REQUEST['subscribe_state'];

require_once('office/account/class.area_name.php');
$o_area_name_utilities = new area_name_utilities();
$area_state = $o_area_name_utilities->get_area_state( array('state'=>$_REQUEST['subscribe_state']) );
$list_area_name = $o_area_name_utilities->get_list( array('where'=>" WHERE area_name LIKE '" . $_SESSION['data_access']->db_quote($_REQUEST['subscribe_state']) . "-%' ") );

$_META_DESCRIPTION = '';
$_META_KEYWORDS = '';
$_PAGE_TITLE = $area_state . ' Leads | ' . $area_state . ' Rental Property Leads | ' . $area_state . ' Rental Leads';

if (isset($_SESSION['affiliate'])) {
	$_affiliate_month_additional_area_pricing = $_SESSION['affiliate']->get_month_additional_area_pricing();
	$_affiliate_month_one_area_pricing = $_SESSION['affiliate']->get_month_one_area_pricing();
	$_affiliate_month_two_area_pricing = $_SESSION['affiliate']->get_month_two_area_pricing();
	$_affiliate_year_additional_area_pricing = $_SESSION['affiliate']->get_year_additional_area_pricing();
	$_affiliate_year_one_area_pricing = $_SESSION['affiliate']->get_year_one_area_pricing();
	$_affiliate_year_two_area_pricing = $_SESSION['affiliate']->get_year_two_area_pricing();
}
else {
	$_affiliate_month_additional_area_pricing = $_SESSION['o_company']->get_month_additional_area_pricing();
	$_affiliate_month_one_area_pricing = $_SESSION['o_company']->get_month_one_area_pricing();
	$_affiliate_month_two_area_pricing = $_SESSION['o_company']->get_month_two_area_pricing();
	$_affiliate_year_additional_area_pricing = $_SESSION['o_company']->get_year_additional_area_pricing();
	$_affiliate_year_one_area_pricing = $_SESSION['o_company']->get_year_one_area_pricing();
	$_affiliate_year_two_area_pricing = $_SESSION['o_company']->get_year_two_area_pricing();
}

$_regular_month_additional_area_pricing = $_SESSION['o_company']->get_month_additional_area_pricing();
$_regular_month_one_area_pricing = $_SESSION['o_company']->get_month_one_area_pricing();
$_regular_month_two_area_pricing = $_SESSION['o_company']->get_month_two_area_pricing();
$_regular_year_additional_area_pricing = $_SESSION['o_company']->get_year_additional_area_pricing();
$_regular_year_one_area_pricing = $_SESSION['o_company']->get_year_one_area_pricing();
$_regular_year_two_area_pricing = $_SESSION['o_company']->get_year_two_area_pricing();

require_once($_SESSION['web_interface']->get_server_path('other/misc/property_type_fields.php'));
require_once($_SESSION['web_interface']->get_server_path('other/misc/select_options.php'));
include_once($_SESSION['web_interface']->get_server_path('other/global/top.php'));
include_once($_SESSION['web_interface']->get_server_path('other/misc/status_error.php'));
?>
<script language="javascript" type="text/javascript">
	if (!document.all)
		document.captureEvents(Event.MOUSEMOVE)
	
	document.onmousemove = getPosition; 
	var X = 0
	var Y = 0 

	function getPosition(args) {
	  if (document.all) {
	    X = event.clientX + document.body.scrollLeft
	    Y = event.clientY + document.body.scrollTop
	  }
	  else { 
	    X = args.pageX
	    Y = args.pageY
	  } 
	}

	function popUp(popUpName) {
    var div;
    if(document.getElementById)
	    div = document.getElementById(popUpName);
    else if(document.all)
	    div = document.all[popUpName];   
    if(div.style.display== '' && div.offsetWidth != undefined && div.offsetHeight != undefined) {
        div.style.display = (div.offsetWidth!=0 && elem.offsetHeight!=0)?'block':'none';
    }
    div.style.display = (div.style.display==''||div.style.display=='block')?'none':'block';
    X = X + 15;   
    div.style.left = X+'px';
    div.style.top = Y+'px';
	}
	
	function calculateTotals() {
		var $total_areas = 0;
		for (var i=0;i<myForm.elements.length;i++) {
			var myElement = myForm.elements[i];
			if (myElement.type == 'radio') {
				var pattern = /^area_nameid_radio/;
				if (pattern.test(myElement.id)) {
					if (myElement.checked == true) {
						$total_areas = $total_areas + 1;
					}
				}
			}
		}
		var $total_month_affiliate_price = 0;
		var $total_month_regular_price = 0;
		var $total_year_affiliate_price = 0;
		var $total_year_regular_price = 0;
		
		if ($total_areas == 1) {
			$total_month_affiliate_price = <?= $_affiliate_month_one_area_pricing ?>;
			$total_month_regular_price = <?= $_regular_month_one_area_pricing ?>;
			$total_year_affiliate_price = <?= $_affiliate_year_one_area_pricing ?> * 12;
			$total_year_regular_price = <?= $_regular_year_one_area_pricing ?> * 12;
		}
		else if ($total_areas == 2) {
			$total_month_affiliate_price = <?= $_affiliate_month_two_area_pricing ?>;
			$total_month_regular_price = <?= $_regular_month_two_area_pricing ?>;
			$total_year_affiliate_price = <?= $_affiliate_year_two_area_pricing ?> * 12;
			$total_year_regular_price = <?= $_regular_year_two_area_pricing ?> * 12;
		}
		else if ($total_areas > 2) {
			$over_areas = $total_areas - 2;
			$over_month_affiliate_price = $over_areas * <?= $_affiliate_month_additional_area_pricing ?>;
			$over_month_regular_price = $over_areas * <?= $_regular_month_additional_area_pricing ?>;
			$total_month_affiliate_price = <?= $_affiliate_month_two_area_pricing ?> + $over_month_affiliate_price;
			$total_month_regular_price = <?= $_regular_month_two_area_pricing ?> + $over_month_regular_price;
			$over_year_affiliate_price = $over_areas * <?= $_affiliate_year_additional_area_pricing ?>;
			$over_year_regular_price = $over_areas * <?= $_regular_year_additional_area_pricing ?>;
			$total_year_affiliate_price = (<?= $_affiliate_year_two_area_pricing ?> + $over_year_affiliate_price) * 12;
			$total_year_regular_price = (<?= $_regular_year_two_area_pricing ?> + $over_year_regular_price) * 12;
		}
		myForm.total_areas.value = $total_areas;
		myForm.total_month_affiliate_price.value = $total_month_affiliate_price;
		myForm.total_month_regular_price.value = $total_month_regular_price;
		myForm.total_year_affiliate_price.value = $total_year_affiliate_price;
		myForm.total_year_regular_price.value = $total_year_regular_price;
	}
</script>
<form name="myForm" id="myForm" method="post" action="/handle.html" return onSubmit="return checkEmail(document.getElementById('main_email_address'));">
<input type="hidden" name="cmd" value="signup" />
<input type="hidden" name="productName" value="FRBO" />
<input type="hidden" name="subscribe_state" value="<?= $_REQUEST['subscribe_state'] ?>" />
<input type="hidden" name="total_areas" value="" />
<input type="hidden" name="total_month_affiliate_price" value="" />
<input type="hidden" name="total_month_regular_price" value="" />
<input type="hidden" name="total_year_affiliate_price" value="" />
<input type="hidden" name="total_year_regular_price" value="" />
<table width="699" height="355" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="7" valign="top"><img src="/web/company_def/img/spacer.gif" width="7" height="1"></td>
		<td width="684" valign="top">

			<!-- Main body Table -->
			<table border="0" cellspacing="0" cellpadding="1" bgcolor="#DDDDDD">
				<tr>
					<td valign="top">

						<!-- Inner1 Main body Table -->
						<table width="682" height="353" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
							<tr><td valign="top" height="3" bgcolor="#D9E2E6"><img src="/web/company_def/img/spacer.gif"></td></tr>
							<tr>
								<td valign="top" height="350" style="padding:11px 17px 10px 15px; line-height:14px " class="big">
									<strong class="blue">Free Trial - <?= $area_state ?> Leads</strong><br>
									<br style="line-height:10px ">
									<div style="height:1px; background-color:#A0A0A0 "><img src="/web/company_def/img/spacer.gif"></div>
									<br style="line-height:10px">

									<!-- Inner2 Main body Table -->
									<table width="100%" cellspacing="0" cellpadding="2" border="0">
										<tr>
											<td valign="top" class="seal_<?= $_REQUEST['state'] ?>_bg">
												Get started today! Your first 14 days are FREE!
												<br><br>
												
												<!-- Select Service Areas Table -->
												<TABLE width="75%" CELLSPACING="0" CELLPADDING="0" BORDER="0">
													<TR>
														<TD COLSPAN="2" ALIGN="LEFT">
															<span class="header1"><strong>1. Choose 1 area for the trial period</strong></span><br/>
															&nbsp;&nbsp;&nbsp&nbsp;If you would like to try more than 1 area, please contact us at (801) 494-0470.<br/>&nbsp;&nbsp;&nbsp;&nbsp;For area details, click the area name or mouseover
														</td>
													</TR>
												</TABLE>

												<!-- Service Areas Table -->
												<TABLE width="75%" CELLSPACING="0" CELLPADDING="0" BORDER="0">
<?php
$count = 0;
foreach ($list_area_name as $item_area_name) {
	if (strtolower($item_area_name->get_area_title()) == 'other') {
		continue;
	}
?>
														<tr>
															<TD ALIGN="left">
																&nbsp;<input type="radio" id="area_nameid_radio" name="area_nameid_radio" onClick="calculateTotals();" style="border:0px;" value="<?= $item_area_name->get_area_nameid() ?>" <?php if ($_REQUEST['area_nameid_radio'] == $item_area_name->get_area_nameid()) { print 'checked="checked"'; } ?> /><a href="/areas_ind_<?= $item_area_name->get_area_nameid() ?>.html" onMouseOver="popUp('areaInfo<?= $item_area_name->get_area_nameid() ?>');" onMouseOut="popUp('areaInfo<?= $item_area_name->get_area_nameid() ?>');"><?= $item_area_name->get_area_title() ?></a>
																<div class="areaInfo" id="areaInfo<?= $item_area_name->get_area_nameid() ?>" name="areaInfo<?= $item_area_name->get_area_nameid() ?>" style="display: none;">
																	<div class="areaInfoHeader"><?= $item_area_name->get_area_title() ?></div>
																	<table class="areaInfoContent">
<?php
	if (!preg_match("/county\s?+$/i", $item_area_name->get_area_title()) && !preg_match("/counties\s?+$/i", $item_area_name->get_area_title())) {
?>
																		<tr><td>Description:</td><td><?= $item_area_name->get_description() ?></td></tr>
<?php
	}
?>
																		<tr><td>Cities:</td><td><?= $item_area_name->get_cities() ?></td></tr>
																	</table>
																</div>
															</TD>
														</tr>
<?php
	$count++;
}
?>
													<TR><TD>&nbsp;</td></tr>
												</TABLE>
												
												<!-- property types -->
												<TABLE width="85%" CELLSPACING="0" CELLPADDING="1" BORDER="0">
													<TR><TD COLSPAN="4" ALIGN="left"><span class="header1"><strong>2. Property Types</strong></span></td></TR>
													<tr><td colspan="4" align="left">Please select the property types you would like. You will only receive information about those listings that match the property types you select below. These options can be changed anytime by logging into your account with the information you will receive in the email following your signup.</td></tr>
													<tr>
														<td colspan="4" align="left">
															<table border="0" cellspacing="0" cellpadding="3">
																<tr><td><input id="checkAllPropertyTypes" name="checkAllPropertyTypes" type="checkbox" onclick="checkAll(this, 'property_type');"/>Check All</td></tr>
																<tr>
<?php
$count = 1;
if (isset($_REQUEST['property_type_checkbox'])) {
	foreach ($_REQUEST['property_type_checkbox'] as $index=>$field) {
		$_REQUEST['property_type_checkbox'][$field] = TRUE;
	}
}
foreach ($propertyTypeFields as $field) {
	if ($_REQUEST['property_type_checkbox'][$field]) {
?>
																	<td style="font-weight: bold;"><input type="checkbox" name="property_type_checkbox[]" id="property_type_checkbox" value="<?= $field ?>" checked="checked" <?php if ($field == 'General' || $field == 'Residential') { print 'disabled readonly'; } ?> /><?= $field ?></td>
<?php
	}
	else {
?>
																	<td><input type="checkbox" name="property_type_checkbox[]" id="property_type_checkbox" value="<?= $field ?>" <?php if ($field == 'General' || $field == 'Residential') { print 'disabled readonly'; } ?> /><?= $field ?></td>
<?php
	}
	if ($count == 4) {
		print '</tr><tr>';
		$count = 0;
	}
	$count++;
}
?>
																</tr>
															</table>
														</td>
													</tr>
													<TR><TD>&nbsp;</td></tr>
												</table>
<?php
if (!isset($_REQUEST['property_type_checkbox'])) {
?>
												<script type="text/javascript">
													document.getElementById('checkAllPropertyTypes').checked = 'checked';
													checkAll(document.getElementById('checkAllPropertyTypes'), 'property_type');
												</script>
<?php
}
?>

												<!-- MIN / MAX Pricing -->
<?php
if (!isset($_REQUEST['minPrice'])) {
	$_REQUEST['minPrice'] = 500;
}
?>
												<!--
												<TABLE width="85%" CELLSPACING="0" CELLPADDING="1" BORDER="0">
													<TR><TD COLSPAN="4" ALIGN="left"><span class="header1"><strong>3. Price Range</strong></span></td></TR>
													<tr><td colspan="4" align="left">Please enter the minimum and maximum price ranges you would like. You will only receive information about those listings that match the price ranges you select below. Please leave the minimum and / or maximum blank if you do not wish to limit the minimum and / or maximum price ranges you would like to receive. After you have signed up, these options can be changed anytime by logging into your account.</td></tr>
													<tr>
														<TD ALIGN="RIGHT" style="white-space: nowrap;" <?= $_SESSION['web_interface']->missing_required_label('minPrice') ?>>Minimum Price:</td>
														<TD><input type="text" name="minPrice" tabindex="1" value="<?= $_REQUEST['minPrice'] ?>" <?= $_SESSION['web_interface']->missing_required_input('minPrice') ?>></td>
														<TD ALIGN="RIGHT" style="white-space: nowrap;" <?= $_SESSION['web_interface']->missing_required_label('maxPrice') ?>>Maximum Price:</td>
														<TD><input type="text" name="maxPrice" tabindex="2" value="<?= $_REQUEST['maxPrice'] ?>" <?= $_SESSION['web_interface']->missing_required_input('maxPrice') ?>></td>
													</tr>
													<TR><TD>&nbsp;</td></tr>
												</table>
												-->
													
												<!-- Contact Info Table -->
												<TABLE width="85%" CELLSPACING="0" CELLPADDING="1" BORDER="0">
													<TR><TD COLSPAN="4" ALIGN="left"><span class="header1"><strong>3. Contact Information</strong></span></td></TR>
													<TR>
														<TD ALIGN="RIGHT" <?= $_SESSION['web_interface']->missing_required_label('first_name') ?>>First Name*:</td>
														<TD><input type="text" name="first_name" tabindex="3" value="<?= $_REQUEST['first_name'] ?>" <?= $_SESSION['web_interface']->missing_required_input('first_name') ?>></td>
														<TD ALIGN="RIGHT" <?= $_SESSION['web_interface']->missing_required_label('last_name') ?>>Last Name*:</td>
														<TD><input type="text" name="last_name" tabindex="4" value="<?= $_REQUEST['last_name'] ?>" <?= $_SESSION['web_interface']->missing_required_input('last_name') ?>></td>
													</tr>
													<TR>
														<TD ALIGN="RIGHT" <?= $_SESSION['web_interface']->missing_required_label('main_email_address') ?>>Email address*:</td>
														<TD><input type="text" id="main_email_address" name="main_email_address" tabindex="5" value="<?= $_REQUEST['main_email_address'] ?>" <?= $_SESSION['web_interface']->missing_required_input('main_email_address') ?>></td>
														<TD ALIGN="right" <?= $_SESSION['web_interface']->missing_required_label('personal_phone') ?>>Contact Phone*:</td>
														<TD><input type="text" name="personal_phone" tabindex="6" value="<?= $_REQUEST['personal_phone'] ?>" size="12" <?= $_SESSION['web_interface']->missing_required_input('personal_phone') ?>> ext. <input type="text" name="personal_phone_ext" tabindex="7" value="<?= $_REQUEST['personal_phone_ext'] ?>" size="5" <?= $_SESSION['web_interface']->missing_required_input('personal_phone_ext') ?>></td>
													</tr>
													<tr>
														<TD ALIGN="right" <?= $_SESSION['web_interface']->missing_required_label('business_name') ?>>Company*:</td>
														<TD><input type="text" name="business_name" tabindex="8" value="<?= $_REQUEST['business_name'] ?>" <?= $_SESSION['web_interface']->missing_required_input('business_name') ?>></td>
													</tr>
												</table>
												<br>

												<!-- Submit, Clear, Checkboxes Table -->		
												<TABLE width="100%" CELLSPACING="0" CELLPADDING="0" BORDER="0">		
													<TR><TD COLSPAN="4" ALIGN="left"><span class="header1"><strong>4. File Attachment</strong></span></td></TR>		
													<TR>
														<TD>
															<input type="checkbox" name="email_attachment" tabindex="9" value="1" style="border:0px;" <?php if ($_REQUEST['email_attachment']) { print 'checked="checked"'; } ?> />&nbsp;In addition to the daily email, would you like a .csv (comma delimited) file attachment sent? <a href="#" onMouseOver="popUp('whatIsCsv');" onMouseOut="popUp('whatIsCsv');">What is this?</a>
															<div class="areaInfo" id="whatIsCsv" name="whatIsCsv" style="display: none;">
																	<div class="areaInfoHeader">CSV File Attachment</div>
																	<table class="areaInfoContent">
																		<tr><td>The .CSV ("Comma Separated Value", also called "Comma Delimited") is a widely supported database format that can be included in the daily email lead file as an attachment. This format, with lead data divided into fields, is commonly used to import lead information into auto dialers, spreadsheets and prospecting/marketing software and systems, and for creating mailing labels. This option is included for you at no extra cost.<br/><br/>This same lead data file can also be accessed and downloaded by logging in online. However, this download option is available only to paying subscribers. </td></tr>
																	</table>
																</div>
														</td>
													</tr>
													<TR><TD COLSPAN="4">&nbsp;</td></tr>

													<TR><TD COLSPAN="4" ALIGN="left"><span class="header1"><strong>5. Terms and Conditions</strong></span></td></TR>		
													<TR>
														<TD>
															<input type="checkbox" name="terms_conditions" tabindex="10" value="1" style="border:0px;" <?php if ($_REQUEST['terms_conditions']) { print 'checked="checked"'; } ?> />
															<b>*I have read the <a href="#" onClick="MyWindow=window.open('/terms_main.html','MyWindow','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=725,height=600'); return false;"><b>Terms & Conditions</b></a> and agree to its terms.
														</td>
													</tr>
													<TR><TD>&nbsp;</td></tr>
													
													<tr><td colspan="4">To ensure successful email delivery, please add info@<?= preg_replace("/^www\./", "", $_SESSION['o_company']->get_company_url()) ?> to your Address Book now.</td></tr>
													<TR><TD>&nbsp;</td></tr>
													
													<TR ALIGN="CENTER">
														<TD align="CENTER">
															<input type="submit" name="submit" tabindex="11" value=" Submit Order ">&nbsp;
														</td>
													</TR>				
													<TR><TD COLSPAN="4" ALIGN="CENTER"><b>Note, items with a * are required.</b></td></tr>
													<TR><TD COLSPAN="4" ALIGN="CENTER">(Be sure to check Accept Terms)</td></tr>
												</TABLE>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
		</TD>
	</TR>
	<TR><TD><IMG SRC="/web/company_def/img/spacer.gif" WIDTH=699 HEIGHT=12 ALT=""></TD></TR>
</TABLE>
</form>
<script language="javascript" type="text/javascript">
	calculateTotals();
	
<?php
if ($_REQUEST['online_access']) {
?>
	document.getElementById('online_access_fields').style.display='inline';
<?php
}
?>
</script>
<?php
include_once($_SESSION['web_interface']->get_server_path('other/global/bottom.php'));
?>

<?php
#
# @copyright Copyright (c) 2017 Green Seed Technologies, Inc. All rights reserved.
#
# @author David Hoover
# @author David McLaughlin <david@greenseedtech.com>
#

require_once('office/account/class.account.php');

switch (strtolower($_REQUEST['cmd'])) {

	//handle a SEND_AFFILIATE_REQUEST request
	case 'send_affiliate_request':
		try {
			$_SESSION['web_interface']->check_required( array(required=>'first_name,last_name,main_email_address,personal_phone,line1,city,state,postal_code,username,password') );
			if (isset($_SESSION['error']['message'])) {
				throw new Exception($_SESSION['error']['message']);
			}
			
			// check email matches
			if ($_REQUEST['main_email_address'] != $_REQUEST['confirm_email_address']) {
				$_SESSION['error']['badfield']['confirm_email_address'] = TRUE;
				$_SESSION['error']['badfield']['main_email_address'] = TRUE;
				$_SESSION['error']['message'] = 'Please verify your email addresses match.';
				throw new Exception($_SESSION['error']['message']);
			}
			
			$o_account = new account();
			if ($o_account->invalid_username($_REQUEST['username'])) {
				$_SESSION['error']['message'] = 'Please enter a valid username.';
				$_SESSION['error']['badfield']['username'] = TRUE;
			}
			else if ($o_account->is_username_taken($_REQUEST['username'])) {
				$_SESSION['error']['message'] = 'That username is already taken. Please choose another.';
				$_SESSION['error']['badfield']['username'] = TRUE;
			}
			if (isset($_SESSION['error']['message'])) {
				throw new Exception($_SESSION['error']['message']);
			}
			
			// send email to admin
			$body = "Potential Affiliate:\n" . 
				"Name: " . $_REQUEST['first_name'] . ' ' . $_REQUEST['last_name'] . "\n" . 
				"Username: " . $_REQUEST['username'] . "\n" . 
				"Passowrd: " . $_REQUEST['password'] . "\n" . 
				"Email Address: " . $_REQUEST['main_email_address'] . "\n" . 
				"Main Phone: " . $_REQUEST['personal_phone'] . "\n" . 
				"Work Phone: " . $_REQUEST['business_phone'] . "\n" . 
				"Business Name: " . $_REQUEST['business_name'] . "\n\n"
			;
			$body .= "Street Address\n" . 
				$_REQUEST['line1'] . "\n"
			;
			$body .= $_REQUEST['city'] . ', ' . $_REQUEST['state'] . ' ' . $_REQUEST['postal_code'] . "\n\n";
			mail('admin@byownerdaily.com', 'Potential Affiliate: ' . $_REQUEST['first_name'] . ' ' . $_REQUEST['last_name'], $body, "From: signup@" . $_SESSION['o_company']->get_url());
			include_once($_SESSION['web_interface']->get_server_path('other/ssl/affiliate_thank_you.php'));
		}
		catch (Exception $exception) {
			$_SESSION['error']['message'] = $exception->getMessage();
			include_once($_SESSION['web_interface']->get_server_path('other/ssl/' . $_REQUEST['page'] . '.php'));
		}
	break;
	
	//handle a SEND_REQUEST_AFFILIATE request
	case 'send_request_affiliate':
		$_SESSION['web_interface']->check_required( array(required=>'program_selection,name,email,phone') );
		if (isset($_SESSION['error'])) {
			include_once($_SESSION['web_interface']->get_server_path('other/ssl/request_affiliate.php'));
		}
		else {
			$email_body = "A 'Request Affiliate' form has been filled out:\r\n" . 
				"Program Type: " . $_REQUEST['program_selection'] . "\r\n" . 
				"Name: " . $_REQUEST['name'] . "\r\n" . 
				"Company or Self: " . $_REQUEST['company'] . "\r\n" . 
				"Email: " . $_REQUEST['email'] . "\r\n" . 
				"Phone: " . $_REQUEST['phone'] . "\r\n" . 
				"Geography:\r\n" . $_REQUEST['geography'] . "\r\n" . 
				"Additional Information:\r\n" . $_REQUEST['additional_information'] . "\r\n" . 
				"Questions:\r\n" . $_REQUEST['questions'] . "\r\n"
			;
			mail('admin@byownerdaily.com','BOD Referral-Affiliate-PL Request',$email_body,"From: " . $_REQUEST['email'] . "\r\n\r\n");
			header('Location: /ssl/confirm_request_affiliate.html');
		}
	break;
	
	//handle a SEND_REQUEST_AREA request
	case 'send_request_area':
		$_SESSION['web_interface']->check_required( array(required=>'name,email,phone,areas_of_request') );
		if (isset($_SESSION['error'])) {
			include_once($_SESSION['web_interface']->get_server_path('other/ssl/request_area.php'));
		}
		else {
			$email_body = "A 'Request Area' form has been filled out:\r\n" . 
				"Name: " . $_REQUEST['name'] . "\r\n" . 
				"Company or Self: " . $_REQUEST['company'] . "\r\n" . 
				"Email: " . $_REQUEST['email'] . "\r\n" . 
				"Phone: " . $_REQUEST['phone'] . "\r\n" . 
				"How did you hear about us:\r\n" . $_REQUEST['hear_about_us'] . "\r\n" . 
				"Area(s) of Request:\r\n" . $_REQUEST['areas_of_request'] . "\r\n"
			;
			mail('admin@byownerdaily.com','Can\'t Find Area or Requesting New Area',$email_body,"From: " . $_REQUEST['email'] . "\r\n\r\n");
			header('Location: /ssl/confirm_request_area.html');
		}
	break;

	// handle a SUBSCRIBE request
	case 'subscribe':
		// auto add the General and Residential property_type
		$_REQUEST['property_type_checkbox'][] = 'General';
		$_REQUEST['property_type_checkbox'][] = 'Residential';
		unset($_SESSION['error']);
		$o_account = new account( array('main_email_address'=>$_REQUEST['main_email_address']) );
		
		// check to make sure requireds
		if ($_REQUEST['subscription_type'] != 'Other') {
			$other_required = ',card_name,card_type,card_number,card_exp_date';
		}
		$_SESSION['web_interface']->check_required( array(required=>'first_name,last_name,personal_phone,main_email_address,password,confirm_password,business_name' . $other_required) );
		
		// upwords and trim
		$_REQUEST['first_name'] = trim(ucwords($_REQUEST['first_name']));
		$_REQUEST['last_name'] = trim(ucwords($_REQUEST['last_name']));
		$_REQUEST['business_name'] = trim(ucwords($_REQUEST['business_name']));
		$_REQUEST['card_name'] = trim(ucwords($_REQUEST['card_name']));
		
		// check and process phone number
		$personal_phone = preg_replace("/[^\d]/", "", $_REQUEST['personal_phone']);
		if (strlen($personal_phone) != 10) {
			$_SESSION['error']['message'] .= '<br/>Please enter your phone number including area code (i.e. 801-494-0470).';
			$_SESSION['error']['badfield']['personal_phone'] = TRUE;
		}
		else {
			$_REQUEST['personal_phone'] = preg_replace("/^(\d{3})(\d{3})(\d{4})$/", "$1-$2-$3", $personal_phone);
		}
				
		// see if an account with that email address already exists
		if ( $o_account->get_accountid() && ($o_account->get_account_type() != 'TRIAL' || $o_account->getStringProductNames() != ',' . $_REQUEST['productName'] . ',') ) {
			$_SESSION['error']['message'] .= '<br/>An account with that Email address already exists. To reactivate your account please call us at (801) 494-0470. Thank you.';
		}
		else if ($o_account->get_accountid() && $o_account->get_account_type() == 'TRIAL') {
			$o_account->set_notes(date('Y-m-d') . ': Trial account signed up. Original Start Date: ' . $o_account->get_start_date() . ' Original End Date: ' . $o_account->get_end_date() . "\n" . $o_account->get_notes());
		}
		
		// check to make sure they have selected at least one area
		$chosen_area = FALSE;
		foreach ($_REQUEST as $key=>$value) {
			if (preg_match("/^area_nameid_checkbox_\d+$/", $key) && $value == 1) {
				$chosen_area = TRUE;
			}
		}
		if (!$chosen_area) {
			$_SESSION['error']['message'] .= '<br/>You must select at least one area.';
		}
		
		// check to make sure they have selected a radio button
		if (!$_REQUEST['subscription_type']) {
			$_SESSION['error']['message'] .= '<br/>You must select an amount to be charged.';
		}

		// check to make sure if they chose the year price that they checked the box
		if ($_REQUEST['subscription_type'] == 'Annual' && !$_REQUEST['year_price_terms']) {
			$_SESSION['error']['message'] .= '<br/>You must check the 12-Month Agreement box.';
		}
		
		if ($_REQUEST['subscription_type'] != 'Other') {
			// check to make sure they have put in a valid credit card number
			$_REQUEST['card_number'] = preg_replace("/[^\d]/", "", $_REQUEST['card_number']);
			$o_account->validate_data( array('data'=>$_REQUEST, 'fields'=>array('card_number'=>'card_number')) );
			if (isset($_SESSION['error']['badfield']['card_number'])) {
				$_SESSION['error']['message'] .= '<br/>Please enter a valid credit card number.';
			}
			else {
				$_REQUEST['card_number'] = $_SESSION['web_interface']->getEncrypt($_REQUEST['card_number']);
			}
			 
			// check to make sure they have put in a valid card_exp_date
			if (!preg_match("/^\d{4}$/", $_REQUEST['card_exp_date'])) {
				$_SESSION['error']['badfield']['card_exp_date'] = TRUE;
				$_SESSION['error']['message'] .= '<br/>Please enter a valid Credit Card Expiration Date.';
			}
			else {
				$month = preg_replace("/^(\d\d)\d\d$/", "$1", $_REQUEST['card_exp_date']);
				$year = '20' . preg_replace("/^\d\d(\d\d)$/", "$1", $_REQUEST['card_exp_date']);
				if (strtotime($year . '-' . $month . '-01') < strtotime(date('Y-m-01'))) {
					$_SESSION['error']['badfield']['card_exp_date'] = TRUE;
					$_SESSION['error']['message'] .= '<br/>Please enter a valid Credit Card Expiration Date.';
				}
			}
		} 
		
		if ($_REQUEST['password'] != $_REQUEST['confirm_password']) {
			$_SESSION['error']['message'] .= '<br/>Your passwords do not match.';
			$_SESSION['error']['badfield']['password'] = TRUE;
			$_SESSION['error']['badfield']['confirm_password'] = TRUE;
		}
		
		// make sure they have agreed to the terms
		if (!$_REQUEST['terms_conditions']) {
			$_SESSION['error']['message'] .= '<br/>You must agree to the Terms and Conditions.';
		}
		
		// check the property types if applicable
		if (isset($_REQUEST['property_type_checkbox'])) {
			// check to see if they have checked them all
			require_once($_SESSION['web_interface']->get_server_path('other/misc/property_type_fields.php'));
			if (count($propertyTypeFields) == count($_REQUEST['property_type_checkbox'])) {
				unset($_REQUEST['property_type_checkbox']);
			}
			else {
				$_REQUEST['property_type_field_checkbox'] = array();
				foreach ($_REQUEST['property_type_checkbox'] as $key=>$value) {
					$_REQUEST['property_type_field_checkbox'][] = $value;
				}
			}
		}
		
		// by this point if no error then should be ok
		if (!isset($_SESSION['error'])) {
			// set some defaults
			$_REQUEST['billing_type'] = 'Credit Card';
				
			$_REQUEST['affiliateid'] = $_COOKIE['AFFILIATEID'];
			$_REQUEST['account_type'] = 'CUSTOMER';
			$_REQUEST['companyid'] = $_SESSION['o_company']->get_companyid();
			$_REQUEST['email_attachment'] = ($_REQUEST['email_attachment']) ? $_REQUEST['email_attachment'] : '0';
			
			// set start and end dates
			$_REQUEST['start_date'] = date('Y-m-d');
			$_REQUEST['end_date'] = '2027-12-31';
			
			// calculate next bill date
			$month = date('n');
			$day = date('j');
			$year = date('Y');
			if ($day > 28) {
				$day = 1;
				$month++;
			}
			if ($month > 12) {
				$month = 1;
				$year++;
			}
			
			if ($_REQUEST['subscription_type'] == 'Annual') {
				$next_bill_date = date('Y-m-d', strtotime("+1 YEAR", strtotime($year . '-' . $month . '-' . $day)));
				if (isset($_SESSION['affiliate'])) {
					$_REQUEST['rate'] = $_REQUEST['total_year_affiliate_price'];
				}
				else {
					$_REQUEST['rate'] = $_REQUEST['total_year_regular_price'];
				}
			}
			else if ($_REQUEST['subscription_type'] == 'Monthly') {
				$next_bill_date = date('Y-m-d', strtotime("+1 MONTH", strtotime($year . '-' . $month . '-' . $day)));
				if (isset($_SESSION['affiliate'])) {
					$_REQUEST['rate'] = $_REQUEST['total_month_affiliate_price'];
				}
				else {
					$_REQUEST['rate'] = $_REQUEST['total_month_regular_price'];
				}
			}
			
			// set username = main_email_address
			$_REQUEST['username'] = $_REQUEST['main_email_address'];
			
			// add the extension on to phones if needed
			if ($_REQUEST['personal_phone_ext']) {
				$_REQUEST['personal_phone'] .= ' ext. ' . $_REQUEST['personal_phone_ext'];
			}
			if ($_REQUEST['business_phone'] && $_REQUEST['business_phone_ext']) {
				$_REQUEST['business_phone'] .= ' ext. ' . $_REQUEST['business_phone_ext'];
			}
			
			$o_account->initialize($_REQUEST);
			$o_account->o_business_address->save();
			$o_account->o_personal_address->save();
			
			// create account
			$o_account->set_business_addressid($o_account->o_business_address->get_addressid());
			$o_account->set_personal_addressid($o_account->o_personal_address->get_addressid());
			if (!$o_account->get_accountid()) {
				$o_account->create();
				// need to do the customer number
				$o_account->set_customer_number($o_account->get_accountid());
			}
			$_REQUEST['password'] = $_SESSION['web_interface']->getHashString( array('salt'=>$o_account->get_accountid(), 'string'=>$_REQUEST['password']) );
			$o_account->initialize( array('password'=>$_REQUEST['password']) );
			$o_account->save();
			
			// save their product type (hard coded for now)
			if (isset($_REQUEST['productName'])) {
				$o_account->setProduct( array('productName'=>$_REQUEST['productName']) );
			}
			
			if ($o_account->get_accountid()) {
				// try to bill them if you can
				if ($_REQUEST['subscription_type'] != 'Other' && !preg_match("/\+testing/", $_REQUEST['main_email_address'])) {
					require_once('common/class.authorize_net.php');
					$o_authorize_net = new authorize_net();
					$r_one_time_charge = $o_authorize_net->one_time_charge(
						array(
							'x_type'=>'AUTH_CAPTURE',
							'x_cust_id'=>$o_account->get_customer_number(),
							'x_token'=>$o_account->getToken(),
							'x_exp_date'=>$o_account->get_card_exp_date(),
							'x_description'=>'Initial signup charge.',
							'x_amount'=>$_REQUEST['rate'],
							'x_first_name'=>$o_account->get_first_name(),
							'x_last_name'=>$o_account->get_last_name(),
							'x_address'=>$o_account->o_business_address->get_line1() . ' ' . $o_account->o_business_address->get_line2(),
							'x_city'=>$o_account->o_business_address->get_city(),
							'x_state'=>$o_account->o_business_address->get_state(),
							'x_zip'=>$o_account->o_business_address->get_postal_code()
						)
					);
					if ($r_one_time_charge['status'] != 1) {
						$_SESSION['error']['message'] .= 'There was an error processing your credit card:<br/>' . $r_one_time_charge['message'];
						$o_account->delete();
					}
				}
				
				if (!isset($_SESSION['error']['message'])) {
					if ($_REQUEST['subscription_type'] != 'Other') {
						require_once('office/account/class.billing.php');
						$o_billing = new billing(
							array(
								'accountid'=>$o_account->get_accountid(),
								'amount'=>$_REQUEST['rate'],
								'type'=>'SIGNUP',
								'approval_code'=>$r_one_time_charge['approval_code'],
								'transaction_id'=>$r_one_time_charge['transaction_id'],
								'notes'=>'Initial signup charge.'
							)
						);
						$o_billing->save();
						// update next bill date
						$o_account->set_next_bill_date($next_bill_date);
						$o_account->save();
					}
			
					// build hash of area_names
					require_once('office/account/class.area_name.php');
					$o_area_name_utilities = new area_name_utilities();
					$list_area_name = $o_area_name_utilities->get_list();
					$hash_area_name = array();
					foreach ($list_area_name as $item_area_name) {
						$hash_area_name[$item_area_name->get_area_nameid()] = $item_area_name->get_area_name();
					}
					
					$area_names = array();
					foreach ($_REQUEST as $key=>$value) {
						if (preg_match("/^area_nameid_checkbox_\d+$/", $key) && $value == 1) {
							$area_nameid = str_replace('area_nameid_checkbox_', '', $key);
							$area_names[] = $hash_area_name[$area_nameid];
						}
					}
					$o_account->save_area_names($area_names);
					
					// save property types
					$o_account->save_property_type_fields($_REQUEST['property_type_field_checkbox']);
					
					// send email to admin
					$body = "A New Signup has occured with the following information:\n" . 
						"Account ID: " . $o_account->get_accountid() . "\n" . 
						"Name: " . $o_account->get_first_name() . ' ' . $o_account->get_last_name() . "\n" . 
						"Email Address: " . $o_account->get_main_email_address() . "\n" . 
						"Main Phone: " . $o_account->get_personal_phone() . "\n" . 
						"Work Phone: " . $o_account->get_business_phone() . "\n" . 
						"Business Name: " . $o_account->get_business_name() . "\n\n"
					;
					$body .= "Street Address\n" . 
						$o_account->o_personal_address->get_line1() . "\n"
					;
					if ($o_account->o_personal_address->get_line2()) {
						$body .= $o_account->o_personal_address->get_line2 . "\n";
					}
					$body .= $o_account->o_personal_address->get_city() . ', ' . $o_account->o_personal_address->get_state() . ' ' . $o_account->o_personal_address->get_postal_code() . "\n\n";
					$body .= "Credit Card Information\n" . 
						"Name on Card: " . $o_account->get_card_name() . "\n" . 
						"Card Type: " . $o_account->get_card_type() . "\n" . 
						"Expiration Date: " . $o_account->get_card_exp_date() . "\n\n"
					;
					$body .= "Areas\n";
					foreach ($o_account->get_list_area_names() as $area_name) {
						$body .= $area_name . "\n";
					}
					if ($_REQUEST['subscription_type'] == 'Annual') {
						if (isset($_SESSION['affiliate'])) {
							$body .= "Affiliate Total: $" . $_REQUEST['total_year_affiliate_price'] . "/Year for " . $_REQUEST['total_areas'] . " area(s)\n";
							$body .= "Regular Total: $" . $_REQUEST['total_year_regular_price'] . "/Year for " . $_REQUEST['total_areas'] . " area(s)\n\n";
						}
						else {
							$body .= "Total: $" . $_REQUEST['total_year_regular_price'] . "/Year for " . $_REQUEST['total_areas'] . " area(s)\n\n";
						}
						$body .= 'The credit card was successfully charged for $' . $_REQUEST['rate'] . '. The next bill date has been set to: ' . $next_bill_date . ".\n\n"; 
					}
					else if ($_REQUEST['subscription_type'] == 'Monthly') {
						if (isset($_SESSION['affiliate'])) {
							$body .= "Affiliate Total: $" . $_REQUEST['total_month_affiliate_price'] . "/Month for " . $_REQUEST['total_areas'] . " area(s)\n";
							$body .= "Regular Total: $" . $_REQUEST['total_month_regular_price'] . "/Month for " . $_REQUEST['total_areas'] . " area(s)\n\n";
						}
						else {
							$body .= "Total: $" . $_REQUEST['total_month_regular_price'] . "/Month for " . $_REQUEST['total_areas'] . " area(s)\n\n";
						}
						$body .= 'The credit card was successfully charged for $' . $_REQUEST['rate'] . '. The next bill date has been set to: ' . $next_bill_date . ".\n\n"; 
					}
					else if ($_REQUEST['subscription_type'] == 'Other') {
						$body .= "Other: " . $_REQUEST['subscription_type_other'] . "\n\n";
					}
					$body .= "Other Information\n";
					if ($_REQUEST['email_attachment']) {
						$body .= "Email Attachment: Yes\n";
					}
					mail('admin@byownerdaily.com', 'New Signup: ' . $o_account->get_first_name() . ' ' . $o_account->get_last_name(), $body, "From: signup@" . $_SESSION['o_company']->get_url());

					// send email to new signup
					require_once('office/account/class.letter.php');
					$o_letter = new letter( array('title'=>'Welcome FRBO Email') );
					$email_segments = $o_letter->merge_segments( array('o_account'=>$o_account) );
					$headers = '';
     			if ($email_segments['cc_list']) {
     				$headers .= "Cc: " . $email_segments['cc_list'] . "\r\n";
     			}
     			if ($email_segments['bcc_list']) {
     				$headers .= "Bcc: " . $email_segments['bcc_list'] . "\r\n";
     			}
     			$headers .= "From: " . $email_segments['from_email'];
					mail($_REQUEST['main_email_address'], $email_segments['subject'], $email_segments['body'], $headers);

					// send the 30 days of listings
					require_once('office/listing/class.listing.php');
					$o_listing_utilities = new listing_utilities();
					//$o_listing_utilities->send_listings( array('accountid'=>$o_account->get_accountid(), 'start_date'=>date('Y-m-d', strtotime("1 MONTH AGO")), 'end_date'=>date('Y-m-d')) );
							
					// if associated with an affiliate then send the affiliate the info as well
					if (isset($_SESSION['affiliate'])) {
						$body = $o_account->get_first_name() . ' ' . $o_account->get_last_name() . ' has signed up for ';
						if ($_REQUEST['subscription_type'] == 'Annual') {
							$body .= "$" . $_REQUEST['total_year_affiliate_price'] . "/Year for " . $_REQUEST['total_areas'] . " area(s)\n";
						}
						else if ($_REQUEST['subscription_type'] == 'Monthly') {
								$body .= "$" . $_REQUEST['total_month_affiliate_price'] . "/Month for " . $_REQUEST['total_areas'] . " area(s)\n";
						}
						if ($_SESSION['affiliate']->get_main_email_address()) {
							mail($_SESSION['affiliate']->get_main_email_address(), 'New Signup', $body, "From: " . $_SESSION['o_company']->get_main_email_address());
						}
					}
					header('Location: /ssl/confirm_subscribe.php');
				}
			}
			else {
				$_SESSION['error']['message'] = 'There was an error creating your account. Please contact Customer Support.';
			}
		}
		include_once($_SESSION['web_interface']->get_server_path('other/ssl/subscribe.php'));
	break;
	
	// handle a TRIAL request
	case 'trial':
		unset($_SESSION['error']);
		$o_account = new account( array('main_email_address'=>$_REQUEST['main_email_address']) );
		
		// check to make sure requireds
		$_SESSION['web_interface']->check_required( array(required=>'first_name,last_name,personal_phone,main_email_address,password,confirm_password,business_name,referral_source,card_name,card_type,card_number,card_exp_date') );
		
		// upfirst the first_name and last_name
		$_REQUEST['first_name'] = trim(ucwords($_REQUEST['first_name']));
		$_REQUEST['last_name'] = trim(ucwords($_REQUEST['last_name']));
		$_REQUEST['business_name'] = trim(ucwords($_REQUEST['business_name']));
		$_REQUEST['line1'] = trim(ucwords($_REQUEST['line1']));
		$_REQUEST['line2'] = trim(ucwords($_REQUEST['line2']));
		$_REQUEST['city'] = trim(ucwords($_REQUEST['city']));
		$_REQUEST['card_name'] = trim(ucwords($_REQUEST['card_name']));
		
		// check and process phone number
		$personal_phone = preg_replace("/[^\d]/", "", $_REQUEST['personal_phone']);
		if (strlen($personal_phone) != 10) {
			$_SESSION['error']['message'] .= '<br/>Please enter your phone number including area code (i.e. 801-494-0470).';
			$_SESSION['error']['badfield']['personal_phone'] = TRUE;
		}
		else {
			$_REQUEST['personal_phone'] = preg_replace("/^(\d{3})(\d{3})(\d{4})$/", "$1-$2-$3", $personal_phone);
		}
		if ($_REQUEST['business_phone']) {
			$business_phone = preg_replace("/[^\d]/", "", $_REQUEST['business_phone']);
			if (strlen($business_phone) != 10) {
				$_SESSION['error']['message'] .= '<br/>Please enter your phone number including area code (i.e. 801-494-0470).';
				$_SESSION['error']['badfield']['business_phone'] = TRUE;
			}
			else {
				$_REQUEST['business_phone'] = preg_replace("/^(\d{3})(\d{3})(\d{4})$/", "$1-$2-$3", $business_phone);
			}
		}
		
		// see if an account with that email address already exists
		if ($o_account->get_accountid() && $o_account->get_account_type() != 'TRIAL') {
			$_SESSION['error']['message'] .= '<br/>An account with that Email address already exists. To reactivate your account please call us at (801) 494-0470. Thank you.';
		}
		else if ($o_account->get_accountid() && $o_account->get_account_type() == 'TRIAL') {
			$o_account->set_notes(date('Y-m-d') . ': Trial account signed up. Original Start Date: ' . $o_account->get_start_date() . ' Original End Date: ' . $o_account->get_end_date() . "\n" . $o_account->get_notes());
		}
		
		// check to make sure they have selected at least one area
		$chosen_area = ($_REQUEST['area_nameid_radio']) ? TRUE : FALSE;
		if (!$chosen_area) {
			$_SESSION['error']['message'] .= '<br/>You must select an area.';
		}

		// check to make sure they have selected a radio button
		if (!$_REQUEST['subscription_type']) {
			$_SESSION['error']['message'] .= '<br/>You must select either Annual or Monthly billing.';
		}

		// check to make sure if they chose the year price that they checked the box
		if ($_REQUEST['subscription_type'] == 'Annual' && !$_REQUEST['year_price_terms']) {
			$_SESSION['error']['message'] .= '<br/>You must check the 12-Month Agreement box.';
		}
		
		if ($_REQUEST['subscription_type'] != 'Other') {
			// check to make sure they have put in a valid credit card number
			$_REQUEST['card_number'] = preg_replace("/[^\d]/", "", $_REQUEST['card_number']);
			$o_account->validate_data( array('data'=>$_REQUEST, 'fields'=>array('card_number'=>'card_number')) );
			if (isset($_SESSION['error']['badfield']['card_number'])) {
				$_SESSION['error']['message'] .= '<br/>Please enter a valid credit card number.';
			}
			else {
				$_REQUEST['card_number'] = $_SESSION['web_interface']->getEncrypt($_REQUEST['card_number']);
			}
			 
			// check to make sure they have put in a valid card_exp_date
			if (!preg_match("/^\d{4}$/", $_REQUEST['card_exp_date'])) {
				$_SESSION['error']['badfield']['card_exp_date'] = TRUE;
				$_SESSION['error']['message'] .= '<br/>Please enter a valid Credit Card Expiration Date.';
			}
			else {
				$month = preg_replace("/^(\d\d)\d\d$/", "$1", $_REQUEST['card_exp_date']);
				$year = '20' . preg_replace("/^\d\d(\d\d)$/", "$1", $_REQUEST['card_exp_date']);
				if (strtotime($year . '-' . $month . '-01') < strtotime(date('Y-m-01'))) {
					$_SESSION['error']['badfield']['card_exp_date'] = TRUE;
					$_SESSION['error']['message'] .= '<br/>Please enter a valid Credit Card Expiration Date.';
				}
			}
		} 
		
		if ($_REQUEST['password'] != $_REQUEST['confirm_password']) {
			$_SESSION['error']['message'] .= '<br/>Your passwords do not match.';
			$_SESSION['error']['badfield']['password'] = TRUE;
			$_SESSION['error']['badfield']['confirm_password'] = TRUE;
		}
				
		// make sure they have agreed to the terms
		if (!$_REQUEST['terms_conditions']) {
			$_SESSION['error']['message'] .= '<br/>You must agree to the Terms and Conditions.';
		}
		
		// by this point if no error then should be ok
		if (!isset($_SESSION['error'])) {
			// set some defaults
			$_REQUEST['billing_type'] = 'Credit Card';
			
			$_REQUEST['affiliateid'] = $_COOKIE['AFFILIATEID'];
			$_REQUEST['account_type'] = 'TRIAL';
			$_REQUEST['companyid'] = $_SESSION['o_company']->get_companyid();
			$_REQUEST['email_attachment'] = ($_REQUEST['email_attachment']) ? $_REQUEST['email_attachment'] : '0';
			if ($_REQUEST['url_link']) {
				$_REQUEST['notes'] .= "Link on website: Yes\n";
			}
			
			// set start and end dates
			$_REQUEST['start_date'] = date('Y-m-d');
			$_REQUEST['end_date'] = date('Y-m-d', strtotime("+14 DAYS"));
			
			// calculate next bill date
			$month = date('n');
			$day = date('j');
			$year = date('Y');
			if ($day > 28) {
				$day = 1;
				$month++;
			}
			if ($month > 12) {
				$month = 1;
				$year++;
			}
			
			if ($_REQUEST['subscription_type'] == 'Annual') {
				$next_bill_date = date('Y-m-d', strtotime("+1 YEAR", strtotime($year . '-' . $month . '-' . $day)));
				if (isset($_SESSION['affiliate'])) {
					$_REQUEST['rate'] = $_REQUEST['total_year_affiliate_price'];
				}
				else {
					$_REQUEST['rate'] = $_REQUEST['total_year_regular_price'];
				}
			}
			else if ($_REQUEST['subscription_type'] == 'Monthly') {
				$next_bill_date = date('Y-m-d', strtotime("+1 MONTH", strtotime($year . '-' . $month . '-' . $day)));
				if (isset($_SESSION['affiliate'])) {
					$_REQUEST['rate'] = $_REQUEST['total_month_affiliate_price'];
				}
				else {
					$_REQUEST['rate'] = $_REQUEST['total_month_regular_price'];
				}
			}
			
			// set username = main_email_address
			$_REQUEST['username'] = $_REQUEST['main_email_address'];
			
			// add the extension on to phones if needed
			if ($_REQUEST['personal_phone_ext']) {
				$_REQUEST['personal_phone'] .= ' ext. ' . $_REQUEST['personal_phone_ext'];
			}
			if ($_REQUEST['business_phone'] && $_REQUEST['business_phone_ext']) {
				$_REQUEST['business_phone'] .= ' ext. ' . $_REQUEST['business_phone_ext'];
			}
			
			$o_account->initialize($_REQUEST);
			$o_account->o_business_address->save();
			$o_account->o_personal_address->save();
			
			// create account
			$o_account->set_business_addressid($o_account->o_business_address->get_addressid());
			$o_account->set_personal_addressid($o_account->o_personal_address->get_addressid());
			if (!$o_account->get_accountid()) {
				$o_account->create();
				// need to do the customer number
				$o_account->set_customer_number($o_account->get_accountid());
			}
			$_REQUEST['password'] = $_SESSION['web_interface']->getHashString( array('salt'=>$o_account->get_accountid(), 'string'=>$_REQUEST['password']) );
			$o_account->initialize( array('password'=>$_REQUEST['password']) );
			$o_account->save();
			
			// save their product type (hard coded for now)
			$o_account->setProduct( array('productName'=>'FSBO') );
			
			if ($o_account->get_accountid()) {
				if (!isset($_SESSION['error']['message'])) {
					// build hash of area_names
					require_once('office/account/class.area_name.php');
					$o_area_name_utilities = new area_name_utilities();
					$list_area_name = $o_area_name_utilities->get_list();
					$hash_area_name = array();
					foreach ($list_area_name as $item_area_name) {
						$hash_area_name[$item_area_name->get_area_nameid()] = $item_area_name->get_area_name();
					}
					
					$area_names = array($hash_area_name[$_REQUEST['area_nameid_radio']]);
					$o_account->save_area_names($area_names);
					
					// send email to admin
					$body = "A New Trial Signup has occured with the following information:\n" . 
						"Account ID: " . $o_account->get_accountid() . "\n" . 
						"Name: " . $o_account->get_first_name() . ' ' . $o_account->get_last_name() . "\n" . 
						"Email Address: " . $o_account->get_main_email_address() . "\n" . 
						"Main Phone: " . $o_account->get_personal_phone() . "\n" . 
						"Work Phone: " . $o_account->get_business_phone() . "\n" . 
						"Business Name: " . $o_account->get_business_name() . "\n\n"
					;
					$body .= "Street Address\n" . 
						$o_account->o_personal_address->get_line1() . "\n"
					;
					if ($o_account->o_personal_address->get_line2()) {
						$body .= $o_account->o_personal_address->get_line2 . "\n";
					}
					$body .= $o_account->o_personal_address->get_city() . ', ' . $o_account->o_personal_address->get_state() . ' ' . $o_account->o_personal_address->get_postal_code() . "\n\n";
					$body .= "Credit Card Information\n" . 
						"Name on Card: " . $o_account->get_card_name() . "\n" . 
						"Card Type: " . $o_account->get_card_type() . "\n" . 
						"Expiration Date: " . $o_account->get_card_exp_date() . "\n\n"
					;
					$body .= "Areas\n";
					foreach ($o_account->get_list_area_names() as $area_name) {
						$body .= $area_name . "\n";
					}
					if ($_REQUEST['subscription_type'] == 'Annual') {
						if (isset($_SESSION['affiliate'])) {
							$body .= "Affiliate Total: $" . $_REQUEST['total_year_affiliate_price'] . "/Year for " . $_REQUEST['total_areas'] . " area(s)\n";
							$body .= "Regular Total: $" . $_REQUEST['total_year_regular_price'] . "/Year for " . $_REQUEST['total_areas'] . " area(s)\n\n";
						}
						else {
							$body .= "Total: $" . $_REQUEST['total_year_regular_price'] . "/Year for " . $_REQUEST['total_areas'] . " area(s)\n\n";
						}
					}
					else if ($_REQUEST['subscription_type'] == 'Monthly') {
						if (isset($_SESSION['affiliate'])) {
							$body .= "Affiliate Total: $" . $_REQUEST['total_month_affiliate_price'] . "/Month for " . $_REQUEST['total_areas'] . " area(s)\n";
							$body .= "Regular Total: $" . $_REQUEST['total_month_regular_price'] . "/Month for " . $_REQUEST['total_areas'] . " area(s)\n\n";
						}
						else {
							$body .= "Total: $" . $_REQUEST['total_month_regular_price'] . "/Month for " . $_REQUEST['total_areas'] . " area(s)\n\n";
						}
					}
					else if ($_REQUEST['subscription_type'] == 'Other') {
						$body .= "Other: " . $_REQUEST['subscription_type_other'] . "\n\n";
					}
					$body .= "Other Information\n";
					if ($_REQUEST['email_attachment']) {
						$body .= "Email Attachment: Yes\n";
					}
					if ($_REQUEST['url_link']) {
						$body .= "Link On Website: Yes\n";
					}
					if ($_REQUEST['referral_source']) {
						$body .= "How Did You Hear: " . $_REQUEST['referral_source'] . "\n";
					}
					mail('admin@byownerdaily.com', 'New Trial Signup: ' . $o_account->get_first_name() . ' ' . $o_account->get_last_name(), $body, "From: signup@" . $_SESSION['o_company']->get_url());

					// send email to new signup
					require_once('office/account/class.letter.php');
					$o_letter = new letter( array('title'=>'Trial Period Welcome (Standard)') );
					$email_segments = $o_letter->merge_segments( array('o_account'=>$o_account) );
					$headers = '';
     			if ($email_segments['cc_list']) {
     				$headers .= "Cc: " . $email_segments['cc_list'] . "\r\n";
     			}
     			if ($email_segments['bcc_list']) {
     				$headers .= "Bcc: " . $email_segments['bcc_list'] . "\r\n";
     			}
     			$headers .= "From: " . $email_segments['from_email'];
					mail($_REQUEST['main_email_address'], $email_segments['subject'], $email_segments['body'], $headers);

					// send the 30 days of listings
					require_once('office/listing/class.listing.php');
					$o_listing_utilities = new listing_utilities();
					//$o_listing_utilities->send_listings( array('accountid'=>$o_account->get_accountid(), 'start_date'=>date('Y-m-d', strtotime("14 DAYS AGO")), 'end_date'=>date('Y-m-d')) );

					header('Location: /ssl/confirm_trial.php');
				}
			}
			else {
				$_SESSION['error']['message'] = 'There was an error creating your account. Please contact Customer Support.';
			}
		}
		include_once($_SESSION['web_interface']->get_server_path('other/ssl/trial.php'));
	break;

	// handle a UPDATE_CREDIT_CARD request
	case 'update_credit_card':
		unset($_SESSION['error']);
		try {
			$_SESSION['web_interface']->check_required( array(required=>'first_name,last_name,personal_phone,main_email_address,card_name,card_type,card_number,card_exp_date') );
			if (isset($_SESSION['error']['message'])) {
				throw new Exception($_SESSION['error']['message']);
			}
			
			// check email matches
			if ($_REQUEST['main_email_address'] != $_REQUEST['confirm_email_address']) {
				$_SESSION['error']['badfield']['confirm_email_address'] = TRUE;
				$_SESSION['error']['badfield']['main_email_address'] = TRUE;
				$_SESSION['error']['message'] = 'Please verify your email addresses match.';
				throw new Exception($_SESSION['error']['message']);
			}
			
			// check to make sure they have put in a valid card_exp_date
			if (!preg_match("/^\d{4}$/", $_REQUEST['card_exp_date'])) {
				$_SESSION['error']['badfield']['card_exp_date'] = TRUE;
				$_SESSION['error']['message'] = 'Please enter a valid Credit Card Expiration Date.';
			}
			else {
				$month = preg_replace("/^(\d\d)\d\d$/", "$1", $_REQUEST['card_exp_date']);
				$year = '20' . preg_replace("/^\d\d(\d\d)$/", "$1", $_REQUEST['card_exp_date']);
				if (strtotime($year . '-' . $month . '-01') < strtotime(date('Y-m-01'))) {
					$_SESSION['error']['badfield']['card_exp_date'] = TRUE;
					$_SESSION['error']['message'] = 'Please enter a valid Credit Card Expiration Date.';
				}
			}
			if (isset($_SESSION['error']['message'])) {
				throw new Exception($_SESSION['error']['message']);
			}
			
			// send email to admin
			$body = "Billing Update:\n" . 
				"Name: " . $_REQUEST['first_name'] . ' ' . $_REQUEST['last_name'] . "\n" . 
				"Email Address: " . $_REQUEST['main_email_address'] . "\n" . 
				"Main Phone: " . $_REQUEST['personal_phone'] . "\n" . 
				"Work Phone: " . $_REQUEST['business_phone'] . "\n" . 
				"Business Name: " . $_REQUEST['business_name'] . "\n\n"
			;
			$body .= "Street Address\n" . 
				$_REQUEST['line1'] . "\n"
			;
			$body .= $_REQUEST['city'] . ', ' . $_REQUEST['state'] . ' ' . $_REQUEST['postal_code'] . "\n\n";
			$body .= "Credit Card Information\n" . 
				"Name on Card: " . $_REQUEST['card_name'] . "\n" . 
				"Card Type: " . $_REQUEST['card_type'] . "\n" . 
				"Expiration Date: " . $_REQUEST['card_exp_date'] . "\n\n"
			;
			mail('admin@byownerdaily.com', 'Billing Update: ' . $_REQUEST['first_name'] . ' ' . $_REQUEST['last_name'], $body, "From: billing@" . $_SESSION['o_company']->get_url());
			include_once($_SESSION['web_interface']->get_server_path('other/ssl/cc_thank_you.php'));
		}
		catch (Exception $exception) {
			$_SESSION['error']['message'] = $exception->getMessage();
			include_once($_SESSION['web_interface']->get_server_path('other/ssl/cc_update.php'));
		}
	break;
	
}

?>

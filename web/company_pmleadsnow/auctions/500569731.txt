currentBid:30
currentBidTime:3:45pm
expires:12/23/2014 4:00PM
area:
city:San Diego
state:CA
zip:92102
beds:4
baths:2
squareFeet:1200
price:1980
type:House
ownerInterestLevel:Low
note:
description:Available now. W/D in unit attached garage. Dogs ok with additional deposit no cats. Must have good credit. Stable income No section 8. Owner may have more properties. 
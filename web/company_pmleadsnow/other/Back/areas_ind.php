<?php
require_once('office/account/class.area_name.php');
$o_area_name = new area_name( array('area_nameid'=>$_REQUEST['area_nameid']) );

$_META_DESCRIPTION = '';
$_META_KEYWORDS = '';
$_PAGE_TITLE = $o_area_name->get_area_state() . ' Leads | ' . $o_area_name->get_area_state() . ' For Sale By Owner Leads | ' . $o_area_name->get_area_state() . ' Real Estate Leads';

include_once($_SESSION['web_interface']->get_server_path('other/global/top.php'));
?>
<table width="699" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="7" valign="top"><IMG src="/web/company_def/img/spacer.gif" alt="" width="7" height="1"></td>
		<td width="684" valign="top">
			<table border="0" cellspacing="0" cellpadding="1" bgcolor="#DDDDDD">
				<tr>
					<td valign="top">
						<table width="682" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
							<tr><td valign="top" height="3" bgcolor="#D9E2E6"><IMG src="/web/company_def/img/spacer.gif" alt=""></td></tr>
							<tr>
								<td valign="top" height="350" style="padding:11px 17px 10px 15px; line-height:14px " class="big">
									<h1 class="PageHead"><?= $o_area_name->get_area_name() ?> Leads</h1>
									<div style="height:1px; background-color:#A0A0A0 "><IMG src="/web/company_def/img/spacer.gif" alt=""></div>
									<br style="line-height:10px ">
									<table width="100%" cellspacing="0" cellpadding="0" border="0">
										<tr>
											<td>
												<h2 class="big2"><?= $o_area_name->get_area_title() ?> Description</h2>
												<span class="copy14"><?= $o_area_name->get_description() ?></span>
												<br><br>
												<a href="ssl/subscribe_<?= $o_area_name->get_area_state_abbreviation() ?>.html">
												<IMG hspace=0 src="<?= $_SESSION['web_interface']->get_path('img/Subscribe_FSBO.gif') ?>" align=left border=0 alt="Subscribe to <?= $o_area_name->get_area_state() ?> lead service" ></A>
												<br><br><br>
												<h2 class="big2"><?= $o_area_name->get_area_title() ?> Leads cover:</h2>
												<span class="copy14"><?= $o_area_name->get_cities() ?></span>
												<h2 class="big2"><?= $o_area_name->get_area_state() ?> Sales Lead Output Characteristics</h2>
												<span class="copy14">
												-	Great quantity of quality <?= $o_area_name->get_area_state() ?> leads produced every month.
												<br>
												- Most <?= $o_area_name->get_area_state() ?> leads sent the day they are published.
												<br>
												-	<?= $o_area_name->get_area_state() ?> leads are consistently greater in number than the national average.		
												</span>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
		<td width="8" valign="top"><IMG src="/web/company_def/img/spacer.gif" alt="" width="8" height="1"></td>
	</tr>
</table>
		</TD>
	</TR>
	<TR><TD><IMG src="/web/company_def/img/spacer.gif" WIDTH=699 HEIGHT=12 ALT=""></TD></TR>
</TABLE>
<?php
include_once($_SESSION['web_interface']->get_server_path('other/global/bottom.php'));
?>

<?php
$_META_DESCRIPTION = '';
$_META_KEYWORDS = '';
$_PAGE_TITLE = '';

include_once($_SESSION['web_interface']->get_server_path('other/global/top.php'));
?>


<table width="699" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="7" valign="top"><IMG src="/web/company_def/img/spacer.gif" alt="" width="7" height="1"></td>
		<td width="684" valign="top">
			<table border="0" cellspacing="0" cellpadding="1" bgcolor="#DDDDDD">
				<tr>
					<td valign="top">
						<table width="682" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
            	<tr>
              	<td valign="top" height="3" bgcolor="#D9E2E6"><IMG src="/web/company_def/img/spacer.gif" alt=""></td>
              </tr>

							<tr>
                <td valign="top" height="350" style="padding:11px 17px 10px 15px; line-height:14px " class="big">
                	<h1 class="PageHead">FAQ</h1>
                	<div style="height:1px; background-color:#A0A0A0 "><IMG src="/web/company_def/img/spacer.gif" alt=""></div>
                	<br style="line-height:10px ">




									<table width="100%" cellspacing="0" cellpadding="0" border="0">
                  	<tr>
	                  	<td>

<h2 class="big2">What is PM Leads Now?</h2>
<span class="copy14">
PM Leads Now announces a new service where landlords and property managers can find each other. We have created a market where property managers can bid for pre-qualified leads of landlords who have requested that a property manager contact them. In a sense, we are a matchmaker.
</span>

<br><br>
<h2 class="big2">Does it cost anything to use the service?</h2>
<span class="copy14">
No.
</span>


<br><br>
<h2 class="big2">How will I know when there is a new lead?</h2>
<span class="copy14">
We send you an email whenever a new lead becomes available in the area or areas you pick. The notification will contain a link to the specific property/landlord on our site. Then you decide if you want to bid for the lead. Only the winning bidder pays anything.
</span>


<br><br>
<h2 class="big2">How do I bid?</h2>
<span class="copy14">
The notification will contain a link specific to the new lead. At the bottom of each new lead is a section entitled Bidder Information. Fill in the few needed items to submit your bid. The auction end date and time is posted on each lead.
</span>


<br><br>
<h2 class="big2">How can I be notified of changes to a specific bid?</h2>
<span class="copy14">
There is a checkbox on the specific auction to enable real-time bid change notifications. In addition, you can always access the website and see all bids in process in your specific area.
</span>


<br><br>
<h2 class="big2">If I win the bid, what contact information do you provide?</h2>
<span class="copy14">
After you win the bid and payment is made, PMLeadsNow will email you the landlord contact information. If available, the property information will also be included.
</span>


<br><br>
<h2 class="big2">How much do leads cost?</h2>
<span class="copy14">
Every lead is different. Since this is an auction process, only the winning bid receives the lead. The lead price is determined by the highest bidder on each lead.
</span>


<br><br>
<h2 class="big2">What is the auction duration?</h2>
<span class="copy14">
When a new property/landlord is added to our website, you will be contacted immediately by email. If a lead is added before 12pm, the bidding duration will likely end the same day. We expect that most leads in auction will end within 24 hours.
</span>


<br><br>
<h2 class="big2">What if nobody bids for the lead?</h2>
<span class="copy14">
In the event that there are no bids for a lead, the auction process will either be reposted, or PMLeadsNow will set a price and then send a "Buy Now" notification to all lead area recipients. In the "Buy Now" scenario, the lead is "first come, first serve" and goes to whoever buys it first.
</span>


<br><br>
<h2 class="big2">How are lead areas determined?</h2>
<span class="copy14">
In general, lead areas are counties. However, in some cases, lead areas are multiple, combined counties, or also split counties.
</span>


<br><br>
<h2 class="big2">Are leads available every day?</h2>
<span class="copy14">
Lead bidding is different for each area. There is no determined number of leads for any area. There may be one or many leads in one day, or there may be none.
</span>


<br><br>
<h2 class="big2">What if I want to cancel the lead notifications, or temporarily stop them?</h2>
<span class="copy14">
Send us an email. In the future we will have a more automated service.
<!-- It's easy! Just log in to your account and click the "Stop Notifications" button. To start the notifications again, just login and click the "Start Notifications" button.-->
</span>


<br><br>
<h2 class="big2">What if I win the bid, then contact the landlord and they aren't interested in my services?</h2>
<span class="copy14">
PMLeadsNow goes through a verbal step-by-step pre-qualification process with the landlord before determining their interest level of having a property manager contact them. Pull out your best closing skills! Of course, beyond the auction process, PMLeadsNow is not part of the property manager pitch and communication with the landlord.
</span>


<br><br>
<h2 class="big2">What does "Owner Interest Level" mean on the lead?</h2>
<span class="copy14">
No two leads are the same. Some leads are "hot", and some are, well, "not so hot". And some are in between. To help better know the landlord's intention, PMLeadsNow asks the landlord a set of detailed questions as part of the pre-qualification process. One of the questions lets the landlord set his or her own self-ranking of interest level regarding their desire to have a property manager contact them.
</span>
											</td>



											</td>
										</tr>
									</table>

								</td>
							</tr>

						</table>
					</td>
				</tr>
			</table>

		</td>
		<td width="8" valign="top"><IMG src="/web/company_def/img/spacer.gif" alt="" width="8" height="1"></td>
	</tr>
</table>

</TD>
</TR>
<TR>
<TD>
<IMG src="/web/company_def/img/spacer.gif" WIDTH=699 HEIGHT=12 ALT=""></TD>
</TR>
</TABLE>
<?php
include_once($_SESSION['web_interface']->get_server_path('other/global/bottom.php'));
?>

<?php
$_META_DESCRIPTION = '';
$_META_KEYWORDS = '';
$_PAGE_TITLE = 'For Rent by Owner Leads | Rental Leads | Property Owner Leads | Property Manager Leads';

include_once($_SESSION['web_interface']->get_server_path('other/global/top.php'));
?>
<table width="699" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="7" valign="top"><IMG src="/web/company_def/img/spacer.gif" alt="" width="7" height="1"></td>
<td width="684" valign="top">
<table border="0" cellspacing="0" cellpadding="1" bgcolor="#DDDDDD">
<tr><td valign="top">

<table width="682" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
<tr>
<td valign="top" height="3" bgcolor="#D9E2E6"><IMG src="/web/company_def/img/spacer.gif" alt=""></td>
</tr>

<tr>
<td valign="top" height="350" style="padding:11px 17px 10px 15px; line-height:14px " class="big">

<h1 class="PageHead">Sample: Property Owner/Landlord Leads</h1>
<!-- <h1 class="PageHead">Property Owner (For Rent by Owner Leads) and Property Manager Leads</h1> -->

<div style="height:1px; background-color:#A0A0A0 "><IMG src="/web/company_def/img/spacer.gif" alt=""></div>
<br style="line-height:10px ">

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>

<!--<td> -->



<!-- <td align="left" valign="top" class="gray"> -->
<td align="left" valign="top"><span class="copy14">
For Rent by Owner (FRBO) Leads in email and/or in Spreadsheet (CSV) format:

<br>
<!--
Below is a partial sample of Owner/Landlord (Owner), Property Manager (PM) and Realtor Property Manager (RPM) Leads.
-->

</span>


<br><br>


<!--</td> -->

</tr>


<tr>
<td align="left" valign="top">



<!-- Ad  -->

<b>1. DATE: January 10   &nbsp;&nbsp;FL: Gainesville Area</b>
<br>PROPERTY TYPE: House
<br><br>
$1200 / 4br - 2400ft� - GREAT HOUSE IN A GREAT LOCATION FOR RENT!! (Gainesville) -- Great peaceful House for family!!! Great location in a friendly neighborhood! 4 bed / 3 bath / 2 car garage $ 1200 rent!! Please contact me if you are interested for more information before it's gone! 727-500-9999

<br><br>
REVERSE LOOKUP for 727-500-9999* (Do Not Call Registry)
<br>
Adam
<br>
Gainesville, FL
<br><br>

____________________
<!-- Ad  -->

<br><br>
<b>2. DATE: January 10   &nbsp;&nbsp;OH: Butler County</b>
<br>PROPERTY TYPE: Apartment
<br><br>
$500 / 2br - Apartment for Rent (Middletown) -- I have a 2 possibly 3 bedroom apartment for rent. This is a four unit building and the apartments have recently been remodeled. I supply the stove and refrigerator and I also pay the water and trash, you are responsible for the gas and electric. I put new furnaces in these units two years ago so the heat is very efficient. I do require a credit check, deposit, a one year lease and I will check previous references. Please call 513-967-9999 if you are interested. 500 Sutphin St.


<br><br>
PROPERTY ADDRESS:
<br>
500 Sutphin St.
<br>
Middletown, OH
<br><br>

____________________
<!-- Ad  -->

<br><br>
<b>3. DATE: January 10  &nbsp;&nbsp;FL: Naples Area</b>
<br>PROPERTY TYPE: Condo

<br><br>
$1100 3 bedroom St. Croix (Livingston/Immokalee Roads) -- 3 bedroom 2 bath unit with private wooded view on third floor 2 parking spaces no pets allowed no smoking allowed tenant pays electric and water must apply to Association and agree to background check Call to schedule a showing: Cathy 239.404.9999


<br><br>
REVERSE LOOKUP for 239-404-9999
<br>
SMITH, CATHY
<br>
123 Elm
<br>
Naples, FL 34109
<br><br>


____________________
<!-- Ad  -->

<br><br>
<b>4. DATE: January 10   &nbsp;&nbsp;FL: Jacksonville Area</b>
<br>PROPERTY TYPE: Apartment

<br><br>
2/1 APARTMENT UNFURNISHED MONTHLY RENT $499.00 CENTRAL HEAT AND AIR CONVENIENT TO SHOPPING,BANKING,FOOD AND BUS ROUTE AND I-295 EXCELLENT CONDITION, VANITY IN MASTERBEDROOM, MINI BLINDS, SPACIOUS WELL MAINTAINED AND MANAGED APARTMENTS VANITY IN MASTER BEDROOM QUALIFICATIONS: MONTHLY INCOME MINIMUM OF $1600. GOOD RENTAL HISTORY, NO CRIMINAL RECORD DEPOSIT $350.00 PRIVATE PROPERTY, APPOINTMENT NEEDED CALL PAUL AT 904-875-9999.

<br><br>
PROPERTY ADDRESS:
<br>
1253 WESTCHASE CT.
<br>
Jacksonville, FL 32210
<br><br>



____________________
<!-- Ad  -->

<br><br>
<b>5. DATE: January 10  &nbsp;&nbsp;FL: Orlando Area</b>
<br>PROPERTY TYPE: House

<br><br>
$6000 / 5br - 3860ft� - Windermere Lakefront (Windermere) -- Luxury Executive Home for Lease- 5 bedroom, 4 full baths, 2 half baths plus a bonus room. Home features include a 3 car garage, screen enclosed salt water pool/spa, boat dock with boat & jet ski lift. Excellent location...walking distance to Publix and K-12 private school. Home is available for a December 1st move-in. Can be rented unfurnished for $5500 per month. 12 month lease or longer required. First, last & security deposit. Contact Phil @ 321-663-9999 for inquiries or to arrange a showing.
<br><br>
PROPERTY ADDRESS:
<br>
11706 S Lake Sawyer Lane
<br>
Windermere, FL 34786
<br><br>



<span class="copy14">
<br>
or delivered in Spreadsheet (CSV) format - only partial data shown
</span>

<span class="copy12">
<!-- FRBO = For Rent by Owner, &nbsp;PM = Property Manager, &nbsp;RPM = Realtor Property Manager
-->


<br>

<IMG style="MARGIN-LEFT: 1px; MARGIN-RIGHT: 15px" 
			src="<?= $_SESSION['web_interface']->get_path('img/LeadPM1.gif') ?>" 
			align="left" vspace="1" border="1" alt="">
</img>

<br>
(Note: Addresses, Phone numbers & dates in these samples have been changed to keep details confidential.)
</span>

<span class="copy14">
<br><br>
Leads include: Property Type, Description, Bed, Bath, Sq. Feet, Price, Lot Size, Year Built, Garage Size, Latitude, Longitude and more fields.

<br><br>
When contact information is available, leads contain: Contact Name, Property/Contact Address, City, State/Province, Zip/Postal, Email, Phone Numbers (U.S. Do Not Call Registry indicated) and more fields.

</span>

</td>
</tr>
</table>

</td>
</tr>

</table>
</td></tr>
</table>

</td>
<td width="8" valign="top"><IMG src="/web/company_def/img/spacer.gif" alt="" width="8" height="1"></td>
</tr>
</table>

</TD>
</TR>
<TR>
<TD>
<IMG src="/web/company_def/img/spacer.gif" WIDTH=699 HEIGHT=12 ALT=""></TD>
</TR>
</TABLE>
<?php
include_once($_SESSION['web_interface']->get_server_path('other/global/bottom.php'));
?>
<?php
$_META_DESCRIPTION = '';
$_META_KEYWORDS = '';
$_PAGE_TITLE = '';

include_once($_SESSION['web_interface']->get_server_path('other/global/top.php'));
?>
<table width="699" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="7" valign="top"><IMG src="/web/company_def/img/spacer.gif" alt="" width="7" height="1"></td>
<td width="684" valign="top">
<table border="0" cellspacing="0" cellpadding="1" bgcolor="#DDDDDD">
<tr><td valign="top">

<table width="682" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
<tr>
<td valign="top" height="3" bgcolor="#D9E2E6"><IMG src="/web/company_def/img/spacer.gif" alt=""></td>
</tr>

<tr>
<td valign="top" height="350" style="padding:11px 17px 10px 15px; line-height:14px " class="big">
<h1 class="PageHead">Attention Property Managers: Rental Leads by Owner/Landlord</h1>
<div style="height:1px; background-color:#A0A0A0 "><IMG src="/web/company_def/img/spacer.gif" alt=""></div>
<br style="line-height:10px ">

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
<td>


<h2 class="big2">PM Leads introduces FRBO Leads - a new/innovative and FRESH source of Owner Leads</h2>

<span class="copy14">
Do you ever use "For Rent" ads as "For Rent by Owner Leads" (FRBO Leads) or Landlord Leads to find Owners/Landlords? 
FRBO Leads - do you spend hours searching for leads? Do you buy Landlord leads? 
PM Leads brings you a better way to prospect Owners!
We turn these rental ads into Leads that represent active Owners/Landlords.
</span>


<p style="margin:25px;">
<h2 class="big2">For Rent by Owner "FRBO" Leads</h2>

<span class="copy14">
<u>Property Managers</u>: Owners are frequently overwhelmed by persistent property management tasks--
your opportunity to help them out by professionally managing their properties!
Our daily FRBO Leads will immediately help you prospect more Owners/Landlords.
<br>

</span>


<p style="margin:25px;">
<h2 class="big2">For Rent by Owner Leads, For Rent Leads, Landlord Leads:
</h2>

<span class="copy14">
 <li>FRESH and usually not more than 24 hours old
 <li>Unique, with duplicates removed
 <li>Categorized to fit your target audience
 <li>Property Type identified: Apt, Condo, Cottage, Duplex, Single Family, Townhouse, Triplex, etc.
 <li>Contact information provided when available: phone, address, email
 <li>Phone numbers are scrubbed against the U.S. Do Not Call Registry
 <li>Delivered daily by email or optional CSV (comma delimited) format provided at no extra cost
 <li>Online database access with Downloadable lead option</li>
</span>



<p style="margin:15px;">
<h2 class="big2">Rental Property Owner Leads Database, FRBO Lead Generation
</h2>


<a class=gray href="for-rent-by-owner.html">View Samples</A>


</td>
</tr>
</table>

</td>
</tr>
<!--  <tr>
</tr> -->
</table>
</td></tr>
</table>

</td>
<td width="8" valign="top"><IMG src="/web/company_def/img/spacer.gif" alt="" width="8" height="1"></td>
</tr>
</table>

</TD>
</TR>
<TR>
<TD>
<IMG src="/web/company_def/img/spacer.gif" WIDTH=699 HEIGHT=12 ALT=""></TD>
</TR>
</TABLE>
<?php
include_once($_SESSION['web_interface']->get_server_path('other/global/bottom.php'));
?>
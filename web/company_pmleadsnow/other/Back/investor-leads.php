<?php
$_META_DESCRIPTION = '';
$_META_KEYWORDS = '';
$_PAGE_TITLE = 'Buy Investor Leads | Property Management Leads | Property Manager Leads | Residential Rental Property Leads | PM Leads';

include_once($_SESSION['web_interface']->get_server_path('other/global/top.php'));
?>
<table width="699" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="7" valign="top"><IMG src="/web/company_def/img/spacer.gif" alt="" width="7" height="1"></td>
<td width="684" valign="top">
<table border="0" cellspacing="0" cellpadding="1" bgcolor="#DDDDDD">
<tr><td valign="top">

<table width="682" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
<tr>
<td valign="top" height="3" bgcolor="#D9E2E6"><IMG src="/web/company_def/img/spacer.gif" alt=""></td>
</tr>

<tr>
<td valign="top" height="350" style="padding:11px 17px 10px 15px; line-height:14px " class="big">
<h1 class="PageHead">Investor Leads</h1>
<div style="height:1px; background-color:#A0A0A0 "><IMG src="/web/company_def/img/spacer.gif" alt=""></div>
<br style="line-height:10px ">

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
<td>


<h2 class="big2">A new way - FRBO Leads - Property Management Leads - FRESH Investor Leads daily!</h2>

<span class="copy14">
Do you ever use "For Rent" ads as Investor sales leads? Do you spend hours searching for leads? 
PM Leads brings you a better way! Do you buy investor leads?
We turn these rental ads into Investor Leads that represent active Property Managers and Owners.
</span>


<p style="margin:25px;">
<h2 class="big2">Residential Rental Property Leads -> Investor Leads</h2>

<span class="copy14">
 <li>FRESH and usually not more than 24 hours old
 <li>Unique, with duplicates removed
 <li>Categorized to fit your target audience
 <li>Property Type identified: Apt, Condo, Cottage, Duplex, Single Family, Townhouse, Triplex, etc.
 <li>Contact information provided when available: phone, address, email
 <li>Phone numbers are scrubbed against the U.S. Do Not Call Registry
 <li>Delivered daily by email or optional CSV (comma delimited) format provided at no extra cost
 <li>Online database access with Downloadable lead option
</span>


<p style="margin:25px;">
<h2 class="big2">Add to your Investment Portfolio:</h2>
<span class="copy14">
Both FRBO's and FSBO's offer the investor an opportunity to prospect an owner/landlord.
<br>

<ul>
<li>
Purchase opportunity: Owners who place a "For Rent by Owner" (FRBO) ad may be looking for the right opportunity to jump out of the rental market and sell their property. These fresh, daily leads become an excellent source for your professsional expertise.
</li>

<br>
<li>
To learn about For Sale by Owner "FSBO" leads, see our sister company:
<a href="http://www.fsboleadsusa.com" TARGET="_blank">FSBO Leads USA</a>
</li>
</span>


<br><br>


</td>
</tr>
</table>

<a class=gray href="for-rent-by-owner.html">View Samples</a>

</td>
</tr>
<!--  <tr>
</tr> -->
</table>
</td></tr>
</table>

</td>
<td width="8" valign="top"><IMG src="/web/company_def/img/spacer.gif" alt="" width="8" height="1"></td>
</tr>
</table>

</TD>
</TR>
<TR>
<TD>
<IMG src="/web/company_def/img/spacer.gif" WIDTH=699 HEIGHT=12 ALT=""></TD>
</TR>
</TABLE>
<?php
include_once($_SESSION['web_interface']->get_server_path('other/global/bottom.php'));
?>
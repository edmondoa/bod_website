<?php
$_META_DESCRIPTION = '';
$_META_KEYWORDS = '';
$_PAGE_TITLE = 'Buy Landlord Leads | Rent by Owner Leads | PM Leads | For Rent Leads | Rental Leads';

include_once($_SESSION['web_interface']->get_server_path('other/global/top.php'));
?>
<table width="699" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="7" valign="top"><IMG src="/web/company_def/img/spacer.gif" alt="" width="7" height="1"></td>
<td width="684" valign="top">
<table border="0" cellspacing="0" cellpadding="1" bgcolor="#DDDDDD">
<tr><td valign="top">

<table width="682" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
<tr>
<td valign="top" height="3" bgcolor="#D9E2E6"><IMG src="/web/company_def/img/spacer.gif" alt=""></td>
</tr>

<tr>
<td valign="top" height="350" style="padding:11px 17px 10px 15px; line-height:14px " class="big">
<h1 class="PageHead">Data Integration: Direct, Private Label or Co-Brand/Affiliate?</h1>


<p style="margin:4px;">
<div style="height:1px; background-color:#A0A0A0 "><IMG src="/web/company_def/img/spacer.gif" alt=""></div>
<br style="line-height:10px ">

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
<td>

<h2 class="big2">Make our Landlord Leads, FRBO Leads and Property Manager Leads look like they came directly from you...Do you buy Landlord leads?</h2>

<span class="copy14">
Landlord Leads, Owner Leads and PM Leads can be integrated into your system.
</span>


<p style="margin:25px;">
<h2 class="big2">Data Integration Options</h2>

<span class="copy14">

<p style="margin:12px;">
<u>Direct Integration</u>: We can establish a direct feed of our Leads to you. From our computer server
to your computer server, the Leads flows seamlessly.


<p style="margin:12px;">
<u>Private Label Email</u>: We email the leads to your customers but the marketing and graphics, the text, the return
email address all reflect your company. 
Your customer views the Leads as if they came directly from you.
You control all customer contact information and billing. You have the control to add/remove/modify customers at will.


<p style="margin:12px;">
<u>Co-Branded Affiliate website</u>: We provide a URL and website that uses yours and our logos. 
You and/or the customer control all customer contact information and modify subscriptions at will.


</span>


<br><br>
Call us to discuss these and any other options. (801) 494-0470


<br><br>
<h2 class="big2" align="center">For Rent by Owner Leads, FRBO Leads, For Rent Leads, Landlord Leads 
<br>Property Management Leads, Property Manager Leads</h2>


</td>
</tr>
</table>

</td>
</tr>
<!--  <tr>
</tr> -->
</table>
</td></tr>
</table>

</td>
<td width="8" valign="top"><IMG src="/web/company_def/img/spacer.gif" alt="" width="8" height="1"></td>
</tr>
</table>

</TD>
</TR>
<TR>
<TD>
<IMG src="/web/company_def/img/spacer.gif" WIDTH=699 HEIGHT=12 ALT=""></TD>
</TR>
</TABLE>
<?php
include_once($_SESSION['web_interface']->get_server_path('other/global/bottom.php'));
?>
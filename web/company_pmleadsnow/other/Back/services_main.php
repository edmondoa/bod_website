<?php
$_META_DESCRIPTION = '';
$_META_KEYWORDS = '';
$_PAGE_TITLE = '';

include_once($_SESSION['web_interface']->get_server_path('other/global/top.php'));
?>
<table width="699" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="7" valign="top"><IMG src="/web/company_def/img/spacer.gif" alt="" width="7" height="1"></td>
<td width="684" valign="top">
<table border="0" cellspacing="0" cellpadding="1" bgcolor="#DDDDDD">
<tr><td valign="top">

<table width="682" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
<tr>
<td valign="top" height="3" bgcolor="#D9E2E6"><IMG src="/web/company_def/img/spacer.gif" alt=""></td>
</tr>

<tr>
<td valign="top" height="350" style="padding:11px 17px 10px 15px; line-height:14px " class="big">

<h1 class="PageHead">Property Manager/Owner/Landlord Leads (PM-Realtor-Investor)</h1>

<div style="height:1px; background-color:#A0A0A0 "><IMG src="/web/company_def/img/spacer.gif" alt=""></div>
<br style="line-height:10px ">

<!-- Main Inner Table -->
<table width="100%" cellspacing="0" cellpadding="2" border="0">
<tr>
<td width="80%" align="left" valign="top">
<!-- Table on Left of Main Inner table -->
<table width="100%" cellspacing="0" cellpadding="2" border="0">
<tr>
<td>
<h2><span class="big2"><?= $_SESSION['o_company']->get_title() ?> saves you time:</span></h2>
<ul>
<li><b>W</b>e scour rental ads in your local newspapers, magazines, websites and other public sources.</li>
<li><b>W</b>e collect and process the leads today and have them in your inbox by tomorrow morning.</li>
<li><b>NO</b> repeat ads.</li>
<li><b>W</b>e do a reverse phone directory lookup on ALL ad phone numbers.</li>
<li><b>O</b>ur telephone Prefix Finder(tm) tells you the city source of mobile and land based phones.</li>
<li><b>W</b>e compare advertised phone numbers with the National Do Not Call Registry and identify registered phone numbers.</li>
</ul>


<p style="margin:15px;">
<h2 class="big2">For Rent by Owner Leads
<br>For Rent Leads
<br>Landlord Leads</h2>



<h2><span class="big2"><?= $_SESSION['o_company']->get_title() ?> saves you money:</span></h2>
<ul>
<li><b>I</b>t won't be necessary to spend hours searching for leads! </li>
<li><b>F</b>ast, early and daily lead delivery helps your efficiency and effectiveness. </li>
<li><b>W</b>e save you time, and time is money. </li>
</ul>

<p style="margin:15px;">
<h2 class="big2">Property Management Leads
<br>Property Manager Leads</h2>

</td>
</tr>
</table>

</td>

<td width="20%" align="left" valign="top">
<!-- Table on Right of Main Inner table -->
<table width="100%" cellspacing="0" cellpadding="2" class="blue_bg" border="0">
<tr>
<td class="gray" align="center"><b>Subscription Services</b></td>
</tr>
<tr>
<td>
<table width="100%" cellspacing="0" cellpadding="6" bgcolor="#ffffff" border="0">
<tr>
<td>
<a href="areas_main.html">Subscribe Now!</a><br><br>
<a class=gray href="for-rent-by-owner.html">View Samples</a>

<!--											
<a href="ssl/request_sample.html">Free Trial</a>
-->

<br><br>
<strong>Pricing:</strong><br>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
<td class="blacktxt" align="left" valign="top" nowrap><a href="areas_main.html">See Areas Covered<IMG src="/web/company_def/img/spacer.gif" alt="" width="25" height="1" border="0"></td>
<!--												     <td class="blacktxt" align="left" valign="top" nowrap>Month-to-month<IMG src="/web/company_def/img/spacer.gif" alt="" width="25" height="1" border="0"></td>
<td class="blacktxt" align="left" valign="top" nowrap>$29 month</td>
-->														 
</tr>
<!--											     <tr>
<td class="blacktxt" align="left" valign="top" nowrap>3-month<IMG src="/web/company_def/img/spacer.gif" alt="" width="25" height="1" border="0"></td>
<td class="blacktxt" align="left" valign="top" nowrap>$26 month*</td>
</tr>
<tr>
<td class="blacktxt" align="left" valign="top" nowrap>6-month<IMG src="/web/company_def/img/spacer.gif" alt="" width="25" height="1" border="0"></td>
<td class="blacktxt" align="left" valign="top" nowrap>$23 month*</td>
</tr>
<tr>
<td class="blacktxt" align="left" valign="top" nowrap>12-month<IMG src="/web/company_def/img/spacer.gif" alt="" width="25" height="1" border="0"></td>
<td class="blacktxt" align="left" valign="top" nowrap>$20 month*</td>
</tr>
-->													 
</table>

<br>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>

<table width="100%" cellspacing="0" cellpadding="2" border="0">
<tr>
<td>
<center>
<h2 class="top_txt_menu">We search through hundreds of thousands of rental ads</h2>													
</center>
</td>
</tr>
</table>
<table width="100%" cellspacing="0" cellpadding="2" border="0">
<tr>
<td>
<center>
<h2 class="top_txt_menu">Collect and scrub the relevant rental ads</h2>													
</center>
</td>
</tr>
</table>
<table width="100%" cellspacing="0" cellpadding="2" border="0">
<tr>
<td>
<center>
<h2 class="top_txt_menu">Convert the ads to Leads and email them to you!</h2>													
</center>
</td>
</tr>
</table>								

</td>
</tr>
</table>

</td>
</tr>
</table>

</td>
<td width="8" valign="top"><IMG src="/web/company_def/img/spacer.gif" alt="" width="8" height="1"></td>
</tr>
</table>

</TD>
</TR>
<TR>
<TD>
<IMG src="/web/company_def/img/spacer.gif" WIDTH=699 HEIGHT=12 ALT=""></TD>
</TR>
</TABLE>
<?php
include_once($_SESSION['web_interface']->get_server_path('other/global/bottom.php'));
?>
<?php
$_META_DESCRIPTION = '';
$_META_KEYWORDS = '';
$_PAGE_TITLE = 'PM Leads | Landlord Leads | Rent by Owner Leads | For Rent Leads | Rental Leads';

include_once($_SESSION['web_interface']->get_server_path('other/global/top.php'));
?> 
<h2 style="border-bottom: 10px solid #DEDEDE; margin-bottom: 10px; padding-bottom: 10px; width: 100%;">Terms & Conditions</h2>
You must agree to the Terms & Conditions to enable a transaction from <strong>PM Leads Now</strong> to you ("Customer").<br/>
<br/>
Customer understands that all leads are derived from freely-available public sources. All data supplied is only for internal use of specific Customer and Customer may not forward, sell, lease, give, etc. to any other party or post data anywhere including websites/written materials, without explicit written permission from <strong>PM Leads Now</strong>.<br/>
<br/>
Customer acknowledges that <strong>PM Leads Now</strong> is not a real estate or property management company. <strong>PM Leads Now</strong> is in the business of supplying information (Leads). Customer is solely responsible for interaction with contact(s) provided in the lead. Customer understands that <strong>PM Leads Now</strong> does not conduct criminal background checks or any screening of contact(s) provided in the lead.<br/>
<br/>
Customer understands that this contract represents an obligation of services and payment (if appropriate) until the complete designated time-interval has expired. <strong>PM Leads Now</strong> reserves the right to deny any petition of service. Customer understands that by agreeing to pay by credit card and supplying credit card information that <strong>PM Leads Now</strong> will charge the credit card once per designated time-interval. <strong>PM Leads Now</strong> reserves the right to terminate service of this contract for any reason at any time including prior to the expiration of the time-interval and will give 3 days notice. <br/>
<br/>
<strong>PM Leads Now</strong> shall in no event be liable for loss of profit, Do Not Call Registry claims/disputes, goodwill or other special or consequential damages suffered by the Customer or others, as a result of <strong>PM Leads Now's</strong> performance or nonperformance according to these Terms & Conditions and as agreed to by the Customer. <br/>
<br/>
Customer hereby indemnifies <strong>PM Leads Now</strong> from any and all claims. Customer agrees that no express or implied warranty is made concerning the Lead (via email, website access or other) including contact information (phone numbers, address/city/state, etc.), details (description, bed, bath, price, etc.). Customer holds <strong>PM Leads Now</strong> harmless from any claim arising from searching and/or extracting actual data and that which is delivered to Customer including claims of infringement of patent, copyright, U.S. National Do Not Call Registry, trade secret, trespass, misrepresentation, misappropriation, or similar proprietary rights of any party. <strong>PM Leads Now</strong> makes no warranty or guarantee to the accuracy of the data provided in the Lead. Customer understands that all information provided in the lead was solely provided by the contact of the lead.<br/>
<br/>
<strong>PM Leads Now</strong> exclusively retains all intellectual property rights associated with its data collection processes. <strong>PM Leads Now</strong> and only the specific Customer both obtain ownership rights to the Lead.<br/>
<br/>
Customer acknowledges that all Agreements are to be prepaid and customer's credit card will be charged immediately.<br/>
<br/>
Version - November 1, 2014<br/>
<br/>
</TD>
			</TR>
			</TD>
		</TR>
		<!-- Last row in main area -->
		<TR>
			<TD><IMG height=0 alt="" src="/web/company_def/img/spacer.gif" width=699></TD>
		</TR>
	</TBODY>
</TABLE>
<?php
include_once($_SESSION['web_interface']->get_server_path('other/global/bottom.php'));
?>


	 

 

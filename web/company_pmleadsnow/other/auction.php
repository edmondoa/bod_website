<?php
$_META_DESCRIPTION = '';
$_META_KEYWORDS = '';
$_PAGE_TITLE = 'PM Leads | Landlord Leads | Rent by Owner Leads | For Rent Leads | Rental Leads';

include_once($_SESSION['web_interface']->get_server_path('other/global/top.php'));

if ($_REQUEST['cmd'] == 'sendBid') {
	$sendBidError = '';
	if (!$_REQUEST['auctionId']) {
		$sendBidError .= '<br/>There was an issue sending your bid. Please try again.';
	}
	if (!$_REQUEST['name'] || !$_REQUEST['email'] || !$_REQUEST['phone']) {
		$sendBidError .= '<br/>Please be sure to fill out all of the information below.';
	}
	if (!$_SESSION['web_interface']->verifyEmail($_REQUEST['email'])) {
		$sendBidError .= '<br/>Please be sure to enter a valid email address.';
	}
	/*
	if (!is_numeric($_REQUEST['bid'])) {
		$sendBidError .= '<br/>Please be sure to enter a valid bid.';
	}
	*/
	if (!$_REQUEST['terms']) {
		$sendBidError .= '<br/>Please be sure to agree to the terms and conditions.';
	}
	/*
	if ($_REQUEST['bid'] <= $auctions[$_REQUEST['auctionId']]['currentBid']) {
		$sendBidError .= '<br/>Your bid must be higher than the current bid.';
	}
	*/
	if (!$sendBidError) {
		$subject = 'New Information Entered for Lead: ' . $_REQUEST['auctionId'];
		$body = "Information\nName: " . $_REQUEST['name'] . "\nEmail: " . $_REQUEST['email'] . "\nPhone: " . $_REQUEST['phone'];
		mail('info@pmleads.com', $subject, $body, "From: info@pmleadsnow.com\r\n");
		$sendBidStatus = 'Your information has been entered. It will be processed shortly. Thank you.';
		$auctionId = $_REQUEST['auctionId'];
		unset($_REQUEST);
		$_REQUEST['auctionId'] = $auctionId;
	}
}
$auction = $auctions[$_REQUEST['auctionId']];
if ($auction['id']) {
?>
<div style="padding: 10px;">
<?php
	if ($sendBidStatus) {
?>
	<p class="look" id="hideaway"><?= $sendBidStatus ?></p>
<?php
	}
	else if ($sendBidError) {
?>
	<p class="alert"><?= preg_replace("/^<br\/>/", "", $sendBidError) ?></p>
<?php
	}
?>
	<table border="0" cellpadding="3" cellspacing="3">
		<tr>
			<td valign="top" width="350">
<!--
				<span style="font-size: 150%; font-weight: bold;">Current Bid: $<?= number_format($auction['currentBid'], 0, '.', ',') ?></span> (as of <?= $auction['currentBidTime'] ?>)<br/>
				<span style="font-size: 125%; font-weight: bold;">Expires: <?= $auction['displayExpires'] ?></span><br/>
-->
				<span style="font-size: 150%; font-weight: bold;">ID: <?= $auction['id'] ?></span><br/>
				<br/>
				Neighborhood / Area: <?= $auction['area'] ?><br/>
				City: <?= $auction['city'] ?><br/>
				State: <?= $auction['state'] ?><br/>
				Zip: <?= $auction['zip'] ?><br/>
				Bed(s): <?= $auction['beds'] ?><br/>
				Bath(s): <?= $auction['baths'] ?><br/>
				SqFt: <?= $auction['squareFeet'] ?><br/>
				Price: $<?= number_format($auction['price'], 0, '.', ',') ?><br/>
				Type: <?= $auction['type'] ?><br/>
				Owner Interest Level: <?= $auction['ownerInterestLevel'] ?>
			</td>
			<td valign="top"><img src="/web/company_pmleadsnow/img/PMLeadsNow_placeholder.png" width="275" /></td>
		</tr>
		<tr>
			<td colspan="2">
<?php
if ($auction['note']) {
?>
				<strong>Note: <?= $auction['note'] ?></strong><br/>
<?php
}
?>
				<strong>Owner's Partial Description:</strong><br/>
				<?= $auction['description'] ?><br/>
				<br/>
				<strong>Bidder Information</strong>
				<form method="post" action="/auction.html">
				<input type="hidden" name="cmd" value="sendBid" />
				<input type="hidden" name="auctionId" value="<?= $auction['id'] ?>" />
				<table border="0" cellpadding="3" cellspacing="3">
					<tr>
						<td>Name:</td>
						<td><input type="text" name="name" value="<?= $_REQUEST['name'] ?>" /></td>
						<td style="width: 25px;">&nbsp;</td>
						<td>Email:</td>
						<td><input type="text" name="email" value="<?= $_REQUEST['email'] ?>" /></td>
					</tr>
					<tr>
<!--
						<td>Bid:</td>
						<td>$<input type="text" name="bid" value="<?= $_REQUEST['bid'] ?>" /></td>
						<td style="width: 25px;">&nbsp;</td>
-->
						<td>Phone:</td>
						<td><input type="text" name="phone" value="<?= $_REQUEST['phone'] ?>" /></td>
					</tr>
					<tr>
						<td colspan="5"><input type="checkbox" name="terms" value="1" /> <strong>* I accept the entire <a href="/terms.html" target="_blank">Terms & Conditions</a></strong></td>
					</tr>
				</table>
				<input type="submit" value="Send" /> 
				</form>
			</td>
		</tr>
	</table>
</div>
<?php
}
else {
?>
&nbsp;&nbsp;We apologize but we were unable to pull up the auction you are looking for. Please check the link and try again. Thank you.
<?php
}
?>
				</TD>
			</TR>
			</TD>
		</TR>
		<!-- Last row in main area -->
		<TR>
			<TD><IMG height=0 alt="" src="/web/company_def/img/spacer.gif" width=699></TD>
		</TR>
	</TBODY>
</TABLE>
<?php
include_once($_SESSION['web_interface']->get_server_path('other/global/bottom.php'));
?>

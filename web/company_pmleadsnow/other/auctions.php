<?php
$_META_DESCRIPTION = '';
$_META_KEYWORDS = '';
$_PAGE_TITLE = 'PM Leads | Landlord Leads | Rent by Owner Leads | For Rent Leads | Rental Leads';

include_once($_SESSION['web_interface']->get_server_path('other/global/top.php'));
?>
<strong class="big blue">Current Auctions</strong>
<br>
- Click on individual auction for more details:

<TABLE cellSpacing=0 cellPadding=0 style="border-left: 5px solid #f4961c; border-right: 1px solid #f4961c; border-top: 1px solid #f4961c; margin-top: 5px; width: 90%;">
	<TBODY>
<?php
$count = 0;
while ($count < count($auctionsByExpires)) {
	$auctionNumber = $auctionsByExpires[$count++];
	if (!$auctionNumber) {
		continue;
	}
	$auction = $auctions[preg_replace("/^.*:(.*)$/", "$1", $auctionNumber)];
?>
  	<TR<?php if (($count%2) != 1) { print ' style="background-color: #d9e2e6;"'; } ?>>
  		<TD style="border-bottom: 1px solid #f4961c; cursor: pointer; padding: 5px;" vAlign=top onClick="window.location='/auctions/<?= $auction['id'] ?>.html';">
  				<span style="font-size: 125%; font-weight: bold;">Current Bid: $<?= number_format($auction['currentBid'], 0, '.', ',') ?></span> (as of <?= $auction['currentBidTime'] ?>)<br/>
  				Expires: <?= $auction['displayExpires'] ?><br/>
  				ID: <?= $auction['id'] ?><br/>
					Owner Interest Level: <?= $auction['ownerInterestLevel'] ?><br/>
  				<?= $auction['shortDescription'] ?>
  			
  		</td>
  	</tr>
<?php
}
?>
</TBODY>
</TABLE>

<!--
<br>
<center><b>
<a href="/signup.html"> Click to sign up and participate in auctions</a>
</b></center>
<br>
-->
				</TD>
			</TR>
			</TD>
		</TR>
		<!-- Last row in main area -->
		<TR>
			<TD><IMG height=0 alt="" src="/web/company_def/img/spacer.gif" width=699></TD>
		</TR>
	</TBODY>
</TABLE>
<?php
include_once($_SESSION['web_interface']->get_server_path('other/global/bottom.php'));
?>

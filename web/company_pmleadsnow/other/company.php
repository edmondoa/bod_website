<?php
$_META_DESCRIPTION = '';
$_META_KEYWORDS = '';
$_PAGE_TITLE = '';

include_once($_SESSION['web_interface']->get_server_path('other/global/top.php'));
?>
<!-- dave s. -->
<table width="699" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="7" valign="top"><IMG src="/web/company_def/img/spacer.gif" alt="" width="7" height="1"></td>
<td width="684" valign="top">
<table border="0" cellspacing="0" cellpadding="1" bgcolor="#DDDDDD">
<tr><td valign="top">

<table width="682" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
<tr>
<td valign="top" height="3" bgcolor="#D9E2E6"><IMG src="/web/company_def/img/spacer.gif" alt=""></td>
</tr>

<tr>
<td valign="top" height="350" style="padding:11px 17px 10px 15px; line-height:14px " class="big">
<h1 class="PageHead">Company</h1>
<div style="height:1px; background-color:#A0A0A0 "><IMG src="/web/company_def/img/spacer.gif" alt=""></div>
<br style="line-height:10px ">

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
<td>

<h2 class="big2">History</h2>
<span class="copy14">
The parent company of <?= $_SESSION['o_company']->get_title() ?> was started in 2002 by a select group of Software Information Extraction experts. In 2003 we added a Real Estate visionary and are focused on the Residential Real Estate and Property Management markets. 

<br><br>
- In 2003 we launched our Real Estate - For Sale by Owner ("FSBO") Leads product.
<br>&nbsp;&nbsp;- In 2003 we delivered FSBO leads covering 4 states.
<br>&nbsp;&nbsp;- In 2005 we expanded and delivered FSBO leads for the entire US.
<br>&nbsp;&nbsp;- In 2007 we added FSBO Leads for Canada.
<br>&nbsp;&nbsp;- Since 2008 we have serviced more FSBO customers (via partners) than any other competitor.


<br><br>
- In 2012 we launched our Residential Property Manager (FRBO, PM, RPM) Leads products.
<br>- In 2014 we launched our Property Manager Partnership Leads product.</span>


<br><br>
<h2 class="big2">Largest national PM Leads database</h2>
<span class="copy14">
<?= $_SESSION['o_company']->get_title() ?> was created with one goal in mind: To build the largest Property Manager Leads database in existence. <?= $_SESSION['o_company']->get_title() ?>  uses advanced cutting edge web and database technologies to capture hundreds of thousands of leads annually.
Property Managers, Realtors, Mortgage Lenders, Title, Investors and other Service Professionals typically use the PM Leads to prospect new business.</span>



<br><br>
<h2 class="big2">Contact</h2>
<table width="100%" cellspacing="0" cellpadding="2" border="0">
<tr>

<td></td>
<!-- <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> -->

<td align="left" valign="top">
<!-- <strong>Contact:</strong> -->

<strong>Phone:</strong> (801) 494-0470
<br><strong>Email:</strong> <a href="mailto:info@PMLeadsNow.com">info@PMLeadsNow.com</a>

<!-- <br><strong>Email:</strong>  <a href="mailto:info@<?= $_SESSION['o_company']->get_title() ?>.com">info@<?=$_SESSION['o_company']->get_title() ?>.com</a> -->

</td>


<!--
<td align="left" valign="top">
<strong>Business Development-Investor Relations</strong>
<br><strong>Phone:</strong> (801) 494-0470
<br><strong>Email:</strong>  <a href="mailto:BizDev@<?= $_SESSION['o_company']->get_title() ?>.com">BizDev@<?= $_SESSION['o_company']->get_title() ?>.com</a>
</td>
-->

</tr>
</table>


</td>
</tr>
</table>

</td>
</tr>

</table>
</td></tr>
</table>

</td>
<td width="8" valign="top"><IMG src="/web/company_def/img/spacer.gif" alt="" width="8" height="1"></td>
</tr>
</table>

</TD>
</TR>
<TR>
<TD>
<IMG src="/web/company_def/img/spacer.gif" WIDTH=699 HEIGHT=12 ALT=""></TD>
</TR>
</TABLE>
<?php
include_once($_SESSION['web_interface']->get_server_path('other/global/bottom.php'));
?>

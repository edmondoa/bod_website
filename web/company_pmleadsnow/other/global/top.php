<?php
$_META_DESCRIPTION = ($_META_DESCRIPTION) ? $_META_DESCRIPTION : 'FRBO Leads for Property Managers';
$_META_KEYWORDS = ($_META_KEYWORDS) ? $_META_KEYWORDS : 'FRBO Leads , For Rent by Owner Leads, For Rent Leads, Landlord Leads, Property Management Leads, Property Manager Leads, Property Manager, FRBO';
$_PAGE_TITLE = ($_PAGE_TITLE) ? $_PAGE_TITLE : 'FRBO Leads | For Rent by Owner Leads | Property Manager Leads| Landlord Leads | Property Management Leads | Property Manager | FRBO';

// get the listing of auctions files
$dir = $_SERVER['DOCUMENT_ROOT'] . '/web/company_pmleadsnow/auctions';
$auctions = array();
$auctionsByExpires = array();
if (is_dir($dir)) {
	if ($dh = opendir($dir)) {
		while (($file = readdir($dh)) !== false) {
			if (is_file($dir . '/' . $file)) {
				$id = preg_replace("/^(.*)\..*$/", "$1", $file);
				$rows = explode("\n", file_get_contents($dir . '/' . $file));
				$auction = array('id'=>$id);
				foreach ($rows as $row) {
					list($key, $value) = explode(":", $row, 2);
					$auction[$key] = $value;
				}
				$auction['shortDescription'] = '';
				if ($auction['area']) {
					$auction['shortDescription'] .= $auction['area'] . ', ';
				}
				if ($auction['city']) {
					$auction['shortDescription'] .= $auction['city'] . ', ';
				}
				if ($auction['state']) {
					$auction['shortDescription'] .= $auction['state'] . ', ';
				}
				if ($auction['zip']) {
					$auction['shortDescription'] .= $auction['zip'] . ', ';
				}
				if ($auction['beds']) {
					$auction['shortDescription'] .= $auction['beds'] . ' bed(s), ';
				}
				if ($auction['baths']) {
					$auction['shortDescription'] .= $auction['baths'] . ' bath(s), ';
				}
				if ($auction['squareFeet']) {
					$auction['shortDescription'] .= $auction['squareFeet'] . ' sqft, ';
				}
				if ($auction['price']) {
					$auction['shortDescription'] .= '$' . number_format($auction['price'], 0, '.', ',') . ', ';
				}
				if ($auction['type']) {
					$auction['shortDescription'] .= $auction['type'] . ', ';
				}
				$auction['shortDescription'] = substr($auction['shortDescription'], 0, -2);
				$auction['displayExpires'] = date('l m/d/y @ ga', strtotime($auction['expires']));
				$auctions[$auction['id']] = $auction;
				$auctionsByExpires[] = strtotime($auction['expires']) . ':' . $auction['id'];
			}
		}
		closedir($dh);
	}
}
sort($auctionsByExpires);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>
<HEAD>
	<TITLE><?= $_PAGE_TITLE ?></TITLE>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<META NAME="description" CONTENT="<?= $_META_DESCRIPTION ?>">
	<META NAME="keywords" CONTENT="<?= $_META_KEYWORDS ?>">
	<META NAME="AUTHOR" CONTENT="<?= $_SESSION['o_company']->get_title() ?>">
	<META NAME="COPYRIGHT" CONTENT="www.<?= $_SESSION['o_company']->get_title() ?>.com">
	<META NAME="CONTACT_ADDR" CONTENT="info@<?= $_SESSION['o_company']->get_title() ?>.com">
	<META NAME="RATING" CONTENT="General">

<!--	<META NAME="robots" CONTENT="all">  -->
<meta name="robots" content="noindex, nofollow">
<?php
if ($_SERVER['SERVER_PORT'] == 443) {
?>
	<link rel="shortcut icon" href="https://bod84058.securesites.net/web/company_def/other/favicon.ico" type="image/x-icon">
<?php
}
?>
	<meta name="google-site-verification" content="10jby2I1JfPVKWnEZnua1dzuM50yg0J8LMFD8HSa72Q" />
	<meta name="msvalidate.01" content="E3A36D47253BBBF9C14C14996A278B68" />
	<LINK href="<?= $_SESSION['web_interface']->get_path('css/other.css') ?>" rel="stylesheet" type="text/css">
	<script src="<?= $_SESSION['web_interface']->get_path('js/global.js') ?>" type="text/javascript"></script>
</HEAD>

<BODY bgColor="#ffffff" background=<?= $_SESSION['web_interface']->get_path('img/bg.jpg') ?> topMargin=0 MARGINWIDTH="0" MARGINHEIGHT="0">

<!-- ImageReady Slices (01_feb009.psd - Slices: 02, 04, 05, 06, 08, 11, bot_left, bot_left_corn, bot_right, bot_right_corn, top_left_corn, top_right_corn) -->

<!-- Outermost table: -->
<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
	<TBODY>
	<TR>
		<TD style="BACKGROUND-POSITION: right top" width="50%"><IMG src="/web/company_def/img/spacer.gif" ALT="frbo leads"></TD>
		<TD vAlign=top>
			<!-- Main table: -->
			<TABLE cellSpacing=0 cellPadding=0 width=766 align=center bgColor="#ffffff" border=0>
				<TBODY>
				<!-- Top row, Main table: -->
				<TR>
					<!-- Top left image -->
					<TD><IMG src="<?= $_SESSION['web_interface']->get_path('img/top_left_corn.jpg') ?>" height="10" width=34 alt="for sale by owner"></TD>
					<!-- Top middle image -->
					<TD style="BACKGROUND-IMAGE: url(<?= $_SESSION['web_interface']->get_path('img/top_px.jpg') ?>)"></TD>
					<!-- Top right image -->
					<TD><IMG src="<?= $_SESSION['web_interface']->get_path('img/top_right_corn.jpg') ?>" height=10 width=33 alt="fsbo leads" ></TD>
				</TR>
					
				<TR>
					<TD style="BACKGROUND-IMAGE: url(<?= $_SESSION['web_interface']->get_path('img/left_px.jpg') ?>)"><IMG src="/web/company_def/img/spacer.gif" height=50 width=34 alt=""></TD>
					<TD vAlign=top width=699 height=508>
					
					<!-- Header and Body but not Footer table: -->
						<TABLE cellSpacing=0 cellPadding=0 width=699 border=0>
							<TBODY>
							<!-- First row, above Nav  -->
						<TR>
							<!-- BOD main logo -->
							<!-- Controls height of row -->
							<TD style="PADDING-TOP: 1px">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tr>
<?php
if (isset($_SESSION['affiliate'])) {
	if ($_SESSION['affiliate']->get_logo_path() && file_exists($_SERVER['DOCUMENT_ROOT'] . $_SESSION['affiliate']->get_logo_path())) {
?>
											<td style="vertical-align: bottom;"><IMG src="<?= $_SESSION['affiliate']->get_logo_path() ?>" border="0" alt="<?= $_SESSION['affiliate']->get_business_name() ?>" /></td>
<?php
	}
}
?>
<!--  height="100" width="200"  -->
											<td align="right" style="vertical-align: bottom;"><a href="/index.html"><IMG src="<?= $_SESSION['web_interface']->get_path('img/company_logo.gif') ?>" border="0" alt="" /></A></td>
										</tr>
									</table>
								</TD>
							</TR>

							<!-- Top Nav Row, table: -->
<!--	this row, below, was modified				<TABLE cellSpacing=0 cellPadding=0 width=699  -->

							<TR>
								<TD vAlign=top width=699 height=40>
									<TABLE cellSpacing=0 cellPadding=0 width=785 style="BACKGROUND-IMAGE: url(<?= $_SESSION['web_interface']->get_path('img/menu_px.jpg') ?>)" border=0>
										<TBODY>
										<TR>
							
											<TD class=top_txt_menu style="PADDING-TOP: 14px" vAlign=top width=90 height=40>
												<DIV align=center>
													<a class=top_txt_menu href="/index.html"><STRONG>Home</STRONG></A>
												</DIV>
											</TD>
						
											<TD style="BACKGROUND-IMAGE: url(<?= $_SESSION['web_interface']->get_path('img/m_px.jpg') ?>)">
												<IMG src="/web/company_def/img/spacer.gif" height=40 width=3 alt="">
											</TD>
											<!--
											<TD class=top_txt_menu style="PADDING-TOP: 14px" vAlign=top width=90 height=40>
												<DIV align=center>
													<A class=top_txt_menu href="/auctions.html"><STRONG>Auctions</STRONG></A>
												</DIV>
											</TD>
											
											<TD style="BACKGROUND-IMAGE: url(<?= $_SESSION['web_interface']->get_path('img/m_px.jpg') ?>)">
												<IMG src="/web/company_def/img/spacer.gif" height=40 width=3 alt="">
											</TD>
											-->
											<TD class=top_txt_menu style="PADDING-TOP: 14px" vAlign=top width=90 height=40>
												<DIV align=center>
													<A class=top_txt_menu href="/samples.html"><STRONG>Samples</STRONG></A>
												</DIV>
											</TD>
											
											<TD style="BACKGROUND-IMAGE: url(<?= $_SESSION['web_interface']->get_path('img/m_px.jpg') ?>)">
												<IMG src="/web/company_def/img/spacer.gif" height=40 width=3 alt="">
											</TD>
												
											<TD class=top_txt_menu style="PADDING-TOP: 14px" vAlign=top width=90 height=40>
												<DIV align=center>
													<A class=top_txt_menu href="/company.html"><STRONG>Company</STRONG></A>
												</DIV>
											</TD>
											
											<TD style="BACKGROUND-IMAGE: url(<?= $_SESSION['web_interface']->get_path('img/m_px.jpg') ?>)">
												<IMG src="/web/company_def/img/spacer.gif" height=40 width=3 alt="">
											</TD>
												
											<TD class=top_txt_menu style="PADDING-TOP: 14px" vAlign=top width=90 height=40>
												<DIV align=center>
													<A class=top_txt_menu href="/faq.html"><STRONG>FAQ</STRONG></A>
												</DIV>
											</TD>
											
											<TD style="BACKGROUND-IMAGE: url(<?= $_SESSION['web_interface']->get_path('img/m_px.jpg') ?>)">
												<IMG src="/web/company_def/img/spacer.gif" height=40 width=3 alt="">
											</TD>
												
											<TD class=top_txt_menu style="PADDING-TOP: 14px" vAlign=top width=90 height=40>
												<DIV align=center>
													<A class=top_txt_menu href="/signup.html"><STRONG>Signup</STRONG></A>
												</DIV>
											</TD>
											
											<TD style="BACKGROUND-IMAGE: url(<?= $_SESSION['web_interface']->get_path('img/m_px.jpg') ?>)">
												<IMG src="/web/company_def/img/spacer.gif" height=40 width=3 alt="">
											</TD>
											
											<TD class=top_txt_menu style="PADDING-TOP: 14px" vAlign=top width="599" height=40>
												<DIV align=center>
													&nbsp;
												</DIV>
											</TD>
											
										</TR>
										</TBODY>
	
									</TABLE>
								</TD>
							</TR>

							<!-- 3rd Row with thin grey bar -->
							<TR>
								<TD style="BACKGROUND-IMAGE: url(<?= $_SESSION['web_interface']->get_path('img/px2.jpg') ?>)">
					<IMG src="/web/company_def/img/spacer.gif" height="8" width="699" alt="">
								</TD>
							</TR>

							<!-- 4th Row, thin, white  -->
							<TR>
								<TD><IMG height=6 alt="" src="/web/company_def/img/spacer.gif" width=699></TD>
							</TR>

							<!-- 5th Row   -->
							<TR>
								<TD vAlign=top width=699 height=355>
									<!-- Main Body table: -->

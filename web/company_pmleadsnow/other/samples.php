<?php
$_META_DESCRIPTION = '';
$_META_KEYWORDS = '';
$_PAGE_TITLE = '';

include_once($_SESSION['web_interface']->get_server_path('other/global/top.php'));
?>
<!-- dave s. -->
<table width="699" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="7" valign="top"><IMG src="/web/company_def/img/spacer.gif" alt="" width="7" height="1"></td>
<td width="684" valign="top">
<table border="0" cellspacing="0" cellpadding="1" bgcolor="#DDDDDD">
<tr><td valign="top">

<table width="682" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
<tr>
<td valign="top" height="3" bgcolor="#D9E2E6"><IMG src="/web/company_def/img/spacer.gif" alt=""></td>
</tr>

<tr>
<td valign="top" height="350" style="padding:11px 17px 10px 15px; line-height:14px " class="big">
<h1 class="PageHead">Lead Sample</h1>
<div style="height:1px; background-color:#A0A0A0 "><IMG src="/web/company_def/img/spacer.gif" alt=""></div>
<br style="line-height:10px ">

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
<td>
<h2 class="big2">What does a complete lead look like?</h2>
<span class="copy14">
ID: 500605851
<br>Neighborhood / Area: Fairmount Park
<b>
<br>Address: 2012 Ralene St.
<br>City: San Diego
<br>State: CA
<br>Zip: 92115
</b>
<br>Bed(s): 4
<br>Bath(s): 1.5
<br>SqFt: 1285
<br>Price: $2,195
<br>Type: House
<br>Garage: 2 car
<br>Owner Interest Level: Medium
<br>
<b>
<br>Name: Ed Barbar
<br>Phone: 619-123-4567
</b>
<br>Email: not given
<br>Notes: Call between 6:30pm-8pm

<br><br>
<b>Owner's Complete Description:</b>
<br>Available beginning of month w/d hookups, attached garage, no smoking, NOW RENTING 4 BED 1.5 BATH 1285 SQFT, Stainless steel appliances gas heater ceiling fans, 2 car garage attic storage hardwood floors granite counters custom paint front porch fenced backyard Mud Room, WALKING DISTANCE TO ROWAN ELEMENTARY AND AZAELA PARK, BACKGROUND CHECK AND REFERENCES REQUIRED FIRST MONTH AND SECURITY DEPOSIT REQUIRED PETS ALLOWED WITH ADDITIONAL SECURITY DEPOSIT, $2195/mo GORGEOUS HOME FOR RENT.
<br><br>

<a href="https://www.google.com/maps/place/2012+Ralene+St,+San+Diego,+CA+92105/@32.7289653,-117.1087415,17z/data=!3m1!4b1!4m2!3m1!1s0x80d95475d7deab2f:0x4fa4343e1839cbfb" target="_blank">See on map: 2012 Ralene St.</a>


</td>
</tr>
</table>

</td>
</tr>

</table>
</td>
</tr>
</table>


<br>
<center><STRONG class="blue big">
<a href="/signup.html">Click to receive free Lead notifications</a>
</STRONG></center>


</td>
<td width="8" valign="top"><IMG src="/web/company_def/img/spacer.gif" alt="" width="8" height="1"></td>
</tr>
</table>

</TD>
</TR>
<TR>
<TD>
<IMG src="/web/company_def/img/spacer.gif" WIDTH=699 HEIGHT=12 ALT=""></TD>
</TR>
</TABLE>
<?php
include_once($_SESSION['web_interface']->get_server_path('other/global/bottom.php'));
?>

<?php
$_META_DESCRIPTION = '';
$_META_KEYWORDS = '';
$_PAGE_TITLE = '';

if ($_REQUEST['cmd'] == 'sendBid') {
	$sendBidError = '';
	if (!$_REQUEST['name'] || !$_REQUEST['email'] || !$_REQUEST['phone']) {
		$sendBidError .= '<br/>Please be sure to fill out all of the information below.';
	}
	if (!$_SESSION['web_interface']->verifyEmail($_REQUEST['email'])) {
		$sendBidError .= '<br/>Please be sure to enter a valid email address.';
	}
	if (!$_REQUEST['terms']) {
		$sendBidError .= '<br/>Please be sure to agree to the terms and conditions.';
	}
	if (!$sendBidError) {
		$subject = 'New Signup on PM Leads Now';
		$body = "Signup Information\nName: " . $_REQUEST['name'] . "\nEmail: " . $_REQUEST['email'] . "\nPhone: " . $_REQUEST['phone'];
		mail('info@pmleads.com', $subject, $body, "From: info@pmleadsnow.com\r\n");
		$sendBidStatus = 'Your informationo has been submitted. It will be processed shortly. Thank you.';
	}
}

include_once($_SESSION['web_interface']->get_server_path('other/global/top.php'));
?>
<!-- dave s. -->
<table width="699" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="7" valign="top"><IMG src="/web/company_def/img/spacer.gif" alt="" width="7" height="1"></td>
<td width="684" valign="top">
<table border="0" cellspacing="0" cellpadding="1" bgcolor="#DDDDDD">
<tr><td valign="top">

<table width="682" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
<tr>
<td valign="top" height="3" bgcolor="#D9E2E6"><IMG src="/web/company_def/img/spacer.gif" alt=""></td>
</tr>

<tr>
<td valign="top" height="350" style="padding:11px 17px 10px 15px; line-height:14px " class="big">
<h1 class="PageHead">Signup</h1>
<div style="height:1px; background-color:#A0A0A0 "><IMG src="/web/company_def/img/spacer.gif" alt=""></div>
<br style="line-height:10px ">


<span class="copy14">
If you would like a notification of current Leads, send your contact information.
<br>(There is no cost to sign up.)
</span>
<br><br><br>


<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
<td>

<?php
	if ($sendBidStatus) {
?>
	<p class="look" id="hideaway"><?= $sendBidStatus ?></p>
<?php
	}
	else if ($sendBidError) {
?>
	<p class="alert"><?= preg_replace("/^<br\/>/", "", $sendBidError) ?></p>
<?php
	}
?>

<form method="post" action="/signup.html">
<input type="hidden" name="cmd" value="sendBid" />
<input type="hidden" name="auctionId" value="<?= $auction['id'] ?>" />
<table border="0" cellpadding="3" cellspacing="3">
	<tr>
		<td>Name:</td>
		<td><input type="text" name="name" value="<?= $_REQUEST['name'] ?>" /></td>
	</tr>
	<tr>
		<td>Email:</td>
		<td><input type="text" name="email" value="<?= $_REQUEST['email'] ?>" /></td>
	</tr>
	<tr>
		<td>Phone:</td>
		<td><input type="text" name="phone" value="<?= $_REQUEST['phone'] ?>" /></td>
	</tr>
	<tr>
		<td colspan="5"><input type="checkbox" name="terms" value="1" /> <strong>* I accept the entire <a href="/terms.html" target="_blank">Terms & Conditions</a></strong></td>
	</tr>
</table>
<input type="submit" value="Send" /> 
</form>

</td>
</tr>
</table>

</td>
</tr>

</table>
</td></tr>
</table>

</td>
<td width="8" valign="top"><IMG src="/web/company_def/img/spacer.gif" alt="" width="8" height="1"></td>
</tr>
</table>

</TD>
</TR>
<TR>
<TD>
<IMG src="/web/company_def/img/spacer.gif" WIDTH=699 HEIGHT=12 ALT=""></TD>
</TR>
</TABLE>
<?php
include_once($_SESSION['web_interface']->get_server_path('other/global/bottom.php'));
?>

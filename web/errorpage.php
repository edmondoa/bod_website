<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Sorry, something didn't work correctly.</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<style type="text/css">
	body {
		font: small Arial, Helvetica, sans-serif;
		background: #fff;
	}
	.hide,.hide a {
		color: #fff!important; 
		cursor: default;
	 }
	div {
		width: 500px;
		margin: 10px auto;
	}	 
	.errorimg {
		display: block;
		padding: 10px;
		margin: 0 auto;
	}	
	p.other { background: #efefef; }
</style>
</head>
<body id="error">
<div>
	We apologize for something is not working correctly.
</div>
</body>
</html>
